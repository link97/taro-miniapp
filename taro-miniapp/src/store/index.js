/** 全局唯一数据中心 **/
import {
    init
} from "@rematch/core";

import userModel from "./user"
import orderModel from "./order"

/** 初始化所有models **/
export default init({
    // 这里的命名很重要，即这个模块的名字
    models: {
        userModel,
        orderModel
    }
});