import Taro from "@tarojs/taro";
import {
	$Req
} from '@utils'
/**
 * 用户信息
 * 包括一些公共调用
 * **/

export default {
	/** store数据 **/
	state: {
		// 全局弹框控制
		// 购买数量
		goodsCount: 1,
		skuId: 0,
		goodsDetail: {}
	},
	/** reducers **/
	reducers: {
		setStatus(state, payload) {
			return {
				...state,
				...payload
			};
		},
	},
	/** actions 可以是一个对象，也可以是一个函数，函数的第1个参数自动被注入dispatch **/
	effects: {}
};