import Taro, { Component } from "@tarojs/taro";
import { Provider } from "@tarojs/redux";
import "@tarojs/async-await";
import store from "./store/index";
import Index from "./pages/index";
import "taro-ui/dist/style/index.scss";
import "./app.scss";
import "@components/index.scss";

// 如果需要在 h5 环境中开启 React Devtools
// 取消以下注释：
// if (process.env.NODE_ENV !== 'production' && process.env.TARO_ENV === 'h5')  {
//   require('nerv-devtools')
// }

class App extends Component {
  config = {
    pages: [
      "pages/index/index",
      "pages/index/login",
      "pages/index/welcome",
      "pages/index/regist",
      "pages/home/index/index",
      "pages/mall/index/index",
      "pages/mall/detail/index",
      "pages/cart/index/index",
      "pages/user/index/index",
      "pages/user/identity/index",
      "pages/user/info/index",
      "pages/user/info/edit",
      "pages/user/payPwd/index",
      "pages/user/income/index",
      "pages/user/txdetails/index",
      "pages/user/transferRecord/index",
      "pages/user/sydetails/index",      
      "pages/user/txdetails/details",
      "pages/user/withdrawal/index",
      "pages/user/withdrawal/status",
      "pages/user/points/index",
      "pages/user/cards/index",
      "pages/user/cards/add",
      "pages/user/bonus/index",
      "pages/user/buyPlaceDetail/index",
      "pages/user/distribution/orderList",
      "pages/user/distribution/nonPayment",
      "pages/user/distribution/writePayment",
      "pages/user/distribution/list",
      "pages/user/distribution/packList",
      "pages/user/distribution/index",
      "pages/user/address/add",
      "pages/user/address/index",
      "pages/user/qrcode/index",
      "pages/user/msg/index/index",
      "pages/user/msg/detail/index",
      "pages/user/msg/chat/index",
      "pages/search/index",
      "pages/search/log",
      "pages/order/index/index",
      "pages/order/index/createFp",
      "pages/order/confirm/index",
      "pages/order/success/index",
      "pages/order/refund/index",
      "pages/order/refund-success/index",
      "pages/vip/index",
      "pages/vip/explainList",
      "pages/vip/explain",
      "pages/vip/success",
      "pages/vip/income",
      "pages/vip/qr",
    ],
    window: {
      backgroundTextStyle: "light",
      navigationBarBackgroundColor: "#fff",
      navigationBarTitleText: "WeChat",
      navigationBarTextStyle: "black",
    },
    tabBar: {
      list: [
        {
          pagePath: "pages/home/index/index",
          text: "主页",
          iconPath: "./assets/imgs/user/home.png",
          selectedIconPath: "./assets/imgs/user/home1.png",
        },
        {
          pagePath: "pages/mall/index/index",
          text: "商城",
          iconPath: "./assets/imgs/user/wall.png",
          selectedIconPath: "./assets/imgs/user/wall1.png",
        },
        {
          pagePath: "pages/cart/index/index",
          text: "购物车",
          iconPath: "./assets/imgs/user/cart.png",
          selectedIconPath: "./assets/imgs/user/cart1.png",
        },
        {
          pagePath: "pages/user/index/index",
          text: "我的",
          iconPath: "./assets/imgs/user/user.png",
          selectedIconPath: "./assets/imgs/user/user1.png",
        },
      ],
      color: "#8a8a8a",
      selectedColor: "#000000",
      backgroundColor: "#F1F1F1",
      borderStyle: "white",
    },
    navigateToMiniProgramAppIdList: [
      //'wx1b3142f1cd4aaa20'
      // "wx8bb75d0aec3c7393",
      "wx5883e2aa0d0606b7"
    ],
    permission: {
      "scope.userLocation": {
        desc: "你的位置信息将用于小程序位置接口的效果展示",
      },
    },
  };

  // 在 App 类中的 render() 函数没有实际作用
  // 请勿修改此函数
  render() {
    return (
      <Provider store={store}>
        <Index />
      </Provider>
    );
  }
}

Taro.render(<App />, document.getElementById("app"));
