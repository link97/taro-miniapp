import Taro, { Component } from "@tarojs/taro";
import { connect } from "@tarojs/redux";
import { View, Text } from "@tarojs/components";
import { AtButton, AtIcon, AtFloatLayout } from "taro-ui";
import { $Req } from "@utils";
import "./index.scss";

@connect(
  ({ userModel }) => ({
    userModel,
  }),
  (dispatch) => ({
    userAction: dispatch.userModel,
  })
)
export default class Index extends Component {
  config = {
    navigationBarTitleText: "注册/登录",
  };

  constructor() {
    super(...arguments);
    const { type } = this.$router.params;
    this.state = {
      type,
      code: null,
      isOpen: false,
      // 用户手机号
      phonenumber: null,
    };
  }

  // 获取用户手机号
  onGetUserPhoneNumber(res) {
    if (!res.detail) {
      return;
    }
    const { encryptedData, iv } = res.detail;

    if (!encryptedData) {
      return false;
    }

    const {
      userModel: { userInfo },
      userAction,
    } = this.props;
    const postData = {
      code: userInfo.code,
      session_key: userInfo.session_key,
      encryptedData,
      iv,
    };
  
    this.decodePhone(postData)
  }

  // 解密手机号
  async decodePhone(postData) {
    let { code, data } = await $Req({
      type: "post",
      url: "/user/getWxPhoneNum",
      data: postData,
    });

    if (code != 200) return;
    console.log(data);
    Taro.setStorageSync("userPhone", data);

    this.getPhoneNumber(data);
  }

  // 允许
  onAllow() {
    const {
      userModel: { userInfo },
      userAction,
    } = this.props;
    userInfo.phonenumbers = this.state.phonenumber;
    userAction.setStatus({ userInfo });
    this.setState({
      isOpen: false,
    });
  }

  // 拒绝
  onReject() {
    this.setState({
      isOpen: false,
    });
  }

  //  绑定手机号
  async getPhoneNumber(phone) {
    const {
      userModel: { userInfo },
      userAction,
    } = this.props;
    let { code } = await $Req({
      type: "post",
      url: "/user/bindPhone?phone=" + phone
    });
    if (code != 200) return;

    Taro.navigateBack({
      delta: 1
    });
  }

  render() {
    return (
      <View className="welcom-index">
        <View className="at-row" style="margin-top: 20PX;">
          <View className="at-col welcom-name">
            <Text>注册/登录</Text>
          </View>
        </View>

        <View className="at-row" style="margin-top: 200PX;">
          <View className="at-col welcom-btn">
            <AtButton
              circle
              type="primary"
              openType="getPhoneNumber"
              className="btn-submit"
              onGetPhoneNumber={(res) => {
                this.onGetUserPhoneNumber(res);
              }}
            >
              绑定手机号
            </AtButton>
          </View>
        </View>
        <AtFloatLayout isOpened={this.state.isOpen}>
          <View className="at-row">
            <View className="at-col float-col">
              <View className="at-row" style="margin-top: 15PX;">
                <View className="at-col">
                  <Text className="t1">你的手机号</Text>
                </View>
              </View>
              <View className="at-row row-tel" style="margin-top: 30PX;">
                <View className="at-col at-col-4">
                  <Text className="t2">{this.state.phonenumber}</Text>
                </View>
                <View className="at-col at-col-7">
                  <Text className="t3">微信绑定号码</Text>
                </View>
                <View className="at-col at-col-1">
                  <AtIcon value="check" size="20" color="#58CE25"></AtIcon>
                </View>
              </View>
              <View className="at-row" style="margin-top: 80PX;">
                <View className="at-col at-col-3 at-col__offset-2">
                  <AtButton className="btn-reject" onClick={() => {}}>
                    拒绝
                  </AtButton>
                </View>
                <View className="at-col at-col-3 at-col__offset-2">
                  <AtButton className="btn-resolve" onClick={() => {}}>
                    允许
                  </AtButton>
                </View>
              </View>
            </View>
          </View>
        </AtFloatLayout>
      </View>
    );
  }
}
