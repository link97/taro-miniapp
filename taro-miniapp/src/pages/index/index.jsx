import Taro, { Component } from "@tarojs/taro";
import { connect } from "@tarojs/redux";
import { View, Text } from "@tarojs/components";
import { AtButton } from "taro-ui";
import { $Req } from "../user/txdetails/node_modules/@utils";
import "./index.scss";

@connect(
  ({ userModel }) => ({
    userModel,
  }),
  (dispatch) => ({
    userAction: dispatch.userModel,
  })
)
export default class Index extends Component {
  config = {
    navigationBarTitleText: "首页",
  };

  constructor() {
    super(...arguments);
    this.state = {
      currentTab: 0,
    };
  }

  componentDidMount() {
    let userInfo = Taro.getStorageSync("userInfo");
    if (userInfo && userInfo.length) {
      userInfo = JSON.parse(userInfo);
      this.props.userAction.setUserInfo(userInfo);
    }
    Taro.switchTab({
      url: "/pages/home/index/index",
    });
  }

  render() {
    return <View className="welcom-index"></View>;
  }
}
