import Taro, { Component } from "@tarojs/taro";
import { connect } from "@tarojs/redux";
import { View, Text } from "@tarojs/components";
import { AtForm, AtInput,AtButton,AtCountdown } from "taro-ui";
import { $Req } from "@utils";
import "./index.scss";

@connect(
  ({ userModel }) => ({
    userModel,
  }),
  (dispatch) => ({
    userAction: dispatch.userModel,
  })
)
export default class Index extends Component {
  config = {
    navigationBarTitleText: "首页",
  };

  constructor() {
    super(...arguments);
    const { type } = this.$router.params;
    this.state = {
        type,
      tel: null,
      code: null,
    };
  }

  componentDidMount() {
  }

  // 获取手机号
  async getPhoneNumber(postData) {
    let { code, message, data } = await $Req({
      type: "get",
      url: "/webuser/getPhoneNumber",
      data: postData,
    });
    if (!data) return;
  }

  // 获取手机验证码
  async getObtainCode() {
    const { userModel:{userInfo} } = this.props;
    let { code, message, data } = await $Req({
      type: "get",
      url: "/webuser/obtaincode",
      data: {
        phonenumbers:userInfo.phonenumbers
      },
    });
    if (!data) return;
    data = data[0];
  }

  render() {
    return (
      <View className="welcom-index">
        <View className="at-row" style="margin-top: 20PX;">
          <View className="at-col welcom-name">
            <Text>绑定手机号</Text>
          </View>
        </View>

        <View className="at-row" style="margin-top: 100PX;">
          <View className="at-col">
            <AtForm>
              <AtInput
                name="tel"
                title="手机号"
                type="number"
                maxLength="11"
                placeholder="请输入绑定银行卡的手机号"
                value={this.state.tel}
                onChange={() => {}}
              />

              <AtInput
                name="code"
                title="手机验证码"
                type="text"
                placeholder="请输入手机验证码"
                value={this.state.code}
                onChange={() => {}}
              >
                <Text>获取验证码</Text>
              </AtInput>
            </AtForm>
          </View>
        </View>

        <View className="at-row" style="margin-top: 100PX;">
          <View className="at-col welcom-btn">
            <AtButton
              circle
              type="primary"
              className="btn-submit"
              onClick={() => {
                this.setState({
                  loading2: true,
                });
                this.getPhoneNumber(res);
              }}
            >
              登录
            </AtButton>
          </View>
        </View>

        {this.state.type === '1'?<View className="at-row" style="margin-top: 100PX;">
          <View className="at-col" style="color:#CCC;text-align:center;">
            <Text>本次跳过</Text>
          </View>
        </View>:null}
      </View>
    );
  }
}
