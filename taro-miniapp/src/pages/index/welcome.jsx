
import Taro, { Component } from "@tarojs/taro";
import { connect } from "@tarojs/redux";
import { View, Text } from "@tarojs/components";
import { AtButton } from "taro-ui";
import "./index.scss";

@connect(
  ({ userModel }) => ({
    userModel,
  }),
  (dispatch) => ({
    userAction: dispatch.userModel,
  })
)
export default class Index extends Component {
  config = {
    navigationBarTitleText: "首页",
  };

  constructor() {
    super(...arguments);
    this.state = {
      currentTab: 0,
    };
  }

  // 获取用户信息
  async getUserInfo(res) {
    const data = await this.props.userAction.getWxUserInfo(res);
    console.log(data);
    
    if (!data){
      Taro.showToast({
        title:'获取用户信息失败',
        icon: 'none',
				duration: 3000
      })
      return;
    };
    
    if (data.phone) {
      Taro.navigateBack(-1);
    } else {
      Taro.redirectTo({
        url: '/pages/index/regist',
      });
    }
  }

  render() {
    return (
      <View className="welcom-index">
        <View className="at-row" style="margin-top: 20PX;">
          <View className="at-col welcom-logo">
            <Image src={"https://bai-tong-fen-xiao.oss-cn-beijing.aliyuncs.com" + "/shopping/home/mall.png"} />
          </View>
        </View>

        <View className="at-row" style="margin-top: 10PX;">
          <View className="at-col welcom-name">
            <Text>分销商城（名称）</Text>
          </View>
        </View>

        <View className="at-row" style="margin-top: 200PX;">
          <View className="at-col welcom-btn">
          {/* openType="getUserInfo" */}
            <AtButton
              circle
              type="primary"
              className="btn-submit"
              onClick={() => {
                wx.getUserProfile({
                  desc: '用于登录完善信息', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
                  success: (res) => {
                    console.log(res);
                    this.getUserInfo(res);
                  }
                })
                // Taro.getUserPro
                // console.log(res,"---- taro getUserinfo Methods -----")
                // this.getUserInfo(res);
              }}
            >
              手机号一键登录
            </AtButton>
          </View>
        </View>

        <View className="at-row" style="margin-top: 100PX;">
          <View className="at-col welcom-weachat">
          {/* openType="getUserInfo" */}
            <AtButton
              
              className="welcom-weachat-btn"
              onClick={() => {
                wx.getUserProfile({
                  desc: '用于登录完善信息', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
                  success: (res) => {
                    console.log(res);
                    this.getUserInfo(res);
                  }
                })
                // console.log(res,"---- taro getUserinfo Methods -----")
                // this.getUserInfo(res);
              }}
            >
              <Image src={"https://bai-tong-fen-xiao.oss-cn-beijing.aliyuncs.com" + "/shopping/home/weachat.png"} />
            </AtButton>
            <View style={{ fontSize: '14PX', marginTop: '5PX', color: '#999' }}>微信登录</View>
          </View>
        </View>
      </View>
    );
  }
}
