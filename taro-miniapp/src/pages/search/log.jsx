import Taro, { Component } from "@tarojs/taro";
import { View, Text, LivePlayer } from "@tarojs/components";
import { AtSearchBar, AtIcon } from "taro-ui";
import "./index.scss";

const img_del = "https://bai-tong-fen-xiao.oss-cn-beijing.aliyuncs.com" + "/shopping/home/del.png";

export default class Index extends Component {
  config = {
    navigationBarTitleText: "搜索记录",
  };

  constructor(props) {
    super(...arguments);
    this.state = {
      searchVal: "",
      list1: [],
      list2: [],
    };
  }
  componentDidShow() {
    let history = Taro.getStorageSync("history");
    if (history) {
      this.setState({
        list1: history
      })
    }

    this.setState({
      searchVal: ""
    })
  }

  render() {
    return (
      <View className="at-row page-search-log">
        <View className="at-col c0">
          <View className="at-row r1">
            <AtSearchBar
              fixed
              value={this.state.searchVal}
              onChange={(v) => {
                this.setState({
                  searchVal: v,
                });
              }}
              onActionClick={(v) => {

                this.state.list1.unshift({
                  name: this.state.searchVal,
                  id: parseInt(Math.random() * 100)
                });
                
                Taro.setStorageSync("history", this.state.list1);

                Taro.navigateTo({
                  url: "/pages/search/index?searchVal=" + this.state.searchVal,
                });
              }}
            />
          </View>
          <View className="at-row at-row__justify--between r2">
            <View className="at-col left">
              <Text>历史搜索</Text>
            </View>
            {
              this.state.list1.length ?
              <View className="at-col right" onClick={
                () => {
                  Taro.removeStorageSync("history")
                  Taro.showToast({
                    title:"已清空"
                  })
                  this.setState({
                    list1: []
                  })
                }
              }>
                <Image src={img_del} />
              </View>
              :
              null
            }
            
          </View>
          <View className="tag-wrap">
          {
            this.state.list1.length ?
              this.state.list1.map((item) => {
                return (
                  <View className="item-tag" key={item.id} onClick={
                    () => {
                      Taro.navigateTo({
                        url: "/pages/search/index?searchVal=" + item.name,
                      });
                    }
                  }>
                    <Text>{item.name}</Text>
                  </View>
                );
              })
            :
            null
          }
          </View>
          {
            this.state.list2.length ?
            <View>
              <View className="at-row at-row__justify--between r4">
                <View className="at-col left">
                  <Text>探索发现</Text>
                </View>
              </View>
              <View className="at-row at-row--wrap r5">
                {this.state.list2.map((item) => {
                  return (
                    <View className="at-col item-tag" key={item.id}>
                      <Text>{item.name}</Text>
                    </View>
                  );
                })}
              </View>
            </View>
            :
            null
          }
          
        </View>
      </View>
    );
  }
}
