import Taro, { Component } from "@tarojs/taro";
import { View, Text } from "@tarojs/components";
import { AtSearchBar } from "taro-ui";
import { $Req } from "@utils";
import "./index.scss";
import "@components/ShowBox.scss";

const img_goods = "https://bai-tong-fen-xiao.oss-cn-beijing.aliyuncs.com" + "/shopping/order/goods.png";

export default class Index extends Component {
  config = {
    navigationBarTitleText: "搜索结果",
  };

  constructor() {
    super(...arguments);
    const { searchVal } = this.$router.params;
    this.state = {
      searchVal,
      vacancyFlag:false,
      goodsList: [],
    };
  }

  onTabClick() {}

  onSubmit() {
    // 结算
    Taro.navigateTo({
      url: "/pages/order/success/index",
    });
  }

  componentDidMount() {
    this.getGoodsList();
  }

  async getGoodsList() {
    const { code, message, data } = await $Req({
      type: "get",
      url: "/goods/getMallGoodsList",
      data: {
        goodsName: this.state.searchVal,
        status: 1
      },
    });
    if (!data) return;

    this.setState({
      goodsList: data.data,
      vacancyFlag: true,
    });
  }

  render() {
    const { searchVal, goodsList, vacancyFlag } = this.state;
    return (
      <View className="page-search-index">
        <View className="at-row r1">
          <AtSearchBar
            fixed
            value={searchVal}
            onChange={(v) => {
              this.setState({
                searchVal: v
              })
            }}
            onActionClick={(v) => {
              this.getGoodsList();

              let history = Taro.getStorageSync("history");
    
              if (searchVal) {
                history.unshift({
                  id: parseInt(Math.random * 100),
                  name: searchVal
                });
            
                Taro.setStorageSync("history", history);
              }

            }}
          />
        </View>
        <View className="at-row">
          <View className="at-col c0">
            {goodsList.map((item) => {
              return (
                <View
                  className="at-row shopping-show-box"
                  key={item.id}
                  onClick={() => {
                    Taro.navigateTo({
                      url: "/pages/mall/detail/index?goodsId=" + item.skuId,
                    });
                  }}
                >
                  <View className="at-col show-box-c0">
                    <View className="at-row show-box-row">
                      <View className="at-col at-col-4 show-box-col-img">
                        <image
                          className="img-goods"
                          style="width:100PX;height:100PX;"
                          src={item.skuCoversUrl}
                        />
                      </View>
                      <View className="at-col at-col-8 show-box-col-main">
                        <View className="at-row">
                          <View className="at-col">
                            <Text className="show-box-title">
                              {item.goodsName}
                            </Text>
                          </View>
                        </View>
                        {
                        item.integralStatus == 1 ?
                        <View className="at-col" style="text-align: left;margin-top: 4PX">
                          <Text style="color: #ccc; font-size:12PX">积分商品</Text>
                        </View>
                        :
                        null
                      }
                        <View
                          className="at-row at-row__justify--between"
                          style="margin-top:10PX;"
                        >
                          <View className="at-col">
                            <Text className="show-box-price-large">
                              ￥{item.skuCurrPrice}
                            </Text>
                            <Text className="show-box-price-small">
                              .0元{item.vip*1==1?'':'/积分'}
                            </Text>
                          </View>
                        </View>
                        <View
                          className="at-row at-row__justify--between"
                          style="margin-top:10PX;"
                        >
                          <View className="at-col">
                            <Text className="show-box-norms">
                              {item.des}
                            </Text>
                          </View>
                        </View>
                      </View>
                    </View>
                  </View>
                </View>
              );
            })}
            {
              goodsList.length==0 && vacancyFlag ? <View className="hintTxt">
                 抱歉，"{searchVal}"暂无搜索结果
              </View> : null 
            }
          </View>
        </View>
      </View>
    );
  }
}
