import Taro, { Component } from "@tarojs/taro";
import { View, Text, Swiper, SwiperItem } from "@tarojs/components";
import { AtSearchBar, AtSegmentedControl } from "taro-ui";
import { $Req } from "@utils";
import "./index.scss";

const img_project = "https://bai-tong-fen-xiao.oss-cn-beijing.aliyuncs.com" + "/shopping/home/project.png";
const img_vip = "https://bai-tong-fen-xiao.oss-cn-beijing.aliyuncs.com" + "/shopping/home/vip.png";
const img_point = "https://bai-tong-fen-xiao.oss-cn-beijing.aliyuncs.com" + "/shopping/home/point.png";

export default class Index extends Component {
  config = {
    navigationBarTitleText: "主页",
  };

  constructor() {
    super(...arguments);
    this.state = {
      searchVar: "",
      goodsList: [],
      swiperList: [],
      tabCurrent: 0,
      vipParam: 0,
    };
  }

  componentDidMount() {
    this.getGoodsList();
    // 获取轮播图
    this.getCarousel();
  }

  initSearchBox() {
    const { searchVar } = this.state;
    return (
      <View className="at-row search-cont"
        onClick={() => {
          Taro.navigateTo({
            url: "/pages/search/log",
          });
        }}
      >
        <AtSearchBar
          fixed
          disabled
          value={searchVar}
        />
      </View>
    );
  }

  async getGoodsList() {
    const { tabCurrent: vip } = this.state;
    let requestData = {
      status: 1
    }
    if(vip*1 == 1) {
        requestData.vip = 1;
    } else if(vip*1 == 0) {
       requestData.recommend = 1;
    } else if(vip*1 == 2) {
      requestData.integralStatus = 1;
   }
    
    const { data } = await $Req({
      type: "get",
      url: "/goods/getMallGoodsList",
      data: requestData
    });

    if (!data) return;

    this.setState({
      goodsList: data.data,
    });
  }

  // 获取轮播图
  async getCarousel() {
    const { code, message, data } = await $Req({
      type: "get",
      url: '/indexProducts/getCarousel',
      data: {}
    })

    if (!data) return;
    this.setState({
      swiperList: data,
    });
  }

  renderSeiperItem = () => {
    let { swiperList } = this.state;

    return swiperList.map((el, index) => (
      <SwiperItem key={el.id}>
        <View style={{ overflow: 'hidden' }} className="at-row swiper-item" onClick={
          () => {
            console.log(el)
            switch (el.goodsType) {
              case 0:
                // 啥也不做
                break;
              case 1:
                Taro.navigateTo({
                  url: "/pages/mall/detail/index?goodsId=" + el.goodsId
                })
                break;
              case 2:
                Taro.navigateTo({
                  url: "/pages/vip/explainList"
                })
                break;
                case 3:
                  Taro.navigateTo({
                    url: "/pages/mall/detail/index?goodsId=" + el.goodsVipId
                  })
                  break;                
            }
          }
        }>
          <View className="at-col">
            <Image src={el.imgUrl} mode={"aspectFill"} alt={el.title} />
          </View>
        </View>
      </SwiperItem>
    ))

  }

  initImgSwiper() {
    return (
      <View className="at-row swiper-cont">
        <View className="at-col">
          <Swiper
            className="test-h"
            indicatorColor="#ccc"
            indicatorActiveColor="#fff"
            circular
            indicatorDots
            autoplay
          >
            {this.renderSeiperItem()}
          </Swiper>
        </View>
      </View>
    );
  }

  initVip() {
    return (
      <View className="at-row vip-cont">
        <View className="at-col vip-cont-c0">
          <View className="at-row vip-cont-r1">
            <View className="at-col left">
              <Text>会员权益</Text>
            </View>
            <View
              className="at-col right">
            </View>
          </View>
          <View className="at-row vip-cont-r2"
            style="text-align:center"
            onClick={() => {
              Taro.navigateTo({
                url: "/pages/vip/explainList",
              });
            }}
          >
            <image mode="aspectFit" style="width:100%;height:132px;" src="https://bai-tong-fen-xiao.oss-cn-beijing.aliyuncs.com/shopping/vips.png"></image>
            {/* <View className="at-col at-col-4">
              <View className="at-row">
                <View
                  className="at-col vip-item project">
                  <View className="at-row at-row__align--center r111">
                    <View className="at-col">
                      <Text>获得产品</Text>
                    </View>
                  </View>
                  <View className="at-row at-row__align--center r112">
                    <View className="at-col">
                      <Image src={img_project} />
                    </View>
                  </View>
                  <View className="at-row at-row__align--center r113">
                    <View className="at-col">
                      <Text></Text>
                    </View>
                  </View>
                  <View className="at-row at-row__align--center r114">
                    <View className="at-col">
                      <Text>价值￥1500元</Text>
                    </View>
                  </View>
                </View>
              </View>
            </View>
            <View
              className="at-col at-col-4">
              <View className="at-row">
                <View className="at-col vip-item point">
                  <View className="at-row at-row__align--center r111">
                    <View className="at-col">
                      <Text>获得积分</Text>
                    </View>
                  </View>
                  <View className="at-row at-row__align--center r112">
                    <View className="at-col">
                      <Image src={img_point} />
                    </View>
                  </View>
                  <View className="at-row at-row__align--center r113">
                    <View className="at-col">
                      <Text>1000积分</Text>
                    </View>
                  </View>
                  <View className="at-row at-row__align--center r114">
                    <View className="at-col">
                      <Text>可用于购物</Text>
                    </View>
                  </View>
                </View>
              </View>
            </View>
            <View className="at-col at-col-4">
              <View className="at-row">
                <View className="at-col vip-item vip">
                  <View className="at-row at-row__align--center r111">
                    <View className="at-col">
                      <Text>获得身份</Text>
                    </View>
                  </View>
                  <View className="at-row at-row__align--center r112">
                    <View className="at-col">
                      <Image src={img_vip} />
                    </View>
                  </View>
                  <View className="at-row at-row__align--center r113">
                    <View className="at-col">
                      <Text>初级会员身份</Text>
                    </View>
                  </View>
                  <View className="at-row at-row__align--center r114">
                    <View className="at-col">
                      <Text>可升级中级会员</Text>
                    </View>
                  </View>
                </View>
              </View>
            </View> */}
          </View>
        </View>
      </View>
    );
  }

  shopList() {

    const { goodsList } = this.state;

    return (
      <View className="at-row list-cont">

        <View className="at-col at-col-6 left">
          { goodsList.length && goodsList.map((item, ii) => {
            if (ii % 2 === 0) {
              return (
                <View
                  className="at-row goods-item"
                  key={item.skuId}
                  onClick={() => {
                    Taro.navigateTo({
                      url:
                        "/pages/mall/detail/index?goodsId=" + item.skuId,
                    });
                  }}
                >
                  <View className="at-col">
                    <View className="at-row">
                      <View className="at-col">
                        <Image mode="widthFix" src={item.skuCoversUrl} />
                      </View>
                    </View>
                    <View className="at-row at-row--wrap goods-t1">
                      <View className="at-col">
                        <Text>
                            {item.goodsName?item.goodsName.length>=19?item.goodsName.substring(0,19)+'...':item.goodsName:null}
                        </Text>
                      </View>
                    </View>
                    <View className="at-row at-row--wrap goods-t2">
                      <View className="at-col">
                        {/* <Text style="padding: 3px 4px; background-color:#eee; border-radius:6px; font-size:12px;">{item.skuTitleType}: {item.skuTitle}</Text> */}
                      </View>
                    </View>
                    <View className="at-row goods-t3">
                      <View className="at-col">
                        <Text className="goods-money1">{item.skuCurrPrice}</Text><Text className="goods-money2">
                          .0元{item.vip*1==1?'':'/积分'}
                        </Text>
                      </View>
                      {
                        item.integralStatus == 1 ?
                        <View className="at-col" style="text-align: right;margin-top: 4PX">
                          <Text className="goods-money1" style="color: #ccc; font-size:12PX">积分商品</Text>
                        </View>
                        :
                        null
                      }
                    </View>
                  </View>
                </View>
              );
            }
          })}
        </View>
        <View className="at-col at-col-6 right">
          { goodsList.length && goodsList.map((item, ii) => {
            if (ii % 2 === 1) {
              return (
                <View
                  className="at-row goods-item"
                  key={item.skuId}
                  onClick={() => {
                    Taro.navigateTo({
                      url:
                        "/pages/mall/detail/index?goodsId=" + item.skuId,
                    });
                  }}
                >
                  <View className="at-col">
                    <View className="at-row">
                      <View className="at-col">
                        <Image mode="widthFix" src={item.skuCoversUrl} />
                      </View>
                    </View>
                    <View className="at-row at-row--wrap goods-t1">
                      <View className="at-col">
                        <Text>
                        {item.goodsName?item.goodsName.length>=19?item.goodsName.substring(0,19)+'...':item.goodsName:null}  
                        </Text>
                      </View>
                    </View>
                    <View className="at-row at-row--wrap goods-t2">
                      <View className="at-col">
                        {/* <Text style="padding: 3px 4px; background-color:#eee; border-radius:6px; font-size:12px;">{item.skuTitleType}: {item.skuTitle}</Text> */}
                      </View>
                    </View>
                    <View className="at-row .goods-t3">
                      <View className="at-col">
                        <Text className="goods-money1">{item.skuCurrPrice}</Text><Text className="goods-money2">
                          .0元{item.vip*1==1?'':'/积分'}
                        </Text>
                      </View>
                      {
                        item.integralStatus == 1 ?
                        <View className="at-col" style="text-align: right;margin-top: 4PX">
                          <Text className="goods-money1" style="color: #ccc; font-size:12PX">积分商品</Text>
                        </View>
                        :
                        null
                      }
                    </View>
                  </View>
                </View>
              );
            }
          })}

        </View>
        {/* {goodsList.length && goodsList.map((item, ii) => {

          return (
            <View className="at-col at-col-6 shop-item">
              <View
                className="at-row goods-item"
                key={item.skuId}
                onClick={() => {
                  Taro.navigateTo({
                    url:
                      "/pages/mall/detail/index?goodsId=" + item.skuId,
                  });
                }}
              >
                <View className="at-col">
                  <View className="at-row">
                    <View className="at-col">
                      <Image mode="widthFix" src={item.skuCoversUrl} />
                    </View>
                  </View>
                  <View className="at-row at-row--wrap .goods-t1">
                    <View className="at-col">
                      <Text>{item.goodsName}</Text>
                    </View>
                  </View>
                  <View className="at-row at-row--wrap .goods-t2">
                    <View className="at-col">
                      <Text style="padding: 3px 4px; background-color:#eee; border-radius:6px; font-size:12px;">{item.skuTitleType}: {item.skuTitle}</Text>
                    </View>
                  </View>
                  <View className="at-row .goods-t3">
                    <View className="at-col">
                      <Text>{item.skuCurrPrice}元{item.vip==0?'/积分':''}</Text>
                    </View>
                  </View>
                </View>
              </View>
            </View>
          );
        })} */}

      </View>
    )
  }

  onTabClick(value) {
    this.setState({
      tabCurrent: value * 1,
    }, () => {
      this.getGoodsList();
    })

  }

  render() {
    const { tabCurrent } = this.state;
    const tabList = ['推荐商品', '会员商品', '积分商品'];
    return (
      <View className="at-row page-home-index">
        <View className="at-col c0">
          {this.initSearchBox()}
          {this.initImgSwiper()}
          {this.initVip()}
          <AtSegmentedControl
            values={tabList}
            onClick={this.onTabClick.bind(this)}
            current={tabCurrent}
          />
          {this.shopList()}
        </View>
      </View>
    );
  }
}
