import Taro, { Component } from '@tarojs/taro'
import { View, Text } from '@tarojs/components'
import { AtButton,AtAvatar,AtIcon    } from 'taro-ui'
import './index.scss'


export default
 class Index extends Component {
   config = {
     navigationBarTitleText: "我的地址",
   };

   constructor(props) {
     super(...arguments);
     this.state = {
       addressList: [
         {
           id: 1,
           name: "赵先生",
           tel: "150****6666",
           describe:"北京市昌平区珠江摩尔大厦3号楼2单元2005",
           select: true
         },{
          id: 2,
          name: "赵先生",
          tel: "150****6666",
          describe:"北京市昌平区珠江摩尔大厦3号楼2单元2005",
          select: true
        },{
          id: 3,
          name: "赵先生",
          tel: "150****6666",
          describe:"北京市昌平区珠江摩尔大厦3号楼2单元2005",
          select: true
        }
       ],
     };
   }

   onSearch() {}

   onChange() {}

   onItemClick() {}

   render() {
     return (
       <View className="at-row page-qrcode-index">
         <View className="at-col col-qrcode">
           <View className="at-row qrcode">
             <View className="at-col" style="padding:20PX 10PX">
               <View className="at-row at-row__align--center">
                 <View className="at-col at-col-2" style="padding:10PX">
                   <AtAvatar
                     circle
                     image="https://jdc.jd.com/img/200"
                   ></AtAvatar>
                 </View>
                 <View className="at-col at-col-8" style="padding:10PX">
                   <View className="at-row at-row__align--center">
                     <View className="at-col">
                       <Text style="font-size: 18PX;font-weight: bold;">
                         王先生
                        </Text>
                     </View>
                   </View>
                   <View className="at-row" style="margin-top:3PX;">
                     <View className="at-col">
                       <Text style="font-size: 12PX;">
                         我正在使用**，消费还能赚钱，快来体验吧
                       </Text>
                     </View>
                   </View>
                 </View>
               </View>
               <View className="at-row at-row__align--center at-row__justify--center">
                 <View className="at-col" style="text-align:center;">
                  <Text style="width:275PX;color:#17A8E6;font-weight: bold;">
                    - - - - - - - - - - - - - - - - - - -
                  </Text>
                 </View>
               </View>
               <View className="at-row at-row__align--center at-row__justify--center">
                 <View className="at-col" style="text-align:center;">
                   <image style="width:275PX;height:275PX;" src={"https://bai-tong-fen-xiao.oss-cn-beijing.aliyuncs.com" + "/shopping/user/qrcode.png"} />
                 </View>
               </View>
             </View>
           </View>
           <View className="at-row" style="margin-top:50PX">
             <View className="at-col">
               <AtButton
                 circle
                 type="primary"
                 className="btn-submit"
                 onClick={() => {}}
               >
                 分享
               </AtButton>
             </View>
           </View>
         </View>
       </View>
     );
   }
 }
