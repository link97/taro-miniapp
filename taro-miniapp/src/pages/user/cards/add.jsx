import Taro, { Component } from "@tarojs/taro";
import { View, Text, Picker } from "@tarojs/components";
import { AtForm, AtInput, AtButton } from "taro-ui";
import { $Req } from "@utils";
import { connect } from "@tarojs/redux";
import "./index.scss";

const img_selected = "https://bai-tong-fen-xiao.oss-cn-beijing.aliyuncs.com" + "/shopping/user/right.png";
const img_select = "https://bai-tong-fen-xiao.oss-cn-beijing.aliyuncs.com" + "/shopping/user/go.png";

@connect(
  ({ userModel }) => ({
    userModel,
  }),
  (dispatch) => ({
    userAction: dispatch.userModel,
  })
)

export default class Index extends Component {
  config = {
    navigationBarTitleText: "添加银行卡",
  };

  constructor(props) {
    super(...arguments);
    const { type } = this.$router.params;
    this.state = {
      selector: [],
      form: {
        cardName: "",
        bank: "请选择银行",
        cardId: "",
        cardMobile: "",
        verifyCode: "",
        bankId: undefined
      },
      btnText: "发送验证码",
      min: 60,
      isSend: true
    };
  }

  componentDidMount() {
    this.getBankList();
  }

  async getBankList() {
    let { code, data } = await $Req({
      type: "get",
      url: "/indexUser/getBankInfo",
      data: {}
    });
    if (code == 200) {
      this.setState({
        selector: data
      })
    }
  }

  async addBankUserMethods(postData) {
    let { code, msg, data } = await $Req({
      type: "post",
      url: "/indexUser/addBankCard",
      data: postData
    });
    
    if (code == 200) {
      Taro.showToast({
        title: msg
      })

      Taro.navigateBack({
        delta: 1,
      });
    }
  }

  async sendCodeMethods(phone) {
    let { code, data } = await $Req({
      type: "post",
      url: "/user/sendMsgCode",
      data: {
        type:1,
        phone
      }
    });
    if (code != 200) return false;

    let timer = setInterval(() => {
      if (this.state.min <= 1) {
        this.setState({
          btnText: '重新获取',
          min: 60,
          isSend: true
        })
        clearInterval(timer);
        return false;
      }

      this.state.min--;
      this.setState({
        btnText: this.state.min + 's后重新获取'
      })
    }, 1000)
  }

  sendCode() {
    // this.btnText

    if (this.state.isSend) {
      if (this.state.form.cardMobile) {
        if (!(/^1[3456789]\d{9}$/.test(this.state.form.cardMobile))) {
          Taro.showToast({
            title: "手机号码错误!",
            icon:"none"
          })
          return false;
        }

        this.sendCodeMethods(this.state.form.cardMobile)
        this.setState({
          isSend: false
        })
      } else {
        Taro.showToast({
          title: "请填写手机号",
          icon: "none"
        })
        return false;
      }
    } else {
      Taro.showToast({
        title: "已经在发送中了...",
        icon: "none"
      })
    }
  }

  addBankUser = () => {
    let { userModel: { userInfo: { userId } } } = this.props;

    this.state.form.userId = userId;

    console.log(this.state.form);
    let { cardId, cardMobile, verifyCode } = this.state.form;

    if (isNaN(cardId)) {
      Taro.showToast({
        title: "卡号错误!",
        icon:"none"
      })
      return false;
    }

    if (cardId.length > 19) {
      Taro.showToast({
        title: "卡号错误!",
        icon:"none"
      })
      return false;
    }

    if (isNaN(verifyCode)) {
      Taro.showToast({
        title: "验证码必须是数字!",
        icon:"none"
      })
      return false;
    }

    if (!cardMobile) {
      Taro.showToast({
        title: "手机号码不能为空!",
        icon:"none"
      })
      return false;
    }

    if (!(/^1[3456789]\d{9}$/.test(cardMobile))) {
      Taro.showToast({
        title: "手机号码错误!",
        icon:"none"
      })
      return false;
    }

    this.addBankUserMethods(this.state.form);
    
  }
  
  render() {
    return (
      <View className="at-row page-cards-add">
        <View className="at-col c0">
          <AtForm>
            <AtInput
              name="name"
              title="持卡人"
              required
              type="text"
              placeholder="请填写持卡人姓名"
              value={this.state.form.cardName}
              onChange={cardName => {
                this.state.form.cardName = cardName
                this.setState({
                  form: this.state.form
                })
              }}
            >
              <Image src={img_selected} />
            </AtInput>
            <Picker
              mode="selector"
              range={this.state.selector}
              rangeKey={"bankName"}
              onChange={({ detail: { value } }) => {
                this.state.form.bank = this.state.selector[value].bankName;
                this.state.form.bankId = this.state.selector[value].bankId;
                this.setState({
                  form: this.state.form
                })
              }}
            >
              <AtInput
                name="bankName"
                title="银行"
                required
                editable={false}
                type="number"
                placeholder="请选择银行"
                value={this.state.form.bank}
              >
                <Image src={img_select} />
              </AtInput>
            </Picker>

            <AtInput
              name="card"
              title="卡号"
              required
              maxLength={19}
              type="number"
              placeholder="请输入卡号"
              value={this.state.form.cardId}
              onChange={cardId => {
                this.state.form.cardId = cardId
                this.setState({
                  form: this.state.form
                })
              }}
            />
            <AtInput
              name="cardMobile"
              title="手机号"
              required
              type="number"
              maxLength="11"
              placeholder="请输入绑定银行卡的手机号"
              value={this.state.form.cardMobile}
              onChange={cardMobile => {
                this.state.form.cardMobile = cardMobile;
                this.setState({
                  form: this.state.form
                })
              }}
            />
            <AtInput
              name="code"
              title="验证码"
              type="number"
              required
              placeholder="请输入验证码"
              value={this.state.form.verifyCode}
              onChange={verifyCode => {
                this.state.form.verifyCode = verifyCode
                this.setState({
                  form: this.state.form
                })
              }}
            />
            <View className="sendcode" onClick={this.sendCode}>
              {this.state.btnText}
            </View>
          </AtForm>
          <View className="at-row at-row__justify--center at-row__align--center">
            <View className="at-col btn-col">
              <AtButton onClick={this.addBankUser} circle type="primary" className="btn-submit">
                提交
              </AtButton>
            </View>
          </View>
        </View>
      </View>
    );
  }
}
