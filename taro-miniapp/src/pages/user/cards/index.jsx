import Taro, { Component } from "@tarojs/taro";
import { AtSwipeAction } from "taro-ui"
import { View, Text } from "@tarojs/components";
import { $Req } from "@utils";
import "./index.scss";

const img_bank_gs = "https://bai-tong-fen-xiao.oss-cn-beijing.aliyuncs.com/shopping/user/bank-gs.png";
const img_bank_js = "https://bai-tong-fen-xiao.oss-cn-beijing.aliyuncs.com/shopping/user/bank-js.png";
const img_bank_ny = "https://bai-tong-fen-xiao.oss-cn-beijing.aliyuncs.com/shopping/user/bank-ny.png";
const img_bank_card = "https://bai-tong-fen-xiao.oss-cn-beijing.aliyuncs.com/shopping/user/bank-card.png";
const img_selected = "https://bai-tong-fen-xiao.oss-cn-beijing.aliyuncs.com/shopping/user/right.png";
const img_select = "https://bai-tong-fen-xiao.oss-cn-beijing.aliyuncs.com/shopping/user/go.png";

export default class Index extends Component {
  config = {
    navigationBarTitleText: "我的银行卡",
  };

  constructor(props) {
    super(...arguments);
    const { type } = this.$router.params;
    this.state = {
      searchVal: "",
      // 提现金额
      money: null,
      showAction: false,
      cardList: [],
    };
  }

  async getBankLists() {
    let { code, message, data } = await $Req({
      type: "get",
      url: "/indexUser/getBankCard",
      data: {}
    });

    data.forEach(el => {
      el.cardId = el.cardId.substr(el.cardId.length - 4, el.cardId.length)
    })

    this.setState({
      cardList: data
    })

  }

  componentDidShow() {
    this.getBankLists();
  }

  render() {
    return (
      <View className="at-row page-cards-index">
        <View className="at-col cards">
          <View className="at-row bank-car-list">
            <View className="at-col c0">
              {this.state.cardList.map((item) => {
                return (
                  <AtSwipeAction 
                  onClick={
                    async () => {
                      // GET 
                      let {data, code} = await $Req({
                        type: "get",
                        url: "/indexUser/delBankCard",
                        data: { id: item.bankId }
                      });

                      if (code == 200) {
                        Taro.showToast({
                          title: "删除成功！"
                        })
                        this.getBankLists();
                      }

                    }
                  } 
                  key={item.bankId} options={[
                    {
                      text: '删除',
                      style: {
                        backgroundColor: '#FF4949'
                      }
                    }
                  ]}>
                    <View
                      className="at-row at-row at-row__justify--between at-row__align--center list-item"
                    >
                      <View className="at-col at-col-9 left">
                        <image src={item.bankImg} />
                        <Text>{item.bankName}({item.cardId})</Text>
                      </View>
                      <View className="at-col at-col-3 right">
                        <image src={item.select ? img_selected : null} />
                      </View>
                    </View>
                  </AtSwipeAction>
                );
              })}
              <View
                className="at-row at-row at-row__justify--between at-row__align--center list-item"
                onClick={() => {
                  Taro.navigateTo({
                    url: "/pages/user/cards/add",
                  });
                }}
              >
                <View className="at-col at-col-9 left">
                  <image src={img_bank_card} />
                  <Text>添加银行</Text>
                </View>
                <View className="at-col at-col-3 right">
                  <image src={img_select} />
                </View>
              </View>
            </View>
          </View>
        </View>
      </View>
    );
  }
}
