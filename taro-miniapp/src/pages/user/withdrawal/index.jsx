import Taro, { Component } from "@tarojs/taro";
import { View, Text } from "@tarojs/components";
import { AtInput, AtButton, AtActionSheet, AtList, AtListItem } from "taro-ui";
import "./index.scss";

const img_bank_gs = "https://bai-tong-fen-xiao.oss-cn-beijing.aliyuncs.com" + "/shopping/user/bank-gs.png";
const img_bank_js = "https://bai-tong-fen-xiao.oss-cn-beijing.aliyuncs.com" + "/shopping/user/bank-js.png";
const img_bank_ny = "https://bai-tong-fen-xiao.oss-cn-beijing.aliyuncs.com" + "/shopping/user/bank-ny.png";
const img_bank_card = "https://bai-tong-fen-xiao.oss-cn-beijing.aliyuncs.com" + "/shopping/user/bank-card.png";
const img_selected = "https://bai-tong-fen-xiao.oss-cn-beijing.aliyuncs.com" + "/shopping/user/selected.png";
const img_select = "https://bai-tong-fen-xiao.oss-cn-beijing.aliyuncs.com" + "/shopping/user/select.png";

export default class Index extends Component {
  config = {
    navigationBarTitleText: "零钱提现",
  };

  constructor(props) {
    super(...arguments);
    const { type } = this.$router.params;
    this.state = {
      searchVal: "",
      // 提现金额
      money: null,
      showAction: false,
      cardList: [
        {
          id: 1,
          name: "中国建设银行(2005)",
          img: img_bank_js,
          select: true,
        },
        {
          id: 2,
          name: "中国工商银行(2005)",
          img: img_bank_gs,
          select: false,
        },
        {
          id: 3,
          name: "中国农业银行(2005)",
          img: img_bank_ny,
          select: false,
        },
      ],
    };
  }

  onSearch() {}

  onChange() {}

  onItemClick() {}

  render() {
    return (
      <View className="at-row page-withdrawal-index">
        <View className="at-col withdrawal">
          <View className="at-row at-row__align--center r1">
            <View className="at-col">
              <Text>提现到微信 </Text>
              <Text
                className="t2"
                onClick={() => {
                  this.setState({
                    showAction: true,
                  });
                }}
              >
                （微信号）
              </Text>
            </View>
          </View>
          <View className="at-row at-row__align--center r2">
            <View className="at-col">
              <Text>提现金额</Text>
            </View>
          </View>
          <View className="at-row r3">
            <View className="at-col">
              <AtInput
                name="value"
                title="￥"
                type="number"
                placeholder="请输入提现金额"
                value={this.state.money}
                onChange={() => {}}
              />
            </View>
          </View>
          <View className="at-row at-row__align--center r4">
            <View className="at-col">
              <Text className="t1">当前零钱余额900.00元</Text>
              <Text className="t2">全部提现</Text>
            </View>
          </View>
          <View className="at-row at-row__align--center r5">
            <View className="at-col">
              <AtButton circle type="primary" className="btn-submit">
                提现
              </AtButton>
            </View>
          </View>
        </View>
        <AtActionSheet isOpened={this.state.showAction} title="选择银行卡">
          <View className="at-row bank-car-list">
            <View className="at-col c0">
              {this.state.cardList.map((item) => {
                return (
                  <View
                    className="at-row at-row at-row__justify--between at-row__align--center list-item"
                    key={item.id}
                  >
                    <View className="at-col at-col-9 left">
                      <image src={item.img} />
                      <Text>{item.name}</Text>
                    </View>
                    <View className="at-col at-col-3 right">
                      <image src={item.select ? img_selected : img_select} />
                    </View>
                  </View>
                );
              })}
              <View className="at-row at-row at-row__justify--between at-row__align--center list-item">
                <View className="at-col at-col-9 left">
                  <image src={img_bank_card} />
                  <Text>添加银行</Text>
                </View>
                <View className="at-col at-col-3 right">
                  <image src={img_select} />
                </View>
              </View>
            </View>
          </View>
        </AtActionSheet>
      </View>
    );
  }
}
