
import Taro, { Component } from '@tarojs/taro'
import { View, Text } from '@tarojs/components'
import { AtTimeline, AtButton  } from 'taro-ui'
import './index.scss'

export default
 class Index extends Component {
   config = {
     navigationBarTitleText: "零钱提现",
   };

   constructor(props) {
     super(...arguments);
     const { type } = this.$router.params;
     this.state = {
       searchVal: "",
       // 提现金额
       money: null,
     };
   }

   onSearch() {}

   onChange() {}

   onItemClick() {}

   render() {
     return (
       <View className="at-row page-withdrawal-status">
         <View className="at-col c0">
           <View className="at-row at-row__align--center r1">
             <View className="at-col">
               <AtTimeline
                 pending
                 items={[
                   {
                     title: "发起提现申请",
                     icon: "check-circle",
                   },
                   {
                     title: "提现到零钱",
                     content: ["预计2020-6-24 20:50到账"],
                     icon: "clock",
                   },
                   {
                     title: "到账成功",
                     icon: "clock",
                   }
                 ]}
               ></AtTimeline>
             </View>
           </View>
           <View className="at-row at-row__justify--between at-row__align--center r2">
             <View className="at-col c-left">
               <Text>提现金额</Text>
             </View>
             <View className="at-col c-right">
               <Text>￥5.00</Text>
             </View>
           </View>
           <View className="at-row at-row__justify--between at-row__align--center r3">
             <View className="at-col c-left">
               <Text>到账银行卡</Text>
             </View>
             <View className="at-col c-right">
               <Text>建设银行 尾号1001</Text>
             </View>
           </View>
           <View className="at-row at-row__justify--between at-row__align--center r4">
             <View className="at-col c-left">
               <Text>服务费</Text>
             </View>
             <View className="at-col c-right">
               <Text>￥0.10</Text>
             </View>
           </View>
           <View className="at-row at-row__align--center r5">
             <View className="at-col">
               <AtButton circle type="primary" className="btn-submit">
                 完成
               </AtButton>
             </View>
           </View>
         </View>
       </View>
     );
   }
 }
