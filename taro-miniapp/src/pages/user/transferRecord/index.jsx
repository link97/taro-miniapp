import Taro, { Component } from '@tarojs/taro'
import { View, Text } from '@tarojs/components'
import { AtAvatar, AtIcon, AtButton, AtSegmentedControl } from 'taro-ui'
import { $Req } from "@utils";
import './index.scss'

export default
  class Index extends Component {
  config = {
    navigationBarTitleText: "收益转让记录",
  };

  constructor() {
    super(...arguments)
    this.state = {
      current: 0,
      switch: false,
      list: []
    }
  }

  componentDidShow() {
    this.getList(0);
  }

  onSubmit() {
    // 结算
    Taro.navigateTo({
      url: "/pages/order/success/index",
    });
  }


  async getList(flag) {

    let postData = {}

    switch (flag) {
      case 0:
        postData = { type: 600 };
        break;
      case 1:
        postData = { type: 700 }
        break;
    }

    let { code, data } = await $Req({
      type: "post",
      url: "/stat/getTransferBenefitTransaction",
      data: postData,
    });
    if (code != 200) return;
    this.setState({
      list: data.data
    })
  }

  render() {
    return (
      <View className="page-user-relate">
        <View style="padding:10px 20px">
          <AtSegmentedControl
            values={['转账给他人', '他人转给我']}
            onClick={(e) => {
              this.getList(e);
              this.setState({
                current: e
              })
            }}
            current={this.state.current}
          />
        </View>
        <View className="at-row">
          <View className="at-col c0">
            {this.state.list.map((item) => {
              return (
                <View className="at-row relate-confirm-user" key={item.id}>
                  <View className="at-col confirm-user-c0 list-item-box">
                    <View className="at-row at-row__align--center">
                      <View className="at-col at-col-2">
                        <AtAvatar
                          circle
                          image={item.type == 1 ? item.helpUserPhoto ? item.helpUserPhoto : "https://jdc.jd.com/img/200" : item.userPhoto ? item.userPhoto : "https://jdc.jd.com/img/200"}
                        ></AtAvatar>
                      </View>
                      <View className="at-col at-col-10">
                        <View className="at-row">
                          <View className="at-col">
                            <View className="at-row" style="justify-content: space-between;">
                              <Text className="user-name">{item.type == 1 ? item.helpUserName : item.userName}</Text>
                              <Text className="user-level-name" style={{ color: item.type == 1 ? "red" : "green" }}>{item.typeName}</Text>
                            </View>

                          </View>
                        </View>
                        <View >
                          <View className="at-col user-date" style="text-align:left; margin-top: 5px;">
                            <Text>交易金额: ￥{item.benefit ? item.benefit : '0'}</Text>
                          </View>
                          <View className="at-col user-date" style="text-align:left; margin-top: 5px;">
                            <Text>交易时间: {item.createTime ? item.createTime : '无'}</Text>
                          </View>
                          <View className="at-col">
                            <Text className="user-level-name">{item.des}</Text>
                          </View>
                        </View>
                      </View>
                    </View>
                  </View>
                </View>
              );
            })}
          </View>
        </View>
      </View>
    );
  }
}
