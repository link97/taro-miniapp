import Taro, { Component } from "@tarojs/taro";
import { View, Text } from "@tarojs/components";
import { AtSearchBar, AtButton } from "taro-ui";
import "./index.scss";

export default class Index extends Component {
  config = {
    navigationBarTitleText: "我的订单",
  };

  constructor(props) {
    super(...arguments);
    const { type } = this.$router.params;
    const title = ["全部订单", "待付款", "待收货", "已完成"];
    wx.setNavigationBarTitle({
      title: title[type],
    });
    this.state = {
      searchVal: "",
      list: [],
    };
  }

  render() {
    return (
      <View className="at-row page-order-index">
        <View className="at-col">
          <View className="at-row search-box">
            <AtSearchBar
              showActionButton
              fixed
              value={this.state.value}
              onChange={(v) => {
                this.onChange(v);
              }}
              onActionClick={() => {
                this.onSearch();
              }}
            />
          </View>
          <View className="at-row order-box">
            <View className="at-col order-list">
              {this.state.list.map((item) => {
                return (
                  <View className="at-row list-item" key={item.id}>
                    <View className="at-col c1">
                      <View className="at-row r1">
                        <View className="at-col at-col-4 c11">
                          <image src={getApp().ossUrl + "/shopping/order/goods.png"} />
                        </View>
                        <View className="at-col at-col-6 c12">
                          <View className="at-row">
                            <Text className="title">
                              新布局大米富硒香米大米2.5kg 长粒香米稻花2020新米
                            </Text>
                          </View>
                          <View className="at-row">
                            <Text className="specs">规格：3斤装</Text>
                          </View>
                          <View className="at-row">
                            <Text className="price">￥199.0元/积分</Text>
                          </View>
                        </View>
                        <View className="at-col at-col-2 c13">
                          <Text className="status">待付款</Text>
                          <Text className="count">x1</Text>
                        </View>
                      </View>
                      <View className="at-row at-row__justify--end r2">
                        <Text>总价￥199.00</Text>
                        <Text>积分抵扣￥80.00</Text>
                        <Text>需付款￥119.00</Text>
                      </View>
                      <View className="at-row at-row__justify--end r3">
                        <AtButton
                          type="secondary"
                          circle
                          size="small"
                          className="btn-left"
                        >
                          取消订单
                        </AtButton>
                        <AtButton
                          type="primary"
                          circle
                          size="small"
                          className="btn-right"
                        >
                          结算
                        </AtButton>
                      </View>
                    </View>
                  </View>
                );
              })}
            </View>
          </View>
        </View>
      </View>
    );
  }
}
