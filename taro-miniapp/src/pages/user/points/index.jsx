import Taro, { Component } from "@tarojs/taro";
import { View, Text } from "@tarojs/components";
import { $Req } from "@utils";
import "./index.scss";

const img_gold = "https://bai-tong-fen-xiao.oss-cn-beijing.aliyuncs.com" + "/shopping/user/gold.png";

export default class Index extends Component {
  config = {
    navigationBarTitleText: "积分",
  };

  constructor(props) {
    super(...arguments);
    this.state = {
      list: [],
      useJf: 0,
      current: 1,
      usedArr:[],
    };
  }

  componentDidShow() {
    this.getJf();
  }

  // 点击订单状态
  onOrderClick(type) {
    Taro.navigateTo({
      url: "/pages/user/order/index?type=" + type,
    });
  }

  async getJf() {
    // 
    let { code, data } = await $Req({
      type: "get",
      url: "/indexUser/getUserIntegralDetail",
      data: {},
    });

    let flag = 0;
    let usedArr = [];
    data.forEach(el => {
      if( el.integral < 0 ) {
        flag += el.integral;
        usedArr.push(el);
      }
    })

    this.setState({
      list: data,
      allArr: data,
      useJf: Math.abs(flag),
      usedArr
    })
  }
  handleClick(val) {
    const {allArr, usedArr} = this.state;
      this.setState({
        current: val,
        list: val==1? allArr : usedArr 
      });
  }
  render() {
    return (
      <View className="page-user-points">
        <View className="at-row at-row__justify--center at-row__align--center r1">
          <View className="at-col">
            <View className="at-row at-row__justify--center at-row__align--center">
              <Text className="points">{this.state.list.length ? this.state.list[0].userTotalIntegral : 0}</Text>
            </View>
            <View className='at-row at-row__justify--center at-row__align--center'>
              <Text className="current">当前积分</Text>
            </View>
          </View>
        </View>
        <View className="at-row at-row__justify--between at-row__align--center r2">
          <View onClick={this.handleClick.bind(this,1)} className={['at-col left',current==1?'selected':'unselected']}>
            <Text>积分记录</Text>
          </View>
          <View onClick={this.handleClick.bind(this,2)} className={['at-col right',current==2?'selected':'unselected']}>
            <Text>已使用{this.state.useJf}</Text>
          </View>
        </View>
        {this.state.list.map((item) => {
          return (
            <View className="at-row at-row__justify--between at-row__align--center list-item">
              <View className="at-col left">
                <View className="at-row at-row__justify--between at-row__align--center">
                  <Text className="name">{item.des}</Text>
                </View>
                <View className="at-row at-row__justify--between at-row__align--center">
                  <Text className="date">{item.updateTime}</Text>
                </View>
              </View>
              <View className="at-col right">
                <Text className="points">{item.integral}</Text>
                <image src={img_gold} />
              </View>
            </View>
          );
        })}
      </View>
    );
  }
}
