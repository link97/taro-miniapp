import Taro, { Component } from "@tarojs/taro";
import { View, Text } from "@tarojs/components";
import { connect } from "@tarojs/redux";
import { $Req } from "@utils";
import {
  AtButton,
} from "taro-ui";
import "./index.scss";

const z_img = 'https://bai-tong-fen-xiao.oss-cn-beijing.aliyuncs.com/imgs/user/z-icon.png';
const g_img = 'https://bai-tong-fen-xiao.oss-cn-beijing.aliyuncs.com/imgs/user/g-icon.png';
const c_img = 'https://bai-tong-fen-xiao.oss-cn-beijing.aliyuncs.com/imgs/user/c-icon.png';
const d_img = 'https://bai-tong-fen-xiao.oss-cn-beijing.aliyuncs.com/imgs/user/d-icon.png';
const zw_img = 'https://bai-tong-fen-xiao.oss-cn-beijing.aliyuncs.com/imgs/user/vacancy-icon.png';
const process_img = [z_img, g_img, c_img];
const process_img_2 = [g_img, c_img];
@connect(
  ({ userModel }) => ({
    userModel,
  }),
  (dispatch) => ({
    userAction: dispatch.userModel,
  })
)
export default class Index extends Component {
  config = {
    navigationBarTitleText: "分销详情",
  };

  constructor(props) {
    super(...arguments);
    this.state = {
      id: '',
      fxData: {},
     // processIdx: 0,
      current: 1,
      helpArr: [],
    }


  }
  componentDidMount() {
    const { id } = this.$router.params;
    let shareId = this.$router.params.userId;
    console.log("获取到分享人的id：", shareId);

    if (shareId) {
      Taro.setStorageSync("shareId", shareId);
    }
    this.setState({
      id
    }, () => {
      this.getOrderDetailForSmall();
    })

  }
  async getOrderDetailForSmall(level) {
    const { id } = this.state;
    
    let resData = {
      orderId: id
    }
    if(level){
      resData.level = level;
    }
    let { code, data } = await $Req({
      type: "get",
      url: "/distribution/getOrderDetailForSmall",
      data: resData
    });
  /*  let processIdx = '', processFlag = true;
    data.progress.map((item, index) => {
      if (item.processStatus * 1 == 0 && processFlag) {
        processIdx = index - 1;
        processFlag = false;
      }
    })*/

    this.setState({
      fxData: data,
      helpArr: data.helpMeList,
     // processIdx
    });
  }
  componentDidShow() {

  }
  goBuyDetail(item) {
      Taro.navigateTo({
        url: `/pages/user/buyPlaceDetail/index?id=${item.orderId}`,
      });
  }
  processHandle(item) {
    console.log(item);

    if(item.processStatus*1 == 0) {
      Taro.showToast({
        title: `还不是${item.processName}，升级为${item.processName}后方可查看`,
        icon:"none"
      })
    } else {
      this.getOrderDetailForSmall(item.level);
    }

  }

  onShareAppMessage(res) {
    const {
      userModel: { userInfo },
    } = this.props;
    console.log(userInfo.userId)
    if (res.from === 'button') {
      // 来自页面内转发按钮
      console.log(res.target);
    }
    return {
      title: "分销详情",
      path: `/pages/user/buyPlaceDetail/index?id=${this.state.id}&userId=${userInfo.userId}`
    };
  }

  render() {
    const { fxData, current, helpArr } = this.state;
    const {
      progress,
      currentLevelBonusPoints,
      currentLevelBonusPresentation,
      payTypeName,
      settleTime,
      orderNo,
      payMoney,
      bonusUserRecordList,
      helpMeList,
      meHelpList,
      levelQuery } = fxData;
    let p_i = [];  

 
    if( progress && progress.length == 2 ) {
      p_i = process_img_2;
    }  else {
      p_i = process_img;
    }
    return (
      <View className="page-user-income">
        <View className="process-wrap">
          {
            progress.map((item, index) => (
              <View className='item' onClick={this.processHandle.bind(this,item)}>
                <View>
                  <Image className="icon" src={item.processStatus * 1 == 0 ? d_img : p_i[index]} />
                </View>
                <View className="title">{item.processName}</View>
              </View>
            ))
          }

        </View>

        <View className='fx-detail'>
          <View className='title'>分销明细</View>
          <View className={['item-wrap', levelQuery * 1 == 3 ? 'gj-color' : '', levelQuery * 1 == 4 ? 'cg-color' : '']}>
            <View className='item-left'>积分</View>
            <View className='item-right'>{currentLevelBonusPoints}</View>
          </View>
          <View className={['item-wrap', levelQuery * 1 == 3 ? 'gj-color' : '', levelQuery * 1 == 4 ? 'cg-color' : '']}>
            <View className='item-left'>拼团分红金</View>
            <View className='item-right'>{currentLevelBonusPresentation}</View>
          </View>
        </View>
        <View className='fx-des'>
          <View className='title' style="display:flex;justify-content: space-between;">
            <View>订单详情</View>
            <View style="color:#DF3D09;font-size:12PX;"  className={[levelQuery * 1 == 3 ? 'gd-g-color' : '', levelQuery * 1 == 4 ? 'gd-c-color' : '']}>{orderStatus == 40000 ? '已关单' : ''}</View>
          </View>
          <View className='item'>
            <View>订单编号:</View>
            <View>{orderNo}</View>
          </View>
          <View className='item'>
            <View>订单来源:</View>
            <View>{payTypeName}</View>
          </View>
          <View className='item'>
            <View>支付金额:</View>
            <View>￥{payMoney}</View>
          </View>
          <View className='item'>
            <View>购买时间:</View>
            <View>{settleTime}</View>
          </View>
        </View>

        <View className='fx-des'>
          <View className='title'>
            <View className="left">拼团分红使用记录</View>
          </View>
          {
            bonusUserRecordList.map((item, index) => (
              <View onClick={this.goBuyDetail.bind(this,item)}>
                {item.orderNo ?
                  <View>
                    <View className='sub-title'>{item.typeName}</View>
                    <View className='item'>
                      <View>订单编号:</View>
                      <View>{item.orderNo}</View>
                    </View>
                    <View className='item'>
                      <View>消耗拼团分红:</View>
                      <View>{item.bonusMoney}</View>
                    </View>
                    <View className='item'>
                      <View>购买时间:</View>
                      <View>{item.time}</View>
                    </View>
                  </View> : <View className='fx-des' >
                    <View className='sub-title'>分红转收益</View>
                    <View className='item'>
                      <View>剩余拼团分红转入收益:</View>
                      <View>{item.bonusMoney}</View>
                    </View>
                    <View className='item'>
                      <View>转入时间:</View>
                      <View>{item.time}</View>
                    </View>
                  </View>}
              </View>
            ))
          }

          {
            (bonusUserRecordList&&bonusUserRecordList.length==0) ? 
            <View style="height:88PX;padding-top:25PX;">
                <Image style="width:55PX;height:48PX;margin:0 auto;display:block;" src= {zw_img}/>
                <View style="color:#CCCCCC;font-size:14PX;text-align:center;padding-top:4PX;">暂无记录</View>
            </View> : null
          }     
        </View>

        <View className='fx-des'>
          <View className='title'>
            <View className={['left', current == 1 ? 'active' : '']} onClick={() => {
              this.setState({
                current: 1,
                helpArr: helpMeList,
              })
            }}>助团我的</View>
            <View className={['right', current == 2 ? 'active' : '']} onClick={() => {
              this.setState({
                current: 2,
                helpArr: meHelpList,
              })
            }}>我助团的</View>
          </View>
          {
            helpArr.map((item, index) => (
              <View className='user-item'>
                <View className='user-info'>
                  <View className='user-avatar'>
                    <Image style="width:100%;height:100%;" mode="aspectFill" src={current == 1?item.userPhoto:item.helpUserPhoto} />
                  </View>
                  <View>
                    <View className='nickName'>{current == 1?item.userName : item.helpUserName}</View>
                    <View className='order-code'>订单编号：{current == 1?item.distributionOrderNo:item.helpOrderNo} </View>
                  </View>
                </View>
                <View className='a'>
                  <View style="color:#666;font-size:12PX;text-align:right;">{item.createTime}</View>
                </View>
              </View>
            ))
          }

        </View>
        <View className='bottom-foot'>
          <AtButton type='primary' circle={true} openType="share">邀请好友</AtButton>
        </View>

      </View>
    );
  }
}
