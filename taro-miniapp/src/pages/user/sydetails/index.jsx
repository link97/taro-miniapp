import Taro, { Component } from "@tarojs/taro";
import { View, Text } from "@tarojs/components";
import { connect } from "@tarojs/redux";
import { $Req } from "@utils";
import "./index.scss";

@connect(
  ({ userModel }) => ({
    userModel,
  }),
  (dispatch) => ({
    userAction: dispatch.userModel,
  })
)
export default class Index extends Component {
  config = {
    navigationBarTitleText: "收益记录",
  };

  constructor() {
    super(...arguments);
    this.state = {
      list: [],
    };
  }

  componentDidMount() {
    // 收益记录
    this.getBenefitTransaction();
  }

  // 我的分销商
  async getBenefitTransaction() {
    const {
      userModel: { userInfo },
    } = this.props;

    const { data } = await $Req({
      type: "post",
      url: "/stat/getBenefitTransaction",
      data: {
        start: 1,
        length: 999999,
        userId: userInfo.userId
      },
    });
    if (!data) return;
    this.setState({
      list: data.data,
    });
  }

  render() {
    const { list } = this.state;
    return (
      <View className="page-user-relate">
        {
          list.map((item, index) => (
            <View className='sy-list-item'>
              <View className='item-left'>
                <View className='title'>{item.typeName}</View>
                <View className='des'>{item.des}</View>
                <View className='des'>订单号：{item.orderNo}</View>
                <View className='des'>{item.createTime}</View>
              </View>
          <View className='item-right'>{item.type*1==500?'-':'+'}￥{item.benefit}</View>
            </View>
          ))
        }
      </View>
    );
  }
}
