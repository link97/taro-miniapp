import Taro, { Component } from "@tarojs/taro";
import { View, Text } from "@tarojs/components";
import { connect } from "@tarojs/redux";
import { $Req } from "@utils";
import { AtForm, AtInput, AtButton } from "taro-ui";
import "./index.scss";

@connect(
  ({ userModel }) => ({
    userModel,
  }),
  (dispatch) => ({
    userAction: dispatch.userModel,
  })
)
export default class Index extends Component {
  config = {
    navigationBarTitleText: "我的",
  };

  constructor(props) {
    super(...arguments);
    const { type } = this.$router.params;
    let {
      userModel: { userInfo },
      userAction,
    } = this.props;

    this.state = {
      //1: 姓名  0:年龄
      type,
      userName: "",
      userOld: "",
    };
  }

  async getUserInfo() {
    let { code, data } = await $Req({
      type: "post",
      url: "/user/getUserCenterInfo",
      data: {},
    });
    
    if (code == 200) {
      this.setState({
        userName: data.name,
        userOld: data.age,
      })
    }
  }

  componentDidShow() {
    this.getUserInfo();
  }

  // 点击列表
  onReset() {}

  // 点击订单状态
  onSubmit() {
    if (this.state.type === "1") {
      this.updateUserName();
    } else {
      this.updateUserAge();
    }
  }

  // 修改用户年龄
  async updateUserAge() {

    if(this.state.userOld > 0 && this.state.userOld <= 100) {
      let {
        userModel: { userInfo },
        userAction,
      } = this.props;
      let { code, message, data } = await $Req({
        type: "post",
        url: "/user/updateUserWx",
        data: {
          age: this.state.userOld,
        },
      });
      if (code !== 200) return;
      Taro.showToast({
        title: "更新成功"
      })
  
      Taro.navigateBack({
        delta: 1,
      });
    } else {
      Taro.showToast({
        title: "年龄超出范围",
        icon: "none"
      })
    }

    
  }

  // 修改用户姓名
  async updateUserName() {
    let {
      userModel: { userInfo },
      userAction,
    } = this.props;
    let { code, message, data } = await $Req({
      type: "post",
      url: "/user/updateUserWx",
      data: {
        name: this.state.userName,
      },
    });
    if (code !== 200) return;

    Taro.showToast({
      title: "更新成功"
    })

    Taro.navigateBack({
      delta: 1,
    });
  }

  render() {
    return (
      <View className="user-info-edit">
        <AtForm
          onSubmit={() => {
            this.onSubmit();
          }}
          onReset={() => {
            this.onReset();
          }}
        >
          {this.state.type === "1" ? (
            <AtInput
              name="value"
              type="text"
              placeholder="姓名"
              value={this.state.userName}
              onChange={(v) => {
                this.setState({
                  userName: v,
                });
              }}
            />
          ) : (
            <AtInput
              name="value"
              type="number"
              placeholder="年龄"
              maxLength="3"
              value={this.state.userOld}
              onChange={(v) => {
                this.setState({
                  userOld: v,
                });
              }}
            />
          )}
        </AtForm>
        <View className="at-row at-row__justify--center at-row__align--center">
          <View className="at-col c11">
            <AtButton
              circle
              type="primary"
              className="btn-submit"
              onClick={() => {
                this.onSubmit();
              }}
            >
              提交
            </AtButton>
          </View>
        </View>
      </View>
    );
  }
}
