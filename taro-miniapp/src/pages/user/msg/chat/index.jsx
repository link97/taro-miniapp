import Taro, { Component } from "@tarojs/taro";
import { View, Text } from "@tarojs/components";
import { AtSearchBar, AtButton } from "taro-ui";
import "./index.scss";

export default class Index extends Component {
  config = {
    navigationBarTitleText: "消息",
  };

  constructor() {
    super(...arguments);
    this.state = {
      currentTab: 0,
      switch: false,
      value: "",
      list: [
        {
          id: 1,
          msg: "您的订单已发货",
          date: "2020年6月29日 15:20",
          name: "您购买的新布局大米富硒香米大米2.5kg长粒...",
        },
        {
          id: 2,
          msg: "您的订单已发货",
          date: "2020年6月29日 15:20",
          name: "您购买的新布局大米富硒香米大米2.5kg长粒...",
        },
        {
          id: 3,
          msg: "您的订单已发货",
          date: "2020年6月29日 15:20",
          name: "您购买的新布局大米富硒香米大米2.5kg长粒...",
        },
      ],
    };
  }

  onTabClick() {}

  onSubmit() {
    // 结算
    Taro.navigateTo({
      url: "/pages/order/success/index",
    });
  }

  render() {
    return (
      <View className="page-user-msg-chat">
        <View className="at-row">
          <View className="at-col c0">
            {/* <View className="at-row list-item">
               <View className="at-col">
                 <View className="at-row at-row__justify--center">
                   <View className="at-col msg-date">
                     <Text>今天 17:50</Text>
                   </View>
                 </View>
                 <View className="at-row">
                   <View className="at-col list-item-box">
                     <View className="at-row">
                       <View className="at-col at-col-3">
                         <image
                           style="width:84PX;height:84PX;"
                           src={"https://bai-tong-fen-xiao.oss-cn-beijing.aliyuncs.com" + "/shopping/order/goods.png"}
                         />
                       </View>
                       <View className="at-col at-col-9">
                         <View className="at-row goods-name">
                           <View className="at-col">
                             <Text>新布局大米富硒香米大米长粒香米稻花香</Text>
                           </View>
                         </View>
                         <View className="at-row goods-money" style="margin-top:10PX">
                           <View className="at-col">
                             <Text className="goods-money1">￥199</Text>
                             <Text className="goods-money2">.0元/积分</Text>
                           </View>
                         </View>
                       </View>
                     </View>
                   </View>
                 </View>
               </View>
             </View> */}

            <View className="at-row list-item">
              <View className="at-col">
                <View className="at-row at-row__justify--center">
                  <View className="at-col msg-date">
                    <Text>今天 17:50</Text>
                  </View>
                </View>

                <View className="at-row at-row__align--center at-row__justify--end chat-right">
                  <View className="at-col chat-user">
                    <Text>这个从哪发货呢</Text>
                    <image src="https://jdc.jd.com/img/200" />
                  </View>
                </View>
              </View>
            </View>

            <View className="at-row list-item">
              <View className="at-col">
                <View className="at-row at-row__justify--center">
                  <View className="at-col msg-date">
                    <Text>今天 17:50</Text>
                  </View>
                </View>
                <View className="at-row at-row__align--center at-row__justify--start chat-left">
                  <View className="at-col chat-service">
                    <image src="https://jdc.jd.com/img/200" />
                    <Text>这个从哪发货呢</Text>
                  </View>
                </View>
              </View>
            </View>
          </View>
        </View>
        <View className="at-row search-box">
          <View className="at-col">
            <AtSearchBar
              placeholder="咨询内容"
              actionName="发送"
              value={this.state.value}
              onChange={(v) => {}}
              onActionClick={() => {}}
            />
          </View>
        </View>
      </View>
    );
  }
}
