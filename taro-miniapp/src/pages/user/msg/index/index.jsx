import Taro, { Component } from "@tarojs/taro";
import { View, Text } from "@tarojs/components";
import { AtAvatar, AtIcon, AtButton } from "taro-ui";
import { connect } from "@tarojs/redux";
import { $Req } from "@utils";
import "./index.scss";

@connect(
  ({ userModel }) => ({
    userModel,
  }),
  (dispatch) => ({
    userAction: dispatch.userModel,
  })
)


export default class Index extends Component {
  config = {
    navigationBarTitleText: "消息",
  };

  constructor() {
    super(...arguments);
    this.state = {
      currentTab: 0,
      switch: false,
      list: [],
    };
  }

  componentDidShow() {
    this.getChar();
  }

  async getChar() {

    let {
      userModel: { userInfo },
      userAction,
    } = this.props;

    let { code, data } = await $Req({
      type: "get",
      url: "/indexUser/getUserMessage",
      data: {
        userId: userInfo.userId
      },
    });
    this.setState({
      list: data
    }, () => {
      setTimeout(() => {
        Taro.hideLoading();
      }, 300)
    })
  }

  onSubmit() {
    // 结算
    Taro.navigateTo({
      url: "/pages/order/success/index",
    });
  }

  render() {
    return (
      <View className="page-user-msg">
        <View className="at-row">
          <View className="at-col c0">

           {
             this.state.list.map(el => (
              <View className="at-row list-item">
                <View className="at-col">
                  <View
                    className="at-row"
                  >
                    <View className="at-col list-item-box">
                      <View
                        className="at-row at-row__align--center"
                      >
                        <View className="at-col at-col-9">
                          <View className="at-row at-row__align--center">
                            <View className="at-col">
                              <Text className="msg-reply">
                                {el.message}
                              </Text>
                            </View>
                          </View>
                          <View className="at-row at-row__align--center">
                            <View className="at-col">
                              <Text className="msg-reply">
                                {el.createTime}
                              </Text>
                            </View>
                            <View className="at-col" style="margin-left: 180px">
                              <Text className="msg-reply">
                                已读
                              </Text>
                            </View>
                          </View>
                        </View>
                      </View>
                    </View>
                  </View>
                </View>
              </View>
             ))
           }
          </View>
        </View>
        <View className="message">
            <View className="contact" onClick={
              () => {
                Taro.showLoading({
                  title: "加载中..."
                });
                this.getChar();
              }
            }>
                <AtIcon value='reload' size='20' color='#fff'></AtIcon>
                <Text>刷新</Text>
              </View>
          </View>
      </View>
    );
  }
}
