import Taro, { Component } from "@tarojs/taro";
import { View, Text } from "@tarojs/components";
import { AtAvatar, AtIcon, AtButton } from "taro-ui";
import "./index.scss";

export default class Index extends Component {
  config = {
    navigationBarTitleText: "消息",
  };

  constructor() {
    super(...arguments);
    this.state = {
      currentTab: 0,
      switch: false,
      list: [],
    };
  }

  onTabClick() {}

  onSubmit() {
    // 结算
    Taro.navigateTo({
      url: "/pages/order/success/index",
    });
  }

  render() {
    return (
      <View className="page-user-msg-detail">
        <View className="at-row">
          <View className="at-col c0">
            <View className="at-row list-item">
              <View className="at-col list-item-box">
                <View className="at-row at-row__align--center">
                  <View className="at-col at-col-2 user-img">
                    <AtAvatar
                      circle
                      image="https://jdc.jd.com/img/200"
                    ></AtAvatar>
                  </View>
                  <View className="at-col at-col-10">
                    <View className="at-row">
                      <View className="at-col express-name">
                        <Text>中通快递</Text>
                      </View>
                    </View>
                    <View className="at-row">
                      <View className="at-col order-number">
                        <Text>单号：15235794135</Text>
                      </View>
                    </View>
                  </View>
                </View>
              </View>
            </View>

            <View className="at-row list-item">
              <View className="at-col list-item-box">
                <View className="at-row at-row__align--center">
                  <View className="at-col at-col-2"></View>
                  <View className="at-col at-col-1" style="margin:0 5PX">
                    <Text className="cicle-shou">收</Text>
                  </View>
                  <View className="at-col">
                    <View className="at-row">
                      <View className="at-col goods-name">
                        <Text className="express-addres">
                          收货地址：北京市 昌平区 回龙观 **街道***大厦5层505
                        </Text>
                      </View>
                    </View>
                  </View>
                </View>
                <View
                  className="at-row at-row__align--center"
                  style="margin-top:20PX;"
                >
                  <View className="at-col at-col-2 express-date">
                    <Text>6月28日 17:53</Text>
                  </View>
                  <View className="at-col at-col-1" style="margin:0 5PX">
                    <AtIcon
                      value="check-circle"
                      size="24"
                      color="#17A8E6"
                    ></AtIcon>
                  </View>
                  <View className="at-col">
                    <View className="at-row at-row__align--center">
                      <View className="at-col msg-status">
                        <Text>已签收</Text>
                      </View>
                    </View>
                    <View className="at-row">
                      <View className="at-col goods-name">
                        <Text className="express-addres">
                          签收人：本人签收 投递员**** 电话：15022335566
                        </Text>
                      </View>
                    </View>
                  </View>
                </View>
              </View>
            </View>
          </View>
        </View>
      </View>
    );
  }
}
