import Taro, { Component } from "@tarojs/taro";
import { View, Text } from "@tarojs/components";
import { AtAvatar, AtIcon, AtTabs, AtTabsPane } from "taro-ui";
import { connect } from "@tarojs/redux";
import { $Req } from "@utils";
import "./index.scss";

@connect(
  ({ userModel }) => ({
    userModel,
  }),
  (dispatch) => ({
    userAction: dispatch.userModel,
  })
)
export default class Index extends Component {
  config = {
    navigationBarTitleText: "分销订单",
  };

  constructor() {
    super(...arguments);
    this.state = {
      id: '',
      list: []
    };
  }

  componentDidMount() {
    const { id } = this.$router.params;

    this.setState({
      id,
    }, () => {
      this.getDistributionOrderList();
    })


  }

  // 我的分销商
  async getDistributionOrderList() {
    const {
      userModel: { userInfo },
    } = this.props;
    const { id } = this.state;
    const { data } = await $Req({
      type: "post",
      url: "/distribution/getDistributionOrderList",
      data: {
        length: 100000,
        start: 0,
        userId: id
      },
    });
    if (!data) return;
    if(data.data.length==0){
      Taro.showToast({
        title:'暂无数据',
        icon: 'none',
				duration: 3000
      })
    }
    this.setState({
      list: data.data,
    });
  }

  render() {
    const { list } = this.state;
    return (
      <View className="page-user-relate">
        {list.map((item) => (
          <View className='fx-order-list'>
            <View className='left'>{item.distributionName}
        {item.recommendAward?<View className='red-font'>+￥{item.recommendAward}推荐奖</View>:null}
            </View>
            <View className='right'>{item.settleTime}</View>
          </View>

        ))}

      </View>
    );
  }
}
