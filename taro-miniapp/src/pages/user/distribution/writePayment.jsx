import Taro, { Component } from "@tarojs/taro";
import { View, Text, Picker } from "@tarojs/components";
import { AtInput, AtImagePicker, AtButton, AtToast } from "taro-ui";
import { connect } from "@tarojs/redux";
import { $Req, fileRequest } from "@utils";
import "./index.scss";

const go_img = 'https://bai-tong-fen-xiao.oss-cn-beijing.aliyuncs.com/imgs/user/go.png';


@connect(
  ({ userModel }) => ({
    userModel,
  }),
  (dispatch) => ({
    userAction: dispatch.userModel,
  })
)
export default class Index extends Component {
  config = {
    navigationBarTitleText: "填写付款信息",
  };

  constructor() {
    super(...arguments);
    this.state = {
      list: [],
      value: '',
      selector: [],
      bank: '请选择银行',
      bankId: '',

      files:[],
      uploadUrl: [],
      id: '',
      hintFlag: false,
      hintTxt: ''
    };
  }
  onPickerChange = e => {
    this.setState({
      selectorChecked: this.state.selector[e.detail.value]
    })
  }
  componentDidMount() {
    const { id } = this.$router.params;

    this.setState({
      id,
    },() => {
      this.getBankList();
    })
  }
  async getBankList() {

    let { code, data } = await $Req({
      type: "get",
      url: "/indexUser/getBankInfo",
      data: {}
    });
    if (code == 200) {
      this.setState({
        selector: data
      })
    }
  }
  async submitPayEvidence() {
    const { bank, bankId, value, uploadUrl,id} = this.state;
    if( !bankId) {
      this.setState({
        hintFlag: true,
        hintTxt: '请先选择银行'
      }) 
      return false;
    }
    if( !value.trim() ) {
      this.setState({
        hintFlag: true,
        hintTxt: '银行卡号不能为空'
      }) 
      return false;
    }

    if( uploadUrl.length==0 ) {
      this.setState({
        hintFlag: true,
        hintTxt: '付款凭证不能为空'
      })
       return false;
    } 


    let { code, data } = await $Req({
      type: "post",
      url: "/distribution/submitPayEvidence",
      data: {
        bankCardNum: value, 
        bankId,
        id,
        receiptUrl: uploadUrl
      },
    });
    console.log(data);
    if( code == 200 ) {
      Taro.showToast({
        title: '提交线下支付证明成功',
        icon: "none",
        success:() => {
          Taro.redirectTo({
            url: `/pages/user/distribution/nonPayment`,
          });
        }
      })
  
    }

  }

  onFileChange(files, operationType,delIdx) {
    if (operationType == 'remove') {
      console.log(delIdx);
        let filesArr =  this.state.files;
        filesArr.splice(delIdx,1);
        this.setState({
          files: filesArr
        })
        return false;
      }

    
    this.uploadImg(files[0].url, 1);
    if(files.length>1) {
      files = [files[1]];
    }
    console.log(files)
    this.setState({
      files 
    })
  }
  async uploadImg(imgUrl) {
    let { code, message, data } = await fileRequest({
      type: "post",
      url: "/file/uploadFile",
      data: imgUrl,
    });
    data = JSON.parse(data);
    if (data.code == 200) {
      //this.state.uploadUrl.push(data.data)
      this.setState({
        uploadUrl: data.data
      })
    } else {
      Taro.showToast({
        title: data.message,
        icon: "none"
      })
    }

  }
  handleChange(e) {
    this.setState({
      value: e
    })
  }
  onImageClick(){

  }
  render() {
    const {hintFlag, hintTxt} = this.state;
    return (
      <View className="page-user-relate">
        <Picker 
          mode="selector"
          range={this.state.selector}
          rangeKey={"bankName"}
          onChange={({ detail: { value } }) => {
            this.setState({
              bank: this.state.selector[value].bankName,
              bankId: this.state.selector[value].bankId 
            })
          }}>
          <View className='yhk-item'>
              <View className="title">
                  <View>银行</View>
                  <View style="margin-left:52PX;font-size: 16PX;color:#999;">{this.state.bank}</View>
              </View>
              <View>
                 <Image style="width:16PX;height:16PX;" src={go_img} />
              </View>  
          </View>
        </Picker>
        <AtInput
          title='卡号'
          type='number'
          value={this.state.value}
          onChange={this.handleChange.bind(this)}
          placeholder='请输入卡号'
          required={true} />
        <View class='sc-tit'>上传凭证</View>  
        <AtImagePicker
          length={3}
          count={1}
          files={this.state.files}
          onChange={this.onFileChange.bind(this)}
          onImageClick={this.onImageClick.bind(this)}
        />

        <View className='submitBtn'>
          <AtButton type='primary' onClick={this.submitPayEvidence.bind(this)} circle={true}>提交</AtButton>
        </View>
        <AtToast isOpened={hintFlag} text={hintTxt} onClose={()=>{
          this.setState({
            hintFlag: false,
            hintTxt: '',
          })
        }}></AtToast>
      </View>
    );
  }
}
