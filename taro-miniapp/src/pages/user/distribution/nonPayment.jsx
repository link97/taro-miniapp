import Taro, { Component } from "@tarojs/taro";
import { View, Text } from "@tarojs/components";
import { AtActionSheet, AtActionSheetItem, AtToast   } from "taro-ui";
import { connect } from "@tarojs/redux";
import { $Req } from "@utils";
import "./index.scss";

const go_img = 'https://bai-tong-fen-xiao.oss-cn-beijing.aliyuncs.com/imgs/user/go.png';


@connect(
  ({ userModel }) => ({
    userModel,
  }),
  (dispatch) => ({
    userAction: dispatch.userModel,
  })
)
export default class Index extends Component {
  config = {
    navigationBarTitleText: "待付款订单",
  };

  constructor() {
    super(...arguments);
    this.state = {
      list: [],
      payTypeFlag: false,
      currentId: '',
      hintFlag: false,
      hintTxt: ''

    };
  }

  componentDidShow() {
    this.getReadyPayOrderList();
  }
  //
  async getReadyPayOrderList() {
    // 
    let { code, data } = await $Req({
      type: "get",
      url: "/distribution/getReadyPayOrderList",
      data: {
        start: 1,
        length: 999999,
      },
    });
    console.log(data);
    this.setState({
      list: data.data
    })
  }
  handleClick(val,id) {
    console.log(id)
    if(val == 1) {
        this.submitPayEvidence();
    } else if(val == 2) {
      Taro.navigateTo({
        url: `/pages/user/distribution/writePayment?id=${this.state.currentId}`,
        success:() => {
          this.setState({
            payTypeFlag: false
          })
        }
      });
    }
  }
  async submitPayEvidence() {

    let { code, data } = await $Req({
      type: "post",
      url: "/distribution/submitPayEvidence",
      data: {
        id: this.state.currentId 
      },
    });
    if( code == 200 ) {
      this.setState({
        hintFlag:true,
        payTypeFlag: false,
        hintTxt: '支付成功'
      })
    }
  }

  render() {
    const { list, payTypeFlag, hintFlag, hintTxt} = this.state;
    return (
      <View className="page-user-relate">
        <View className='non-pay-list'>
          {
            list.map((item, index) => (
              <View className='non-pay-item'>
                <View className='non-pay-title'>{item.distributionName}</View>
                <View className='non-pay-des'>订单编号：{item.orderNo}</View>
                <View className='non-pay-des'>下单时间：{item.createTime}</View>
                <View style="display: flex;justify-content: space-between;margin-top:6PX;">
                  <View></View>
                  <View class='pay-btn' onClick={() => {
                    if(item.orderStatus == 20000 ) {
                      this.setState({
                        payTypeFlag: true,
                        currentId: item.id
                      })
                    }
                  }}>{item.orderStatus * 1 == 20000 ? '立即付款' : '请等待平台核对订单款项'}</View>
                </View>
              </View>
            ))
          }
        </View>

        <AtActionSheet isOpened={payTypeFlag}>
          <AtActionSheetItem onClick={ this.handleClick.bind(this,1) }>
             现金支付
          </AtActionSheetItem>
          <AtActionSheetItem onClick={ this.handleClick.bind(this,2) }>
            银行卡支付
          </AtActionSheetItem>
        </AtActionSheet>
        <AtToast isOpened={hintFlag} text={hintTxt} onClose={() => {
          this.setState({
            hintFlag: false
          })
        }}></AtToast>
      </View>
    );
  }
}
