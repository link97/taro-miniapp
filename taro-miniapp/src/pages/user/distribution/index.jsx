import Taro, { Component } from "@tarojs/taro";
import { View, Text } from "@tarojs/components";
import { AtAvatar, AtIcon, AtTabs, AtTabsPane } from "taro-ui";
import { connect } from "@tarojs/redux";
import { $Req } from "@utils";
import "./index.scss";

const tag_1 = 'https://bai-tong-fen-xiao.oss-cn-beijing.aliyuncs.com/imgs/user/vip-icon.png';
const tag_2 = 'https://bai-tong-fen-xiao.oss-cn-beijing.aliyuncs.com/imgs/user/fx-icon.png';
const tag_3 = 'https://bai-tong-fen-xiao.oss-cn-beijing.aliyuncs.com/imgs/user/no-vip-icon.png';

@connect(
  ({ userModel }) => ({
    userModel,
  }),
  (dispatch) => ({
    userAction: dispatch.userModel,
  })
)
export default class Index extends Component {
  config = {
    navigationBarTitleText: "分销关系链",
  };

  constructor() {
    super(...arguments);
    this.state = {
      currentTab: 0,
      list: [],
      level:1,
      otherData:{}
    };
  }

  componentDidMount() {
    // 获取一级分销商
    this.getMyRecommend();
  }

  // 我的分销商
  async getMyRecommend() {
    const {
      userModel: { userInfo },
    } = this.props;
    const {level} = this.state;
    const { data } = await $Req({
      type: "get",
      url: "/distribution/getShareRelation",
      data: {
        shareLevel:level
      },
    });
    if (!data) return;
    console.log(data.data,"+++++")
    this.setState({
      list: data.data,
      otherData: data.otherData || {}
    });
  }

  onTabClick() { }

  onSubmit() {
    // 结算
    Taro.navigateTo({
      url: "/pages/order/success/index",
    });
  }
  tabHandleClick(v) {
      this.setState({
        currentTab: v,
        list:[],
        level: v*1+1
      },() => {
        this.getMyRecommend();
      });
  }
  listHtml(item) {
    return (
      <View className="user-list" data-id={item.userId} onClick={({currentTarget:{dataset:{id}}}) => {
        Taro.navigateTo({
          url: `/pages/user/distribution/orderList?id=${id}`,
        });
        
      }}>
        <View className="user-info">
          <AtAvatar circle image={item.userPhoto?item.userPhoto:"https://jdc.jd.com/img/200"}></AtAvatar>
          <View>
            <View className='nickName'>{item.userName}</View>
            <View className='des' style="display:flex;align-items:center;">
              {item.level ==0?<Image src={tag_3}></Image>:null}
              {item.level !=0 && item.level !=1?<Image src={tag_2}></Image>:null}
              {item.level ==1?<Image src={tag_1}></Image>:null}
              
              <Text style="line-height:1;">{item.levelName}</Text>
            </View>
          </View>
        </View>
        <View className='user-right'>
          <View>分销产品{distributionOrderNum}</View>
          <View>{item.createTime}</View>
        </View>
      </View>
    )
  }
  render() {
    const tabList = [
      { title: "一级分销" },
      { title: "二级分销" }
    ];
    const {currentTab, list, otherData} = this.state;
    return (
      <View className="page-user-relate">
        <AtTabs
          current={currentTab}
          tabList={tabList}
          onClick={this.tabHandleClick.bind(this)}
        >
          <AtTabsPane current={currentTab} index={0}>
            {list.map((item) => (
              this.listHtml(item)
            ))}
          </AtTabsPane>

          <AtTabsPane current={currentTab} index={1}>
            {list.map((item) => (
              this.listHtml(item) 
            ))}
          </AtTabsPane>


        </AtTabs>
        {otherData.createTime?<View className='inviteWrap'>
          <View className='title'>
            我的邀请人
            </View>
          <View style="display:flex;align-items: center;justify-content: space-between;">
            <View className="user-info">
              <AtAvatar circle image={otherData.userPhoto?otherData.userPhoto:'https://jdc.jd.com/img/200'}></AtAvatar>
            <View className='nickName'>{otherData.userName}</View>
            </View>
            <View style="color:#999;font-size:12PX;">{otherData.createTime}</View>
          </View>
        </View>:null}
      </View>
    );
  }
}
