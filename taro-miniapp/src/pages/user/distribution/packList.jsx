import Taro, { Component } from "@tarojs/taro";
import { View, Text } from "@tarojs/components";
import { AtAvatar, AtIcon, AtTabs, AtTabsPane } from "taro-ui";
import { connect } from "@tarojs/redux";
import { $Req } from "@utils";
import "./index.scss";

const go_img = 'https://bai-tong-fen-xiao.oss-cn-beijing.aliyuncs.com/imgs/user/go.png';

const path_1 = 'https://bai-tong-fen-xiao.oss-cn-beijing.aliyuncs.com/imgs/user/path_1.png';
const path_2 = 'https://bai-tong-fen-xiao.oss-cn-beijing.aliyuncs.com/imgs/user/path_2.png';
const path_3 = 'https://bai-tong-fen-xiao.oss-cn-beijing.aliyuncs.com/imgs/user/path_3.png';

const path_active_1 = 'https://bai-tong-fen-xiao.oss-cn-beijing.aliyuncs.com/imgs/user/path_active_1.png';
const path_active_2 = 'https://bai-tong-fen-xiao.oss-cn-beijing.aliyuncs.com/imgs/user/path_active_2.png';
const path_active_3 = 'https://bai-tong-fen-xiao.oss-cn-beijing.aliyuncs.com/imgs/user/path_active_3.png';

const path_img = [path_1, path_2, path_3];
const path_active_img = [path_active_1, path_active_2, path_active_3];

@connect(
  ({ userModel }) => ({
    userModel,
  }),
  (dispatch) => ({
    userAction: dispatch.userModel,
  })
)
export default class Index extends Component {
  config = {
    navigationBarTitleText: "套餐包列表",
  };

  constructor(props) {
    super(...arguments);
    this.state = {
      ftGaojiList: [],
      ftZhongjiList: [],
      sfList: []
    };
  }

  componentDidShow() {
    this.getJf();
  }

  // 点击订单状态
  onOrderClick(type) {
    Taro.navigateTo({
      url: "/pages/user/order/index?type=" + type,
    });
  }

  async getJf() {
    // 
    let { code, data } = await $Req({
      type: "post",
      url: "/distribution/getMyBonusList?start=1&length=999999&packStatus=1",
    });
    this.setState({
      ftGaojiList: data.ftGaojiList,
      ftZhongjiList: data.ftZhongjiList,
      sfList: data.sfList
    })
  }



  render() {
    const { ftGaojiList, ftZhongjiList, sfList } = this.state;
    return (
      <View className="page-user-relate">
        <View className='level-title'>
          <View>购买的分销套餐包</View>
          <View className='fk-title' onClick={()=>{
            Taro.navigateTo({
              url: "/pages/user/distribution/nonPayment",
            });
          }}>待付款订单</View>
        </View>
        {
          sfList.map((item, index) => (
            <View className='level-item' onClick={()=>{
              Taro.navigateTo({
                url: `/pages/user/distribution/list?id=${item.id}`,
              });
            }}>
              <View className='item-title'>
                <View style="display:flex;">{item.distributionName} </View>
                <View><Image className='go-icon' src={go_img} /></View>
              </View>
              <View className='bonus-num'>订单号：{item.orderNo}</View>
              {/* <View className='bonus-num'>拼团分红：{item.bonusPresentation}</View> */}
              {/* <View className='item-progress'>
                <View>
                  当前进度：
                </View>
                <View className="progress">
                  {
                    item.progress.map((i, index) => (
                      <View className="p-item" style={{marginLeft: index>=1?'-7PX':''}}>
                        <Image mode="heightFix" src={i.processStatus * 1 == 0 ? path_img[index] : path_active_img[index]}></Image>
                        <View className='txt' style={{ color: i.processStatus * 1 == 0 ? '#999' : '#fff' }}>{i.processName}</View>
                      </View>
                    ))
                  }
                </View>
              </View> */}
              <View className='item-time'>{item.createTime}</View>
            </View>
          ))
        }
      </View>
    );
  }
}
