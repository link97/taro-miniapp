import Taro, { Component } from "@tarojs/taro";
import { View, Text } from "@tarojs/components";
import { connect } from "@tarojs/redux";
import { $Req } from "@utils";
import { AtButton, AtAvatar, AtIcon } from "taro-ui";
import "./index.scss";

@connect(
  ({ userModel }) => ({
    userModel,
  }),
  (dispatch) => ({
    userAction: dispatch.userModel,
  })
)
export default class Index extends Component {
  config = {
    navigationBarTitleText: "我的地址",
  };

  constructor(props) {
    super(...arguments);
    this.state = {
      addressList: [],
      selectAddressFlag: false
    };
  }

  componentDidShow() {
    this.getUserAddress();

    if (this.$router.params.getAddress) {
      this.setState({
        selectAddressFlag: true
      })
    }

  }

  // 获取地址列表
  async getUserAddress() {
    let {
      userModel: { userInfo },
      userAction,
    } = this.props;
    let { code, message, data } = await $Req({
      type: "get",
      url: "/indexUser/getShippingAddress",
      data: {
        userId: userInfo.userId
      },
    });
    if (!data) return;
    this.setState({
      addressList: data,
    });
  }

  render() {
    let {
      userModel: { userInfo },
      userAction,
    } = this.props;
    return (
      <View className="at-row page-address-index">
        <View className="at-col address">
          <View className="at-row">
            <View className="at-col c0">
              {this.state.addressList.map((item) => {
                return (
                  <View
                    className="at-row at-row at-row__justify--between at-row__align--center address-list-item"
                    key={item.userId}
                  >
                    <View className="at-col at-col-2" style="padding:10PX">
                      <AtAvatar
                        circle
                        image={item.userPhoto||'https://jdc.jd.com/img/200'}
                      ></AtAvatar>
                    </View>
                    <View className="at-col at-col-8" style="padding:10PX" 
                      onClick={() => {
                        if (this.state.selectAddressFlag) {
                          Taro.navigateBack(-1);
                        }

                        Taro.setStorageSync("address", item)
                  
                      }}
                    >
                      <View className="at-row at-row__align--center">
                        <View className="at-col">
                          <Text className="add-name">{item.receiverName}</Text>
                          <Text className="add-tel">{item.receiverMobile}</Text>
                          {item.defaultFlag === 1 ? (
                            <Text className="add-status">默认</Text>
                          ) : null}
                        </View>
                      </View>
                      <View className="at-row" style="margin-top:3PX;">
                        <View className="at-col">
                          <Text className="add-describe">
                            {item.receiverDistrict}
                            {item.receiverAddress}
                          </Text>
                        </View>
                      </View>
                    </View>
                    <View
                      className="at-col at-col-2"
                      style="text-align:right;padding:10PX"
                      onClick={() => {
                        userAction.setStatus({
                          addressDetail: item,
                        });
                        Taro.navigateTo({
                          url: "/pages/user/address/add",
                        });
                      }}
                    >
                      <AtIcon value="edit" size="20" color="#4D4D4D"></AtIcon>
                    </View>
                  </View>
                );
              })}
              <View className="at-row address-add-btn">
                <View className="at-col">
                  <AtButton
                    circle
                    type="primary"
                    className="btn-submit"
                    onClick={() => {
                      userAction.setStatus({
                        addressDetail: null,
                      });
                      Taro.navigateTo({
                        url: "/pages/user/address/add",
                      });
                    }}
                  >
                    添加收货地址
                  </AtButton>
                </View>
              </View>
            </View>
          </View>
        </View>
      </View>
    );
  }
}
