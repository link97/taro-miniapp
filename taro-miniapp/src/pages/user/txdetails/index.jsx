import Taro, { Component } from "@tarojs/taro";
import { View, Text } from "@tarojs/components";
import { connect } from "@tarojs/redux";
import { $Req } from "@utils";
import "./index.scss";

@connect(
  ({ userModel }) => ({
    userModel,
  }),
  (dispatch) => ({
    userAction: dispatch.userModel,
  })
)
export default class Index extends Component {
  config = {
    navigationBarTitleText: "提现详情",
  };

  constructor() {
    super(...arguments);
    this.state = {
      list1: [],
    };
  }

  componentDidMount() {
    // 获取一级分销商
    this.getMyRecommend();
  }

  // 我的分销商
  async getMyRecommend() {
    const {
      userModel: { userInfo },
    } = this.props;

    const { data } = await $Req({
      type: "post",
      url: "/cashOutOrder/getCashOutOrderList",
      data: {
        start: 1,
        length: 999999,
        userId: userInfo.userId
      },
    });
    if (!data) return;
    this.setState({
      list1: data.data,
    });
  }

  render() {
    return (
      <View className="page-user-relate">
        {this.state.list1.map((item) => (
          <View className="list-wrap">
            <View className="flex-between">
              <View className="title">{item.bandUserName ? item.bandUserName : item.userName}({!item.cashOutType ? "银行卡" : "零钱"})</View>
              <View className="money">￥{item.ammount}</View>
            </View>
            <View className="flex-between">
              <View className="time">{item.createTime}</View>
              <View className={[(item.status != 1 && item.status != 0) ? 'sucess' : 'default']}>
                {item.status == 0 ? "待提现" : item.status == 1 ? "提现中" : "已提现"}
              </View>
            </View>
          </View>
        ))}
      </View>
    );
  }
}
