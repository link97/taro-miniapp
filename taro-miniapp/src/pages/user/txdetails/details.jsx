import Taro, { Component } from "@tarojs/taro";
import { View, Text } from "@tarojs/components";
import { connect } from "@tarojs/redux";
import { AtTimeline, AtButton } from 'taro-ui'
import "./details.scss";

@connect(
  ({ userModel }) => ({
    userModel,
  }),
  (dispatch) => ({
    userAction: dispatch.userModel,
  })
)

export default class Index extends Component {
  config = {
    navigationBarTitleText: "提现进度",
  };

  constructor() {
    this.state = {
      details: {}
    };
  }

  componentDidShow() {
    console.log(JSON.parse(this.$router.params.details))
    this.setState({
      details: JSON.parse(this.$router.params.details)
    })
  }

  render() {
    return (
      <View className="page-user-relate" style="padding: 20px">
        <View style="border-bottom:1px solid #eee; padding-bottom: 20px;">
          <AtTimeline 
            items={[
              { title: '发起提现申请', icon: 'check-circle' }, 
              { title: '提现到' + (this.state.details.cashOutType == 1 ? "零钱" : "银行卡") , icon: 'check-circle' }, 
              { title: '到账成功', icon: this.state.details.status == 2 ? "check-circle'" : ''}
            ]}
          >
          </AtTimeline>
        </View>
        <View style="margin-top: 20px">
            <View className="at-row">
              <Text>提现金额</Text>
                <Text>￥{this.state.details.ammount || 0}元</Text>
            </View>
            {
              !this.state.details.cashOutType ?
                <View className="at-row">
                  <Text>到账银行卡</Text>
                  <Text>{this.state.details.bandName} ({this.state.details.bandNum.substr(this.state.details.bandNum.length - 4, this.state.details.bandNum.length)})</Text>
                </View>
              :
              null
            }
            
            <View className="at-row">
              <Text>提现形式</Text>
          <Text>{this.state.details.cashOutLineType == 1 ? "线下" : "线上"}</Text>
            </View>
        </View>
        <View style="margin-top: 50px">
          <AtButton circle type='primary' size='small' onClick={() => {
            Taro.navigateBack(-1);
          }}>完成</AtButton>
        </View>
      </View>
    );
  }
}
