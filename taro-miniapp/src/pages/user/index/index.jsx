import Taro, { Component } from "@tarojs/taro";
import { View, Text } from "@tarojs/components";
import { connect } from "@tarojs/redux";
import {
  AtList,
  AtListItem,
  AtAvatar,
  AtIcon,
  AtBadge,
  AtButton,
} from "taro-ui";
import { $Req } from "@utils";
import BaseComponent from "@components/BaseComponent";
import "./index.scss";

const cj =
  "https://bai-tong-fen-xiao.oss-cn-beijing.aliyuncs.com/shopping/icon/cj.png";
const zj =
  "https://bai-tong-fen-xiao.oss-cn-beijing.aliyuncs.com/shopping/icon/zj.png";
const gj =
  "https://bai-tong-fen-xiao.oss-cn-beijing.aliyuncs.com/shopping/icon/gj.png";
const cgj =
  "https://bai-tong-fen-xiao.oss-cn-beijing.aliyuncs.com/shopping/icon/cgj.png";


const tag_1 = 'https://bai-tong-fen-xiao.oss-cn-beijing.aliyuncs.com/imgs/user/vip-icon.png';
const tag_2 = 'https://bai-tong-fen-xiao.oss-cn-beijing.aliyuncs.com/imgs/user/fx-icon.png';
const tag_3 = 'https://bai-tong-fen-xiao.oss-cn-beijing.aliyuncs.com/imgs/user/no-vip-icon.png';

const orderIcon = require('@assets/imgs/user/order-icon.png')

@connect(
  ({ userModel }) => ({
    userModel,
  }),
  (dispatch) => ({
    userAction: dispatch.userModel,
  })
)
export default class User extends Component {
  config = {
    navigationBarTitleText: "我的",
  };

  constructor(props) {
    super(...arguments);

    this.state = {
      currentTab: 0,
      //分销等级ID
      distributionLevelId: 1,
      // 积分
      attachScore: 0,
      // 分红金额
      profitAmount: 0,
      user: {},
      msgTotal: 0,
    };
  }

  componentDidShow() {
    const { userAction } = this.props;

    this.getUserInfo();
    let token = Taro.getStorageSync("token");
    let u = Taro.getStorageSync("userInfo");

    if (!token || !u) {
      userAction.setStatus({
        userInfo: {},
      });
    }
  }

  // 获取消息数量
  async getChar() {
    let {
      userModel: { userInfo },
      userAction,
    } = this.props;

    let { code, data } = await $Req({
      type: "get",
      url: "/indexUser/getUserMessage",
      data: {
        userId: userInfo.userId,
      },
    });
    let total = data.filter((el) => el.status == 0);
    this.setState({
      msgTotal: total.length,
    });
  }

  async getUserInfo() {
    let { code, data } = await $Req({
      type: "post",
      url: "/user/getUserCenterInfo",
      data: {},
    });

    if (code == 200) {
      this.setState({
        user: data,
      });
      console.log("后台返回的用户信息：" + JSON.stringify(data));
      Taro.setStorageSync("level", data.level);
    }
  }

  // 点击列表
  onListItemClick(op) {
    const {
      userModel: { userInfo },
      userAction,
    } = this.props;

    // 整完加上
    if (!userInfo.userId) {
      userAction.setStatus({
        isOpen: true,
      });
      return;
    }

    switch (op) {
      case 1:
        Taro.navigateTo({
          url: "/pages/user/income/index",
        });
        break;
      case 2:
        Taro.navigateTo({
          url: "/pages/user/points/index",
        });
        break;
      case 3:
        this.onOrderClick(0);
        break;
      case 4:
        this.onOrderClick(1);
        break;
      case 5:
        this.onOrderClick(2);
        break;
      case 6:
        this.onOrderClick(3);
        break;
      case 7:
        Taro.navigateTo({
          url: "/pages/user/bonus/index",
        });
        break;
      case 8:
        Taro.navigateTo({
          url: "/pages/user/distribution/index",
        });
        break;
      case 9:
        Taro.navigateTo({
          url: "/pages/vip/index",
        });
        break;
      case 10:
        Taro.navigateTo({
          url: "/pages/user/identity/index",
        });
        break;
      case 11:
        Taro.navigateTo({
          url: "/pages/user/address/index",
        });
        break;
      case 12:
        Taro.navigateTo({
          url: "/pages/user/qrcode/index",
        });
        break;
      case 13:
        Taro.navigateTo({
          url: "/pages/user/msg/index/index",
        });
        break;
      case 14:
        Taro.navigateTo({
          url: "/pages/user/cards/index",
        });
        break;
      case 15:
        Taro.navigateTo({
          url: "/pages/user/distribution/list",
        });
        break;
      case 16:
        Taro.navigateTo({
          url: "/pages/user/distribution/packList",
        });
        break;
      default:
        break;
    }
  }

  // 点击订单状态
  onOrderClick(type) {
    Taro.navigateTo({
      url: "/pages/order/index/index?type=" + type,
    });
  }

  // 设置用户信息
  getUserDom() {
    const {
      userModel: { userInfo },
      userAction,
    } = this.props;

    let { user } = this.state;

    if (!userInfo.userId) {
      return (
        <View className="user-info">
          <View
            className="at-row at-row__align--center r1"
            style="margin-top:70PX;"
            onClick={() => {
              Taro.navigateTo({
                url: "/pages/index/welcome",
              });
            }}
          >
            <View className="at-col at-col-3 c11">
              <image
                class="user-logo"
                src={
                  "https://bai-tong-fen-xiao.oss-cn-beijing.aliyuncs.com/shopping/user/defaultAvatar.png"
                }
              />
            </View>
            <View className="at-col at-col-9 c12">
              <Text className="name">点击登录账户</Text>
            </View>
          </View>
        </View>
      );
    }
    return (
      <View className="user-info">
        <View
          className="at-row at-row__align--center r1"
          style="margin-top:70PX;"
        >
          <View
            className="at-col at-col-3 c11"
            onClick={() => {
              Taro.removeStorageSync("token");
              Taro.removeStorageSync("userInfo");
              Taro.removeStorageSync("userPhone");

              let { userAction } = this.props;
              this.setState({
                currentTab: 0,
                distributionLevelId: 1,
                attachScore: 0,
                profitAmount: 0,
                user: {},
                msgTotal: 0,
              })
              userAction.setStatus({
                userInfo: {},
              });

            }}
          >
            <image
              class="user-logo loginSuccess"
              src={
                user.userPhoto
                  ? user.userPhoto
                  : "https://bai-tong-fen-xiao.oss-cn-beijing.aliyuncs.com/shopping/user/defaultAvatar.png"
              }
            />
          </View>
          <View className="at-col at-col-7 c12" onClick={() => {
            Taro.navigateTo({
              url: "/pages/user/info/index",
            });
          }}>
            <View className="at-row at-row__align--center row-name">
              <View>
                <Text className="name">{user.name}</Text>
              </View>
            </View>
              {user.level == 0 ? <View className='des'><Image src={tag_3}></Image>{user.levelName}</View> : null}
              {user.level != 0 && user.level != 1 ? <View className='des'><Image src={tag_2}></Image>{user.levelName}</View> : null}
              {user.level == 1 ? <View className='des'><Image src={tag_1}></Image>{user.levelName}</View> : null}
            <View
              className="at-row at-row__align--center row-tel"
              style="margin-top:3PX;"
            >
              <View className="at-col">
                <image
                  style="width:12PX;height:12PX;vertical-align: middle;"
                  src={
                    "https://bai-tong-fen-xiao.oss-cn-beijing.aliyuncs.com" +
                    "/shopping/user/tel.png"
                  }
                />
                <Text style="font-size:12PX">
                  {user.userPhone || "未绑定手机号"}
                </Text>
                <Text style="font-size:12PX">
                  （{user.haveBouns ? "享受提成" : "不享受提成"}）
                </Text>
              </View>
            </View>
          </View>
          <View className="at-col at-col-1" onClick={() => {
            Taro.navigateTo({
              url: "/pages/user/info/index",
            });
          }}>
            <AtIcon value="chevron-right" size="22" color="#fff"></AtIcon>
          </View>
        </View>
      </View>
    );
  }

  // 获取收益
  getInComeDom() {
    const {
      userModel: { userInfo },
      userAction,
    } = this.props;

    let { user } = this.state;

    return (
      <View className="user-income">
        <View className="at-row at-row__justify--around at-row__align--center r1">
          <View
            className="at-col at-col-3 c11"
            onClick={() => {
              this.onListItemClick(1);
            }}
          >
            <View className="at-row at-row__justify--center at-row__align--center r111">
              <image
                src={
                  "https://bai-tong-fen-xiao.oss-cn-beijing.aliyuncs.com" +
                  "/shopping/user/wallet.png"
                }
              />
              <Text className='title'>收益</Text>
            </View>
            <View className="at-row at-row__justify--center at-row__align--center r112">
              <Text>{user.benefit}</Text>
            </View>
          </View>
          <View
            className="at-col at-col-3 c12"
            onClick={() => {
              this.onListItemClick(2);
            }}
          >
            <View className="at-row at-row__justify--center at-row__align--center r121">
              <image
                src={
                  "https://bai-tong-fen-xiao.oss-cn-beijing.aliyuncs.com" +
                  "/shopping/user/integral.png"
                }
              />
              <Text className='title'>积分</Text>
            </View>
            <View className="at-row at-row__justify--center at-row__align--center r122">
              <Text>{user.integral}</Text>
            </View>
          </View>

          <View
            className="at-col at-col-3 c12"
            onClick={() => {
              this.onListItemClick(15);
            }}
          >
            <View className="at-row at-row__justify--center at-row__align--center r121 r123">
              <image
                src={orderIcon}
              />
              <Text className='title'>订单</Text>
            </View>
            <View className="at-row at-row__justify--center at-row__align--center r122">
              <Text>{user.distributionNum}</Text>
            </View>
          </View>

          <View
            className="at-col at-col-3 c12"
            onClick={() => {
              this.onListItemClick(16);
            }}
          >
            <View className="at-row at-row__justify--center at-row__align--center r121 r124">
              <image
                src={"https://bai-tong-fen-xiao.oss-cn-beijing.aliyuncs.com/test/20210329/20210329133521-text.png"}
              />
              <Text className='title'>套餐包</Text>
            </View>
            <View className="at-row at-row__justify--center at-row__align--center r122">
              <Text>{user.distributionPackNum}</Text>
            </View>
          </View>

        </View>
      </View>
    );
  }

  // 获取订单
  getOrderDom() {
    let { user } = this.state;
    return (
      <View className="user-order">
        <View className="my-order">我的订单</View>
        <View className="at-row at-row__justify--around at-row__align--center r1">
          <View
            className="at-col at-col-2"
            onClick={() => {
              this.onListItemClick(3);
            }}
          >
            <View className="at-row at-row__justify--around">
              <AtBadge value={user.allOrderNum} maxValue={99}>
                <image
                  src={
                    "https://bai-tong-fen-xiao.oss-cn-beijing.aliyuncs.com" +
                    "/shopping/user/order.png"
                  }
                />
              </AtBadge>
            </View>
            <View className="at-row at-row__justify--around text">
              <Text>全部订单</Text>
            </View>
          </View>
          <View
            className="at-col at-col-2"
            onClick={() => {
              this.onListItemClick(4);
            }}
          >
            <View className="at-row at-row__justify--around">
              <AtBadge value={user.readyPayOrderNum} maxValue={99}>
                <image
                  src={
                    "https://bai-tong-fen-xiao.oss-cn-beijing.aliyuncs.com/shopping/user/no-pay.png"
                  }
                />
              </AtBadge>
            </View>
            <View className="at-row at-row__justify--around text">
              <Text>待付款</Text>
            </View>
          </View>
          <View
            className="at-col at-col-2"
            onClick={() => {
              this.onListItemClick(5);
            }}
          >
            <View className="at-row at-row__justify--around">
              <AtBadge value={user.readyReciveOrderNum} maxValue={99}>
                <image
                  src={
                    "https://bai-tong-fen-xiao.oss-cn-beijing.aliyuncs.com/shopping/user/no-sh.png"
                  }
                />
              </AtBadge>
            </View>
            <View className="at-row at-row__justify--around text">
              <Text>待收货</Text>
            </View>
          </View>
          <View
            className="at-col at-col-2"
            onClick={() => {
              this.onListItemClick(6);
            }}
          >
            <View className="at-row at-row__justify--around">
              <AtBadge value={user.closeOrderNum} maxValue={99}>
                <image
                  src={
                    "https://bai-tong-fen-xiao.oss-cn-beijing.aliyuncs.com/shopping/user/ok.png"
                  }
                />
              </AtBadge>
            </View>
            <View className="at-row at-row__justify--around text">
              <Text>已完成</Text>
            </View>
          </View>
        </View>
      </View>
    );
  }

  render() {
    const { unMessageNum } = this.state.user;
    return (
      <BaseComponent>
        <View className="page-user-index">
          {this.getUserDom()}
          {this.getInComeDom()}
          {this.getOrderDom()}
          <AtList hasBorder={false}>
            <AtListItem
              title="我的拼团关系链"
              arrow="right"
              onClick={() => {
                this.onListItemClick(7);
              }}
            />
            <AtListItem
              title="我的分销关系链"
              arrow="right"
              onClick={() => {
                this.onListItemClick(8);
              }}
            />
            {/* <AtListItem
              title="我的互动分红"
              arrow="right"
              onClick={() => {
                this.onListItemClick(9);
              }}
            /> */}
            <AtListItem
              title="身份认证"
              arrow="right"
              onClick={() => {
                this.onListItemClick(10);
              }}
            />
            <AtListItem
              title="我的收货地址"
              arrow="right"
              onClick={() => {
                this.onListItemClick(11);
              }}
            />
            {/* <AtListItem
              title="我的二维码"
              arrow="right"
              onClick={() => {
                this.onListItemClick(12);
              }}
            /> */}
            {/* <AtListItem
                title="我的消息"
                arrow="right"
                onClick={() => {
                  this.onListItemClick(13);
                }}
              >
              </AtListItem> */}
            <AtListItem
              title="我的银行卡"
              arrow="right"
              onClick={() => {
                this.onListItemClick(14);
              }}
            />
          </AtList>
          <View className="message">
            <AtButton openType="contact">
              <View className="contact">
                <AtIcon value="user" size="20" color="#fff"></AtIcon>
                <Text>客服</Text>
              </View>
            </AtButton>
          </View>

          <View className="message msg" id="msg" style="bottom: 100px">
            <AtBadge value={unMessageNum} maxValue={99}>
              <AtButton
                onClick={() => {
                  this.onListItemClick(13);
                }}
              >
                <View className="contact">
                  <AtIcon value="message" size="20" color="#fff"></AtIcon>
                  <Text>消息</Text>
                </View>
              </AtButton>
            </AtBadge>
          </View>
        </View>
      </BaseComponent>
    );
  }
}
