import Taro, { Component } from "@tarojs/taro";
import { View, Text, Image } from "@tarojs/components";
import { connect } from "@tarojs/redux";
import { AtRate } from "taro-ui";
import { $Req } from "@utils";
import BaseComponent from "@components/BaseComponent";
import "./index.scss";

const cj = "https://bai-tong-fen-xiao.oss-cn-beijing.aliyuncs.com/shopping/cj.png";
const zj = "https://bai-tong-fen-xiao.oss-cn-beijing.aliyuncs.com/shopping/zj.png";
const gj = "https://bai-tong-fen-xiao.oss-cn-beijing.aliyuncs.com/shopping/gj.png";
const tcb = "https://bai-tong-fen-xiao.oss-cn-beijing.aliyuncs.com/test/20210323/20210323170419-wx8bb75d0aec3c7393.o6zAJs2tpVhGmPb5cdO4v4eKWDzo.lYOFMtMIFIq88e616c598f60a9b1aff56123fd6f0a59.png";

@connect(
  ({ userModel }) => ({
    userModel,
  }),
  (dispatch) => ({
    userAction: dispatch.userModel,
  })
)

export default class Index extends Component {
  config = {
    navigationBarTitleText: "分销商",
  };

  constructor(props) {
    super(...arguments);
    this.state = {
      fxList: []
    }
  }

  componentDidMount() {
    const {
      userModel: { userInfo }
    } = this.props;

    let shareId = this.$router.params.userId;
    console.log("分享人id:" + shareId);
    if (shareId && userInfo.userId != shareId) {
      console.log("分享人id 设置缓存！！！:" + shareId);
      Taro.setStorageSync("shareId", shareId);
    }
  }

  componentDidShow() {
    this.getFxList();
  }

  async getFxList() {
    let { data } = await $Req({
      type: "post",
      url: "/distribution/getDistributionGoodsList",
      data: {
        status: 1,
        start: 1,
        length: 100000
      }
    })

    console.log(data.data);

    data.data.forEach((el, index) => {
      if (el.levelFlag == "CHAO_GAO_JI") {
        el.imgUrl = cj;
      }

      if (el.levelFlag == "ZHONG_JI") {
        el.imgUrl = zj;
      }

      if (el.levelFlag == "GAO_JI") {
        el.imgUrl = gj;
      }

      // if (el.goodsType == 1) {
      //   el.des = `<p>“${el.distributionName}”套餐包包含：<br/></p>`
      //   if (el.packageGoodList) {
      //     el.packageGoodList.forEach((item, index) => {
      //       el.des += `<p>${index + 1}、${item.goodName || '暂无名称'}<br/></p>`
      //     })
      //   }
      // }
      

      if (el.levelFlag == "CHAO_GAO_JI") {
        data.data.splice(index, 1);
      }
      
    })

    // 特殊处理这个数据很垃圾
    // if (data.data[1]) {
    //   data.data[1].des = `<p>“${data.data[1].distributionName}”套餐包包含：<br/></p>`
    //   if (data.data[1].packageGoodList) {
    //     data.data[1].packageGoodList.forEach((item, index) => {
    //       data.data[1].des += `<p>${index + 1}、${item.goodName || '暂无名称'}<br/></p>`
    //     })
    //   }
      
    // }

    this.setState({
      fxList: data.data
    })
  }

  render() {
    return (
      <BaseComponent>
        <View className="at-col" style={{ padding: "10px", boxSizing: "border-box" }}>
          {
            this.state.fxList.map((el, index) => (
              <View onClick={
                () => {
                  Taro.navigateTo({
                    url: "/pages/vip/explain?productDetails=" + JSON.stringify(el)
                  })
                }
              }>
                {
                  el.imgUrl ? 
                    <image style="width: 100%; height: 100px; margin-bottom: 10px;" src={el.imgUrl} />
                  :
                  el.goodsType == 1
                  ?
                    <View  style="display:flex; align-items: center; width: 100%; line-height: 100px; margin-bottom: 10px; box-sizing:border-box; padding-left:20px; border-radius: 6px; color:#0374a5; font-size: 26px; background: linear-gradient(270deg, #7CC2E1 0%, #6DCDFF 100%);">
                      <image style="width: 60px; height: 60px;" src={tcb}></image>
                      <View style="padding-left:10px;color:#0374a5; font-size: 26px; ">{el.distributionName}</View>
                    </View>
                  :
                    <View style="width: 100%; line-height: 100px; margin-bottom: 10px; box-sizing:border-box; padding-left:30px; border-radius: 6px; color:#0374a5; font-size: 26px; background: linear-gradient(270deg, #7CC2E1 0%, #6DCDFF 100%);">{el.distributionName}</View>
                }
              </View>
            ))
          }
        </View>
      </BaseComponent>
    );
  }
}
