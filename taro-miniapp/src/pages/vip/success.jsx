import Taro, { Component } from '@tarojs/taro'
import { View, Text } from '@tarojs/components'
import { AtButton, AtIcon } from 'taro-ui'
import { connect } from "@tarojs/redux";
import './index.scss'

const imt_vip = 'https://bai-tong-fen-xiao.oss-cn-beijing.aliyuncs.com/imgs/vip/vip.png';
const imt_point = 'https://bai-tong-fen-xiao.oss-cn-beijing.aliyuncs.com/imgs/vip/point.png';
const imt_wallet = 'https://bai-tong-fen-xiao.oss-cn-beijing.aliyuncs.com/imgs/vip/wallet.png';

@connect(
  ({ userModel }) => ({
    userModel,
  }),
  (dispatch) => ({
    userAction: dispatch.userModel,
  })
)

export default
 class Index extends Component {
   config = {
     navigationBarTitleText: "购买成功",
   };

   constructor () {
    super(...arguments)
    this.state = {
      currentTab: 0,
      switch: false,
      productFxDetails: {}
    }
  }

  componentDidShow() {
    let details = JSON.parse(this.$router.params.productDetails);
    this.setState({
      productFxDetails: details
    })
  }

  onShareAppMessage(res) {
    const {
      userModel: { userInfo },
    } = this.props;

    if (res.from === 'button') {
      // 来自页面内转发按钮
      console.log(res.target)
    }
    return {
      title: '刚买了初级分销商！转发领福利',
      path: `/pages/vip/explain?productDetails=${JSON.stringify(this.state.productFxDetails)}&userId=${userInfo.userId}`
    }
  }

   render() {
     let { productFxDetails } = this.state;
      return (
        <View className="page-vip-success">
          <View className="at-row at-row__justify--center at-row__align--center vip-status">
            <View className="at-col at-col-2" style="padding-left:15PX;margin-right:10PX">
              <AtIcon
                value="check-circle"
                size="50"
                color="#fff"
              ></AtIcon>
            </View>
            <View className="at-col at-col-10">
                <View className="at-row">
                    <View className="at-col text1">
                      <Text>支付成功 您获得{productFxDetails.distributionName}身份</Text>
                    </View>
                </View>
                <View className="at-row" style="margin-top:5PX">
                    <View className="at-col text2">
                        <Text>您获得以下权益，可以到“我的”内查看</Text>
                    </View>
                </View>
            </View>
          </View>
          
          <View className="at-row">
            <View className="at-col c0">

              <View className="at-row" style="margin-top:15PX;">
                <View className="at-col color1">
                  <View className="at-row at-row__align--center">
                    <View className="at-col at-col-1">
                        <view class="color-box color11">
                            <image src={imt_vip} />
                        </view>
                    </View>
                    <View className="at-col at-col-8">
                      <Text >{productFxDetails.distributionName}会员身份</Text>
                    </View>
                  </View>
                </View>
              </View>

              <View className="at-row" style="margin-top:15PX;">
                <View className="at-col color2">
                  <View className="at-row at-row__align--center">
                    <View className="at-col at-col-1">
                        <view class="color-box color21">
                            <image src={imt_point} />
                        </view>
                    </View>
                    <View className="at-col at-col-8">
                      <Text >积分+{productFxDetails.bonusPoints} （可用于购买商品）</Text>
                    </View>
                    {/* <View className="at-col at-col-3" style="text-align:right;">
                        <Text onClick={
                          () => {
                            Taro.navigateTo({
                              url: "/pages/user/points/index"
                            })
                          }
                        }>查看详情</Text>
                    </View> */}
                  </View>
                </View>
              </View>

              {/* <View className="at-row" style="margin-top:15PX;">
                <View className="at-col color3">
                  <View className="at-row at-row__align--center">
                    <View className="at-col at-col-1">
                        <view class="color-box color31">
                            <image src={imt_wallet} />
                        </view>
                    </View>
                    <View className="at-col at-col-8">
                      <Text >钱包余额+{productFxDetails.bonusPresentation}元</Text>
                    </View>
                  </View>
                </View>
              </View> */}

              {/* <View className="at-row at-row__justify--between at-row__align--center" style="margin-top:30PX;">
                <View className="at-col" style="font-size: 18PX;font-weight: 400;">
                  <Text>已分享：</Text>
                  <Text style="color: #FAA023;">0</Text>
                </View>
              </View> */}

              <View className="at-row at-row__justify--between at-row__align--center" style="margin-top:6PX;">
                <View className="at-col" style="font-size: 13PX; margin-top:20px">
                  <Text>请将会员链接分享给好友、分享两人后可购买互助增分红</Text>
                </View>
              </View>

              <View className="at-row at-row__justify--between at-row__align--center" style="margin-top:50PX;">
                <View className="at-col">
                <AtButton circle type="primary" openType="share" className="btn-submit">
                分享给好友
                 </AtButton>
                </View>
              </View>

            </View>
          </View>
        </View>
      );
    }
 }
