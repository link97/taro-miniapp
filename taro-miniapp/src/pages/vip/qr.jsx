
import Taro, { Component } from '@tarojs/taro'
import { View, Text } from '@tarojs/components'
import { AtButton,AtIcon    } from 'taro-ui'
import './index.scss'

export default
 class Index extends Component {
   config = {
     navigationBarTitleText: "扫码支付",
   };

   constructor () {
    super(...arguments)
    this.state = {
      currentTab: 0,
      switch: false,
    };
  }

   onTabClick() {
   }

   render() {
      return (
        <View className="page-vip-qr">
          <View className="at-row">
            <View className="at-col c0">
              <View className="at-row at-row__justify--center at-row__align--center vip-status">
                <View className="at-col" style="text-align:center;">
                  <image
                    style="width:275PX;height:275PX;"
                    src={"https://bai-tong-fen-xiao.oss-cn-beijing.aliyuncs.com" + "/shopping/user/qrcode.png"}
                  />
                </View>
              </View>

              <View className="at-row">
                <View className="at-col" style="text-align: center;">
                  <Text>初级会员身份</Text>
                </View>
              </View>

              <View className="at-row" style="margin-top:50PX;">
                <View className="at-col">
                  <Text>
                    请扫描图中二维码支付费用，支付完成后请拨打客户电 话处理
                  </Text>
                </View>
              </View>

              <View className="at-row" style="margin-top:10PX;">
                <View className="at-col">
                  <Text>客服热线：（甲方提供电话）</Text>
                </View>
              </View>

              <View className="at-row at-row__justify--between at-row__align--center"
                style="margin-top:50PX;"
              >
                <View className="at-col">
                  <AtButton
                    circle
                    type="primary"
                    className="btn-submit"
                    onClick={() => {
                      this.onSubmit();
                    }}
                  >
                    拨打电话
                  </AtButton>
                </View>
              </View>
            </View>
          </View>
        </View>
      );
    }
 }
