import Taro, { Component } from "@tarojs/taro";
import { View, Text } from "@tarojs/components";
import { connect } from "@tarojs/redux";
import { $Req } from "@utils";
import { AtRate, AtButton, AtIcon } from "taro-ui";
import "./index.scss";

const cj =
  "https://bai-tong-fen-xiao.oss-cn-beijing.aliyuncs.com/shopping/icon/cj.png";
const zj =
  "https://bai-tong-fen-xiao.oss-cn-beijing.aliyuncs.com/shopping/icon/zj.png";
const gj =
  "https://bai-tong-fen-xiao.oss-cn-beijing.aliyuncs.com/shopping/icon/gj.png";
const cgj =
  "https://bai-tong-fen-xiao.oss-cn-beijing.aliyuncs.com/shopping/icon/cgj.png";

@connect(
  ({ userModel }) => ({
    userModel,
  }),
  (dispatch) => ({
    userAction: dispatch.userModel,
  })
)
export default class Index extends Component {
  config = {
    navigationBarTitleText: "会员身份",
  };

  constructor(props) {
    super(...arguments);
    this.state = {
      // rateVal: "2",
      fhData: {},
    };
  }

  onItemClick() {}

  componentDidMount() {
    this.getUserVip();
  }

  // 获取地址列表
  async getUserVip() {
    let {
      userModel: { userInfo },
    } = this.props;
    let { code, message, data } = await $Req({
      type: "post",
      url: "/distribution/getMyBonusList",
      data: {
        start: 1,
        length: 999999,
      },
    });
    if (!data) return;
    this.setState({
      fhData: data,
    });
  }

  render() {
    let {
      userModel: { userInfo },
    } = this.props;

    let { fhData } = this.state;

    return (
      <View className="at-row page-user-vip">
        <View className="at-col">
          <View
            className="at-row at-row__align--center line"
            onClick={() => {
              if (!fhData.level || fhData.level == 1) {
                Taro.showToast({
                  title: "非分销商会员不可进入",
                  icon: "none"
                })
                return false;
              }
              Taro.navigateTo({
                url: "/pages/vip/income?level=" + fhData.level,
              });
            }}
          >
            <View className="at-col left">
              <View className="at-row at-row__align--center">
                <View className="at-col levelName" style="padding:0;">
                  <Text className="name">{userInfo.username}</Text>
                  {fhData.level == "1" ? (
                    <Text className="level">普通会员</Text>
                  ) : null}
                  {fhData.level == "2" ||
                  fhData.level == "3" ||
                  fhData.level == "4" ||
                  fhData.level == "5" ? (
                    <image
                      mode="aspectFit"
                      src={
                        fhData.level == "2"
                          ? cj
                          : fhData.level == "3"
                          ? zj
                          : fhData.level == "4"
                          ? gj
                          : cgj
                      }
                    />
                  ) : null}
                </View>
              </View>
              {fhData.level ? (
                <View className="at-row at-row__align--center">
                  <View className="at-col" style="padding:0;">
                    <AtRate
                      size="15"
                      max={4}
                      value={fhData.level - 1}
                      style="color: red"
                    />
                  </View>
                </View>
              ) : null}
            </View>
            <View className="at-col right">
              <AtIcon value="chevron-right" size="24"></AtIcon>
            </View>
          </View>

          <View className="pd">
            <View className="title">订单</View>
            {this.state.fhData.sfList.map((item) => {
              return (
                <View
                  key={item.id}
                  className="at-row at-row__align--center line"
                >
                  <View className="at-col left">
                    <Text>
                      {item.levelFlagInit == "CHU_JI"
                        ? "初级分销商"
                        : item.levelFlagInit == "ZHONG_JI"
                        ? "中级分销商"
                        : item.levelFlagInit == "GAO_JI"
                        ? "高级分销商"
                        : item.levelFlagInit == "CHAO_GAO_JI"
                        ? "超高级"
                        : "无"}
                    </Text>
                    <View>
                      <Text className={"did"} style="color: #666">
                        分红：{item.currentBonus}
                      </Text>
                    </View>
                  </View>
                  <View className="at-col right">
                    <Text className={"doing"}>
                      {item.levelFlagQueue == "CHU_JI"
                        ? "初级分销商"
                        : item.levelFlagQueue == "ZHONG_JI"
                        ? "中级分销商"
                        : item.levelFlagQueue == "GAO_JI"
                        ? "高级分销商"
                        : item.levelFlagQueue == "CHAO_GAO_JI"
                        ? "超高级"
                        : "无"}
                    </Text>
                    <View>
                      <Text className={"did"}>{item.createTime}</Text>
                    </View>
                  </View>
                </View>
              );
            })}
          </View>

          {this.state.fhData.ftChujiList.length ? (
            <View className="pd">
              <View className="title">初级复投记录</View>
              {this.state.fhData.ftChujiList.map((item) => {
                return (
                  <View
                    key={item.id}
                    className="at-row at-row__align--center line"
                  >
                    <View className="at-col left">
                      <Text>
                        {item.levelFlagInit == "CHU_JI"
                          ? "初级分销商"
                          : item.levelFlagInit == "ZHONG_JI"
                          ? "中级分销商"
                          : item.levelFlagInit == "GAO_JI"
                          ? "高级分销商"
                          : item.levelFlagInit == "CHAO_GAO_JI"
                          ? "超高级"
                          : "无"}
                      </Text>
                      <View>
                        <Text className={"did"} style="color: #666">
                          分红：{item.currentBonus}
                        </Text>
                      </View>
                    </View>
                    <View className="at-col right">
                      <Text className={"doing"}>
                        {item.levelFlagQueue == "CHU_JI"
                          ? "初级分销商"
                          : item.levelFlagQueue == "ZHONG_JI"
                          ? "中级分销商"
                          : item.levelFlagQueue == "GAO_JI"
                          ? "高级分销商"
                          : item.levelFlagQueue == "CHAO_GAO_JI"
                          ? "超高级"
                          : "无"}
                      </Text>
                      <View>
                        <Text className={"did"}>{item.createTime}</Text>
                      </View>
                    </View>
                  </View>
                );
              })}
            </View>
          ) : null}

          {this.state.fhData.ftZhongjiList.length ? (
            <View className="pd">
              <View className="title">中级复投记录</View>
              {this.state.fhData.ftZhongjiList.map((item) => {
                return (
                  <View
                    key={item.id}
                    className="at-row at-row__align--center line"
                  >
                    <View className="at-col left">
                      <Text>
                        {item.levelFlagInit == "CHU_JI"
                          ? "初级分销商"
                          : item.levelFlagInit == "ZHONG_JI"
                          ? "中级分销商"
                          : item.levelFlagInit == "GAO_JI"
                          ? "高级分销商"
                          : item.levelFlagInit == "CHAO_GAO_JI"
                          ? "超高级"
                          : "无"}
                      </Text>
                      <View>
                        <Text className={"did"} style="color: #666">
                          分红：{item.currentBonus}
                        </Text>
                      </View>
                    </View>
                    <View className="at-col right">
                      <Text className={"doing"}>
                        {item.levelFlagQueue == "CHU_JI"
                          ? "初级分销商"
                          : item.levelFlagQueue == "ZHONG_JI"
                          ? "中级分销商"
                          : item.levelFlagQueue == "GAO_JI"
                          ? "高级分销商"
                          : item.levelFlagQueue == "CHAO_GAO_JI"
                          ? "超高级"
                          : "无"}
                      </Text>
                      <View>
                        <Text className={"did"}>{item.createTime}</Text>
                      </View>
                    </View>
                  </View>
                );
              })}
            </View>
          ) : null}

          {this.state.fhData.ftGaojiList.length ? (
            <View className="pd">
              <View className="title">高级复投记录</View>
              {this.state.fhData.ftGaojiList.map((item) => {
                return (
                  <View
                    key={item.id}
                    className="at-row at-row__align--center line"
                  >
                    <View className="at-col left">
                      <Text>
                        {item.levelFlagInit == "CHU_JI"
                          ? "初级分销商"
                          : item.levelFlagInit == "ZHONG_JI"
                          ? "中级分销商"
                          : item.levelFlagInit == "GAO_JI"
                          ? "高级分销商"
                          : item.levelFlagInit == "CHAO_GAO_JI"
                          ? "超高级"
                          : "无"}
                      </Text>
                      <View>
                        <Text className={"did"} style="color: #666">
                          分红：{item.currentBonus}
                        </Text>
                      </View>
                    </View>
                    <View className="at-col right">
                      <Text className={"doing"}>
                        {item.levelFlagQueue == "CHU_JI"
                          ? "初级分销商"
                          : item.levelFlagQueue == "ZHONG_JI"
                          ? "中级分销商"
                          : item.levelFlagQueue == "GAO_JI"
                          ? "高级分销商"
                          : item.levelFlagQueue == "CHAO_GAO_JI"
                          ? "超高级"
                          : "无"}
                      </Text>
                      <View>
                        <Text className={"did"}>{item.createTime}</Text>
                      </View>
                    </View>
                  </View>
                );
              })}
            </View>
          ) : null}

          {this.state.fhData.ftChaoGaojiList.length ? (
            <View className="pd">
              <View className="title">超高级复投记录</View>
              {this.state.fhData.ftChaoGaojiList.map((item) => {
                return (
                  <View
                    key={item.id}
                    className="at-row at-row__align--center line"
                  >
                    <View className="at-col left">
                      <Text>
                        {item.levelFlagInit == "CHU_JI"
                          ? "初级分销商"
                          : item.levelFlagInit == "ZHONG_JI"
                          ? "中级分销商"
                          : item.levelFlagInit == "GAO_JI"
                          ? "高级分销商"
                          : item.levelFlagInit == "CHAO_GAO_JI"
                          ? "超高级"
                          : "无"}
                      </Text>
                      <View>
                        <Text className={"did"} style="color: #666">
                          分红：{item.currentBonus}
                        </Text>
                      </View>
                    </View>
                    <View className="at-col right">
                      <Text className={"doing"}>
                        {item.levelFlagQueue == "CHU_JI"
                          ? "初级分销商"
                          : item.levelFlagQueue == "ZHONG_JI"
                          ? "中级分销商"
                          : item.levelFlagQueue == "GAO_JI"
                          ? "高级分销商"
                          : item.levelFlagQueue == "CHAO_GAO_JI"
                          ? "超高级"
                          : "无"}
                      </Text>
                      <View>
                        <Text className={"did"}>{item.createTime}</Text>
                      </View>
                    </View>
                  </View>
                );
              })}
            </View>
          ) : null}

          {/* <View
            className="at-row at-row__align--center line"
            onClick={() => {
              Taro.navigateTo({
                url: "/pages/vip/income",
              });
            }}
          >
            <View className="at-col left">
              <Text>高级会员(复投)</Text>
            </View>
            <View className="at-col right">
              <Text className="doing">进行中</Text>
            </View>
          </View>
          <View
            className="at-row at-row__align--center line"
            onClick={() => {
              Taro.navigateTo({
                url: "/pages/vip/explain",
              });
            }}
          >
            <View className="at-col left">
              <Text>高级会员</Text>
            </View>
            <View className="at-col right">
              <Text className="did">已结束</Text>
            </View>
          </View>
          <View
            className="at-row at-row__align--center line"
            onClick={() => {
              Taro.navigateTo({
                url: "/pages/vip/income",
              });
            }}
          >
            <View className="at-col left">
              <Text>中级会员（复投）</Text>
            </View>
            <View className="at-col right">
              <Text className="doing">进行中</Text>
            </View>
          </View>
          <View
            className="at-row at-row__align--center line"
            onClick={() => {
              Taro.navigateTo({
                url: "/pages/vip/income",
              });
            }}
          >
            <View className="at-col left">
              <Text>初级会员</Text>
            </View>
            <View className="at-col right">
              <Text className="did">已结束</Text>
            </View>
          </View> */}

          <View
            className="at-row at-row__align--center"
            style="margin-top:30PX;"
          >
            <View className="at-col">
              <AtButton
                circle
                type="primary"
                className="btn-submit"
                onClick={() => {
                  Taro.navigateTo({
                    url: "/pages/vip/explainList",
                  });
                }}
              >
                购买下一级分销商
              </AtButton>
            </View>
          </View>
        </View>
      </View>
    );
  }
}
