import Taro, { Component } from "@tarojs/taro";
import { View, Text } from "@tarojs/components";
import { AtButton, AtAvatar } from "taro-ui";
import { connect } from "@tarojs/redux";
import { $Req } from "@utils";
import "./index.scss";

const imt_vip = 'https://bai-tong-fen-xiao.oss-cn-beijing.aliyuncs.com/imgs/vip/vip.png';
const imt_level = 'https://bai-tong-fen-xiao.oss-cn-beijing.aliyuncs.com/imgs/vip/viplevel.png';
const zjz =
  "https://bai-tong-fen-xiao.oss-cn-beijing.aliyuncs.com/shopping/icon/zjz.png";
const gjz =
  "https://bai-tong-fen-xiao.oss-cn-beijing.aliyuncs.com/shopping/icon/gjz.png";
const cgjz =
  "https://bai-tong-fen-xiao.oss-cn-beijing.aliyuncs.com/shopping/icon/cgjz.png";

@connect(
  ({ userModel }) => ({
    userModel,
  }),
  (dispatch) => ({
    userAction: dispatch.userModel,
  })
)
export default class Index extends Component {
  config = {
    navigationBarTitleText: "收益",
  };

  constructor() {
    super(...arguments);
    this.state = {
      currentTab: 0,
      switch: false,
      sy: {},
      userList: [],
      level: null,
    };
  }

  componentDidShow() {
    this.setState({
      level: this.$router.params.level,
    });
  }

  componentDidMount() {
    this.getInCome();
    this.getFX();
  }

  // 获取收益
  async getInCome() {
    let { code, message, data } = await $Req({
      type: "get",
      url: "/indexUser/getUserIntegral",
      data: {},
    });
    if (!data) return;
    this.setState({
      sy: data,
    });
  }

  // 分享关系链
  async getFX() {
    let { code, message, data } = await $Req({
      type: "get",
      url: "/distribution/getShareRelation",
      data: {},
    });
    if (!data) return;
    this.setState({
      userList: data.data,
    });
  }

  // 分享好友

  onShareAppMessage(res) {
    const {
      userModel: { userInfo },
    } = this.props;

    if (res.from === "button") {
      // 来自页面内转发按钮
      console.log(res.target);
    }
    return {
      title: "加入我的团队，带你一起赚钱",
      path: `/pages/vip/explainList?userId=${userInfo.userId}`,
    };
  }

  render() {
    let { level } = this.state;

    return (
      <View className="page-vip-income">
        <View className="at-row">
          <View className="at-col c0">
            <View className="at-row income-title" style="margin-top:15PX;">
              <View className="at-col at-col-8">
                <image
                  mode="aspectFit"
                  src={
                    level == "0"
                      ? imt_level
                      : level == "1"
                      ? imt_level
                      : level == "2"
                      ? imt_level
                      : level == "3"
                      ? zjz
                      : level == "4"
                      ? gjz
                      : level == "5"
                      ? cgjz
                      : imt_level
                  }
                />

                <Text className="level">
                  {level == "0"
                    ? "非会员"
                    : level == "1"
                    ? "VIP会员"
                    : level == "2"
                    ? "初级会员"
                    : level == "3"
                    ? "中级会员"
                    : level == "4"
                    ? "高级会员"
                    : level == "5"
                    ? "超高级会员"
                    : "无"}
                </Text>
              </View>
            </View>

            <View className="at-row" style="margin:15PX 0;">
              <View className="at-col list-item-box">
                <View
                  className="at-row at-row__align--center"
                  style="margin-bottom:10PX;"
                >
                  <View className="at-col">
                    <Text>当前收益</Text>
                  </View>
                </View>
                <View className={level == "0"
                    ? 'at-row at-row__align--center income-item'
                    : level == "1"
                    ? 'at-row at-row__align--center income-item'
                    : level == "2"
                    ? 'at-row at-row__align--center income-item'
                    : level == "3"
                    ? 'at-row at-row__align--center income-item zj'
                    : level == "4"
                    ? 'at-row at-row__align--center income-item gj'
                    : level == "5"
                    ? 'at-row at-row__align--center income-item cgj'
                    : 'at-row at-row__align--center income-item'}>
                  <View className="at-col at-col-4">
                    <Text>积分</Text>
                  </View>
                  <View className="at-col at-col-8">
                    <Text>{this.sy.integral}</Text>
                  </View>
                </View>
                 <View className={level == "0"
                    ? 'at-row at-row__align--center income-item'
                    : level == "1"
                    ? 'at-row at-row__align--center income-item'
                    : level == "2"
                    ? 'at-row at-row__align--center income-item'
                    : level == "3"
                    ? 'at-row at-row__align--center income-item zj'
                    : level == "4"
                    ? 'at-row at-row__align--center income-item gj'
                    : level == "5"
                    ? 'at-row at-row__align--center income-item cgj'
                    : 'at-row at-row__align--center income-item'}>
                  <View className="at-col at-col-4">
                    <Text>收益(可提现)</Text>
                  </View>
                  <View className="at-col at-col-8">
                    <Text>{this.sy.benefit}</Text>
                  </View>
                </View>
                <View className={level == "0"
                    ? 'at-row at-row__align--center income-item'
                    : level == "1"
                    ? 'at-row at-row__align--center income-item'
                    : level == "2"
                    ? 'at-row at-row__align--center income-item'
                    : level == "3"
                    ? 'at-row at-row__align--center income-item zj'
                    : level == "4"
                    ? 'at-row at-row__align--center income-item gj'
                    : level == "5"
                    ? 'at-row at-row__align--center income-item cgj'
                    : 'at-row at-row__align--center income-item'}>
                  <View className="at-col at-col-4">
                    <Text>等级</Text>
                  </View>
                  <View className="at-col at-col-8">
                    <Text className="level">
                      {level == "0"
                        ? "非会员"
                        : level == "1"
                        ? "VIP会员"
                        : level == "2"
                        ? "初级会员"
                        : level == "3"
                        ? "中级会员"
                        : level == "4"
                        ? "高级会员"
                        : level == "5"
                        ? "超高级会员"
                        : "无"}
                    </Text>
                  </View>
                </View>
              </View>
            </View>

            <View className="at-row" style="margin:15PX 0;">
              <View className="at-col">
                <Text>我邀请了</Text>
              </View>
              <View className="at-col" style="text-align:right">
                <Text>{this.state.userList.length}人</Text>
              </View>
            </View>

            {this.state.userList.map((el) => (
              <View className="at-row" style="margin-bottom:10PX;">
                <View className="at-col list-item-box">
                  <View className="at-row at-row__align--center">
                    <View className="at-col at-col-2">
                      <AtAvatar
                        circle
                        image="https://jdc.jd.com/img/200"
                      ></AtAvatar>
                    </View>
                    <View className="at-col at-col-10">
                      <View className="at-row">
                        <View className="at-col">
                          <Text className="user-name">{el.userName}</Text>
                        </View>
                      </View>
                      <View
                        className="at-row at-row__align--center"
                        style="margin-top: 5PX;"
                      >
                        <view class="color-box">
                          <image src={imt_vip} />
                        </view>
                        <View className="at-col">
                          <Text className="user-level1">{el.levelName}</Text>
                          {/* <Text className="user-level2">第一轮</Text> */}
                        </View>
                      </View>
                    </View>
                  </View>
                </View>
              </View>
            ))}

            <View
              className="at-row at-row__justify--between at-row__align--center"
              style="margin-top:50PX;"
            >
              <View className="at-col">
                <AtButton
                  circle
                  type="primary"
                  className="btn-submit"
                  openType="share"
                >
                  分享给好友
                </AtButton>
              </View>
            </View>
          </View>
        </View>
      </View>
    );
  }
}
