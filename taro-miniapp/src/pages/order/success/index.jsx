import Taro, { Component } from "@tarojs/taro";
import { View, Text } from "@tarojs/components";
import { AtButton, AtAvatar, AtIcon } from "taro-ui";
import { $Req } from "@utils";
import "./index.scss";
import "@components/ShowBox.scss";

const img_goods =
  "https://bai-tong-fen-xiao.oss-cn-beijing.aliyuncs.com" +
  "/shopping/order/goods.png";

Date.prototype.Format = function (fmt) {
  var o = {
    "M+": this.getMonth() + 1, //月份
    "d+": this.getDate(), //日
    "h+": this.getHours(), //小时
    "m+": this.getMinutes(), //分
    "s+": this.getSeconds(), //秒
    "q+": Math.floor((this.getMonth() + 3) / 3), //季度
    "S": this.getMilliseconds() //毫秒
  };
  
  if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
  for (var k in o){
    if (new RegExp("(" + k + ")").test(fmt)) {
      fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    }				
  }
    
  return fmt;
}

export default class Index extends Component {
  config = {
    navigationBarTitleText: "支付完成",
  };

  constructor() {
    super(...arguments);
    this.state = {
      currentTab: 0,
      switch: false,
      list: [],
      orderDetails: {},
      user: {}
    };
  }
  componentDidMount() {
    this.getUserInfo();
  }
  async getUserInfo() {
    let { code, data } = await $Req({
      type: "post",
      url: "/user/getUserCenterInfo",
      data: {},
    });

    if (code == 200) {
      this.setState({
        user: data,
      });
      console.log("后台返回的用户信息：" + JSON.stringify(data));
      Taro.setStorageSync("level", data.level);
    }
  }
  componentDidShow() {
    let orderDetails = JSON.parse(this.$router.params.orderDetails ? this.$router.params.orderDetails : "{}");
    console.log(orderDetails);
    this.setState({
      orderDetails,
    });
    let shareId = this.$router.params.userId;
    console.log("获取到分享人的id：", shareId);

    if (shareId) {
      Taro.setStorageSync("shareId", shareId);
    }
  }
  onShareAppMessage(res) {
    const {
      userModel: { userInfo },
    } = this.props;
    return {
      title: "支付成功",
      path: `/pages/order/success/index?orderDetails=${JSON.stringify(result)}&userId=${userInfo.userId}`,
    };
  }
  onTabClick() {}

  render() {
    let { orderDetails, user } = this.state;
    return (
      <View className="page-order-success">
        <View className="at-row at-row__justify--center at-row__align--center order-status">
          <View className="at-col" style="text-align:center;">
            <AtIcon value="check-circle" size="50" color="#fff"></AtIcon>
            <Text>支付成功 </Text>
          </View>
        </View>
        <View className="at-row">
          <View className="at-col c0">
            <View className="at-row order-success-user">
              <View className="at-col success-user-c0 list-item-box">
                <View className="at-row at-row__align--center">
                  <View className="at-col at-col-2">
                    <AtAvatar
                      circle
                      image={user.userPhoto||'https://jdc.jd.com/img/200'}
                    ></AtAvatar>
                  </View>
                  <View className="at-col at-col-8">
                    <View className="at-row">
                      <View className="at-col">
                        <Text className="user-name">{orderDetails.receiverName}</Text>
                        <Text className="user-tel">{orderDetails.receiverMobile}</Text>
                      </View>
                    </View>
                    <View className="at-row">
                      <View className="at-col" style="white-space: initial;">
                        <Text className="user-address" style="display:block;"> 
                          {orderDetails.receiverDistrict}{orderDetails.receiverAddress}
                        </Text>
                      </View>
                    </View>
                  </View>
                  {/* <View className="at-col at-col-2" style="text-align:right;">
                    <AtIcon
                      value="chevron-right"
                      size="25"
                      color="#000"
                    ></AtIcon>
                  </View> */}
                </View>
              </View>
            </View>

            {orderDetails.skuList &&
              orderDetails.skuList.map((item) => {
                return (
                  <View className="at-row shopping-show-box">
                    <View className="at-col show-box-c0">
                      <View className="at-row show-box-row">
                        <View className="at-col at-col-3 show-box-col-img">
                          <image className="img-goods" src={item.skuCoversUrl} />
                        </View>
                        <View className="at-col at-col-8 show-box-col-main" style="margin-left:10px">
                          <View className="at-row">
                            <View className="at-col">
                              <Text className="show-box-title">
                                {item.goodsName}
                              </Text>
                            </View>
                          </View>
                          <View className="at-row">
                            <View className="at-col">
                              <Text className="show-box-norms">
                                {item.skuTitleType}:{item.skuTitle}
                              </Text>
                            </View>
                          </View>
                        </View>
                        <View
                          className="at-col at-col-1"
                          style="padding-top: 20PX;text-align: right;font-size: 12PX;color: #999;"
                        >
                          <Text>x{item.quantity}</Text>
                        </View>
                      </View>
                      <View className="at-row at-row__align--center">
                        <View className="at-col at-col__offset-9">
                          {
                            item.vip*1 == 1 ?
                            (<AtButton circle className="btn-submit" openType="share" >分享</AtButton>)
                          : (                          <AtButton circle className="btn-submit" onClick={
                            () => {
                              Taro.navigateTo({
                                url: "/pages/order/index/index?type=0"
                              })
                            }
                          }>
                            退款
                          </AtButton>) }

                        </View>
                      </View>
                    </View>
                  </View>
                );
              })}

            <View className="at-row">
              <View className="at-col list-item-box">
                <View className="at-row at-row__align--center">
                  <View className="at-col">
                    <Text>订单编号：{orderDetails.ordersNo}</Text>
                  </View>
                </View>
                <View className="at-row at-row__align--center">
                  <View className="at-col">
                  <Text>支付时间：{orderDetails.createTime ? orderDetails.createTime : new Date().Format("yyyy-MM-dd hh:mm:ss")}</Text>
                  </View>
                </View>
              </View>
            </View>

            <View className="at-row at-row__justify--between at-row__align--center">
              <View className="at-col at-col-8">
                <Text>共计</Text>
              </View>
              <View className="at-col at-col-4" style="text-align:right;">
                <Text>￥{orderDetails.totalAmount}</Text>
              </View>
            </View>

            <View className="at-row at-row__justify--between at-row__align--center">
              <View className="at-col at-col-8">
                <Text>会员优惠</Text>
              </View>
              <View className="at-col at-col-4" style="text-align:right;">
                <Text>￥{orderDetails.totalAmount - orderDetails.finalAmount}</Text>
              </View>
            </View>
              {
                orderDetails.useIntegral != undefined && orderDetails.useIntegral != null ? 
                  <View className="at-row at-row__justify--between at-row__align--center">
                      <View className="at-col at-col-8">
                        <Text>积分抵扣</Text>
                      </View>
                      <View className="at-col at-col-4" style="text-align:right;">
                        <Text>{orderDetails.useIntegral}</Text>
                      </View>
                  </View>
                    :
                  null
              }
            

            <View className="at-row at-row__justify--between at-row__align--center">
              <View className="at-col at-col-8">
                <Text>合计</Text>
              </View>
              <View className="at-col at-col-4" style="text-align:right;">
                <Text>￥{orderDetails.finalAmount}</Text>
              </View>
            </View>

            <View className="at-row at-row__justify--between at-row__align--center">
              <View className="at-col at-col-8">
                <Text>已支付</Text>
              </View>
              <View className="at-col at-col-4" style="text-align:right;">
                <Text>￥{orderDetails.finalAmount}</Text>
              </View>
            </View>
          </View>
        </View>
      </View>
    );
  }
}
