import  { Component } from "@tarojs/taro";
import { View, Text } from "@tarojs/components";
import { AtButton, AtAvatar, AtIcon } from "taro-ui";
import "./index.scss";
import "@components/ShowBox.scss";

const img_goods = "https://bai-tong-fen-xiao.oss-cn-beijing.aliyuncs.com" + "/shopping/order/goods.png";

export default class Index extends Component {
  config = {
    navigationBarTitleText: "支付完成",
  };

  constructor() {
    super(...arguments);
    this.state = {
      currentTab: 0,
      switch: false,
      list: [],
    };
  }

  render() {
    return (
      <View className="page-order-success">
        <View className="at-row at-row__justify--center at-row__align--center order-status">
          <View className="at-col" style="text-align:center;">
            <AtIcon value="check-circle" size="50" color="#fff"></AtIcon>
            <Text>支付成功 </Text>
          </View>
        </View>
        <View className="at-row">
          <View className="at-col c0">
            <View className="at-row order-success-user">
              <View className="at-col success-user-c0 list-item-box">
                <View className="at-row at-row__align--center">
                  <View className="at-col at-col-2">
                    <AtAvatar
                      circle
                      image="https://jdc.jd.com/img/200"
                    ></AtAvatar>
                  </View>
                  <View className="at-col at-col-8">
                    <View className="at-row">
                      <View className="at-col">
                        <Text className="user-name">赵先生</Text>
                        <Text className="user-tel">15055556666</Text>
                      </View>
                    </View>
                    <View className="at-row">
                      <View className="at-col">
                        <Text className="user-address">
                          北京市 昌平区 北七家镇某街道某号
                        </Text>
                      </View>
                    </View>
                  </View>
                  <View className="at-col at-col-2" style="text-align:right;">
                    <AtIcon
                      value="chevron-right"
                      size="25"
                      color="#000"
                    ></AtIcon>
                  </View>
                </View>
              </View>
            </View>

            {this.state.list.map((item) => {
              return (
                <View className="at-row shopping-show-box">
                  <View className="at-col show-box-c0">
                    <View className="at-row show-box-row">
                      <View className="at-col at-col-3 show-box-col-img">
                        <image className="img-goods" src={img_goods} />
                      </View>
                      <View className="at-col at-col-8 show-box-col-main">
                        <View className="at-row">
                          <View className="at-col">
                            <Text className="show-box-title">{item.name}</Text>
                          </View>
                        </View>
                        <View className="at-row">
                          <View className="at-col">
                            <Text className="show-box-norms">{item.norms}</Text>
                          </View>
                        </View>
                      </View>
                      <View
                        className="at-col at-col-1"
                        style="padding-top: 20PX;text-align: right;font-size: 12PX;color: #999;"
                      >
                        <Text>x{item.count}</Text>
                      </View>
                    </View>
                    <View className="at-row at-row__align--center">
                      <View className="at-col at-col__offset-9">
                        <AtButton circle className="btn-submit">
                          退款
                        </AtButton>
                      </View>
                    </View>
                  </View>
                </View>
              );
            })}

            <View className="at-row">
              <View className="at-col list-item-box">
                <View className="at-row at-row__align--center">
                  <View className="at-col">
                    <Text>订单编号：20062300001</Text>
                  </View>
                </View>
                <View className="at-row at-row__align--center">
                  <View className="at-col">
                    <Text>支付时间：2020年6月23日 15:20:26</Text>
                  </View>
                </View>
              </View>
            </View>

            <View className="at-row at-row__justify--between at-row__align--center">
              <View className="at-col at-col-8">
                <Text>共计</Text>
              </View>
              <View className="at-col at-col-4" style="text-align:right;">
                <Text>￥199.00</Text>
              </View>
            </View>

            <View className="at-row at-row__justify--between at-row__align--center">
              <View className="at-col at-col-8">
                <Text>会员优惠</Text>
              </View>
              <View className="at-col at-col-4" style="text-align:right;">
                <Text>￥199.00</Text>
              </View>
            </View>

            <View className="at-row at-row__justify--between at-row__align--center">
              <View className="at-col at-col-8">
                <Text>积分抵扣</Text>
              </View>
              <View className="at-col at-col-4" style="text-align:right;">
                <Text>￥199.00</Text>
              </View>
            </View>

            <View className="at-row at-row__justify--between at-row__align--center">
              <View className="at-col at-col-8">
                <Text>合计</Text>
              </View>
              <View className="at-col at-col-4" style="text-align:right;">
                <Text>￥199.00</Text>
              </View>
            </View>

            <View className="at-row at-row__justify--between at-row__align--center">
              <View className="at-col at-col-8">
                <Text>已支付</Text>
              </View>
              <View className="at-col at-col-4" style="text-align:right;">
                <Text>￥199.00</Text>
              </View>
            </View>
          </View>
        </View>
      </View>
    );
  }
}
