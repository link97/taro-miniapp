import Taro, { Component } from "@tarojs/taro";
import { View, Text } from "@tarojs/components";
import { AtSearchBar, AtTabs, AtTabsPane } from "taro-ui";
import { $Req } from "@utils";
import "./index.scss";

export default class Index extends Component {
  config = {
    navigationBarTitleText: "商城",
  };

  constructor() {
    super(...arguments);
    this.state = {
      searchVar: "",
      currentTab: 0,
      tabList: [],
      dataList: [],
    };
  }
  componentDidMount() {
    this.getGoodsClass();
  }

  // 获取商品分类
  async getGoodsClass() {
    const { data } = await $Req({
      type: "get",
      url: "/goods/getCategoryList",
      data: {
        stop: 0,
        length: 99999999
      },
    });
    if (!data) return;
    const list = data.data.map((item) => {
      return { ...item, title: item.categoryName, children: [] };
    });

    this.setState({
      tabList: list,
      currentTab: 0,
    });

    setTimeout(() => {
      this.getGoodsType(0);
    }, 500)
    
  }

  // 获取商品分类
  async getGoodsType(index) {
    const { tabList } = this.state;
    console.log(tabList);
    const { code, message, data } = await $Req({
      type: "get",
      url: "/goods/getMallGoodsList",
      data: {
        categoryId: tabList[index].id,
        special: 0,
        status: 1,
      },
    });
    if (!data) return;
    tabList[index].children = data.data;
    this.setState({
      tabList,
    });
  }

  initSearchBox() {
    const { searchVar } = this.state;
    return (
      <View className="at-row search-cont" 
        onClick={() => {
          Taro.navigateTo({
            url: "/pages/search/log",
          });
        }}
      >
        <AtSearchBar
          fixed
          disabled
          value={searchVar}
        />
      </View>
    );
  }

  onTabClick() {}

  render() {
    return (
      <View className="at-row page-mall-index">
        <View className="at-col">
          {this.initSearchBox()}
          <View className="at-row mall-tab">
            <View className="at-col">
              <AtTabs
                height="100%"
                current={this.state.currentTab}
                tabDirection="vertical"
                scroll
                tabList={this.state.tabList}
                onClick={(v) => {
                  this.setState({
                    currentTab: v,
                  });
                  this.getGoodsType(v);
                }}
              >
                {this.state.tabList.map((item) => {
                  return (
                    <AtTabsPane
                      key={item.id}
                      tabDirection="vertical"
                      scroll
                      current={this.state.currentTab}
                      index={item.id}
                    >
                      {item.children.map((node, iii) => {
                        return (
                          <View
                            className="at-row mall-list-item"
                            key={node.skuId}
                            onClick={() => {
                              Taro.navigateTo({
                                url:
                                  "/pages/mall/detail/index?goodsId=" +
                                  node.skuId,
                              });
                            }}
                          >
                            <View className="at-col mall-list-item-col">
                              <View className="at-row">
                                <View className="at-col at-col-5">
                                  <image mode={"aspectFill"} src={node.skuCoversUrl} />
                                </View>
                                <View className="at-col at-col-7">
                                  <View className="at-row goods-name">
                                    <View className="at-col">
                                      <Text>{node.goodsName}</Text>
                                    </View>
                                  </View>
                                  {
                                    node.integralStatus == 1 ?
                                    <View className="at-col" style="text-align: left;margin-top: 4PX; margin-left: 4PX">
                                      <Text style="color: #ccc; font-size:12PX">积分商品</Text>
                                    </View>
                                    :
                                    null
                                  }
                                  <View className="goods-money">
                                    <View className="at-col">
                                      {/* <Text style="padding: 3px 4px; background-color:#eee; border-radius:6px; font-size:12px;">{node.skuTitleType}: {node.skuTitle}</Text> */}
                                    </View>
                                    <View className="at-col">
                                      <Text className="goods-money1">
                                        ￥{node.skuCurrPrice}
                                      </Text>
                                      <Text className="goods-money2">
                                        .0元{node.vip == 1 ? '': '/积分'}
                                      </Text>
                                    </View>
                                  </View>
                                </View>
                              </View>
                            </View>
                          </View>
                        );
                      })}
                    </AtTabsPane>
                  );
                })}
              </AtTabs>
            </View>
          </View>
        </View>
      </View>
    );
  }
}
