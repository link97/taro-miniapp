import Taro, { Component } from "@tarojs/taro";
import { View, Text } from "@tarojs/components";

import "./ShowBox.scss";

const img_selected = "https://bai-tong-fen-xiao.oss-cn-beijing.aliyuncs.com" + "/shopping/user/selected.png";
const img_select = "https://bai-tong-fen-xiao.oss-cn-beijing.aliyuncs.com" + "/shopping/user/select.png";
const img_goods = "https://bai-tong-fen-xiao.oss-cn-beijing.aliyuncs.com" + "/shopping/order/goods.png";

export default class ShowBox extends Component {
  static options = {
    addGlobalClass: true,
  };

  render() {
    return (
      <View className="at-row shopping-show-box">
        <View className="at-col shopping-show-box-c0">
          <View className="at-row shopping-show-box-row">
            <View className="at-col at-col-1 shopping-show-box-col-select">
              <image className="img-select" src={img_select} />
            </View>
            <View className="at-col at-col-3 shopping-show-box-col-img">
              <image className="img-goods" />
            </View>
            <View className="at-col at-col-8 shopping-show-box-col-main">
              <View className="at-row">
                <View className="at-col">
                  <Text className="shopping-show-box-title">
                    新布局大米富硒香米大米长粒香米稻花香
                  </Text>
                </View>
              </View>
              <View className="at-row">
                <View className="at-col">
                  <Text className="shopping-show-box-price-large">￥199</Text>
                  <Text className="shopping-show-box-price-small">
                    .0元/积分
                  </Text>
                </View>
              </View>
            </View>
          </View>
        </View>
      </View>
    );
  }
}
