import Taro, { Component } from "@tarojs/taro";
import { connect } from "@tarojs/redux";
import { View, Text, Image } from "@tarojs/components";
import {
  AtModal,
  AtModalHeader,
  AtModalContent,
  AtModalAction,
  AtButton,
} from "taro-ui";

import "./index.scss";

@connect(
  ({ userModel }) => ({
    userModel,
  }),
  (dispatch) => ({
    userAction: dispatch.userModel,
  })
)
export default class BaseComponent extends Component {
  config = {
    navigationBarTitleText: "",
  };

  constructor() {
    super(...arguments);
    this.state = {
      isOpen: false,
    };
  }

  componentDidMount() {}

  render() {
    const {
      userModel: { isOpen },
      userAction,
    } = this.props;
    return (
      <View style="height:100%;">
        {this.props.children}
        <AtModal isOpened={isOpen} className="globle-user-modal">
          <AtModalHeader>您还未登录</AtModalHeader>
          <View style={{ fontSize: "16PX", color: '#333', marginTop:'10PX' }}>请您登录后再进行操作</View>
          <Image
            style={{ width: "104PX", height: "104PX", marginTop: '20PX', marginBottom:'15PX' }}
            src={
              "https://bai-tong-fen-xiao.oss-cn-beijing.aliyuncs.com/shopping/loginImg.png"
            }
          ></Image>
          <AtModalAction className="modal-action">
            <AtButton
              circle
              size="small"
              onClick={() => {
                userAction.setStatus({
                  isOpen: false,
                });
              }}
            >
              暂不登录
            </AtButton>
            <AtButton
              circle
              size="small"
              type="primary"
              onClick={() => {
                userAction.setStatus({
                  isOpen: false,
                });
                Taro.navigateTo({
                  url: "/pages/index/welcome",
                });
              }}
            >
              登录/注册
            </AtButton>
          </AtModalAction>
        </AtModal>
      </View>
    );
  }
}
