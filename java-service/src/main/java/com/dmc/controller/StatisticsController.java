package com.dmc.controller;
import com.dmc.model.RestRespResult;
import com.dmc.service.StatisticsService;
import com.dmc.vo.OverviewVo;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 统计相关接口
 */
@Controller
@RequestMapping("/statistics")
public class StatisticsController {
    @Autowired
    private StatisticsService statisticsService;

    @RequestMapping(value = "/overview",method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value="概况")
    public RestRespResult<OverviewVo> getOverview(String sDate, String eDate) {
        RestRespResult restRespResult=RestRespResult.ok("获取信息成功");
        OverviewVo overviewVo=statisticsService.getOverview(sDate,eDate);
        restRespResult.setData(overviewVo);
        return restRespResult;
    }
}
