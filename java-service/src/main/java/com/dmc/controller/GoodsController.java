package com.dmc.controller;



import com.dmc.model.RestRespResult;
import com.dmc.service.GoodsService;
import com.dmc.vo.*;
import io.swagger.annotations.*;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;

/**
 * <p>
 * 商品控制器
 * </p>
 *
 * @author dmc
 * @since 2020-09-04
 */
@Api(value = "商品相关接口", description = "商品相关接口",tags={"GoodsController"})
@Controller
@RequestMapping("/goods")
public class GoodsController {

    @Resource
    private GoodsService goodsService;


    @RequestMapping(value = "/addCategory",method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value="添加品类")
    public RestRespResult<String> addCategory(@RequestBody TGoodsCategoryVo vo) {
        goodsService.addCategory(vo);
        return RestRespResult.ok("添加商品品类成功");
    }

    @RequestMapping(value = "/deleteCategory",method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value="删除品类")
    public RestRespResult<String> deleteCategory(Long id) {
        goodsService.deleteCategory(id);
        return RestRespResult.ok("删除商品品类成功");
    }

    @RequestMapping(value = "/updateCategory",method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value="修改品类")
    public RestRespResult<String> updateCategory(@RequestBody TGoodsCategoryVo vo) {
        goodsService.updateCategory(vo);
        return RestRespResult.ok("修改商品品类成功");
    }

    @RequestMapping(value = "/getCategoryList",method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value="获取品类列表")
    public RestRespResult<DataTable<TGoodsCategoryVo>> getCategoryList(TGoodsCategoryVo vo) {
        RestRespResult result = RestRespResult.ok("获取商品品类成功");
        DataTable<TGoodsCategoryVo> categoryList = goodsService.getCategoryList(vo);
        result.setData(categoryList);
        return result;
    }

    @RequestMapping(value = "/getGoodsList",method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value="获取后台商品列表")
    public RestRespResult<DataTable<TGoodsSpuVo>> getGoodsList(@RequestBody TGoodsSpuVo vo) {
        RestRespResult result = RestRespResult.ok("获取后台商品列表");
        DataTable<TGoodsSpuVo> data = goodsService.getGoodsList(vo);
        result.setData(data);
        return result;
    }

    @RequestMapping(value = "/saveOrUpdateGoods",method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value="添加或者更新商品",tags = {"三期"})
    public RestRespResult<String> saveOrUpdateGoods(@RequestBody TGoodsSpuVo vo) {
        goodsService.saveOrUpdateGoods(vo);
        return RestRespResult.ok("添加商品品类成功");
    }

    @RequestMapping(value = "/deleteGoods",method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value="删除商品")
    public RestRespResult<String> deleteGoods(Long id) {
        goodsService.deleteGoods(id);
        return RestRespResult.ok("删除商品成功");
    }

    @RequestMapping(value = "/getMallGoodsList",method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value="获取小程序商城列表以及搜索",tags = {"三期"})
    public RestRespResult<DataTable<TGoodsListSkuVo>> getMallGoodsList(TGoodsListSkuVo vo) {
        RestRespResult result = RestRespResult.ok("获取商品成功");
        DataTable<TGoodsListSkuVo> mallGoodsList = goodsService.getMallGoodsList(vo);
        result.setData(mallGoodsList);
        return result;
    }

    @RequestMapping(value = "/getGoodsDetail",method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value="根据SKUID获取商品详情",tags = {"三期"})
    public RestRespResult<TGoodsDetailVo>  getGoodsDetail(@ApiParam(value="商品SKUID",name="skuId")@RequestParam(required=false)Long skuId,
                                                          @ApiParam(value="商品SPUID",name="spuId")@RequestParam(required=false)Long spuId,
                                                          @ApiParam(value="商品规格",name="skuTitle")String skuTitle) {
        RestRespResult result = RestRespResult.ok("获取商品详情成功");
        TGoodsDetailVo goodsDetail = goodsService.getGoodsDetail(skuId,spuId,skuTitle);
        result.setData(goodsDetail);
        return  result;
    }


    @RequestMapping(value = "/getGoodsDetailForPc",method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value="后台获取商品详情",tags = {"三期"})
    public RestRespResult<TGoodsDetailVo>  getGoodsDetailForPc(@ApiParam(value="商品SPUID",name="spuId")@RequestParam(required=false)Long spuId) {
        RestRespResult result = RestRespResult.ok("获取商品详情成功");
        TGoodsDetailVo goodsDetail = goodsService.getGoodsDetailForPc(spuId);
        result.setData(goodsDetail);
        return  result;
    }

    @RequestMapping(value = "/oldGoodsHandle",method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value="后台获取商品详情",tags = {"三期"})
    public RestRespResult<String>  oldGoodsHandle(@ApiParam(value="商品SPUID",name="spuId")@RequestParam(required=false)Long spuId) {
        RestRespResult result = RestRespResult.ok("获取商品详情成功");
        goodsService.oldGoodsHandle();

        return  result;
    }
}
