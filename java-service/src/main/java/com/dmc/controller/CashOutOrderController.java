package com.dmc.controller;


import com.dmc.model.RestRespResult;
import com.dmc.service.CashOutOrderService;
import com.dmc.service.GoodsService;
import com.dmc.vo.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * <p>
 * 商品控制器
 * </p>
 *
 * @author dmc
 * @since 2020-09-04
 */
@Api(value = "提现相关接口", description = "提现相关接口")
@Controller
@RequestMapping("/cashOutOrder")
public class CashOutOrderController {

    @Resource
    private CashOutOrderService cashOutOrderService;


    @RequestMapping(value = "/addCashOutOrder",method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value="添加提现订单")
    public RestRespResult<CashOutOrdersVo> addCashOutOrder(@RequestBody CashOutOrdersVo vo) {
        CashOutOrdersVo cashOutOrdersVo = cashOutOrderService.addCashOutOrder(vo);
        String message = cashOutOrdersVo.getMessage();
        if(StringUtils.isBlank(message)){
            message="提现成功";
        }
        RestRespResult result = RestRespResult.ok(message);
        result.setData(cashOutOrdersVo);
        return result;
    }

    @RequestMapping(value = "/updateCashOutOrder",method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value="修改提现订单")
    public RestRespResult<String> updateCashOutOrder(@RequestBody CashOutOrdersVo vo) {
        cashOutOrderService.updateCashOutOrder(vo);
        return RestRespResult.ok("修改提现订单成功");
    }

    @RequestMapping(value = "/getCashOutOrderList",method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value="获取提现订单列表")
    public RestRespResult<DataTable<CashOutOrdersVo>> getCashOutOrderList(@RequestBody CashOutOrdersVo vo) {
        RestRespResult result = RestRespResult.ok("获取提现订单列表成功");
        DataTable<CashOutOrdersVo> categoryList = cashOutOrderService.getCashOutOrderList(vo);
        result.setData(categoryList);
        return result;
    }

}
