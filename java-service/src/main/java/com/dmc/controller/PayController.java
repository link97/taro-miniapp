package com.dmc.controller;


import com.dmc.dict.PayModeDict;
import com.dmc.dict.PayTypeDict;
import com.dmc.model.RestResp;
import com.dmc.model.RestRespResult;
import com.dmc.service.PayService;
import com.dmc.service.impl.AbstractPayServiceImpl;
import com.dmc.util.IpUtil;
import com.dmc.util.util.QrCodeUtils;
import com.dmc.vo.PaymentVo;
import com.dmc.vo.ValidatedGroups;
import com.google.zxing.WriterException;
import com.ijpay.core.kit.IpKit;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * 支付相关接口
 */
@RestController
@RequestMapping("/pay")
@Slf4j
public class PayController {

    /**
     * 支付接口
     * @param payment 支付参数
     * @return
     */
    @RequestMapping(value = "/pay", method = {RequestMethod.GET,RequestMethod.POST})
    public RestRespResult<Map<String, String>> pay(HttpServletRequest request, HttpServletResponse response,
                                                   @Validated({ValidatedGroups.Pay.class}) PaymentVo payment, BindingResult results) {
        if(results.hasErrors()){
            List<ObjectError> ls=results.getAllErrors();
            throw new IllegalArgumentException(ls.get(0).getDefaultMessage());
        }
        String ip = IpUtil.getIpAddr(request);
        payment.setIp(ip);
        String payModel = payment.getPayModel();
        PayService payService = AbstractPayServiceImpl.getPayService(payment.getPayType(), payModel);
        RestRespResult<Map<String, String>> restResp = payService.pay(payment);
        return restResp;
    }


    /**
     * 查询订单支付状态接口
     * @param payment 支付参数
     * @return
     */
    @RequestMapping(value = "/queryPay", method = RequestMethod.GET)
    public RestRespResult<Map<String, String>> queryPay(HttpServletRequest request,
                                                        @Validated({ValidatedGroups.QueryPay.class}) PaymentVo payment, BindingResult results) {
        if(results.hasErrors()){
            List<ObjectError> ls=results.getAllErrors();
            throw new IllegalArgumentException(ls.get(0).getDefaultMessage());
        }
        PayService payService = AbstractPayServiceImpl.getPayService(payment.getPayType(), payment.getPayModel());
        return payService.queryPay(payment);
    }

    /**
     * 微信支付回调接口
     * @return
     */
    @RequestMapping(value = "/wechat/callback", method = {RequestMethod.GET,RequestMethod.POST})
    public String wechatCallback(HttpServletRequest request) {
        log.info("wechatCallback ");
        PayService payService = AbstractPayServiceImpl.getPayService(PayTypeDict.PAY_TYPE_WECHAT.getCn(), PayModeDict.PAY_MODE_MINI_APP.getCn());
        RestRespResult restResp = payService.payCallBack(request);
        return (String)restResp.getData();
    }

    /**
     * 支付宝支付回调接口
     * @return
     */
    @RequestMapping(value = "/alipay/callback", method = {RequestMethod.GET,RequestMethod.POST})
    public String aliPayCallback(HttpServletRequest request) {
        log.info("aliPayCallback ");
        PayService payService = AbstractPayServiceImpl.getPayService(PayTypeDict.PAY_TYPE_ALIPAY.getCn(), PayModeDict.PAY_MODE_MINI_NATIVE.getCn());
        RestRespResult restResp = payService.payCallBack(request);
        return (String)restResp.getData();
    }


    /**
     * 提现接口
     * @param payment 支付参数
     * @return
     */
    @RequestMapping(value = "/transfer", method = RequestMethod.POST)
    public RestRespResult<String> transfer(HttpServletRequest request,
                                           @Validated({ValidatedGroups.Transfer.class}) PaymentVo payment, BindingResult results) {
        if(results.hasErrors()){
            List<ObjectError> ls=results.getAllErrors();
            throw new IllegalArgumentException(ls.get(0).getDefaultMessage());
        }
        String ip = IpKit.getRealIp(request);
        payment.setIp(ip);
        PayService payService = AbstractPayServiceImpl.getPayService(payment.getPayType(), payment.getPayModel());
        return payService.transfer(payment);
    }

    /**
     * 退款接口
     * @param payment 支付参数
     * @return
     */
    @RequestMapping(value = "/refund", method = RequestMethod.POST)
    public RestRespResult<String> refund(HttpServletRequest request,
                                         @Validated({ValidatedGroups.RefundPay.class}) PaymentVo payment, BindingResult results) {
        if(results.hasErrors()){
            List<ObjectError> ls=results.getAllErrors();
            throw new IllegalArgumentException(ls.get(0).getDefaultMessage());
        }
        String ip = IpKit.getRealIp(request);
        payment.setIp(ip);
        //Order order = orderService.getOrderById(Long.valueOf(payment.getOrderId()));
        //Payment paymentCheckout = getPayment(userId, PaymentTypeDict.PAYMENT_TYPE_PAY.getCn(),orderId,false);
        PayService payService = AbstractPayServiceImpl.getPayService(payment.getPayType(), payment.getPayModel());
        return payService.refund(payment);
    }


    /**
     * 查询订单支付状态接口
     * @param payment 支付参数
     * @return
     */
    @RequestMapping(value = "/queryRefund", method = RequestMethod.GET)
    public RestRespResult<String> queryRefund(HttpServletRequest request,
                                              @Validated({ValidatedGroups.QueryPay.class}) PaymentVo payment, BindingResult results) {
        if(results.hasErrors()){
            List<ObjectError> ls=results.getAllErrors();
            throw new IllegalArgumentException(ls.get(0).getDefaultMessage());
        }
        PayService payService = AbstractPayServiceImpl.getPayService(payment.getPayType(), payment.getPayModel());
        return payService.queryRefund(payment);
    }

    /**
     * 微信退款回调接口
     * @param payment 支付参数
     * @return
     */
    @RequestMapping(value = "/wechat/refundCallback", method = RequestMethod.GET)
    public RestResp wechatRefundCallback(HttpServletRequest request, @Validated PaymentVo payment) {
        PayService payService = AbstractPayServiceImpl.getPayService(payment.getPayType(), payment.getPayModel());
        return payService.refundCallBack(payment);
    }

    /**
     * 支付宝退款回调接口
     * @param payment 支付参数
     * @return
     */
    @RequestMapping(value = "/alipay/refundCallback", method = RequestMethod.GET)
    public RestResp aliPayRefundCallback(HttpServletRequest request, @Validated PaymentVo payment) {
        PayService payService = AbstractPayServiceImpl.getPayService(payment.getPayType(), payment.getPayModel());
        return payService.refundCallBack(payment);
    }

}
