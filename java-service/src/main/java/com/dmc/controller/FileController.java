package com.dmc.controller;

import com.dmc.entity.CarouselInfo;
import com.dmc.model.RestListRespResult;
import com.dmc.model.RestResp;
import com.dmc.model.RestRespResult;
import com.dmc.service.AliyunOssService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;
import java.io.InputStream;

/**
 * @
 * @date 2020/9/516:09
 */
@Api(value = "文件操作相关接口", description = "文件操作相关接口")
@Controller
@RequestMapping("/file")
public class FileController {
    @Resource
    private AliyunOssService aliyunOssService;

    @ApiOperation(value="上传图片")
    @RequestMapping(value = "/uploadFile",method = RequestMethod.POST)
    @ResponseBody
    public RestRespResult<String> uploadFile(@RequestParam("file") MultipartFile file)throws IOException {
        Assert.notNull(file, "参数不能为空");
        InputStream is = file.getInputStream();
        String resultFilePath=aliyunOssService.uploadFile(file.getOriginalFilename(),is);
        RestRespResult result = RestRespResult.ok("上传成功");
        result.setData(resultFilePath);
        return result;
    }
}
