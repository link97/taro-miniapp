package com.dmc.controller;


import cn.hutool.core.lang.Assert;
import com.dmc.dict.DistriButionOrderTypeDict;
import com.dmc.model.*;
import com.dmc.service.DistributionService;
import com.dmc.util.ExportUtil;
import com.dmc.vo.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

/**
 * <p>
 * 分销控制器
 * </p>
 *
 * @author dmc
 * @since 2020-09-04
 */
@Api(value = "分销相关", description = "分销相关接口",tags={"DistributionController"})
@Controller
@RequestMapping("/distribution")
@Slf4j
public class DistributionController {

    @Resource
    private DistributionService distributionService;



    @RequestMapping(value = "/addDistributionGoods",method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value="添加分销商商品")
    public RestRespResult<String> addDistributionGoods(@RequestBody DistributionGoodsVo vo) {
        distributionService.saveGoods(vo);
        return RestRespResult.ok("添加分销商商品成功");
    }

    @RequestMapping(value = "/updateDistributionGoods",method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value="编辑分销商商品")
    public RestRespResult<String> updateDistributionGoods(@RequestBody DistributionGoodsVo vo) {
        distributionService.updateGoods(vo);
        return RestRespResult.ok("编辑分销商商品类成功");
    }

    @RequestMapping(value = "/getDistributionGoodsList",method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value="获取分销商品列表",tags={"三期"})
    public RestRespResult<DataTable<DistributionGoodsVo>> getDistributionGoodsList(@RequestBody DistributionGoodsVo vo) {
        DataTable<DistributionGoodsVo> goods = distributionService.getDistributionGoodsList(vo);
        RestRespResult result = RestRespResult.ok("获取分销商商品类成功");
        result.setData(goods);
        return result;
    }

    @RequestMapping(value = "/getShareRelation",method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value="获取分享关系链",tags={"二期"})
    public RestRespResult<DataTable<DistributionShareRelationVo>> getShareRelation(@ApiParam(value="1:一代 2:二代",name="shareLevel")@RequestParam(defaultValue = "1") Integer shareLevel) {
        DataTable<DistributionShareRelationVo> relations = distributionService.getShareRelation(shareLevel);
        RestRespResult result = RestRespResult.ok("获取分销链成功");
        result.setData(relations);
        return result;
    }

    @RequestMapping(value = "/getShareRelationForPc",method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value="PC端获取一级分销以及二级分销",tags={"二期"})
    public RestRespResult<DataTable<DistributionShareRelationPcVo>> getShareRelationForPc(DistributionShareRelationPcVo vo) {
        DataTable<DistributionShareRelationPcVo> relations = distributionService.getShareRelationForPc(vo);
        RestRespResult result = RestRespResult.ok("获取分销链成功");
        result.setData(relations);
        return result;
    }

    @RequestMapping(value = "/getHelpRelation",method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value="获取分红关系链")
    public RestRespResult<DataTable<DistributionHelpRelationVo>> getHelpRelation(@ApiParam(value="0:我被助益 1:我助益别人",name="type")@RequestParam(required=false) Integer type) {
        DataTable<DistributionHelpRelationVo> relations = distributionService.getHelpRelation(type);
        RestRespResult result = RestRespResult.ok("获取分红链成功");
        result.setData(relations);
        return result;
    }

    @RequestMapping(value = "/getProtol",method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value="获取有效分销协议")
    public RestRespResult<DistributionProcotolVo> getProtol() {
        DistributionProcotolVo procotol = distributionService.getProtol();
        Assert.notNull(procotol,"无有效的分销协议");
        RestRespResult result = RestRespResult.ok("获取助益关系链成功");
        result.setData(procotol);
        return result;
    }

    @RequestMapping(value = "/getProtolList",method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value="获取分销协议列表")
    public RestRespResult<DataTable<DistributionProcotolVo>>  getProtolList(@RequestBody DistributionProcotolVo vo) {
        DataTable<DistributionProcotolVo> table = distributionService.getProtolList(vo);
        RestRespResult result = RestRespResult.ok("获取分销协议列表成功");
        result.setData(table);
        return result;
    }

    @RequestMapping(value = "/saveOrUpdateProcotol",method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value="修改或者更新协议")
    public RestRespResult<DataTable<DistributionProcotolVo>>  saveOrUpdateProcotol(@RequestBody DistributionProcotolVo vo) {
        distributionService.saveOrUpdateProcotol(vo);
        return RestRespResult.ok("修改或者更新协议成功");
    }



    @RequestMapping(value = "/getDistributionOrderList",method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value="获取分销订单信息列表",tags = {"三期"})
    public RestRespResult<DataTable<DistributionOrderVo>> getDistributionOrderList(@RequestBody DistributionOrderVo vo,Long pId) {
        if (pId!=null){
            vo.setPId(pId);
        }
        DataTable<DistributionOrderVo> orderList = distributionService.getDistributionOrderList(vo);
        RestRespResult result = RestRespResult.ok("获取分销订单信息列表成功");
        result.setData(orderList);
        return result;
    }


    @RequestMapping(value = "/getReadyPayOrderList",method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value="获取待付款订单",tags = {"二期"})
    public RestRespResult<DataTable<DistributionOrderVo>> getReadyPayOrderList() {
        DataTable<DistributionOrderVo> orderList = distributionService.getReadyPayOrderList();
        RestRespResult result = RestRespResult.ok("获取代付款订单成功");
        result.setData(orderList);
        return result;
    }

    @RequestMapping(value = "/getOrderDetailForSmall",method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value="小程序获取分销订单详情",tags = {"二期"})
    public RestRespResult<DistributionOrderVo> getDistributionOrderDetail(@ApiParam(value="分销订单ID",name="orderId")@RequestParam(defaultValue = "0") long orderId,
                                                                          @ApiParam(value="级别 2:中级 3:高级 4:超高级",name="level")@RequestParam(required = false) Integer level) {
       log.info(" getOrderDetailForSmall orderId:{},level:{}",orderId,level);
        DistributionOrderVo vo = distributionService.getDistributionOrderDetail(orderId,level);
        RestRespResult result = RestRespResult.ok("获取订单详情成功成功");
        result.setData(vo);
        return result;
    }

    @RequestMapping(value = "/getOrderDetailForPc",method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value="PC端获取分销订单详情",tags = {"二期"})
    public RestListRespResult<DistributionOrderDetailPcVo> getOrderDetailForPc(@ApiParam(value="分销订单ID",name="orderId")@RequestParam(defaultValue = "0") long orderId) {
        RestListRespResult<DistributionOrderDetailPcVo> result  = distributionService.getOrderDetailForPc(orderId);
        return result;
    }

    @RequestMapping(value = "/getMyBonusList",method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value="获取我发分红列表",tags = {"二期"})
    public RestRespResult<MyBonusVo> getMyBonusList(@ApiParam(value="0:分销订单 1:分销套餐包订单",name="packStatus")@RequestParam(defaultValue="0")Integer packStatus,
                                                    @ApiParam(value="套餐包订单ID 用于查询套餐包内商品子订单",name="packageOrederId")@RequestParam(required=false)Long packageOrederId) {
        MyBonusVo myBonusVo = distributionService.getMyBonusList(packStatus, packageOrederId);
        RestRespResult result = RestRespResult.ok("获取我发分红列表成功");
        result.setData(myBonusVo);
        return result;
    }

    @RequestMapping(value = "/postProcess",method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value="分销商测试接口")
    public RestRespResult<DataTable<DistributionOrderVo>> postProcess(Long orderId,Integer payType) {
        distributionService.postProcess(orderId,payType,false,false);
        RestRespResult result = RestRespResult.ok("分销商测试接口");
        return result;
    }


    @RequestMapping(value = "/addDistributionOrder",method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value="添加分销商订单",tags={"二期"})
    public RestRespResult<DistributionOrderVo> addDistributionOrder(@RequestBody DistributionOrderVo vo) {
        vo.setOrderType(DistriButionOrderTypeDict.DISTRIBUTION_PAY_TYPE_0.getCn());
        vo = distributionService.addDistributionOrder(vo,false);
        RestRespResult result = RestRespResult.ok("添加分销商订单成功");
        result.setData(vo);
        return result;
    }

    @RequestMapping(value = "/addDistributionPackageOrder",method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value="添加分销商套餐包订单",tags={"三期"})
    public RestRespResult<DistributionPackageOrderVo> addDistributionPackageOrder(@RequestBody DistributionOrderVo vo) {
        vo = distributionService.addDistributionPackageOrder(vo,false);
        RestRespResult result = RestRespResult.ok("添加分销商套餐订单成功");
        result.setData(vo);
        return result;
    }

   @RequestMapping(value = "/submitPayEvidence",method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value="提交线下支付证明",tags={"二期"})
    public RestRespResult<DistributionOrderVo> submitPayEvidence(@RequestBody DistributionOrderVo vo) {
        vo = distributionService.submitPayEvidence(vo);
        RestRespResult result = RestRespResult.ok("提交线下支付证明成功");
        result.setData(vo);
        return result;
    }


    @RequestMapping(value = "/reviewOfflineOrder", method = RequestMethod.POST)
    @ApiOperation(value = "线下支付审核 参数传输id即可")
    @ResponseBody
    public RestRespResult<String> reviewOfflineOrder(@RequestBody DistributionOrderVo vo) {
        RestRespResult result = RestRespResult.ok("审核支付成功");
        distributionService.reviewOfflineOrder(vo);
        return result;
    }


    @RequestMapping(value = "/saveOrUpdatePackage", method = RequestMethod.POST)
    @ApiOperation(value = "添加或者修改套餐包 以ID判断",tags = {"三期"})
    @ResponseBody
    public RestRespResult<String> saveOrUpdatePackage(@RequestBody DistributionGoodsPackageVo vo) {
        RestRespResult result = RestRespResult.ok("添加套餐包成功");
        distributionService.saveOrUpdatePackage(vo);
        return result;
    }

    @RequestMapping(value = "/saveOrUpdateAchievementConfig",method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value="更新或者添加业绩奖配置",tags = {"三期"})
    public RestRespResult<String> saveOrUpdateAchievementConfig(@RequestBody AchievementBonusConfigVo vo) {
        distributionService.saveOrUpdateAchievementConfig(vo);
        RestRespResult result = RestRespResult.ok("更新或者添加业绩奖配置成功");
        return result;
    }

    @RequestMapping(value = "/getAchievementConfigList",method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value="获取业绩奖配置列表",tags = {"三期"})
    public RestRespResult<List<AchievementBonusConfigVo>> getAchievementConfigList(@RequestBody AchievementBonusConfigVo vo) {
        List<AchievementBonusConfigVo> achievementConfigList = distributionService.getAchievementConfigList(vo);
        RestRespResult result = RestRespResult.ok("更新或者添加业绩奖配置成功");
        result.setData(achievementConfigList);
        return result;
    }


    @RequestMapping(value = "/inportOldData",method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value="导入老数据")
    public RestRespResult<String> inportOldData(@RequestParam(value = "file") MultipartFile file) {
        distributionService.inportOldData(file);
        RestRespResult result = RestRespResult.ok("导入老数据成功");
        return result;
    }

    @RequestMapping(value = "/export",method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value="导出订单")
    public void export(HttpServletResponse response, @RequestBody DistributionOrderVo vo) {
        DataTable<DistributionOrderVo> orderList = distributionService.getDistributionOrderList(vo);
        List<DistributionOrderVo> data = orderList.getData();
        List<Object> exportList = new ArrayList<>();
        if(!CollectionUtils.isEmpty(data)){
            for (DistributionOrderVo datum : data) {
                DistributionOrderExportVo export = new DistributionOrderExportVo();
                export.setOrderNo(datum.getOrderNo());
                export.setShareUserName(export.getShareUserName());
                export.setUserName(export.getUserName());
                exportList.add(export);
            }
        }

        try {
            ExportUtil.exportModelStart(response,"分销订单",exportList,DistributionOrderExportVo.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
