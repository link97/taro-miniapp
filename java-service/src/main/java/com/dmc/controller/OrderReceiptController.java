package com.dmc.controller;


import com.dmc.model.RestRespResult;
import com.dmc.service.CashOutOrderService;
import com.dmc.service.OrderReceiptService;
import com.dmc.vo.CashOutOrdersVo;
import com.dmc.vo.DataTable;
import com.dmc.vo.OrderReceiptVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

/**
 * <p>
 * 商品控制器
 * </p>
 *
 * @author dmc
 * @since 2020-09-04
 */
@Api(value = "发票相关接口", description = "发票相关接口")
@Controller
@RequestMapping("/orderReceipt")
public class OrderReceiptController {

    @Resource
    private OrderReceiptService orderReceiptService;


    @RequestMapping(value = "/addOrderReceipt",method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value="添加发票")
    public RestRespResult<CashOutOrdersVo> addCashOutOrder(@RequestBody OrderReceiptVo vo) {
        OrderReceiptVo cashOutOrdersVo = orderReceiptService.addOrderReceipt(vo);
        RestRespResult result = RestRespResult.ok("添加发票成功");
        result.setData(cashOutOrdersVo);
        return result;
    }

    @RequestMapping(value = "/updateOrderReceipt",method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value="修改发票")
    public RestRespResult<String> updateCashOutOrder(@RequestBody OrderReceiptVo vo) {
        orderReceiptService.updateOrderReceipt(vo);
        return RestRespResult.ok("修改发票成功");
    }

    @RequestMapping(value = "/getOrderReceiptList",method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value="获取发票列表")
    public RestRespResult<DataTable<OrderReceiptVo>> getOrderReceiptList(@RequestBody OrderReceiptVo vo) {
        RestRespResult result = RestRespResult.ok("获取发票列表成功");
        DataTable<OrderReceiptVo> categoryList = orderReceiptService.getOrderReceiptList(vo);
        result.setData(categoryList);
        return result;
    }

    @RequestMapping(value = "/getOrderReceiptByOrderId",method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value="获取订单发票")
    public RestRespResult<DataTable<OrderReceiptVo>> getOrderReceiptByOrderId(@RequestBody OrderReceiptVo vo) {
        RestRespResult result = RestRespResult.ok("获取订单发票成功");
        OrderReceiptVo data = orderReceiptService.getOrderReceiptByOrderId(vo);
        result.setData(data);
        return result;
    }


}
