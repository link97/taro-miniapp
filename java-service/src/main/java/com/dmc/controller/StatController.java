package com.dmc.controller;

import com.dmc.dict.BenefitTranTypeDict;
import com.dmc.model.RestRespResult;
import com.dmc.service.PaymentService;
import com.dmc.service.UserAccountService;
import com.dmc.util.SessionUtil;
import com.dmc.util.id.IdUtil;
import com.dmc.vo.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 概况等统计信息相关接口
 * </p>
 *
 * @author dmc
 * @since 2020-09-04
 */
@Api(value = "概况等统计信息相关接口", description = "概况等统计信息相关接口",tags ={"StatController"})
@Controller
@RequestMapping("/stat")
public class StatController {

    @Resource
    private PaymentService paymentService;

    @Resource
    private UserAccountService userAccountService;

    @RequestMapping(value = "/getStatInfo",method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value="获取概况")
    public RestRespResult<StatInfoVo> getStatInfo() {
        StatInfoVo statInfoVo = paymentService.getStatInfo();
        RestRespResult result = RestRespResult.ok("获取概况成功");
        result.setData(statInfoVo);
        return result;
    }

    @RequestMapping(value = "/getIntegralDetail",method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value="积分明细")
    public RestRespResult<DataTable<IntegralDetailVo>> getIntegralDetail(IntegralDetailVo vo) {
        RestRespResult result = RestRespResult.ok("获取积分明细");
        DataTable<IntegralDetailVo> integralTransactionList=userAccountService.getUserIntegralDetailList(vo);
        result.setData(integralTransactionList);
        return result;
    }

    @RequestMapping(value = "/getTransactionDetail",method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value="交易明细")
    public RestRespResult<DataTable<PaymentVo>> getTransactionDetail(PaymentVo vo) {
        RestRespResult result = RestRespResult.ok("获取交易明细");
        DataTable<PaymentVo> paymentVoDataTable=paymentService.getTransactionDetailList(vo);
        result.setData(paymentVoDataTable);
        return result;
    }

    @RequestMapping(value = "/getBenefitTransaction",method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value="收益记录",tags ={"三期"})
    public RestRespResult<DataTable<BenefitTransactionVo>> getBenefitTransaction(@RequestBody BenefitTransactionVo vo) {
        RestRespResult result = RestRespResult.ok("获取收益记录");
        DataTable<BenefitTransactionVo> benefitVoDataTable=paymentService.getBenefitTransaction(vo);
        result.setData(benefitVoDataTable);
        return result;
    }

    @RequestMapping(value = "/getTransferBenefitTransaction",method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value="收益orderType type:{600:转账给他人,700:被别人转账}",tags ={"三期"})
    public RestRespResult<DataTable<BenefitTransactionVo>> getTransferBenefitTransaction(@RequestBody BenefitTransactionVo vo) {
        RestRespResult result = RestRespResult.ok("获取收益转让记录成功");
        Long currUid = SessionUtil.getCurrUid();
        Integer type = vo.getType();
        if (type == null){
            type = 600;
            vo.setType(type);
        }
        vo.setUserId(currUid);
        DataTable<BenefitTransactionVo> benefitVoDataTable=paymentService.getBenefitTransaction(vo);
        result.setData(benefitVoDataTable);
        return result;
    }
}
