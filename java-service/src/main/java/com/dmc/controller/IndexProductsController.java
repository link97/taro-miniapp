package com.dmc.controller;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollectionUtil;
import com.dmc.entity.CarouselInfo;
import com.dmc.entity.ProductsRecommend;
import com.dmc.model.RestListRespResult;
import com.dmc.model.RestResp;
import com.dmc.model.RestRespResult;
import com.dmc.service.CarouselInfoService;
import com.dmc.service.MessageService;
import com.dmc.service.ProductsRecommendService;
import com.dmc.vo.CarouselInfoVo;
import com.dmc.vo.DataTable;
import com.dmc.vo.DistributionGoodsVo;
import com.dmc.vo.MessageVo;
import io.swagger.annotations.*;
import org.apache.commons.lang.StringUtils;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 推荐商品 轮播图 前端控制器
 * </p>
 *
 * @author dmc
 * @since 2020-09-04
 */
@Api(value = "首页商品相关接口", description = "首页商品相关接口")
@Controller
@RequestMapping("/indexProducts")
public class IndexProductsController {
    @Resource
    private CarouselInfoService carouselInfoService;
    @Resource
    private ProductsRecommendService productsRecommendService;
    @Resource
    private MessageService messageService;


    @RequestMapping(value = "/getCarousel",method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value="获取轮播图")
    public RestListRespResult<CarouselInfoVo> getCarousel() {
        RestListRespResult<CarouselInfoVo> restRespResult=new RestListRespResult<CarouselInfoVo>();
        List<CarouselInfoVo> voList = new ArrayList<>();
        List<CarouselInfo> carouselInfoList=carouselInfoService.selectList(null);
        if(CollectionUtil.isNotEmpty(carouselInfoList)){
            for (CarouselInfo carouselInfo : carouselInfoList) {
                CarouselInfoVo vo = new CarouselInfoVo();
                BeanUtil.copyProperties(carouselInfo,vo);
                Integer goodsType = carouselInfo.getGoodsType();
                if(goodsType!=null && goodsType == 3){
                    vo.setGoodsVipId(carouselInfo.getGoodsId());
                    vo.setGoodsId(null);
                }
                voList.add(vo);
            }
        }
        restRespResult.setData(voList);
        return restRespResult;
    }

    @ApiOperation(value="添加轮播图")
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name = "imgUrl", value = "图片地址",required = true, dataType = "String", paramType = "query", defaultValue = "https://n.sinaimg.cn/sinakd202094s/201/w1080h721/20200904/f8a1-iytwsaz9557547.png"),
            @ApiImplicitParam(name = "urlLink", value = "链接地址",required = false, dataType = "String", paramType = "query", defaultValue = "https://news.sina.com.cn/c/2020-09-04/doc-iivhuipp2547495.shtml"),
            @ApiImplicitParam(name = "title", value = "标题",required = true, dataType = "String", paramType = "query", defaultValue = "解放军苏35资料图"),
            @ApiImplicitParam(name = "remarks", value = "描述",required = false, dataType = "String", paramType = "query", defaultValue = "解放军苏35资料图"),
            @ApiImplicitParam(name = "sort", value = "排序",required = true, dataType = "Integer", paramType = "query", defaultValue = "1"),
            @ApiImplicitParam(name = "delFlag", value = "逻辑删除标记（0：显示；1：隐藏）",required = true, dataType = "Integer", paramType = "query", defaultValue = "0"),
            @ApiImplicitParam(name = "createTime", value = "创建日期",required = false, dataType = "Date", paramType = "query"),
            @ApiImplicitParam(name = "updateTime", value = "更新日期",required = false, dataType = "Date", paramType = "query")
    })
    @RequestMapping(value = "/addCarousel",method = RequestMethod.POST)
    @ResponseBody
    public RestResp addCarousel(@RequestBody CarouselInfo carouselInfo) {
        Assert.notNull(carouselInfo, "参数不能为空");
        if(StringUtils.isBlank(carouselInfo.getImgUrl())) {
            Assert.isTrue(false, "轮播图片不能为空");
        }
        boolean result=carouselInfoService.insert(carouselInfo);
        if(result) {
            return RestResp.ok("添加成功！");
        }else{
            return RestResp.error(RestResp.ERROR,"添加失败！");
        }
    }
    @RequestMapping(value = "/editCarousel",method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value="编辑轮播图")
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name = "id", value = "id",required = true, dataType = "Long", paramType = "query", defaultValue = "1"),
            @ApiImplicitParam(name = "imgUrl", value = "图片地址",required = true, dataType = "String", paramType = "query", defaultValue = "https://n.sinaimg.cn/sinakd202094s/201/w1080h721/20200904/f8a1-iytwsaz9557547.png"),
            @ApiImplicitParam(name = "urlLink", value = "链接地址",required = false, dataType = "String", paramType = "query", defaultValue = "https://news.sina.com.cn/c/2020-09-04/doc-iivhuipp2547495.shtml"),
            @ApiImplicitParam(name = "title", value = "标题",required = true, dataType = "String", paramType = "query", defaultValue = "解放军苏35资料图"),
            @ApiImplicitParam(name = "remarks", value = "描述",required = false, dataType = "String", paramType = "query", defaultValue = "解放军苏35资料图"),
            @ApiImplicitParam(name = "sort", value = "排序",required = true, dataType = "Integer", paramType = "query", defaultValue = "1"),
            @ApiImplicitParam(name = "delFlag", value = "逻辑删除标记（0：显示；1：隐藏）",required = true, dataType = "Integer", paramType = "query", defaultValue = "0"),
            @ApiImplicitParam(name = "createTime", value = "创建日期",required = false, dataType = "Date", paramType = "query"),
            @ApiImplicitParam(name = "updateTime", value = "更新日期",required = false, dataType = "Date", paramType = "query")
    })
    public RestResp editCarousel(@RequestBody CarouselInfo carouselInfo) {
        Assert.notNull(carouselInfo, "参数不能为空");
        boolean result=carouselInfoService.updateAllColumnById(carouselInfo);
        if(result) {
            return RestResp.ok("更新成功！");
        }else{
            return RestResp.error(RestResp.ERROR,"更新失败！");
        }
    }
    @RequestMapping(value = "/delCarousel",method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value="删除轮播图")
    public RestResp delCarousel(Long id) {
        boolean result=carouselInfoService.deleteById(id);
        if(result) {
            return RestResp.ok("删除成功！");
        }else{
            return RestResp.error(RestResp.ERROR,"删除失败！");
        }
    }
    @RequestMapping(value = "/getProductsRecommend",method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value="获取推荐商品")
    public RestListRespResult<ProductsRecommend> getProductsRecommend() {
        RestListRespResult<ProductsRecommend> restRespResult=new RestListRespResult<ProductsRecommend>();
        List<ProductsRecommend> productsRecommendList=productsRecommendService.selectList(null);
        restRespResult.setData(productsRecommendList);
        return restRespResult;
    }
    @ApiOperation(value="添加推荐商品")
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name = "productsId", value = "商品ID",required = true, dataType = "Long", paramType = "query", defaultValue = "1"),
            @ApiImplicitParam(name = "sort", value = "排序",required = true, dataType = "Integer", paramType = "query", defaultValue = "1"),
            @ApiImplicitParam(name = "createTime", value = "创建日期",required = false, dataType = "Date", paramType = "query"),
            @ApiImplicitParam(name = "updateTime", value = "更新日期",required = false, dataType = "Date", paramType = "query")
    })
    @RequestMapping(value = "/addProductsRecommend",method = RequestMethod.POST)
    @ResponseBody
    public RestResp addProductsRecommend(@RequestBody ProductsRecommend productsRecommend) {
        Assert.notNull(productsRecommend, "参数不能为空");
        Assert.isTrue(productsRecommend.getProductsId()!=null, "轮播图片不能为空");
        boolean result=productsRecommendService.insert(productsRecommend);
        if(result) {
            return RestResp.ok("添加成功！");
        }else{
            return RestResp.error(RestResp.ERROR,"添加失败！");
        }
    }
    @RequestMapping(value = "/delProductsRecommend",method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value="删除推荐商品")
    public RestResp delProductsRecommend(Long id) {
        boolean result=productsRecommendService.deleteById(id);
        if(result) {
            return RestResp.ok("删除成功！");
        }else{
            return RestResp.error(RestResp.ERROR,"删除失败！");
        }
    }


    @RequestMapping(value = "/getPlatformMessage",method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value="获取站内信列表",tags={"三期"})
    public RestRespResult<List<DistributionGoodsVo>> getPlatformMessage(@ApiParam(value="0:未读 1:已读 不传查全部",name="status")@RequestParam(required=false) Integer status) {
        List<MessageVo> messageList = messageService.getPlatformMessage(status);
        RestRespResult result = RestRespResult.ok("获取站内信列表成功");
        result.setData(messageList);
        result.setTotal(messageList.size());
        return result;
    }

    @RequestMapping(value = "/getPlatformMessageNum",method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value="获取站内信列表数量",tags={"三期"})
    public RestRespResult<Integer> getPlatformMessageNum(@ApiParam(value="0:未读 1:已读 不传查全部",name="status")@RequestParam(required=false) Integer status) {
        int num = messageService.getPlatformMessageNum(status);
        RestRespResult result = RestRespResult.ok("获取站内信数量成功");
        result.setData(num);
        return result;
    }
}
