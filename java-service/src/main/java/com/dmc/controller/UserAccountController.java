package com.dmc.controller;

import com.dmc.entity.*;
import com.dmc.model.*;
import com.dmc.service.*;
import com.dmc.util.SessionUtil;
import com.dmc.util.util.ValidateUtil;
import com.dmc.vo.BankCardVo;
import com.dmc.vo.DataTable;
import com.dmc.vo.UserAuthenticateVo;
import com.dmc.vo.UserShippingAddressVo;
import com.sun.javafx.collections.MappingChange;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.beans.Transient;
import java.sql.Wrapper;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 身份认证 我的收货地址 身份认证 我的银行卡前端控制器
 * </p>
 *
 * @author dmc
 * @since 2020-09-04
 */
@Api(value = "首页用户相关接口", description = "首页用户相关接口",tags = {"UserAccountController"})
@Controller
@RequestMapping("/indexUser")
public class UserAccountController {
    @Resource
    private UserAuthenticateService userAuthenticateService;
    @Resource
    private UserBankCardService userBankCardService;
    @Resource
    private UserShippingAddressService userShippingAddressService;
    @Resource
    private BankInfoService bankInfoService;
    @Resource
    private UserAccountService userAccountService;
    @Resource
    private UserService userService;
    //我的收货地址
    @RequestMapping(value = "ShippingAddress",method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value="获取我的收货地址")
    public RestListRespResult<UserShippingAddress> getShippingAddress() {
        RestListRespResult<UserShippingAddress> restRespResult=new RestListRespResult<UserShippingAddress>();
        Map<String,Object> paramMap = new HashMap<>();
        paramMap.put("user_id", SessionUtil.getCurrUid());
        List<UserShippingAddress> userShippingAddressList=userShippingAddressService.selectByMap(paramMap);
        restRespResult.setData(userShippingAddressList);
        return restRespResult;
    }


    //我的收货地址
    @RequestMapping(value = "getShippingAddress",method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value="获取我的收货地址")
    public RestListRespResult<UserShippingAddressVo> getShippingAddress1() {
        RestListRespResult<UserShippingAddressVo> restRespResult=new RestListRespResult<UserShippingAddressVo>();
        Map<String,Object> paramMap = new HashMap<>();
        Long currUid = SessionUtil.getCurrUid();
        User user = userService.get(currUid);
        paramMap.put("user_id", currUid);
        List<UserShippingAddressVo> voList = new ArrayList<>();
        List<UserShippingAddress> userShippingAddressList=userShippingAddressService.selectByMap(paramMap);
        if(!CollectionUtils.isEmpty(userShippingAddressList)){
            for (UserShippingAddress userShippingAddress : userShippingAddressList) {
                UserShippingAddressVo vo = new UserShippingAddressVo();
                BeanUtils.copyProperties(userShippingAddress,vo);
                vo.setUserPhoto(user.getUserPhoto());
                voList.add(vo);
            }
        }

        restRespResult.setData(voList);
        return restRespResult;
    }

    @ApiOperation(value="添加我的收货地址")
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name = "userId", value = "用户id",required = true, dataType = "Long", paramType = "query", defaultValue = "1"),
            @ApiImplicitParam(name = "receiverName", value = "收货姓名",required = false, dataType = "String", paramType = "query", defaultValue = "赠送"),
            @ApiImplicitParam(name = "receiverMobile", value = "收货移动电话",required = true, dataType = "String", paramType = "query", defaultValue = "15701065824"),
            @ApiImplicitParam(name = "receiverDistrict", value = "区/县",required = false, dataType = "String", paramType = "query", defaultValue = "北京"),
            @ApiImplicitParam(name = "receiverAddress", value = "详细地址",required = false, dataType = "String", paramType = "query", defaultValue = "东城区18号楼一单元101"),
            @ApiImplicitParam(name = "receiverZip", value = "邮编",required = false, dataType = "String", paramType = "query", defaultValue = "10000001"),
            @ApiImplicitParam(name = "defaultFlag", value = "默认收货地址0 默认；1 默认地址",required = true, dataType = "Integer", paramType = "query", defaultValue = "0"),
            @ApiImplicitParam(name = "createTime", value = "创建日期",required = false, dataType = "Date", paramType = "query"),
            @ApiImplicitParam(name = "updateTime", value = "更新日期",required = false, dataType = "Date", paramType = "query")
    })
    @RequestMapping(value = "/addShippingAddress",method = RequestMethod.POST)
    @ResponseBody
    public RestResp addShippingAddress(@RequestBody UserShippingAddress userShippingAddress) {
        Assert.notNull(userShippingAddress, "参数不能为空");
        Assert.isTrue(userShippingAddress.getUserId()!=null, "用户ID不能为空");
        if(StringUtils.isBlank(userShippingAddress.getReceiverName())) {
            Assert.isTrue(false, "收货人不能为空");
        }
        if(StringUtils.isBlank(userShippingAddress.getReceiverMobile())) {
            Assert.isTrue(false, "收货人电话不能为空");
        }
        if(!ValidateUtil.phone(userShippingAddress.getReceiverMobile())){
            Assert.isTrue(false, "手机号码有误");
        }
        Map<String,Object> paramMap = new HashMap<>();
        paramMap.put("user_id", SessionUtil.getCurrUid());
        paramMap.put("default_flag", 1);
        List<UserShippingAddress> userShippingAddressList=userShippingAddressService.selectByMap(paramMap);
        if(!CollectionUtils.isEmpty(userShippingAddressList)){
            //把该用户的收货地址置为不是默认
            for (UserShippingAddress u:userShippingAddressList){
                u.setDefaultFlag(0);
            }
            userShippingAddressService.updateAllColumnBatchById(userShippingAddressList);
        }
        boolean result=userShippingAddressService.insert(userShippingAddress);
        if(result) {
            return RestResp.ok("添加成功！");
        }else{
            return RestResp.ok("添加失败！");
        }
    }
    @RequestMapping(value = "/editShippingAddress",method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value="编辑我的收货地址")
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name = "id", value = "id",required = true, dataType = "Integer", paramType = "query", defaultValue = "1"),
            @ApiImplicitParam(name = "userId", value = "用户id",required = true, dataType = "Long", paramType = "query", defaultValue = "1"),
            @ApiImplicitParam(name = "receiverName", value = "收货姓名",required = false, dataType = "String", paramType = "query", defaultValue = "赠送111"),
            @ApiImplicitParam(name = "receiverMobile", value = "收货移动电话",required = true, dataType = "String", paramType = "query", defaultValue = "15701065824"),
            @ApiImplicitParam(name = "receiverDistrict", value = "区/县",required = false, dataType = "String", paramType = "query", defaultValue = "北京"),
            @ApiImplicitParam(name = "receiverAddress", value = "详细地址",required = false, dataType = "String", paramType = "query", defaultValue = "东城区18号楼一单元101"),
            @ApiImplicitParam(name = "receiverZip", value = "邮编",required = false, dataType = "String", paramType = "query", defaultValue = "1000"),
            @ApiImplicitParam(name = "defaultFlag", value = "默认收货地址0 默认；1 默认地址",required = true, dataType = "Integer", paramType = "query", defaultValue = "0"),
            @ApiImplicitParam(name = "createTime", value = "创建日期",required = false, dataType = "Date", paramType = "query"),
            @ApiImplicitParam(name = "updateTime", value = "更新日期",required = false, dataType = "Date", paramType = "query")
    })
    public RestResp editShippingAddress(@RequestBody UserShippingAddress userShippingAddress) {
        Assert.notNull(userShippingAddress, "参数不能为空");
        boolean result=userShippingAddressService.updateAllColumnById(userShippingAddress);
        if(result) {
            return RestResp.ok("更新成功！");
        }else{
            return RestResp.error(RestResp.ERROR,"更新失败！");
        }
    }
    @RequestMapping(value = "/delShippingAddress",method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value="删除我的收货地址")
    public RestResp delShippingAddress(Integer id) {
        boolean result=userShippingAddressService.deleteById(id);
        if(result) {
            return RestResp.ok("删除成功！");
        }else{
            return RestResp.error(RestResp.ERROR,"删除失败！");
        }
    }
    //身份认证
    @RequestMapping(value = "/getAuthenticate",method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value="获取身份认证记录")
    public RestRespResult<UserAuthenticate> getAuthenticate() {
        RestRespResult result = RestRespResult.ok("获取数据成功");
        UserAuthenticate userAuthenticate=userAuthenticateService.getUserAuthenticateByUserId(SessionUtil.getCurrUid());
        if(userAuthenticate!=null && userAuthenticate.getStatus() == 1){
            userAuthenticate.setImgUrl("");
            userAuthenticate.setNegativeImgUrl("");
        }
        result.setData(userAuthenticate);
        return result;
    }
    @RequestMapping(value = "/getAuthenticateAll",method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value="获取所有身份认证记录")
    public RestRespResult<DataTable<UserAuthenticateVo>> getAuthenticateAll(UserAuthenticateVo userAuthenticate) {
        RestRespResult result = RestRespResult.ok("获取所有身份认证记录成功");
        DataTable<UserAuthenticateVo> UserAuthenticateVoList=userAuthenticateService.getUserAuthenticateAll(userAuthenticate);
        result.setData(UserAuthenticateVoList);
        return result;
    }
    @ApiOperation(value="添加身份认证记录")
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name = "userId", value = "用户id",required = true, dataType = "Long", paramType = "query", defaultValue = "1"),
            @ApiImplicitParam(name = "fullName", value = "姓名",required = false, dataType = "String", paramType = "query", defaultValue = "小王"),
            @ApiImplicitParam(name = "identificationNo", value = "身份证号码",required = false, dataType = "String", paramType = "query", defaultValue = "110101199003079833"),
            @ApiImplicitParam(name = "channels", value = "认证渠道",required = false, dataType = "Integer", paramType = "query", defaultValue = "0"),
            @ApiImplicitParam(name = "status", value = "认证状态,0提交成功等待审核 1 认证成功 2 不是有效的身份证信息",required = true, dataType = "Integer", paramType = "query", defaultValue = "1"),
            @ApiImplicitParam(name = "imgUrl", value = "身份证图片",required = false, dataType = "String", paramType = "query", defaultValue = "https://n.sinaimg.cn/sinakd202094s/201/w1080h721/20200904/f8a1-iytwsaz9557547.png"),
            @ApiImplicitParam(name = "negativeImgUrl", value = "身份证反面图片",required = false, dataType = "String", paramType = "query", defaultValue = "https://n.sinaimg.cn/sinakd202094s/201/w1080h721/20200904/f8a1-iytwsaz9557547.png"),
            @ApiImplicitParam(name = "checkTime", value = "认证通过日期",required = false, dataType = "Date", paramType = "query"),
            @ApiImplicitParam(name = "createTime", value = "创建日期",required = false, dataType = "Date", paramType = "query"),
            @ApiImplicitParam(name = "updateTime", value = "更新日期",required = false, dataType = "Date", paramType = "query")
    })
    @RequestMapping(value = "/addAuthenticate",method = RequestMethod.POST)
    @ResponseBody
    public RestResp addAuthenticate(@RequestBody UserAuthenticate userAuthenticate) {
        Assert.notNull(userAuthenticate, "参数不能为空");
        Assert.isTrue(userAuthenticate.getUserId()!=null, "用户ID不能为空");
        if(StringUtils.isBlank(userAuthenticate.getImgUrl())) {
            Assert.isTrue(false, "认证人身份证图片不能为空");
        }
        UserAuthenticate userAuthenticateList=userAuthenticateService.getUserAuthenticateByUserId(userAuthenticate.getUserId());
        boolean result=false;
        if(userAuthenticateList!=null){
            userAuthenticate.setId(userAuthenticateList.getId());
            result=userAuthenticateService.updateAllColumnById(userAuthenticate);
        }else{
            result=userAuthenticateService.insert(userAuthenticate);
        }
        if(result) {
            return RestResp.ok("添加成功！");
        }else{
            return RestResp.error(RestResp.ERROR,"添加失败！");
        }
    }
    @RequestMapping(value = "/editAuthenticate",method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value="编辑身份认证记录")
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name = "id", value = "id",required = true, dataType = "Integer", paramType = "query", defaultValue = "1"),
            @ApiImplicitParam(name = "userId", value = "用户id",required = true, dataType = "Long", paramType = "query", defaultValue = "1"),
            @ApiImplicitParam(name = "fullName", value = "姓名",required = false, dataType = "String", paramType = "query", defaultValue = "小王"),
            @ApiImplicitParam(name = "identificationNo", value = "身份证号码",required = false, dataType = "String", paramType = "query", defaultValue = "110101199003079833"),
            @ApiImplicitParam(name = "channels", value = "认证渠道",required = false, dataType = "Integer", paramType = "query", defaultValue = "0"),
            @ApiImplicitParam(name = "status", value = "认证状态",required = true, dataType = "Integer", paramType = "query", defaultValue = "1"),
            @ApiImplicitParam(name = "imgUrl", value = "身份证图片",required = false, dataType = "String", paramType = "query", defaultValue = "https://n.sinaimg.cn/sinakd202094s/201/w1080h721/20200904/f8a1-iytwsaz9557547.png"),
            @ApiImplicitParam(name = "negativeImgUrl", value = "身份证反面图片",required = false, dataType = "String", paramType = "query", defaultValue = "https://n.sinaimg.cn/sinakd202094s/201/w1080h721/20200904/f8a1-iytwsaz9557547.png"),
            @ApiImplicitParam(name = "checkTime", value = "认证通过日期",required = false, dataType = "Date", paramType = "query"),
            @ApiImplicitParam(name = "createTime", value = "创建日期",required = false, dataType = "Date", paramType = "query"),
            @ApiImplicitParam(name = "updateTime", value = "更新日期",required = false, dataType = "Date", paramType = "query")
    })
    public RestResp editAuthenticate(@RequestBody UserAuthenticate userAuthenticate) {
        Assert.notNull(userAuthenticate, "参数不能为空");
        boolean result=userAuthenticateService.updateAllColumnById(userAuthenticate);
        if(result) {
            return RestResp.ok("更新成功！");
        }else{
            return RestResp.error(RestResp.ERROR,"更新失败！");
        }
    }
    @RequestMapping(value = "/delAuthenticate",method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value="删除身份认证记录")
    public RestResp delAuthenticate(Integer id) {
        boolean result=userAuthenticateService.deleteById(id);
        if(result) {
            return RestResp.ok("删除成功！");
        }else{
            return RestResp.error(RestResp.ERROR,"删除失败！");
        }
    }
//我的银行卡
    @RequestMapping(value = "/getBankCard",method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value="获取银行卡")
    public RestListRespResult<BankCardVo> getBankCard() {
        RestListRespResult<BankCardVo> restRespResult=new RestListRespResult<BankCardVo>();
        BankCardVo bankCardVo=new BankCardVo();
        bankCardVo.setUserId(SessionUtil.getCurrUid());
        List<BankCardVo> userBankCardList=userBankCardService.getBankCardList(bankCardVo);
        restRespResult.setData(userBankCardList);
        return restRespResult;
    }
    @ApiOperation(value="添加银行卡")
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name = "userId", value = "用户id",required = true, dataType = "Long", paramType = "query", defaultValue = "1"),
            @ApiImplicitParam(name = "bankId", value = "银行卡所属银行ID",required = false, dataType = "Integer", paramType = "query", defaultValue = "1"),
            @ApiImplicitParam(name = "verifyCode", value = "手机验证码",required = false, dataType = "Integer", paramType = "query", defaultValue = "1001"),
            @ApiImplicitParam(name = "cardId", value = "银行卡号",required = true, dataType = "Long", paramType = "query", defaultValue = "2222222222222222221"),
            @ApiImplicitParam(name = "cardName", value = "持卡人",required = false, dataType = "String", paramType = "query",defaultValue = "信鸽"),
            @ApiImplicitParam(name = "cardMobile", value = "银行绑定手机号码",required = false, dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "delFlag", value = "逻辑删除标记（0：显示；1：隐藏）",required = true, dataType = "Integer", paramType = "query", defaultValue = "0"),
            @ApiImplicitParam(name = "createTime", value = "创建日期",required = false, dataType = "Date", paramType = "query"),
            @ApiImplicitParam(name = "updateTime", value = "更新日期",required = false, dataType = "Date", paramType = "query")
    })
    @RequestMapping(value = "/addBankCard",method = RequestMethod.POST)
    @ResponseBody
    public RestResp addBankCard(@RequestBody BankCardVo userBankCard) {
        Assert.notNull(userBankCard, "参数不能为空");
        Assert.isTrue(userBankCard.getUserId()!=null, "用户ID不能为空");
        Assert.isTrue(userBankCard.getCardId()!=null, "银行卡号不能为空");
        Assert.isTrue(userBankCard.getCardMobile()!=null, "银行绑定手机号码不能为空");
        if(!ValidateUtil.phone(userBankCard.getCardMobile())){
            Assert.isTrue(false, "手机号码有误");
        }
        Assert.isTrue(userBankCard.getCardName()!=null, "持卡人不能为空");
        Assert.isTrue(userBankCard.getVerifyCode()!=null, "验证码不能为空");
        boolean result=userBankCardService.addBankCard(userBankCard);
        if(result) {
            return RestResp.ok("添加成功！");
        }else{
            return RestResp.error(RestResp.ERROR,"添加失败！");
        }
    }
    @RequestMapping(value = "/editBankCard",method = RequestMethod.POST)
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name = "id", value = "id",required = true, dataType = "Integer", paramType = "query", defaultValue = "1"),
            @ApiImplicitParam(name = "userId", value = "用户id",required = true, dataType = "Long", paramType = "query", defaultValue = "1"),
            @ApiImplicitParam(name = "type", value = "银行卡所属银行,0未知",required = false, dataType = "Integer", paramType = "query", defaultValue = "0"),
            @ApiImplicitParam(name = "cardId", value = "银行卡号",required = true, dataType = "Long", paramType = "query", defaultValue = "2222222222222222221"),
            @ApiImplicitParam(name = "openDate", value = "开卡日期",required = false, dataType = "Date", paramType = "query"),
            @ApiImplicitParam(name = "remarks", value = "备注",required = false, dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "delFlag", value = "逻辑删除标记（0：显示；1：隐藏）",required = true, dataType = "Integer", paramType = "query", defaultValue = "0"),
            @ApiImplicitParam(name = "createTime", value = "创建日期",required = false, dataType = "Date", paramType = "query"),
            @ApiImplicitParam(name = "updateTime", value = "更新日期",required = false, dataType = "Date", paramType = "query")
    })
    @ResponseBody
    @ApiOperation(value="编辑银行卡")
    public RestResp editBankCard(@RequestBody UserBankCard userBankCard) {
        Assert.notNull(userBankCard, "参数不能为空");
        boolean result=userBankCardService.updateAllColumnById(userBankCard);
        if(result) {
            return RestResp.ok("更新成功！");
        }else{
            return RestResp.error(RestResp.ERROR,"更新失败！");
        }
    }
    @RequestMapping(value = "/delBankCard",method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value="删除银行卡")
    public RestResp delBankCard(Integer id) {
        boolean result=userBankCardService.deleteById(id);
        if(result) {
            return RestResp.ok("删除成功！");
        }else{
            return RestResp.error(RestResp.ERROR,"删除失败！");
        }
    }

    @RequestMapping(value = "/getBankInfo",method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value="获取银行信息")
    public RestListRespResult<BankInfo> getBankInfo() {
        RestListRespResult<BankInfo> restRespResult=new RestListRespResult<BankInfo>();
        List<BankInfo> bankInfoList=bankInfoService.selectList(null);
        restRespResult.setData(bankInfoList);
        return restRespResult;
    }
    @ApiOperation(value="添加银行信息")
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name = "bankName", value = "银行名称",required = false, dataType = "String", paramType = "query", defaultValue = "中国银行"),
    })
    @RequestMapping(value = "/addBankInfo",method = RequestMethod.POST)
    @ResponseBody
    public RestResp addBankInfo(@RequestBody BankInfo bankInfo) {
        Assert.notNull(bankInfo, "参数不能为空");
        Assert.isTrue(bankInfo.getBankName()!=null, "银行名称不能为空");
        Assert.isTrue(bankInfo.getBankImg()!=null, "银行logo不能为空");
        boolean result=bankInfoService.insert(bankInfo);
        if(result) {
            return RestResp.ok("添加成功！");
        }else{
            return RestResp.error(RestResp.ERROR,"添加失败！");
        }
    }
    @RequestMapping(value = "/editBank",method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value="编辑银行")
    public RestResp editBank(@RequestBody BankInfo bankInfo) {
        Assert.notNull(bankInfo, "参数不能为空");
        boolean result=bankInfoService.updateAllColumnById(bankInfo);
        if(result) {
            return RestResp.ok("更新成功！");
        }else{
            return RestResp.error(RestResp.ERROR,"更新失败！");
        }
    }
    @RequestMapping(value = "/delBank",method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value="删除银行")
    public RestResp delBank(Integer bankId) {
        boolean result=bankInfoService.deleteById(bankId);
        if(result) {
            return RestResp.ok("删除成功！");
        }else{
            return RestResp.error(RestResp.ERROR,"删除失败！");
        }
    }
    @RequestMapping(value = "/getUserIntegral",method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value="获取用户积分信息")
    public RestRespResult<TUserAccount> getUserIntegral() {
        RestRespResult restRespResult=RestRespResult.ok("获取信息成功");
        TUserAccount userAccount=userAccountService.initUserAccountByUserId(SessionUtil.getCurrUid());
        restRespResult.setData(userAccount);
        return restRespResult;
    }
    @RequestMapping(value = "/getUserIntegralDetail",method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value="获取用户积分使用记录")
    public RestRespResult<TUserAccount> getUserIntegralDetail() {
        RestRespResult restRespResult=RestRespResult.ok("获取信息成功");
        List<IntegralTransaction> integralTransactionList=userAccountService.getUserIntegralList(SessionUtil.getCurrUid());
        restRespResult.setData(integralTransactionList);
        return restRespResult;
    }

    @RequestMapping(value = "/getUserMessage",method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value="获取用户消息列表")
    public RestRespResult<Message> getUserMessage(Long userId) {
        RestRespResult restRespResult=RestRespResult.ok("获取信息成功");
        Assert.isTrue(userId!=null,"参数userId不能为空");
        List<Message> messageList=userAccountService.getUserMessage(userId);
        restRespResult.setData(messageList);
        return restRespResult;
    }

    @RequestMapping(value = "/checkPayPassword",method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value="验证支付密码",tags = {"三期"})
    public RestRespResult<Message> checkPayPassword(Long userId,String password) {
        RestRespResult restRespResult=RestRespResult.ok("验证支付密码成功");
        userAccountService.checkPayPassword(userId,password);
        return restRespResult;
    }

    @RequestMapping(value = "/addPayPassword",method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value="添加支付密码",tags = {"三期"})
    public RestRespResult<Message> addPayPassword(Long userId,String password) {
        RestRespResult restRespResult=RestRespResult.ok("添加支付密码成功");
        userAccountService.addPayPassword(userId,password);
        return restRespResult;
    }

    @RequestMapping(value = "/updatePayPassword",method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value="修改支付密码",tags = {"三期"})
    public RestRespResult<Message> updatePayPassword(Long userId,String password,String passwordOld) {
        RestRespResult restRespResult=RestRespResult.ok("修改支付密码成功");
        userAccountService.updatePayPassword(userId,password,passwordOld);
        return restRespResult;
    }

    @RequestMapping(value = "/updatePayPasswordForPc",method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value="后台修改支付密码",tags = {"三期"})
    public RestRespResult<Message> updatePayPasswordForPc(Long userId,String payPassWord) {
        RestRespResult restRespResult=RestRespResult.ok("修改支付密码成功");
        userAccountService.updatePayPasswordForPc(userId,payPassWord);
        return restRespResult;
    }

    @RequestMapping(value = "/checkHavePayPassword",method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value="校验是否有支付密码",tags = {"三期"})
    public RestRespResult<Boolean> checkHavePayPassword(Long userId) {
        RestRespResult restRespResult=RestRespResult.ok("修改支付密码成功");
        Boolean have = userAccountService.checkHavePayPassword(userId);
        restRespResult.setData(have);
        return restRespResult;
    }
}
