package com.dmc.controller;

import cn.hutool.core.lang.Assert;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.dmc.conf.Constant;
import com.dmc.dict.UserLevelDict;
import com.dmc.dto.AuthTokenDTO;
import com.dmc.jwt.AuthTokenDetails;
import com.dmc.jwt.JsonWebTokenUtility;
import com.dmc.model.*;
import com.dmc.service.PlatformBankCardService;
import com.dmc.service.UserAccountService;
import com.dmc.service.UserService;
import com.dmc.util.SessionUtil;
import com.dmc.util.util.WXBizDataCrypt;
import com.dmc.vo.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.*;

/**
 * 平台银行卡信息控制器
 */
@RestController
@RequestMapping("/platformBankCard")
@Slf4j
@Api(value = "平台银行卡信息相關接口",tags = {"PlatformBankCardController"})
public class PlatformBankCardController {

    @Autowired
    private PlatformBankCardService platformBankCardService;


    @RequestMapping(value = "/getBankList",method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value="获取平台银行卡信息",tags = {"二期"})
    public RestListRespResult<PlatformBankCardVo> getBankList(@RequestBody(required=false) PlatformBankCardVo vo) {
        List<PlatformBankCardVo> voList = platformBankCardService.getBankList(vo);
        RestListRespResult result=new RestListRespResult();
        result.setData(voList);
        return result;
    }


    @RequestMapping(value = "/saveOrUpdate",method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value="添加或者编辑平台银行卡信息",tags = {"二期"})
    public RestRespResult<String> saveOrUpdate(@RequestBody PlatformBankCardVo vo) {
        platformBankCardService.saveOrUpdate(vo);
        RestRespResult result = RestRespResult.ok("添加或者编辑平台银行卡信息成功");
        return result;
    }

    @RequestMapping(value = "/editStop",method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value="设置收款或者非收款",tags = {"二期"})
    public RestRespResult<String> editStop(@RequestBody PlatformBankCardVo vo) {
        platformBankCardService.editStop(vo);
        RestRespResult result = RestRespResult.ok("设置成功");
        return result;
    }

    @RequestMapping(value = "/delete",method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value="删除平台银行卡信息",tags = {"二期"})
    public RestRespResult<String> delete(PlatformBankCardVo vo) {
        platformBankCardService.delete(vo);
        RestRespResult result = RestRespResult.ok("删除成功");
        return result;
    }


}
