/*
 * Copyright (c) 2016 xiaomaihd and/or its affiliates.All Rights Reserved.
 *            http://www.xiaomaihd.com
 */
package com.dmc.jwt;

import cn.hutool.core.lang.Assert;
import cn.hutool.core.util.StrUtil;
import com.dmc.conf.UrlThreadLocal;
import com.dmc.conf.WebSecurityConfig;
import com.dmc.model.User;
import com.dmc.service.UserService;
import com.dmc.service.impl.UserServiceImpl;
import com.dmc.service.impl.WeChatPayServiceImpl;
import com.dmc.util.util.SpringApplicationContextUtil;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.extern.slf4j.Slf4j;

import javax.crypto.spec.SecretKeySpec;
import java.security.Key;
import java.util.Base64;
import java.util.Date;
import java.util.List;

/**
 * Created by YangFan on 2016/11/28 上午10:01.
 * <p/>
 */
@Slf4j
public class JsonWebTokenUtility {
    private SignatureAlgorithm signatureAlgorithm;
    private Key secretKey;

    public JsonWebTokenUtility() {

        // 这里不是真正安全的实践
        // 为了简单，我们存储一个静态key在这里，
        signatureAlgorithm = SignatureAlgorithm.HS512;
        String encodedKey =
                "L7A/6zARSkK1j7Vd5SDD9pSSqZlqF7mAhiOgRbgv9Smce6tf4cJnvKOjtKPxNNnWQj+2lQEScm3XIUjhW+YVZg==";
        secretKey = deserializeKey(encodedKey);
    }

    public String createJsonWebToken(AuthTokenDetails authTokenDetails) {
        String token =
                Jwts.builder().setSubject(authTokenDetails.getId().toString())
                        .claim("username", authTokenDetails.getUsername())
                        .claim("roleNames", authTokenDetails.getRoleNames())
                        .setExpiration(authTokenDetails.getExpirationDate())
                        .signWith(getSignatureAlgorithm(),
                                getSecretKey()).compact();
        return token;
    }

    private Key deserializeKey(String encodedKey) {
        byte[] decodedKey = Base64.getDecoder().decode(encodedKey);
        Key key =
                new SecretKeySpec(decodedKey, getSignatureAlgorithm().getJcaName());
        return key;
    }

    private Key getSecretKey() {
        return secretKey;
    }

    public SignatureAlgorithm getSignatureAlgorithm() {
        return signatureAlgorithm;
    }

    public AuthTokenDetails parseAndValidate(String token) {
        AuthTokenDetails authTokenDetails = null;
        try {
            Claims claims =
                    Jwts.parser().setSigningKey(getSecretKey()).parseClaimsJws(token).getBody();
            String userId = claims.getSubject();
            Long userLong = Long.valueOf(userId);

            String requestUrl = UrlThreadLocal.getuRL();
            boolean contains = WebSecurityConfig.stopExUrls.contains("{" + requestUrl + "}");
            if(!contains){
                //如果不属于禁用也能调用的接口,则中断调用
                //获取用户,查看 用户是否禁用
                UserService userService = SpringApplicationContextUtil.getBean(UserServiceImpl.class);
                User user = userService.get(userLong);
                //您的账户因为违规扰乱市场,影像市场运营,平台作封号处理,如需申诉请联系平台客服
                if(user == null){
                    throw new IllegalArgumentException("请重新登陆");
                }else{
                    Assert.isTrue(user!=null && user.getStop() == 0,"您的账户因为"+user.getStopDes()+",平台作封号处理,如需申诉请联系平台客服" );
                }
            }

            String username = (String) claims.get("username");
            List<String> roleNames = (List) claims.get("roleNames");
            Date expirationDate = claims.getExpiration();

            authTokenDetails = new AuthTokenDetails();
            authTokenDetails.setId(userLong);
            authTokenDetails.setUsername(username);
            authTokenDetails.setRoleNames(roleNames);
            authTokenDetails.setExpirationDate(expirationDate);
        } catch (JwtException ex) {
            log.error(ex.getMessage(), ex);
        }
        return authTokenDetails;
    }

    private String serializeKey(Key key) {
        String encodedKey =
                Base64.getEncoder().encodeToString(key.getEncoded());
        return encodedKey;
    }
}
