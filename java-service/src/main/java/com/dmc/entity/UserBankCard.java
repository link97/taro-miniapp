package com.dmc.entity;

import com.baomidou.mybatisplus.enums.IdType;

import java.beans.Transient;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;



import com.baomidou.mybatisplus.annotations.Version;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 我的银行卡
 * </p>
 *
 * @
 * @since 2020-09-05
 */
@Data
@Accessors(chain = true)
@TableName("t_user_bank_card")
@ApiModel("我的银行卡")
public class UserBankCard extends Model<UserBankCard> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    @ApiModelProperty(value="主键",name = "id")
    private Integer id;

    /**
     * 用户id
     */
    @TableField("user_id")
    @ApiModelProperty(value="用户id",name = "userId")
    private Long userId;

    /**
     * 银行卡所属银行ID
     */
    @TableField("bank_id")
    @ApiModelProperty(value="bankId",name = "银行卡所属银行ID")
    private Integer bankId;

    /**
     * 银行卡号
     */
    @TableField("card_id")
    @ApiModelProperty(value="银行卡号",name = "cardId")
    private Long cardId;

    /**
     * 银行绑定手机号码
     */
    @TableField("card_mobile")
    @ApiModelProperty(value="银行绑定手机号码",name = "cardMobile")
    private String cardMobile;

    /**
     * 逻辑删除标记（0：显示；1：隐藏）
     */
    @TableField("del_flag")
    @ApiModelProperty(value="逻辑删除标记（0：显示；1：隐藏）",name = "delFlag")
    private Integer delFlag;

    @TableField("create_time")
    @ApiModelProperty(value="创建时间",name = "createTime")
    private Date createTime;

    @TableField("update_time")
    @ApiModelProperty(value="更新时间",name = "updateTime")
    private Date updateTime;
    /**
     * 持卡人
     */
    @TableField("card_name")
    @ApiModelProperty(value="持卡人",name = "cardName")
    private String cardName;
    //默认标志
    @TableField("default_flag")
    @ApiModelProperty(value="默认标志",name = "defaultFlag")
    private Integer defaultFlag;
    //银行名称
    @TableField(exist = false)
    @ApiModelProperty(value="银行名称",name = "bankName")
    private String bankName;
    //银行标志
    @TableField(exist = false)
    @ApiModelProperty(value="银行标志",name = "bankImg")
    private String bankImg;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
