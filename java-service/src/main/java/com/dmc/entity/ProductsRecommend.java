package com.dmc.entity;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;



import com.baomidou.mybatisplus.annotations.Version;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 推荐商品
 * </p>
 *
 * @
 * @since 2020-09-05
 */
@Data
@Accessors(chain = true)
@TableName("t_products_recommend")
@ApiModel("推荐商品")
public class ProductsRecommend extends Model<ProductsRecommend> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 商品ID
     */
    @TableField("products_id")
    @ApiModelProperty(value="商品ID",name = "productsId")
    private Long productsId;

    /**
     * 排序
     */
    @ApiModelProperty(value="排序",name = "sort")
    private Integer sort;

    @TableField("create_time")
    @ApiModelProperty(value="更新时间",name = "createTime")
    private Date createTime;

    @TableField("update_time")
    @ApiModelProperty(value="更新时间",name = "updateTime")
    private Date updateTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
