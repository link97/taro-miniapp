package com.dmc.entity;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;



import com.baomidou.mybatisplus.annotations.Version;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 我的收货地址
 * </p>
 *
 * @
 * @since 2020-09-05
 */
@Data
@Accessors(chain = true)
@TableName("t_user_shipping_address")
@ApiModel("我的收货地址")
public class UserShippingAddress extends Model<UserShippingAddress> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    @ApiModelProperty(value="主键",name = "id")
    private Integer id;

    /**
     * 用户id
     */
    @TableField("user_id")
    @ApiModelProperty(value="用户id",name = "userId")
    private Long userId;

    /**
     * 收货姓名
     */
    @TableField("receiver_name")
    @ApiModelProperty(value="收货姓名",name = "receiverName")
    private String receiverName;

    /**
     * 收货移动电话
     */
    @TableField("receiver_mobile")
    @ApiModelProperty(value="收货移动电话",name = "receiverMobile")
    private String receiverMobile;

    /**
     * 区/县
     */
    @TableField("receiver_district")
    @ApiModelProperty(value="区/县",name = "receiverDistrict")
    private String receiverDistrict;

    /**
     * 详细地址
     */
    @TableField("receiver_address")
    @ApiModelProperty(value="详细地址",name = "receiverAddress")
    private String receiverAddress;

    /**
     * 邮编
     */
    @TableField("receiver_zip")
    @ApiModelProperty(value="邮编",name = "receiverZip")
    private String receiverZip;

    /**
     * 默认收货地址0 默认；1 默认地址
     */
    @TableField("default_flag")
    @ApiModelProperty(value="默认收货地址0 默认；1 默认地址",name = "defaultFlag")
    private Integer defaultFlag;

    @TableField("create_time")
    @ApiModelProperty(value="创建时间",name = "createTime")
    private Date createTime;

    @TableField("update_time")
    @ApiModelProperty(value="更新时间",name = "updateTime")
    private Date updateTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
