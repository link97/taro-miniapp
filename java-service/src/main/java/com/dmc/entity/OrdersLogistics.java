package com.dmc.entity;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;



import com.baomidou.mybatisplus.annotations.Version;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 订单物流表
 * </p>
 *
 * @
 * @since 2020-09-12
 */
@Data
@Accessors(chain = true)
@TableName("t_orders_logistics")
@ApiModel("订单物流信息")
public class OrdersLogistics extends Model<OrdersLogistics> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(value = "id", type = IdType.AUTO)
    @ApiModelProperty(value="主键",name = "id")
    private Integer id;

    /**
     * 订单号
     */
    @TableField("orders_no")
    @ApiModelProperty(value="订单号",name = "ordersNo")
    private String ordersNo;

    /**
     * 物流ID
     */
    @TableField("logistics_id")
    @ApiModelProperty(value="物流ID",name = "logisticsId")
    private Integer logisticsId;

    /**
     * 物流公司
     */
    @ApiModelProperty(value="物流公司",name = "logisticsName")
    @TableField("logistics_name")
    private String logisticsName;
    /**
     * 退换货地址
     */
    @ApiModelProperty(value="退换货地址",name = "returnAddress")
    @TableField("return_address")
    private String returnAddress;

    /**
     * 物流状态 0 是新建  1是签收   2是拒收
     */
    @TableField("logistics_state")
    @ApiModelProperty(value="物流状态 0 是新建  1是签收   2是拒收",name = "logisticsState")
    private Integer logisticsState;

    /**
     * 类型  1是发货   2是退换货
     */
    @ApiModelProperty(value="类型  1是发货   2是退换货",name = "type")
    private Integer type;

    /**
     * 删除标示(默认0未删除)
     */
    @TableField("delete_flag")
    @ApiModelProperty(value="删除标示(默认0未删除)",name = "deleteFlag")
    private Integer deleteFlag;

    /**
     * 快递单号
     */
    @ApiModelProperty(value="快递单号",name = "deliveryOrdersNo")
    @TableField("delivery_orders_no")
    private String deliveryOrdersNo;

    /**
     * 创建时间
     */
    @TableField("create_time")
    @ApiModelProperty(value="创建时间",name = "createTime")
    private Date createTime;

    /**
     * 修改时间
     */
    @TableField("update_time")
    @ApiModelProperty(value="修改时间",name = "updateTime")
    private Date updateTime;

    /**
     * 退货原因
     */
    @TableField(exist = false)
    @ApiModelProperty(value="退货原因",name = "returnDes")
    private String  returnDes;
    /**
     * 退货图片
     */
    @TableField(exist = false)
    @ApiModelProperty(value="退货图片",name = "returnImg")
    private String  returnImg;
    /**
     * 退款图片
     */
    @TableField(exist = false)
    @ApiModelProperty(value="退款图片",name = "returnImg")
    private String  refundImg;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
