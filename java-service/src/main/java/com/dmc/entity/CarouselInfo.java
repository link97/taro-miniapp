package com.dmc.entity;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;



import com.baomidou.mybatisplus.annotations.Version;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 首页-轮播图
 * </p>
 *
 * @
 * @since 2020-09-05
 */
@Data
@TableName("t_carousel_info")
@ApiModel("首页-轮播图")
public class CarouselInfo extends Model<CarouselInfo> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    @ApiModelProperty(value="主键",name = "id")
    private Long id;

    /**
     * 图片地址
     */
    @TableField("img_url")
    @ApiModelProperty(value="图片地址",name = "imgUrl")
    private String imgUrl;

    /**
     * 链接地址
     */
    @TableField("url_link")
    @ApiModelProperty(value="链接地址",name = "urlLink")
    private String urlLink;

    /**
     * 标题
     */
    @ApiModelProperty(value="标题",name = "title")
    private String title;

    /**
     * 描述
     */
    @ApiModelProperty(value="描述",name = "remarks")
    private String remarks;

    /**
     * 排序
     */
    @ApiModelProperty(value="排序",name = "sort")
    private Integer sort;

    /**
     * 商品类型 0:无跳转 1:商品 2:分销
     */
    @TableField("goods_type")
    @ApiModelProperty(value="商品类型 0:无跳转 1:商品 2:分销",name = "goodsType")
    private Integer goodsType;

    /**
     * 商品ID
     */
    @TableField("goods_id")
    @ApiModelProperty(value="商品ID",name = "goodsId")
    private Long goodsId;


    /**
     * 逻辑删除标记（0：显示；1：隐藏）
     */
    @TableField("del_flag")
    @ApiModelProperty(value="辑删除标记（0：显示；1：隐藏）",name = "delFlag")
    private Integer delFlag;

    @TableField("create_time")
    @ApiModelProperty(value="创建时间",name = "createTime")
    private Date createTime;

    @TableField("update_time")
    @ApiModelProperty(value="更新时间",name = "updateTime")
    private Date updateTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
