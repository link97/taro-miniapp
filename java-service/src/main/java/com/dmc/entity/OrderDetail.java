package com.dmc.entity;

import com.baomidou.mybatisplus.enums.IdType;
import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;



import com.baomidou.mybatisplus.annotations.Version;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 订单明细表
 * </p>
 *
 * @
 * @since 2020-09-07
 */
@Data
@Accessors(chain = true)
@TableName("t_order_detail")
@ApiModel("订单明细信息")
public class OrderDetail extends Model<OrderDetail> {

    private static final long serialVersionUID = 1L;

    /**
     * 订单明细ID
     */
    @TableId(value = "order_detail_id", type = IdType.AUTO)
    @ApiModelProperty(value="主键",name = "orderDetailId")
    private Integer orderDetailId;

    /**
     * 订单ID
     */
    @TableField("orders_id")
    @ApiModelProperty(value="订单ID",name = "ordersId")
    private Integer ordersId;

    /**
     * 订单号
     */
    @TableField("orders_no")
    @ApiModelProperty(value="订单号",name = "ordersNo")
    private String ordersNo;

    /**
     * 购买商品数量
     */
    @ApiModelProperty(value="购买商品数量",name = "quantity")
    private Integer quantity;

    /**
     * 0:普通商品 1:会员商品
     */
    @ApiModelProperty(value="0:普通商品 1:会员商品",name = "vip")
    private Integer vip;

    /**
     * 商品ID
     */
    @TableField("sku_id")
    @ApiModelProperty(value="商品ID",name = "skuId")
    private Long skuId;

    /**
     * spu Id
     */
    @TableField("goods_spu_id")
    @ApiModelProperty(value="spu Id",name = "goodsSpuId")
    private Long goodsSpuId;

    /**
     * 商品sku编号
     */
    @TableField("sku_no")
    @ApiModelProperty(value="商品sku编号",name = "skuNo")
    private String skuNo;

    /**
     * 商品sku规格类型 如重量  颜色
     */
    @TableField("sku_title_type")
    @ApiModelProperty(value="商品sku规格类型 如重量  颜色",name = "skuTitleType")
    private String skuTitleType;

    /**
     * 商品sku规格
     */
    @TableField("sku_title")
    @ApiModelProperty(value="商品sku规格",name = "skuTitle")
    private String skuTitle;

    /**
     * 商品封面图片地址
     */
    @TableField("sku_covers_url")
    @ApiModelProperty(value="商品封面图片地址",name = "skuCoversUrl")
    private String skuCoversUrl;

    /**
     * 商品图片地址
     */
    @TableField("sku_img_url")
    @ApiModelProperty(value="商品图片地址",name = "skuImgUrl")
    private String skuImgUrl;

    /**
     * 商品库存
     */
    @TableField("sku_num")
    @ApiModelProperty(value="商品库存",name = "skuNum")
    private BigDecimal skuNum;

    /**
     * 商品销售数量
     */
    @TableField("sku_sale_num")
    @ApiModelProperty(value="商品销售数量",name = "skuSaleNum")
    private BigDecimal skuSaleNum;

    /**
     * sku 原价
     */
    @TableField("sku_cost_price")
    @ApiModelProperty(value="sku 原价",name = "skuCostPrice")
    private BigDecimal skuCostPrice;

    /**
     * sku 现价
     */
    @TableField("sku_curr_price")
    @ApiModelProperty(value="sku 现价",name = "skuCurrPrice")
    private BigDecimal skuCurrPrice;

    /**
     * 商品描述
     */
    @ApiModelProperty(value="商品描述",name = "des")
    private String des;

    /**
     * 状态:1启用,0禁用
     */
    @TableField("sku_status")
    @ApiModelProperty(value="状态:1启用,0禁用",name = "skuStatus")
    private Integer skuStatus;

    /**
     * 商品SPU编号，唯一
     */
    @TableField("spu_no")
    @ApiModelProperty(value="品SPU编号，唯一",name = "spuNo")
    private String spuNo;

    /**
     * 商品名称
     */
    @TableField("goods_name")
    @ApiModelProperty(value="商品名称",name = "goodsName")
    private String goodsName;

    /**
     * 分类id
     */
    @TableField("category_id")
    @ApiModelProperty(value="分类id",name = "categoryId")
    private Long categoryId;

    /**
     * 0:普通商品 1:特殊商品
     */
    @ApiModelProperty(value="0:普通商品 1:特殊商品",name = "special")
    private Integer special;

    /**
     * 商品详情
     */
    @TableField("sku_detail")
    @ApiModelProperty(value="商品详情",name = "skuDetail")
    private String skuDetail;

    /**
     * 商品详情图片地址
     */
    @TableField("sku_detail_img_url")
    @ApiModelProperty(value="商品详情图片地址",name = "skuDetailImgUrl")
    private String skuDetailImgUrl;

    @TableField("create_time")
    @ApiModelProperty(value="创建时间",name = "createTime")
    private Date createTime;

    @TableField("update_time")
    @ApiModelProperty(value="更新时间",name = "updateTime")
    private Date updateTime;


    @Override
    protected Serializable pkVal() {
        return this.orderDetailId;
    }

}
