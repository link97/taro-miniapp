package com.dmc.entity;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;



import com.baomidou.mybatisplus.annotations.Version;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 银行信息
 * </p>
 *
 * @
 * @since 2020-09-05
 */
@Data
@Accessors(chain = true)
@TableName("t_bank_info")
@ApiModel("银行信息")
public class BankInfo extends Model<BankInfo> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "bank_id", type = IdType.AUTO)
    @ApiModelProperty(value="主键",name = "bankId")
    private Integer bankId;

    /**
     * 银行名称
     */
    @TableField("bank_name")
    @ApiModelProperty(value="银行名称",name = "bankName")
    private String bankName;
    /**
     * 银行编码
     */
    @TableField("bank_code")
    @ApiModelProperty(value="银行编码",name = "bankCode")
    private String bankCode;
    /**
     * 银行logo
     */
    @TableField("bank_img")
    @ApiModelProperty(value="银行logo",name = "bankImg")
    private String bankImg;

    /**
     * 逻辑删除标记（0：显示；1：隐藏）
     */
    @TableField("del_flag")
    @ApiModelProperty(value="逻辑删除标记（0：显示；1：隐藏）",name = "delFlag")
    private Integer delFlag;


    @Override
    protected Serializable pkVal() {
        return this.bankId;
    }

}
