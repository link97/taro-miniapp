package com.dmc.entity;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * <p>
 * 身份认证
 * </p>
 *
 * @
 * @since 2020-09-05
 */
@Data
@Accessors(chain = true)
@TableName("t_user_authenticate")
@ApiModel("身份认证信息")
public class UserAuthenticate extends Model<UserAuthenticate> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    @ApiModelProperty(value="主键",name = "id")
    private Integer id;

    /**
     * 用户id
     */
    @TableField("user_id")
    @ApiModelProperty(value="用户id",name = "userId")
    private Long userId;

    /**
     * 姓名
     */
    @TableField("full_name")
    @ApiModelProperty(value="姓名",name = "fullName")
    private String fullName;

    /**
     * 身份证号码
     */
    @TableField("identification_no")
    @ApiModelProperty(value="身份证号码",name = "identificationNo")
    private String identificationNo;

    /**
     * 认证渠道
     */
    @ApiModelProperty(value="认证渠道",name = "channels")
    private Integer channels;

    /**
     * 认证状态
     */
    @ApiModelProperty(value="认证状态",name = "status")
    private Integer status;

    /**
     * 身份证图片
     */
    @TableField("img_url")
    @ApiModelProperty(value="身份证图片",name = "imgUrl")
    private String imgUrl;

    /**
     * 身份证反面图片
     */
    @TableField("negative_img_url")
    @ApiModelProperty(value="身份证反面图片",name = "negativeImgUrl")
    private String negativeImgUrl;

    /**
     * 认证通过日期
     */
    @TableField("check_time")
    @ApiModelProperty(value="认证通过日期",name = "checkTime")
    private Date checkTime;

    @TableField("create_time")
    @ApiModelProperty(value="创建时间",name = "createTime")
    private Date createTime;

    @TableField("update_time")
    @ApiModelProperty(value="更新时间",name = "updateTime")
    private Date updateTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
