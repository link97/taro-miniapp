package com.dmc.entity;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;



import com.baomidou.mybatisplus.annotations.Version;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 购物车数据表
 * </p>
 *
 * @
 * @since 2020-09-07
 */
@Data
@Accessors(chain = true)
@TableName("t_shopping_cart")
@ApiModel("购物车信息")
public class ShoppingCart extends Model<ShoppingCart> {

    private static final long serialVersionUID = 1L;

    /**
     * 购物车ID
     */
    @TableId(value = "cart_id", type = IdType.AUTO)
    @ApiModelProperty(value="购物车ID",name = "cartId")
    private Integer cartId;

    /**
     * 商品ID
     */
    @TableField("sku_id")
    @ApiModelProperty(value="商品ID",name = "skuId")
    private Long skuId;

    /**
     * 商品sku编号
     */
    @TableField("sku_no")
    @ApiModelProperty(value="商品sku编号",name = "skuNo")
    private String skuNo;

    /**
     * 用户id
     */
    @TableField("user_id")
    @ApiModelProperty(value="用户id",name = "userId")
    private Long userId;

    /**
     * 购买商品数量
     */
    @ApiModelProperty(value="购买商品数量",name = "quantity")
    private Integer quantity;

    @TableField("create_time")
    @ApiModelProperty(value="创建时间",name = "createTime")
    private Date createTime;

    @TableField("update_time")
    @ApiModelProperty(value="更新时间",name = "updateTime")
    private Date updateTime;


    @Override
    protected Serializable pkVal() {
        return this.cartId;
    }

}
