package com.dmc.entity;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;



import com.baomidou.mybatisplus.annotations.Version;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 物流表
 * </p>
 *
 * @
 * @since 2020-09-12
 */
@Data
@Accessors(chain = true)
@TableName("t_logistics")
@ApiModel("物流信息")
public class Logistics extends Model<Logistics> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(value = "logistics_id", type = IdType.AUTO)
    @ApiModelProperty(value="主键",name = "logisticsId")
    private Integer logisticsId;

    /**
     * 物流公司
     */
    @TableField("logistics_name")
    @ApiModelProperty(value="物流公司",name = "logisticsName")
    private String logisticsName;

    /**
     * 删除标示(默认0未删除)
     */
    @TableField("delete_flag")
    @ApiModelProperty(value="删除标示(默认0未删除)",name = "deleteFlag")
    private Integer deleteFlag;

    /**
     * 创建时间
     */
    @TableField("create_time")
    @ApiModelProperty(value="创建时间",name = "createTime")
    private Date createTime;

    /**
     * 修改时间
     */
    @TableField("update_time")
    @ApiModelProperty(value="修改时间",name = "updateTime")
    private Date updateTime;


    @Override
    protected Serializable pkVal() {
        return this.logisticsId;
    }

}
