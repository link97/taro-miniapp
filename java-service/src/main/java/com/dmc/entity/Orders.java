package com.dmc.entity;

import com.baomidou.mybatisplus.enums.IdType;
import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * <p>
 * 订单表
 * </p>
 *
 * @
 * @since 2020-09-07
 */
@Data
@TableName("t_orders")
@ApiModel("订单信息")
public class Orders extends Model<Orders> {

    private static final long serialVersionUID = 1L;

    /**
     * 订单ID
     */
    @TableId(value = "orders_id", type = IdType.AUTO)
    @ApiModelProperty(value="主键",name = "ordersId")
    private Integer ordersId;

    /**
     * 用户id
     */
    @TableField("user_id")
    @ApiModelProperty(value="用户id",name = "userId")
    private Long userId;

    /**
     * 订单号
     */
    @TableField("orders_no")
    @ApiModelProperty(value="订单号",name = "ordersNo")
    private String ordersNo;
    /**
     * 父订单号
     */
    @TableField("father_orders_no")
    @ApiModelProperty(value="父订单号",name = "fatherOrdersNo")
    private String fatherOrdersNo;
    /**
     * 是否为父订单（0表示否、1表示是）
     */
    @TableField("father_orders")
    @ApiModelProperty(value="是否为父订单（0表示否、1表示是）",name = "fatherOrders")
    private Integer fatherOrders;

    /**
     * 订单生成时间
     */
    @TableField("create_time")
    @ApiModelProperty(value="订单生成时间",name = "createTime")
    private Date createTime;

    /**
     * 收货人姓名
     */
    @TableField("receiver_name")
    @ApiModelProperty(value="收货人姓名",name = "receiverName")
    private String receiverName;

    /**
     * 区/县
     */
    @TableField("receiver_district")
    @ApiModelProperty(value="区/县",name = "receiverDistrict")
    private String receiverDistrict;

    /**
     * 详细地址
     */
    @TableField("receiver_address")
    @ApiModelProperty(value="详细地址",name = "receiverAddress")
    private String receiverAddress;

    /**
     * 收货移动电话
     */
    @TableField("receiver_mobile")
    @ApiModelProperty(value="收货移动电话",name = "receiverMobile")
    private String receiverMobile;

    /**
     * 1、微信支付 2、积分支付 3 扫码支付 4微信混合支付 5扫码混合支付
     */
    @TableField("pay_way")
    @ApiModelProperty(value="1、微信支付 2、积分支付 3 扫码支付 6线下现金支付 7线下转账支付 4微信混合支付 5扫码混合支付 8线下现金支付 9线下转账支付 10收益支付",name = "payWay")
    private Integer payWay;

    /**
     * 订单享受的折扣
     */
    @TableField("discount_rate")
    @ApiModelProperty(value="订单享受的折扣",name = "discountRate")
    private BigDecimal discountRate;
    /**
     * 订单奖励百分比
     */
    @TableField("reword_rate")
    @ApiModelProperty(value="订单奖励百分比",name = "rewordRate")
    private BigDecimal rewordRate;

    /**
     * 商品总金额
     */
    @TableField("total_amount")
    @ApiModelProperty(value="商品总金额",name = "totalAmount")
    private BigDecimal totalAmount;

    /**
     * 商品原总金额
     */
    @TableField("original_amount")
    @ApiModelProperty(value="商品原总金额",name = "originalAmount")
    private BigDecimal originalAmount;



    /**
     * 应付金额
     */
    @TableField("final_amount")
    @ApiModelProperty(value="应付金额",name = "finalAmount")
    private BigDecimal finalAmount;

    /**
     * 使用积分值
     */
    @TableField("use_integral")
    @ApiModelProperty(value="使用积分值",name = "useIntegral")
    private BigDecimal useIntegral;

    /**
     * 订单状态20000、未付款(含义：等待付款)；20001、付款成功(含义：已经付款)；20002、已经发货；20003、收货确认(含义：已经完成)；20004、交易关闭(含义：取消订单)；20007、异常订单（退换货等） 20005申请退款 20006退款成功
     */
    @TableField("orders_state")
    @ApiModelProperty(value="订单状态20000、未付款(含义：等待付款)；20001、付款成功(含义：已经付款)；20002、已经发货；20003、收货确认(含义：已经完成)；20004、交易关闭(含义：取消订单)；20007、异常订单（退换货等） 20005申请退款 20006退款成功",name = "ordersState")
    private Long ordersState;

    /**
     * 订单赠送积分
     */
    @TableField("order_point_scount")
    @ApiModelProperty(value="订单赠送积分",name = "orderPointScount")
    private Integer orderPointScount;

    /**
     * 会员商品订单（0:否 1：是）
     */
    @TableField("vip_order")
    @ApiModelProperty(value="会员商品订单（0:否 1：是）",name = "vipOrder")
    private Integer vipOrder;

    /**
     * 积分商品订单（0:否 1：是）
     */
    @TableField("integral_order")
    @ApiModelProperty(value="积分商品订单（0:否 1：是）",name = "integralOrder")
    private Integer integralOrder;
    /**
     * 交易状态（0:未支付 1：已支付）
     */
    @TableField("trade_state")
    @ApiModelProperty(value="交易状态（0:未支付 1：已支付）",name = "tradeState")
    private Integer tradeState;

    /**
     * 物流状态 0 是新建  1是签收   2是拒收
     */
    @TableField("logistics_state")
    @ApiModelProperty(value="物流状态 0 是新建  1是签收   2是拒收",name = "logisticsState")
    private Integer logisticsState;

    /**
     * 订单修改时间
     */
    @TableField("update_time")
    @ApiModelProperty(value="主键",name = "orderDetailId")
    private Date updateTime;

    /**
     * 删除标示(默认0未删除)
     */
    @TableField("delete_flag")
    @ApiModelProperty(value="删除标示(默认0未删除)",name = "deleteFlag")
    private Integer deleteFlag;

    /**
     * 是否有发票（0没有，1有）
     */
    @TableField("is_invoice")
    @ApiModelProperty(value="是否有发票（0没有，1有）",name = "isInvoice")
    private Integer isInvoice;

    /**
     * 快递单号
     */
    @TableField("delivery_orders_no")
    @ApiModelProperty(value="快递单号",name = "deliveryOrdersNo")
    private String deliveryOrdersNo;

    /**
     * 退款金额
     */
    @TableField("refund_amount")
    @ApiModelProperty(value="退款金额",name = "refundAmount")
    private BigDecimal refundAmount;


    /**
     * 订单支付单据ID
     */
    @TableField("payment_id")
    @ApiModelProperty(value="订单支付单据ID",name = "paymentId")
    private Long paymentId;

    /**
     * 订单退款单据ID
     */
    @TableField("refund_payment_id")
    @ApiModelProperty(value="订单退款单据ID",name = "refundPaymentId")
    private Long refundPaymentId;

    /**
     * 退款积分
     */
    @TableField("refund_integral")
    @ApiModelProperty(value="退款积分",name = "refundIntegral")
    private BigDecimal refundIntegral;

    /**
     * 退款申请时间
     */
    @TableField("refund_time")
    @ApiModelProperty(value="退款申请时间",name = "refundTime")
    private Date refundTime;
    /**
     * 退款原因
     */
    @TableField("refund_des")
    @ApiModelProperty(value="退款原因",name = "refundDes")
    private String refundDes;
    /**
     * 退款图片
     */
    @TableField("refund_img")
    @ApiModelProperty(value="退款图片",name = "refundImg")
    private String refundImg;
    /**
     * 订单支付时间
     */
    @TableField("pay_time")
    @ApiModelProperty(value="订单支付时间",name = "payTime")
    private Date payTime;
    /**
     * 退货时间
     */
    @TableField("return_time")
    @ApiModelProperty(value="退货时间",name = "returnTime")
    private Date returnTime;
    /**
     * 退货原因
     */
    @TableField("return_des")
    @ApiModelProperty(value="退货原因",name = "returnDes")
    private String  returnDes;
    /**
     * 退货图片
     */
    @TableField("return_img")
    @ApiModelProperty(value="退货图片",name = "returnImg")
    private String  returnImg;

    @Override
    protected Serializable pkVal() {
        return this.ordersId;
    }

}
