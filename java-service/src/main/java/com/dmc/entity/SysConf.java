package com.dmc.entity;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;



import com.baomidou.mybatisplus.annotations.Version;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 系统设置表
 * </p>
 *
 * @
 * @since 2020-09-12
 */
@Data
@Accessors(chain = true)
@TableName("t_sys_conf")
@ApiModel("系统设置信息")
public class SysConf extends Model<SysConf> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "conf_id", type = IdType.AUTO)
    @ApiModelProperty(value="主键",name = "confId")
    private Integer confId;

    /**
     * 配置类型,1订单交易设置
     */
    @TableField("conf_type")
    @ApiModelProperty(value="配置类型,1订单交易设置",name = "confType")
    private Integer confType;

    /**
     * 订单取消时间,分钟数
     */
    @TableField("order_cancel_time")
    @ApiModelProperty(value="订单取消时间,分钟数",name = "orderCancelTime")
    private Integer orderCancelTime;

    /**
     * 订单自动确认时间，天数
     */
    @TableField("order_auto_arrival")
    @ApiModelProperty(value="订单自动确认时间，天数",name = "orderAutoArrival")
    private Integer orderAutoArrival;

    /**
     * 退换货地址
     */
    @ApiModelProperty(value="退换货地址",name = "returnAddress")
    @TableField("return_address")
    private String returnAddress;

    @TableField("create_time")
    @ApiModelProperty(value="创建时间",name = "createTime")
    private Date createTime;

    @TableField("update_time")
    @ApiModelProperty(value="更新时间",name = "updateTime")
    private Date updateTime;


    @Override
    protected Serializable pkVal() {
        return this.confId;
    }

}
