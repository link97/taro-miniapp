package com.dmc.entity;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;



import com.baomidou.mybatisplus.annotations.Version;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 消息表
 * </p>
 *
 * @author 
 * @since 2020-09-25
 */
@Data
@Accessors(chain = true)
@TableName("t_message")
@ApiModel("我的消息信息")
public class Message extends Model<Message> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    @ApiModelProperty(value="主键",name = "id")
    private Long id;

    /**
     * 用户ID
     */
    @TableField("user_id")
    @ApiModelProperty(value="用户ID",name = "userId")
    private Long userId;

    /**
     * 0:订单发货 1:分销商升级 
     */
    @ApiModelProperty(value="0:订单发货 1:分销商升级 3:站内信",name = "type")
    private Integer type;

    /**
     * 订单ID
     */
    @TableField("order_id")
    @ApiModelProperty(value="订单ID",name = "orderId")
    private Long orderId;

    /**
     * 订单类型 1:商品订单 2:分销商订单 
     */
    @TableField("order_type")
    @ApiModelProperty(value="单类型 1:商品订单 2:分销商订单 3:提现订单 4:库存",name = "orderType")
    private Integer orderType;

    /**
     * 消息体
     */
    @ApiModelProperty(value="消息体",name = "message")
    private String message;

    /**
     * 0:未读 1:已读 
     */
    @ApiModelProperty(value="0:未读 1:已读 ",name = "status")
    private Integer status;

    @TableField("create_time")
    @ApiModelProperty(value="创建时间",name = "createTime")
    private Date createTime;

    @TableField("update_time")
    @ApiModelProperty(value="更新时间",name = "updateTime")
    private Date updateTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
