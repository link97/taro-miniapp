package com.dmc.dict;

/**
 * 支付线上线下装填字典表
 */
public enum OrderLineStatusDict {
    ORDER_LINE_STATUS_ON(0, "线上"),
    ORDER_LINE_STATUS_OFF(1, "线下");

    private final int cn;
    private final String desc;

    OrderLineStatusDict(int cn, String desc) {
        this.cn = cn;
        this.desc = desc;
    }

    public static String getDescByCn(Integer cn) {
        if(cn == null){
            return "无";
        }
        for (OrderLineStatusDict e : OrderLineStatusDict.values()) {
            if (e.cn== cn){
                return e.getDesc();
            }
        }
        return "无";
    }

    public int getCn() {
        return cn;
    }

    public String getDesc() {
        return desc;
    }}
