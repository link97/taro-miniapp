package com.dmc.dict;

/**
 * 支付状态字典表
 */
public enum PaymentStatusDict {
    PAYMENT_STATUS_READY(0, "待支付"),
    PAYMENT_STATUS_SUCCESS(1, "成功"),
    PAYMENT_STATUS_FAIL(2, "失败");

    private final Integer cn;
    private final String desc;

    PaymentStatusDict(Integer cn, String desc) {
        this.cn = cn;
        this.desc = desc;
    }

    public Integer getCn() {
        return cn;
    }

    public String getDesc() {
        return desc;
    }

}
