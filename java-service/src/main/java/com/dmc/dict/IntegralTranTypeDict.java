package com.dmc.dict;

/**
 * 积分流水类型
 */
public enum IntegralTranTypeDict {
    INTEGRAL_TRAN_TYPE_DISTRUBUTION(100, "购买分销商积分"),
    NTEGRAL_TRAN_TYPE_GOODS(200, "购买商品积分"),
    NTEGRAL_TRAN_TYPE_GOODS_300(300, "售后退还"),
    NTEGRAL_TRAN_TYPE_GOODS_400(400, "积分返积分");

    private final int cn;
    private final String desc;

    IntegralTranTypeDict(int cn, String desc) {
        this.cn = cn;
        this.desc = desc;
    }

    public static String getDescByCn(Integer cn) {
        if(cn == null){
            return "无";
        }
        for (IntegralTranTypeDict e : IntegralTranTypeDict.values()) {
            if (e.cn== cn){
                return e.getDesc();
            }
        }
        return "无";
    }

    public int getCn() {
        return cn;
    }

    public String getDesc() {
        return desc;
    }}
