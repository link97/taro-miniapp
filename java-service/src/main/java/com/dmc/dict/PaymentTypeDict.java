package com.dmc.dict;

/**
 * 支付记录表类型字典表
 */
public enum PaymentTypeDict {
    PAYMENT_TYPE_PAY(1, "支付"),
    PAYMENT_TYPE_REFUND(2, "退款"),
    PAYMENT_TYPE_TRANSFER(3, "提现");

    private final Integer cn;
    private final String desc;

    PaymentTypeDict(Integer cn, String desc) {
        this.cn = cn;
        this.desc = desc;
    }

    public Integer getCn() {
        return cn;
    }

    public String getDesc() {
        return desc;
    }}
