package com.dmc.dict;

import net.sf.jsqlparser.expression.operators.relational.MinorThanEquals;

import java.util.ArrayList;
import java.util.List;

/**
 * 分销商购买支付类型
 */
public enum DistriButionPayTypeDict {
    DISTRIBUTION_PAY_TYPE_1(1, "微信支付"),
    DISTRIBUTION_PAY_TYPE_2(2, "支付宝支付"),
    DISTRIBUTION_PAY_TYPE_3(3, "中级分红购买"),
    DISTRIBUTION_PAY_TYPE_7(7, "高级分红购买"),
    DISTRIBUTION_PAY_TYPE_4(4, "财富购买"),
    DISTRIBUTION_PAY_TYPE_5(5, "线下现金购买"),
    DISTRIBUTION_PAY_TYPE_6(6, "线下转账购买"),
    DISTRIBUTION_PAY_TYPE_8(8, "收益金支付");

    private final int cn;
    private final String desc;

    DistriButionPayTypeDict(int cn, String desc) {
        this.cn = cn;
        this.desc = desc;
    }

    public static String getDescByCn(Integer cn) {
        if(cn == null){
            return "无";
        }
        for (DistriButionPayTypeDict e : DistriButionPayTypeDict.values()) {
            if (e.cn== cn){
                return e.getDesc();
            }
        }
        return "无";
    }

    public int getCn() {
        return cn;
    }

    public String getDesc() {
        return desc;
    }

    /**
     * 校验是否是现金支付方式
     * @return
     */
    public static Boolean checkCashPayType(Integer type){
        if(type == null){
            return false;
        }
        return DISTRIBUTION_PAY_TYPE_1.cn == type
                || DISTRIBUTION_PAY_TYPE_2.cn == type
                || DISTRIBUTION_PAY_TYPE_5.cn == type
                || DISTRIBUTION_PAY_TYPE_6.cn == type
                || DISTRIBUTION_PAY_TYPE_8.cn == type;
    }

    /**
     * 校验是否是现金支付方式
     * @return
     */
    public static List<Integer> getCashPayType(){
        List<Integer> list = new ArrayList<>();
        list.add(DISTRIBUTION_PAY_TYPE_1.cn);
        list.add(DISTRIBUTION_PAY_TYPE_2.cn);
        list.add(DISTRIBUTION_PAY_TYPE_4.cn);
        list.add(DISTRIBUTION_PAY_TYPE_5.cn);
        list.add(DISTRIBUTION_PAY_TYPE_6.cn);
        list.add(DISTRIBUTION_PAY_TYPE_8.cn);
        return list;
    }

    /**
     * 校验是否是分红支付方式
     * @return
     */
    public static Boolean checkFhType(Integer type){
        if(type == null){
            return false;
        }
        return DISTRIBUTION_PAY_TYPE_3.cn == type
                || DISTRIBUTION_PAY_TYPE_7.cn == type;
    }

    /**
     * 校验是否是超高级支付方式
     * @return
     */
    public static Boolean checkCgType(Integer type){
        if(type == null){
            return false;
        }
        return DISTRIBUTION_PAY_TYPE_4.cn == type;
    }
}
