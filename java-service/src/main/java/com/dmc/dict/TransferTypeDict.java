package com.dmc.dict;

public enum TransferTypeDict {

    TRANSFER_TYPE_WECHAT_NUM(0, "微信零钱"),
    TRANSFER_TYPE_BAND_NUM(1, "银行卡");

    private final int cn;
    private final String desc;

    TransferTypeDict(int cn, String desc) {
        this.cn = cn;
        this.desc = desc;
    }
    public static String getDescByCn(Integer cn) {
        if(cn == null){
            return "无";
        }
        for (TransferTypeDict e : TransferTypeDict.values()) {
            if (e.cn== cn){
                return e.getDesc();
            }
        }
        return "无";
    }
    public int getCn() {
        return cn;
    }

    public String getDesc() {
        return desc;
    }
}
