package com.dmc.dict;

/**
 * 支付类型字典类
 */
public enum PayTypeDict {

    PAY_TYPE_WECHAT("MIC_DISTR_PAY_TYPE_WEBCHAT", "微信"),
    PAY_TYPE_ALIPAY("MIC_DISTR_PAY_TYPE_ALIPAY", "支付宝"),
    PAY_TYPE_UNIONPAY("MIC_DISTR_PAY_TYPE_UNIONPAY", "银联"),
    PAY_TYPE_ERCODE("MIC_DISTR_PAY_TYPE_ERCODE", "二维码支付"),
    PAY_TYPE_BALANCE("MIC_DISTR_PAY_TYPE_BALANCE", "收益余额支付");

    private final String cn;
    private final String desc;

    PayTypeDict(String cn, String desc) {
        this.cn = cn;
        this.desc = desc;
    }

    public String getCn() {
        return cn;
    }

    public String getDesc() {
        return desc;
    }}
