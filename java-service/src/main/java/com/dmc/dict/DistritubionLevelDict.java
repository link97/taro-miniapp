package com.dmc.dict;

import com.dmc.conf.Constant;
import org.apache.commons.lang3.StringUtils;

/**
 * 分销等级
 */
public enum DistritubionLevelDict {
    DISTRIBUTION_LEVEL_2(2,"ZHONG_JI"),
    DISTRIBUTION_LEVEL_3(3,"GAO_JI"),
    DISTRIBUTION_LEVEL_4(4, "CHAO_GAO_JI");

    private final int cn;
    private final String desc;

    DistritubionLevelDict(int cn, String desc) {
        this.cn = cn;
        this.desc = desc;
    }

    public static String getDescByCn(Integer cn) {
        if(cn == null){
            return "无";
        }
        for (DistritubionLevelDict e : DistritubionLevelDict.values()) {
            if (e.cn== cn){
                return e.getDesc();
            }
        }
        return "无";
    }

    /**
     * 根据分销商等级获取对应会员等级
     * @param distritubionLevel
     * @return
     */
    public static int getLevel(String distritubionLevel) {
        if(StringUtils.isBlank(distritubionLevel)){
            return DISTRIBUTION_LEVEL_2.cn;
        }
        int level = 0;
        if(distritubionLevel.equalsIgnoreCase(Constant.ZHONG_JI_DISTRIBUTION_FLAG)){
            level =  DISTRIBUTION_LEVEL_2.cn;
        }else if(distritubionLevel.equalsIgnoreCase(Constant.GAO_JI_DISTRIBUTION_FLAG)){
            level =  DISTRIBUTION_LEVEL_3.cn;
        }else if(distritubionLevel.equalsIgnoreCase(Constant.CHAO_GAO_JI_DISTRIBUTION_FLAG)){
            level =  DISTRIBUTION_LEVEL_4.cn;
        }
        return level;
    }

    public int getCn() {
        return cn;
    }

    public String getDesc() {
        return desc;
    }}
