package com.dmc.dict;

/**
 * 分销商订单类型
 */
public enum DistriButionOrderTypeDict {
    //0:分销商订单 1:套餐包订单 2:套餐包子订单
    DISTRIBUTION_PAY_TYPE_0(0, "分销商订单"),
    DISTRIBUTION_PAY_TYPE_1(1, "套餐包订单"),
    DISTRIBUTION_PAY_TYPE_2(2, "套餐包子订单");

    private final int cn;
    private final String desc;

    DistriButionOrderTypeDict(int cn, String desc) {
        this.cn = cn;
        this.desc = desc;
    }

    public static String getDescByCn(Integer cn) {
        if(cn == null){
            return "无";
        }
        for (DistriButionOrderTypeDict e : DistriButionOrderTypeDict.values()) {
            if (e.cn== cn){
                return e.getDesc();
            }
        }
        return "无";
    }

    public int getCn() {
        return cn;
    }

    public String getDesc() {
        return desc;
    }

}
