package com.dmc.dict;

/**
 * 支付方式字典类
 */
public enum PayModeDict {

    PAY_MODE_MINI_APP("MIC_DISTR_PAY_MODEL_MINI_APP", "小程序"),
    PAY_MODE_MINI_NATIVE("MIC_DISTR_PAY_MODEL_NATIVE", "NATIVE"),
    PAY_MODE_MINI_ENTERPRISE("MIC_DISTR_PAY_MODEL_ENTERPRISE", "企业付款");
   ;

    private final String cn;
    private final String desc;

    PayModeDict(String cn, String desc) {
        this.cn = cn;
        this.desc = desc;
    }

    public String getCn() {
        return cn;
    }

    public String getDesc() {
        return desc;
    }}
