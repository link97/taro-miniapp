package com.dmc.dict;

import com.dmc.conf.Constant;
import org.apache.commons.lang3.StringUtils;

/**
 * 分销订单状态字典表
 */
public enum UserLevelDict {
    USER_LEVEL_0(0, "非会员"),
    USER_LEVEL_1(1, "VIP会员"),
    USER_LEVEL_2(2, "初级会员"),
    USER_LEVEL_3(3, "中级会员"),
    USER_LEVEL_4(4, "高级会员"),
    USER_LEVEL_5(5, "财富会员");

    private final int cn;
    private final String desc;

    UserLevelDict(int cn, String desc) {
        this.cn = cn;
        this.desc = desc;
    }

    public static String getDescByCn(Integer cn) {
        if(cn == null){
            return "无";
        }
        for (UserLevelDict e : UserLevelDict.values()) {
            if (e.cn== cn){
                return e.getDesc();
            }
        }
        return "无";
    }

    /**
     * 根据分销商等级获取对应会员等级
     * @param goodsLevelFlag
     * @return
     */
    public static int getLevel(String goodsLevelFlag) {
        if(StringUtils.isBlank(goodsLevelFlag)){
            return USER_LEVEL_0.cn;
        }
        int level = 0;
        if(goodsLevelFlag.equalsIgnoreCase(Constant.ZHONG_JI_DISTRIBUTION_FLAG)){
            level =  USER_LEVEL_3.cn;
        }else if(goodsLevelFlag.equalsIgnoreCase(Constant.GAO_JI_DISTRIBUTION_FLAG)){
            level =  USER_LEVEL_4.cn;
        }else if(goodsLevelFlag.equalsIgnoreCase(Constant.CHAO_GAO_JI_DISTRIBUTION_FLAG)){
            level =  USER_LEVEL_5.cn;
        }
        return level;
    }

    public int getCn() {
        return cn;
    }

    public String getDesc() {
        return desc;
    }}
