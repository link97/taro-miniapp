package com.dmc.dict;

/**
 * 支付状态字典表
 */
public enum OrderStatusDict {
    ORDER_STATUS_CREATED(20000L, "待付款"),
    ORDER_STATUS_PAIED(20001L, "已付款"),
    ORDER_STATUS_DELIVERY(20002L, "已发货"),
    ORDER_STATUS_SUCCESS(20003L, "交易成功"),
    ORDER_STATUS_CLOSED(20004L, "交易关闭"),
    ORDER_STATUS_REFUND(20005L, "申请退款"),
    ORDER_STATUS_REFUNDED(20006L, "退款成功"),
    ORDER_STATUS_RETURN(20007L, "申请退货"),
    ORDER_STATUS_RETURNED(20008L, "退货成功");

    private final long cn;
    private final String desc;

    OrderStatusDict(long cn, String desc) {
        this.cn = cn;
        this.desc = desc;
    }

    public static String getDescByCn(Long cn) {
        if(cn == null){
            return "无";
        }
        for (OrderStatusDict e : OrderStatusDict.values()) {
            if (e.cn== cn){
                return e.getDesc();
            }
        }
        return "无";
    }

    public long getCn() {
        return cn;
    }

    public String getDesc() {
        return desc;
    }}
