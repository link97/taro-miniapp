package com.dmc.dict;

/**
 * 提现订单状态字典表
 */
public enum CashOrderStatusDict {
    ORDER_STATUS_CREATED(0, "待提现"),
    ORDER_STATUS_CASHING(1, "提现中"),
    ORDER_STATUS_DELIVERY(2, "已提现");

    private final int cn;
    private final String desc;

    CashOrderStatusDict(int cn, String desc) {
        this.cn = cn;
        this.desc = desc;
    }
    public static String getDescByCn(Integer cn) {
        if(cn == null){
            return "无";
        }
        for (CashOrderStatusDict e : CashOrderStatusDict.values()) {
            if (e.cn== cn){
                return e.getDesc();
            }
        }
        return "无";
    }
    public int getCn() {
        return cn;
    }

    public String getDesc() {
        return desc;
    }

}
