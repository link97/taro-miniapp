package com.dmc.dict;

/**
 * 前端小程序支付方式
 */
public enum SmallPayModeDict {

    SMALL_PAY_MODE_1(1, "微信支付"),
    SMALL_PAY_MODE_2(2, "扫码支付"),
    SMALL_PAY_MODE_3(5, "现金支付"),
    SMALL_PAY_MODE_4(6, "银行卡支付"),
    SMALL_PAY_MODE_7(7, "收益支付");

    private final int cn;
    private final String desc;

    SmallPayModeDict(int cn, String desc) {
        this.cn = cn;
        this.desc = desc;
    }

    public static SmallPayModeDict getSmallPayMode(Integer cn) {
        if(cn == null){
            return null;
        }
        for (SmallPayModeDict e : SmallPayModeDict.values()) {
            if (e.cn== cn){
                return e;
            }
        }
        return null;
    }
    public static String getSmallPayUrl(Integer cn) {
        if(cn == null){
            return null;
        }
        String url = null;
        if(SMALL_PAY_MODE_4.getCn() == cn){
            url = "https://bai-tong-fen-xiao.oss-cn-beijing.aliyuncs.com/test/20201101/20201101164455-pay_6.png";
        }else if(SMALL_PAY_MODE_3.getCn() == cn){
            url = "https://bai-tong-fen-xiao.oss-cn-beijing.aliyuncs.com/test/20201111/20201111220741-64e563f41adec47064c5b1e74aab0b3.png";
        }else if(SMALL_PAY_MODE_2.getCn() == cn){
            url = "https://bai-tong-fen-xiao.oss-cn-beijing.aliyuncs.com/test/20201101/20201101163853-pay_1.png";
        }else if(SMALL_PAY_MODE_1.getCn() == cn){
            url = "https://bai-tong-fen-xiao.oss-cn-beijing.aliyuncs.com/test/20201101/2020110116449-pay__1.png";
        }else if(SMALL_PAY_MODE_7.getCn() == cn){
            url = "https://bai-tong-fen-xiao.oss-cn-beijing.aliyuncs.com/test/20210324/20210324234643-balance.png";
        }
        return url;
    }



    public int getCn() {
        return cn;
    }

    public String getDesc() {
        return desc;
    }}
