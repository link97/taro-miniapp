package com.dmc.dict;

public enum WxLoginTypeDict {

    WX_LOGIN_OPENID(0, "小程序登录"),
    WX_LOGIN_OPENID_PHONE(1, "当前微信绑定手机号登录"),
    WX_LOGIN_OTHER_PHONE(2, "其他手机号登录");

    private final Integer cn;
    private final String desc;

    WxLoginTypeDict(Integer cn, String desc) {
        this.cn = cn;
        this.desc = desc;
    }

    public Integer getCn() {
        return cn;
    }

    public String getDesc() {
        return desc;
    }
}
