package com.dmc.dict;

import io.swagger.models.auth.In;

/**
 * 收益流水类型
 */
public enum BenefitTranTypeDict {
    BENEFIT_TRAN_TYPE_100(100, "拼团收益"),
    BENEFIT_TRAN_TYPE_200(200, "推荐奖"),
    BENEFIT_TRAN_TYPE_300(300, "自推荐奖"),
    BENEFIT_TRAN_TYPE_400(400, "商品佣金"),
    BENEFIT_TRAN_TYPE_500(500, "收益提现"),
    BENEFIT_TRAN_TYPE_600(600, "转账给他人"),
    BENEFIT_TRAN_TYPE_700(700, "被别人转账"),
    BENEFIT_TRAN_TYPE_800(800, "购买普通商品"),
    BENEFIT_TRAN_TYPE_900(900, "普通商品退回"),
    BENEFIT_TRAN_TYPE_1000(1000, "购买分销商"),
    BENEFIT_TRAN_TYPE_1100(1100, "购买分销商套餐"),
    BENEFIT_TRAN_TYPE_1200(1200, "提成奖金"),
    BENEFIT_TRAN_TYPE_1300(1300, "积分返现");

    private final int cn;
    private final String desc;

    BenefitTranTypeDict(int cn, String desc) {
        this.cn = cn;
        this.desc = desc;
    }

    public static String getDescByCn(Integer cn) {
        if(cn == null){
            return "无";
        }
        for (BenefitTranTypeDict e : BenefitTranTypeDict.values()) {
            if (e.cn== cn){
                return e.getDesc();
            }
        }
        return "无";
    }

    public int getCn() {
        return cn;
    }

    public String getDesc() {
        return desc;
    }

    /**
     * 检验是那个订单类型
     * @param type 0:无 1:分销订单 2:商品订单 3:提现订单
     * @return
     */
    public static int checkOrderType(Integer type){
        if(type == null){
            return 0;
        }
        int result = 0;
        if(type == BENEFIT_TRAN_TYPE_100.cn
                || type == BENEFIT_TRAN_TYPE_200.cn
                ||  type == BENEFIT_TRAN_TYPE_300.cn ){
            result = 1;
        }else if(type == BENEFIT_TRAN_TYPE_400.cn ){
            result = 2;
        }else if(type == BENEFIT_TRAN_TYPE_500.cn ){
            result = 3;
        }
        return result;
    }

}
