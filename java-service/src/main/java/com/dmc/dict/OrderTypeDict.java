package com.dmc.dict;

/**
 * 订单类型字典表
 */
public enum OrderTypeDict {
    ORDER_TYPE_GOODS(1, "商品订单"),
    ORDER_TYPE_DISTRBUTION(2, "分销商订单"),
    ORDER_TYPE_TRANSFER(3, "提现订单"),
    ORDER_TYPE_DISTRBUTION_PACKAGE(4, "分销商套餐包订单");

    private final int cn;
    private final String desc;

    OrderTypeDict(int cn, String desc) {
        this.cn = cn;
        this.desc = desc;
    }

    public int getCn() {
        return cn;
    }

    public String getDesc() {
        return desc;
    }}
