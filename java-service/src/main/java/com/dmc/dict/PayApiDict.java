package com.dmc.dict;

/**
 * 支付API
 */
public enum PayApiDict {

    PAY_API_WECHAT_PAY("https://api.mch.weixin.qq.com/pay/unifiedorder", "微信统一下单接口"),
    PAY_API_WECHAT_QUERY("https://api.mch.weixin.qq.com/pay/orderquery", "微信支付查询接口"),
    PAY_API_WECHAT_REFUND("https://api.mch.weixin.qq.com/secapi/pay/refund", "微信退款接口");

    private final String cn;
    private final String desc;

    PayApiDict(String cn, String desc) {
        this.cn = cn;
        this.desc = desc;
    }

    public String getCn() {
        return cn;
    }

    public String getDesc() {
        return desc;
    }}
