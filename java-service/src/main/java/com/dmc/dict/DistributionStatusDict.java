package com.dmc.dict;

/**
 * 分销订单状态字典表
 */
public enum DistributionStatusDict {
    ORDER_STATUS_CREATED(20000, "待付款"),
    ORDER_STATUS_AUDIT(21000, "待审核"),
    ORDER_STATUS_PAIED(30000, "已付款"),
    ORDER_STATUS_CLOSE(40000, "已结束");

    private final int cn;
    private final String desc;

    DistributionStatusDict(int cn, String desc) {
        this.cn = cn;
        this.desc = desc;
    }

    public static String getDescByCn(Integer cn) {
        if(cn == null){
            return "无";
        }
        for (DistributionStatusDict e : DistributionStatusDict.values()) {
            if (e.cn== cn){
                return e.getDesc();
            }
        }
        return "无";
    }

    public int getCn() {
        return cn;
    }

    public String getDesc() {
        return desc;
    }}
