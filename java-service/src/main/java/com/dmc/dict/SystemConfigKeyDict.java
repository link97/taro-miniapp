package com.dmc.dict;

/**
 * 系统配置key
 */
public enum SystemConfigKeyDict {
    SYSTME_CONFIG_CASHOUT_START_KEY("SYSTME_CONFIG_CASHOUT_START_KEY", "是否开启在线提现"),
    SYSTME_CONFIG_CASHOUT_START_VALUE_OPEN("OPEN", "开启在线提现开启"),
    SYSTME_CONFIG_CASHOUT_START_VALUE_CLOSE("COLSE", "开启在线提现关闭");

    private final String cn;
    private final String desc;

    SystemConfigKeyDict(String cn, String desc) {
        this.cn = cn;
        this.desc = desc;
    }

    public String getCn() {
        return cn;
    }

    public String getDesc() {
        return desc;
    }}
