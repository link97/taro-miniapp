package com.dmc.dict;


/**
 * 分销商购买支付类型
 */
public enum DistributionBonusRecordTypeDict {
    DISTRIBUTION_BONUS_RECORD_TYPE_1(100, "购买分销商"),
    DISTRIBUTION_BONUS_RECORD_TYPE_2(200, "被互助助益"),
    DISTRIBUTION_BONUS_RECORD_TYPE_3(300, "互助助益其他订单"),
    DISTRIBUTION_BONUS_RECORD_TYPE_4(400, "复投分销商"),
    DISTRIBUTION_BONUS_RECORD_TYPE_5(500, "分销商升级"),
    DISTRIBUTION_BONUS_RECORD_TYPE_6(600, "收益");

    private final int cn;
    private final String desc;

    DistributionBonusRecordTypeDict(int cn, String desc) {
        this.cn = cn;
        this.desc = desc;
    }

    public static String getDescByCn(Integer cn) {
        if(cn == null){
            return "无";
        }
        for (DistributionBonusRecordTypeDict e : DistributionBonusRecordTypeDict.values()) {
            if (e.cn== cn){
                return e.getDesc();
            }
        }
        return "无";
    }

    public static boolean checkUseType(Integer cn) {
        if(cn == null){
            return false;
        }
        return DISTRIBUTION_BONUS_RECORD_TYPE_4.cn == cn
                ||DISTRIBUTION_BONUS_RECORD_TYPE_5.cn == cn
                ||DISTRIBUTION_BONUS_RECORD_TYPE_6.cn == cn;
    }

    public int getCn() {
        return cn;
    }

    public String getDesc() {
        return desc;
    }

}
