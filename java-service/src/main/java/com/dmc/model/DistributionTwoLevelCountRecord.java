package com.dmc.model;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;



import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 两级分销订单汇总记录表
 * </p>
 *
 * @author 
 * @since 2020-10-27
 */
@Data
@Accessors(chain = true)
@TableName("t_distribution_two_level_count_record")
public class DistributionTwoLevelCountRecord extends Model<DistributionTwoLevelCountRecord> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * t_distribution_two_level_count ID
     */
    @TableField("two_level_count_id")
    private Long twoLevelCountId;

    /**
     * 分销订单ID
     */
    @TableField("order_id")
    private Long orderId;

    /**
     * 助力分销订单ID
     */
    @TableField("help_order_id")
    private Long helpOrderId;

    /**
     * 用户ID
     */
    @TableField("user_id")
    private Long userId;

    /**
     * 下级助力用户ID
     */
    @TableField("help_user_id")
    private Long helpUserId;

    @TableField("create_time")
    private Date createTime;

    @TableField("update_time")
    private Date updateTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
