package com.dmc.model;

import com.baomidou.mybatisplus.enums.IdType;
import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * <p>
 * 助益关系链
 * </p>
 *
 * @
 * @since 2020-09-09
 */
@Data
@Accessors(chain = true)
@TableName("t_distribution_help_relation")
public class DistributionHelpRelation extends Model<DistributionHelpRelation> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 分销订单ID
     */
    @TableField("distribution_order_id")
    private Long distributionOrderId;

    /**
     * 订单购买人
     */
    @TableField("user_id")
    private Long userId;

    /**
     * 助益时,订单的级别
     */
    @TableField("level_flag")
    private String levelFlag;

    /**
     * 被助益订单
     */
    @TableField("help_order_id")
    private Long helpOrderId;

    /**
     * 被助人
     */
    @TableField("help_user_id")
    private Long helpUserId;
    /**
     * 0:我被助益 1:我助益别人
     */
    @TableField("type")
    private Integer type;

    /**
     * 助益金额
     */
    @TableField("help_money")
    private BigDecimal helpMoney;

    @TableField("create_time")
    private Date createTime;

    @TableField("update_time")
    private Date updateTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
