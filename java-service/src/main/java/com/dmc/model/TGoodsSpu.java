package com.dmc.model;

import com.baomidou.mybatisplus.enums.IdType;

import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;



import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 商品spu表
 * </p>
 *
 * @author dmc
 * @since 2020-09-05
 */
@Data
@TableName("t_goods_spu")
public class TGoodsSpu extends Model<TGoodsSpu> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 商品SPU编号，唯一
     */
    @TableField("spu_no")
    private String spuNo;

    /**
     * 商品名称
     */
    @TableField("goods_name")
    private String goodsName;

    /**
     * 商品sku规格类型 如重量  颜色
     */
    @TableField("sku_title_type")
    private String skuTitleType;

    /**
     * 分类id
     */
    @TableField("category_id")
    private Long categoryId;

    /**
     * 0:普通商品 1:特殊商品
     */
    private Integer special;

    /**
     *0:普通商品 1:积分商品
     */
    private Integer integralStatus;

    /**
     * 积分商品的积分价格
     */
    private BigDecimal integral;



    /**
     * 0:普通商品 1:推荐商品
     */
    private Integer recommend;

    /**
     * 商品单人购买数量限制
     */
    @TableField("pay_num_limit_status")
    private Integer payNumLimitStatus;
    /**
     * 商品单人购买数量限制
     */
    @TableField("pay_num_limit")
    private Integer payNumLimit;

    /**
     * 0:普通商品 1:会员商品
     */
    private Integer vip;


    /**
     * 商品图片地址
     */
    @TableField("sku_img_url")
    private String skuImgUrl;

    /**
     * 商品详情
     */
    @TableField("sku_detail")
    private String skuDetail;

    /**
     * 商品详情图片地址
     */
    @TableField("sku_detail_img_url")
    private String skuDetailImgUrl;

    /**
     * 删除标识:0:未删除,1删除
     */
    private Integer deleted;


    @TableField("create_time")
    private Date createTime;

    @TableField("update_time")
    private Date updateTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
