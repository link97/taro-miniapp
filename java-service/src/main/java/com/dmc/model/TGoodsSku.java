package com.dmc.model;

import com.baomidou.mybatisplus.enums.IdType;
import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;



import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 商品SKU表
 * </p>
 *
 * @author dmc
 * @since 2020-09-05
 */
@Data
@TableName("t_goods_sku")
public class TGoodsSku extends Model<TGoodsSku> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * spu Id
     */
    @TableField("goods_spu_id")
    private Long goodsSpuId;

    /**
     * 商品sku编号
     */
    @TableField("sku_no")
    private String skuNo;

    /**
     * 商品sku规格类型 如重量  颜色
     */
    @TableField("sku_title_type")
    private String skuTitleType;

    /**
     * 商品sku规格
     */
    @TableField("sku_title")
    private String skuTitle;

    /**
     * 商品封面图片地址
     */
    @TableField("sku_covers_url")
    private String skuCoversUrl;

    /**
     * 商品图片地址
     */
    @TableField("sku_img_url")
    private String skuImgUrl;

    /**
     * 商品库存
     */
    @TableField("sku_num")
    private Integer skuNum;

    /**
     * 删除标识:0:未删除,1删除
     */
    private Integer deleted;


    /**
     * 商品销售数量
     */
    @TableField("sku_sale_num")
    private Integer skuSaleNum;

    /**
     * sku 原价
     */
    @TableField("sku_cost_price")
    private BigDecimal skuCostPrice;

    /**
     * 需要使用积分
     */
    @TableField("integral")
    private BigDecimal integral;

    /**
     * sku 现价
     */
    @TableField("sku_curr_price")
    private BigDecimal skuCurrPrice;

    /**
     * 商品描述
     */
    private String des;

    /**
     * 状态:1启用,0禁用
     */
    private Integer status;

    @TableField("create_time")
    private Date createTime;

    @TableField("update_time")
    private Date updateTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
