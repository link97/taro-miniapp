package com.dmc.model;

import com.baomidou.mybatisplus.enums.IdType;
import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;



import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 互助分红使用记录
 * </p>
 *
 * @author 
 * @since 2020-10-31
 */
@Data
@Accessors(chain = true)
@TableName("t_distribution_bonus_user_record")
public class DistributionBonusUserRecord extends Model<DistributionBonusUserRecord> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.INPUT)
    private Long id;

    /**
     * 产生分红的订单
     */
    @TableField("order_id")
    private Long orderId;

    /**
     * 使用分红时,订单的级别
     */
    @TableField("level_flag")
    private String levelFlag;

    /**
     * 使用分红的订单
     */
    @TableField("use_order_id")
    private Long useOrderId;

    /**
     * 分红金额
     */
    @TableField("bonus_presentation")
    private BigDecimal bonusPresentation;

    /**
     * 使用后当时金额
     */
    @TableField("bonus_balance")
    private BigDecimal bonusBalance;

    /**
     * 使用分红类型
     */
    private Integer type;

    /**
     * 描述
     */
    private String des;

    @TableField("create_time")
    private Date createTime;

    @TableField("update_time")
    private Date updateTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
