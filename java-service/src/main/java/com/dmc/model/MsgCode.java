package com.dmc.model;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

@Data
@ApiModel("验证码")
@TableName("t_msg_code")
public class MsgCode extends Model<TMsgCode> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    @ApiModelProperty(value="用户ID",name = "id")
    private Long id;
    @ApiModelProperty(value="手机号",name = "phone")
    private String phone;
    @ApiModelProperty(value="验证码",name = "code")
    private String code;
    @ApiModelProperty(value="创建时间",name = "createTime")
    private Date createTime;
    @ApiModelProperty(value="是否停用 0:启用 1:停用",name = "stop")
    private Integer stop;
    @ApiModelProperty(value="验证码类型,0默认用户验证,1绑定银行卡",name = "type")
    private Integer type;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }
}
