package com.dmc.model;

import com.baomidou.mybatisplus.enums.IdType;
import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;



import com.baomidou.mybatisplus.annotations.Version;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 积分流水
 * </p>
 *
 * @
 * @since 2020-09-08
 */
@Data
@Accessors(chain = true)
@TableName("t_integral_transaction")
@ApiModel
public class IntegralTransaction extends Model<IntegralTransaction> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 用户ID
     */
    @TableField("user_id")
    private Long userId;

    /**
     * 流水类型,1兑换普通商品 2分享商品 3分享好友  4下级分销
     */
    @ApiModelProperty(value="流水类型,100购买普通商品 200分享商品",name = "type")
    private Integer type;

    /**
     * 积分
     */
    @ApiModelProperty(value="积分",name = "integral")
    private BigDecimal integral;

    /**
     * 产生积分的订单
     */
    @TableField("order_id")
    @ApiModelProperty(value="订单号",name = "orderId")
    private Long orderId;

    /**
     * 描述
     */
    @ApiModelProperty(value="描述",name = "des")
    private String des;

    @TableField("create_time")
    @ApiModelProperty(value="创建时间",name = "createTime")
    private Date createTime;

    @TableField("update_time")
    @ApiModelProperty(value="更新数据时间",name = "updateTime")
    private Date updateTime;
    @TableField("account_total_integral")
    @ApiModelProperty(value="用户总积分",name = "accountTotalIntegral")
    private BigDecimal accountTotalIntegral;

    //非数据库字段
    @TableField(exist = false)
    @ApiModelProperty(value="用户账户总积分",name = "userTotalIntegral")
    private BigDecimal userTotalIntegral;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
