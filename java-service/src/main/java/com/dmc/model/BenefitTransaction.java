package com.dmc.model;

import com.baomidou.mybatisplus.enums.IdType;
import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;



import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 收益流水
 * </p>
 *
 * @
 * @since 2020-09-08
 */
@Data
@Accessors(chain = true)
@TableName("t_benefit_transaction")
public class BenefitTransaction extends Model<BenefitTransaction> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 用户ID
     */
    @TableField("user_id")
    private Long userId;

    /**
     * 转账另一人ID
     */
    @TableField("other_user_id")
    private Long otherUserId;


    /**
     * 订单ID
     */
    @TableField("order_id")
    private Long orderId;
    /**
     * 变更后收益
     */
    private BigDecimal currentTotalBenefit;
    /**
     * 当前类型变更后收益
     */
    private BigDecimal currentTypeBenefit;

    /**
     * 流水类型
     */
    private Integer type;

    /**
     * 收益
     */
    private BigDecimal benefit;



    /**
     * 描述
     */
    private String des;

    @TableField("create_time")
    private Date createTime;

    @TableField("update_time")
    private Date updateTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    public BenefitTransaction() {
    }

    public BenefitTransaction(Long userId, Long otherUserId, Long orderId, BigDecimal currentTotalBenefit, Integer type, BigDecimal benefit, String des) {
        this.userId = userId;
        this.otherUserId = otherUserId;
        this.orderId = orderId;
        this.currentTotalBenefit = currentTotalBenefit;
        this.type = type;
        this.benefit = benefit;
        this.des = des;
    }
}
