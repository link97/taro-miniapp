package com.dmc.model;

import com.baomidou.mybatisplus.enums.IdType;
import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;



import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 会员荣誉身份配置
 * </p>
 *
 * @author 
 * @since 2021-03-17
 */
@Data
@Accessors(chain = true)
@TableName("t_vip_config")
public class VipConfig extends Model<VipConfig> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.INPUT)
    private Long id;

    /**
     * 会员荣誉身份名称
     */
    @TableField("vip_name")
    private String vipName;

    /**
     * 会员荣誉身份级别
     */
    private Integer level;

    /**
     * 业绩累积代数
     */
    private Integer generations;



    /**
     * 分销商数量
     */
    @TableField("distribution_user_num")
    private Integer distributionUserNum;

    /**
     * 分销商会员级别
     */
    @TableField("distribution_user_level")
    private Integer distributionUserLevel;

    /**
     * 业基金额
     */
    private BigDecimal achievement;

    /**
     * 1:停用 0:正常使用
     */
    private Integer status;

    /**
     * 会员荣誉身份描述
     */
    private String des;

    /**
     * 删除标示(默认0未删除)
     */
    private Integer deleted;

    @TableField("create_time")
    private Date createTime;

    @TableField("update_time")
    private Date updateTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
