package com.dmc.model;

import com.baomidou.mybatisplus.enums.IdType;

import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;



import com.baomidou.mybatisplus.annotations.Version;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 业绩奖配置详情
 * </p>
 *
 * @author 
 * @since 2021-03-18
 */
@Data
@Accessors(chain = true)
@TableName("t_achievement_bonus_config_detail")
public class AchievementBonusConfigDetail extends Model<AchievementBonusConfigDetail> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.INPUT)
    private Long id;

    /**
     * 会员荣誉身份名称
     */
    @TableField("config_id")
    private Long configId;

    /**
     * 代数
     */
    private Integer generations;
    /**
     * 中级奖金
     */
    @ApiModelProperty(value="中级奖金",name = "zhongJiBonus")
    @TableField("zhong_ji_bonus")
    private BigDecimal zhongJiBonus;

    /**
     * 高级奖金
     */
    @ApiModelProperty(value="高级奖金",name = "gaoJiBonus")
    @TableField("gao_ji_bonus")
    private BigDecimal gaoJiBonus;
    /**
     * 会员荣誉身份描述
     */
    private String des;

    /**
     * 删除标示(默认0未删除)
     */
    private Integer deleted;

    @TableField("create_time")
    private Date createTime;

    @TableField("update_time")
    private Date updateTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
