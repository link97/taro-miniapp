package com.dmc.model;

import com.baomidou.mybatisplus.enums.IdType;
import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;



import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 分销商品表
 * </p>
 *
 * @
 * @since 2020-09-12
 */
@Data
@Accessors(chain = true)
@TableName("t_distribution_order_goods")
public class DistributionOrderGoods extends Model<DistributionOrderGoods> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 订单信息
     */
    @TableField("order_id")
    private Long orderId;

    /**
     * 分销名称
     */
    @TableField("distribution_name")
    private String distributionName;

    /**
     * 分销级别标识
     */
    @TableField("level_flag")
    private String levelFlag;
    /**
     * 推荐人奖励
     */
    @TableField("share_rewards")
    private BigDecimal shareRewards;

    /**
     * VIP会员推荐人奖励
     */
    @TableField("vip_share_rewards")
    private BigDecimal vipShareRewards;

    /**
     * 购买消费限制 如必须购买288才能购买初级分销商
     */
    @TableField("buy_limit")
    private BigDecimal buyLimit;

    /**
     * 分销价格
     */
    private BigDecimal price;

    /**
     * 赠送积分
     */
    @TableField("bonus_points")
    private BigDecimal bonusPoints;

    /**
     * 赠送分红
     */
    @TableField("bonus_presentation")
    private BigDecimal bonusPresentation;

    /**
     * 晋升所需分红限制
     */
    @TableField("promoted_presentation_limit")
    private BigDecimal promotedPresentationLimit;

    /**
     * 晋升可得分红
     */
    @TableField("promoted_presentation_add")
    private BigDecimal promotedPresentationAdd;

    /**
     * 晋升可得收益
     */
    @TableField("promoted_income_add")
    private BigDecimal promotedIncomeAdd;

    /**
     * 状态:1上架,0下架
     */
    private Integer status;

    /**
     * 分销商品描述
     */
    private String des;

    @TableField("create_time")
    private Date createTime;

    @TableField("update_time")
    private Date updateTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
