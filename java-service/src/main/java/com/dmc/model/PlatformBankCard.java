package com.dmc.model;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;



import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 平台银行账户信息
 * </p>
 *
 * @author 
 * @since 2020-10-28
 */
@Data
@Accessors(chain = true)
@TableName("t_platform_bank_card")
public class PlatformBankCard extends Model<PlatformBankCard> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 银行卡所属银行ID
     */
    @TableField("bank_id")
    private Integer bankId;

    /**
     * 银行名称
     */
    @TableField("bank_name")
    private String bankName;

    /**
     * 银行卡号
     */
    @TableField("bank_num")
    private String bankNum;

    /**
     * 银行卡号持卡人姓名
     */
    @TableField("bank_user_name")
    private String bankUserName;

    /**
     * 0:启用 1:禁用
     */
    private Integer stop;

    @TableField("create_time")
    private Date createTime;

    @TableField("update_time")
    private Date updateTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
