package com.dmc.model;

import com.baomidou.mybatisplus.enums.IdType;
import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;



import com.baomidou.mybatisplus.annotations.Version;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 分销订单表
 * </p>
 *
 * @
 * @since 2020-09-08
 */
@Data
@Accessors(chain = true)
@TableName("t_distribution_order")
public class DistributionOrder extends Model<DistributionOrder> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 用户ID
     */
    @TableField("user_id")
    private Long userId;

    /**
     * 套餐包ID
     */
    @TableField("package_id")
    private Long packageId;

    /**
     * 上级分销订单ID
     */
    @TableField("p_id")
    private Long pId;

    @TableField("order_no")
    private String orderNo;

    /**
     * 邀请人ID
     */
    @TableField("share_user_id")
    private Long shareUserId;

    /**
     * 分销ID
     */
    @TableField("distribution_goods_id")
    private Long distributionGoodsId;

    /**
     * 购买单据ID
     */
    @TableField("checkout_id")
    private Long checkoutId;

    /**
     * 支付方式 0:金钱购买 1:分红支付 2:分红支付-终极会员支付
     */
    @TableField("pay_type")
    private Integer payType;

    /**
     * 0:分销商订单 1:套餐包订单 2:套餐包子订单
     */
    @TableField("order_type")
    private Integer orderType;

    /**
     * 订单状态 0:待支付 1:已支付
     */
    @TableField("order_status")
    private Integer orderStatus;

    /**
     * 是否特权订单 0:否 1:是
     */
    @TableField("vip_orders")
    private Integer vipOrders;
    /**
     * 成为vip的时间
     */
    @TableField("vip_time")
    private Date vipTime;
    /**
     * 购买金额
     */
    @TableField("price")
    private BigDecimal price;

    /**
     * 赠送积分
     */
    @TableField("bonus_points")
    private BigDecimal bonusPoints;
    /**
     * 银行表主键ID
     */
    @TableField("bank_id")
    private Integer bankId;
    /**
     * 银行卡号
     */
    @TableField("bank_card_num")
    private String bankCardNum;

    /**
     * 转账凭证地址
     */
    @TableField("receipt_url")
    private String receiptUrl;
    /**
     * 平台银行表主键ID
     */
    @TableField("platform_id")
    private Integer platformId;

    /**
     * 平台银行卡所属银行ID
     */
    @TableField("platform_bank_id")
    private Integer platformBankId;

    /**
     * 平台银行名称
     */
    @TableField("bank_name")
    private String bankName;

    /**
     * 平台银行卡号
     */
    @TableField("bank_num")
    private String bankNum;

    /**
     * 平台银行卡号持卡人姓名
     */
    @TableField("bank_user_name")
    private String bankUserName;

    /**
     * 获得分红
     */
    @TableField("bonus_presentation")
    private BigDecimal bonusPresentation;

    @TableField("settle_time")
    private Date settleTime;

    @TableField("create_time")
    private Date createTime;

    @TableField("update_time")
    private Date updateTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
