package com.dmc.model;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;



import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 分销商品套餐表关联商品表
 * </p>
 *
 * @author 
 * @since 2021-03-17
 */
@Data
@Accessors(chain = true)
@TableName("t_distribution_package_goods")
public class DistributionPackageGoods extends Model<DistributionPackageGoods> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.INPUT)
    private Long id;

    /**
     * 套餐包ID
     */
    @TableField("package_id")
    private Long packageId;

    /**
     * 商品ID
     */
    @TableField("good_id")
    private Long goodId;

    /**
     * 商品数量
     */
    @TableField("good_num")
    private Integer goodNum;

    /**
     * 套餐包描述
     */
    private String des;

    /**
     * 删除标示(默认0未删除)
     */
    private Integer deleted;

    @TableField("create_time")
    private Date createTime;

    @TableField("update_time")
    private Date updateTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
