package com.dmc.model;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 支付相关记录表
 * </p>
 *
 * @author dmc
 * @since 2020-07-25
 */
@Accessors(chain = true)
public class Payment extends Model<Payment> {

    private static final long serialVersionUID = -8684754987654567L;

    /**
     * 自定义主键
     */
    private Long id;

    /**
     * 用户主键
     */
    @TableField("user_id")
    private Long userId;

    /**
     * 交易编码
     */
    @TableField("pay_no")
    private String payNo;

    /**
     * 订单编码
     */
    @TableField("order_id")
    private String orderId;

    /**
     * 订单类型
     */
    @TableField("order_type")
    private Integer orderType;

    /**
     * 0:线上 1:线下
     */
    @TableField("offline_status")
    private Integer offlineStatus;


    /**
     *金额
     */
    @TableField("amount")
    private BigDecimal amount;

    /**
     * 交易编号
     */
    @TableField("checkout_id")
    private Long checkoutId;

    /**
     * 1:付款 2:退款 3:提现
     */
    @TableField("pay_type")
    private Integer payType;

    /**
     * 业务单据状态
     */
    @TableField("payment_status")
    private Integer paymentStatus;

    /**
     * 第三方接口调用状态
     */
    @TableField("trade_status")
    private String tradeStatus;

    /**
     * 第三方接口返回详情
     */
    @TableField("payment_result")
    private String paymentResult;

    /**
     * 发起时间时间
     */
    @TableField("start_time")
    private Date startTime;

    /**
     * 成功完成时间
     */
    @TableField("complete_time")
    private Date completeTime;

    @TableField("create_time")
    private Date createTime;

    @TableField("last_updated")
    private Date lastUpdated;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Integer getOrderType() {
        return orderType;
    }

    public void setOrderType(Integer orderType) {
        this.orderType = orderType;
    }

    public Integer getOfflineStatus() {
        return offlineStatus;
    }

    public void setOfflineStatus(Integer offlineStatus) {
        this.offlineStatus = offlineStatus;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Long getCheckoutId() {
        return checkoutId;
    }

    public void setCheckoutId(Long checkoutId) {
        this.checkoutId = checkoutId;
    }

    public Integer getPayType() {
        return payType;
    }

    public void setPayType(Integer payType) {
        this.payType = payType;
    }

    public Integer getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(Integer paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getTradeStatus() {
        return tradeStatus;
    }

    public void setTradeStatus(String tradeStatus) {
        this.tradeStatus = tradeStatus;
    }

    public String getPaymentResult() {
        return paymentResult;
    }

    public void setPaymentResult(String paymentResult) {
        this.paymentResult = paymentResult;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getCompleteTime() {
        return completeTime;
    }

    public void setCompleteTime(Date completeTime) {
        this.completeTime = completeTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public String getPayNo() {
        return payNo;
    }

    public void setPayNo(String payNo) {
        this.payNo = payNo;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
