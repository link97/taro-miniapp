package com.dmc.model;

import com.baomidou.mybatisplus.enums.IdType;

import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;



import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 业绩奖配置
 * </p>
 *
 * @author 
 * @since 2021-03-18
 */
@Data
@Accessors(chain = true)
@TableName("t_achievement_bonus_config")
public class AchievementBonusConfig extends Model<AchievementBonusConfig> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.INPUT)
    private Long id;

    /**
     * 业绩奖配置名称
     */
    @TableField("config_name")
    private String configName;

    /**
     * 分销级别标识
     */
    @TableField("level_flag")
    private String levelFlag;

    /**
     * 累积代数
     */
    @TableField("generations")
    private Integer generations;

    /**
     * 享受此配置的业绩总额限制
     */
    @TableField("achievement_limit")
    private BigDecimal achievementLimit;

    /**
     * 1:停用 0:正常使用
     */
    private Integer status;

    /**
     * 会员荣誉身份描述
     */
    private String des;

    /**
     * 删除标示(默认0未删除)
     */
    private Integer deleted;

    @TableField("create_time")
    private Date createTime;

    @TableField("update_time")
    private Date updateTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
