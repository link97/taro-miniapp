package com.dmc.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("权限资源")
public class Resource implements java.io.Serializable {
	@ApiModelProperty(value="资源ID",name = "id")
	private Long id;
	@ApiModelProperty(value="资源类型 0:菜单 1:功能权限",name = "type")
	private String type;
	@ApiModelProperty(value="权限资源父ID",name = "pid")
	private Long pid;
	@ApiModelProperty(value="权限资源父名称",name = "pname")
	private String pname;
	@ApiModelProperty(value="访问方法",name = "method")
	private String method;
	@ApiModelProperty(value="资源名称",name = "name")
	private String name;
	@ApiModelProperty(value="备注",name = "remark")
	private String remark;
	@ApiModelProperty(value="排序 从小到大",name = "seq")
	private Integer seq;
	@ApiModelProperty(value="资源对应接口或者页面地址",name = "url")
	private String url;
	@ApiModelProperty(value="角色ID",name = "roleId")
	private String roleId;
	@ApiModelProperty(value="角色名称",name = "roleName")
	private String roleName;



}
