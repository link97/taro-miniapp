package com.dmc.model;

import com.baomidou.mybatisplus.enums.IdType;
import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * <p>
 * 发票表
 * </p>
 *
 * @author
 * @since 2020-09-24
 */
@Data
@Accessors(chain = true)
@TableName("t_order_receipt")
public class OrderReceipt extends Model<OrderReceipt> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 用户ID
     */
    @TableField("user_id")
    private Long userId;

    /**
     * 订单ID
     */
    @TableField("order_id")
    private Long orderId;

    /**
     * 订单类型 1:商品订单 2:分销商订单 
     */
    @TableField("order_type")
    private Integer orderType;

    /**
     * 发票类型 1:增值税专用发票 2:增值税普通发票
     */
    @TableField("receipt_type")
    private Integer receiptType;

    /**
     * 发票状态 0:提交发票申请 1:已开区发票
     */
    @TableField("receipt_status")
    private Integer receiptStatus;

    /**
     * 电子发票地址
     */
    @TableField("receipt_url")
    private String receiptUrl;

    /**
     * 邮箱地址
     */
    @TableField("email")
    private String email;


    /**
     * 发票金额
     */
    @TableField("receipt_price")
    private BigDecimal receiptPrice;

    /**
     * 开票公司名称
     */
    @TableField("receipt_company")
    private String receiptCompany;

    /**
     * 纳税人识别号
     */
    @TableField("receipt_taxpayer_num")
    private String receiptTaxpayerNum;

    /**
     * 开户行名称
     */
    @TableField("receipt_brand_name")
    private String receiptBrandName;

    /**
     * 银行账号
     */
    @TableField("receipt_brand_num")
    private String receiptBrandNum;

    /**
     * 地址
     */
    private String address;

    /**
     * 电话
     */
    private String phone;
    /**
     * 发票用户名称
     */
    @TableField("receipt_user_name")
    private String receiptUserName;

    @TableField("receipt_time")
    private Date receiptTime;

    @TableField("create_time")
    private Date createTime;

    @TableField("update_time")
    private Date updateTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
