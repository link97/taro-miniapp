package com.dmc.model;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;



import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用户商品购买数量统计
 * </p>
 *
 * @author 
 * @since 2021-04-01
 */
@Data
@Accessors(chain = true)
@TableName("t_user_spu_pay_num")
public class UserSpuPayNum extends Model<UserSpuPayNum> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 用户
     */
    @TableField("user_id")
    private Long userId;

    /**
     * 购买商品ID
     */
    @TableField("spu_id")
    private Long spuId;

    @TableField("num")
    private Integer num;

    @TableField("create_time")
    private Date createTime;

    @TableField("update_time")
    private Date updateTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
