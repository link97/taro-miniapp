package com.dmc.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * Error
 * <p>
 * Error
 *
 * @author yangfan
 */
@Data
@ApiModel("返回值基类")
public class RestRespResult<T> implements java.io.Serializable {


    public static final Integer OK = 200;
    public static final Integer ERROR = 500;
    public static final Integer NO_PERMISSION = 10001;
    public static final Integer NO_SESSION = 10002;
    public static final Integer NOT_FOUND = 404;

    // 默认成功
    @ApiModelProperty(value="返回体 code",name = "code")
    private Integer code = OK;
    @ApiModelProperty(value="返回体描述",name = "message")
    private String message;
    @ApiModelProperty(value="返回体实际数据",name = "data")
    private T data;
    private String path;
    private Date timestamp;
    @ApiModelProperty(value="返回数据总数 分页时",name = "total")
    private int total;
    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    private RestRespResult() {

    }

    private RestRespResult(T data) {
        this.data = data;
    }

    private RestRespResult(Integer code, String message, String path) {
        this.code = code;
        this.message = message;
        this.path = path;
        this.timestamp = new Date();
    }

    private RestRespResult(Integer code, String message) {
        this(code, message, null);
    }


    public static RestRespResult ok(String message) {
        return new RestRespResult(OK, message);
    }

    public static RestRespResult error(Integer code, String message) {
        return new RestRespResult(code, message);
    }

    public static RestRespResult error(Integer code, String message, String path) {
        return new RestRespResult(code, message, path);
    }
}
