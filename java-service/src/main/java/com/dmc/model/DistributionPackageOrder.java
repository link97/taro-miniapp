package com.dmc.model;

import com.baomidou.mybatisplus.enums.IdType;
import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;



import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 分销套餐包订单表
 * </p>
 *
 * @author 
 * @since 2021-03-17
 */
@Data
@Accessors(chain = true)
@TableName("t_distribution_package_order")
public class DistributionPackageOrder extends Model<DistributionPackageOrder> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.INPUT)
    private Long id;

    /**
     * 订单编码
     */
    @TableField("order_no")
    private String orderNo;

    /**
     * 用户ID
     */
    @TableField("user_id")
    private Long userId;

    /**
     * 金额购买单据ID
     */
    @TableField("checkout_id")
    private Long checkoutId;

    /**
     * 邀请人ID
     */
    @TableField("share_user_id")
    private Long shareUserId;

    /**
     * 支付方式 0:金钱购买 1:分红支付 2:分红支付-终极会员支付
     */
    @TableField("pay_type")
    private Integer payType;

    /**
     * 订单状态 0:待支付 1:已支付
     */
    @TableField("order_status")
    private Integer orderStatus;

    /**
     * 套餐包ID
     */
    @TableField("package_id")
    private Long packageId;

    /**
     * 银行表主键ID
     */
    @TableField("bank_id")
    private Integer bankId;

    /**
     * 银行卡号
     */
    @TableField("bank_card_num")
    private String bankCardNum;

    /**
     * 转账凭证地址
     */
    @TableField("receipt_url")
    private String receiptUrl;

    /**
     * 平台银行表主键ID
     */
    @TableField("platform_id")
    private Integer platformId;

    /**
     * 银行卡所属银行ID
     */
    @TableField("platform_bank_id")
    private Integer platformBankId;

    /**
     * 银行名称
     */
    @TableField("bank_name")
    private String bankName;

    /**
     * 银行卡号
     */
    @TableField("bank_num")
    private String bankNum;

    /**
     * 银行卡号持卡人姓名
     */
    @TableField("bank_user_name")
    private String bankUserName;

    /**
     * 当时折扣
     */
    private BigDecimal discount;

    /**
     * 当时价格
     */
    private BigDecimal price;

    /**
     * 获得分红
     */
    @TableField("bonus_presentation")
    private BigDecimal bonusPresentation;

    /**
     * 结算时间
     */
    @TableField("settle_time")
    private Date settleTime;

    @TableField("create_time")
    private Date createTime;

    @TableField("update_time")
    private Date updateTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
