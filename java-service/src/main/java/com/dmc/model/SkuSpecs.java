package com.dmc.model;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;



import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 商品SKU规格表
 * </p>
 *
 * @author 
 * @since 2021-03-22
 */
@Data
@Accessors(chain = true)
@TableName("t_sku_specs")
public class SkuSpecs extends Model<SkuSpecs> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 规格类型
     */
    @TableField("sku_type")
    private String skuType;

    /**
     * 规格名称
     */
    @TableField("sku_title")
    private String skuTitle;

    /**
     * 商品ID
     */
    @TableField("sku_id")
    private Long skuId;

    /**
     * 状态:1上架,0下架
     */
    private Integer status;

    /**
     * 删除标识:0:未删除,1删除
     */
    private Integer deleted;

    @TableField("create_time")
    private Date createTime;

    @TableField("update_time")
    private Date updateTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
