package com.dmc.model;

import com.baomidou.mybatisplus.enums.IdType;
import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;



import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 提现表
 * </p>
 *
 * @author
 * @since 2020-09-13
 */
@Data
@Accessors(chain = true)
@TableName("t_cash_out_orders")
public class CashOutOrders extends Model<CashOutOrders> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 用户ID
     */
    @TableField("user_id")
    private Long userId;

    /**
     * 提现订单编码
     */
    @TableField("order_code")
    private String orderCode;

    /**
     * 持卡人姓名
     */
    @TableField("band_user_name")
    private String bandUserName;

    /**
     * 备注
     */
    @TableField("remark")
    private String remark;

    /**
     * 提现类型 0:银行卡 1:零钱
     */
    @TableField("cash_out_type")
    private Integer cashOutType;

    /**
     * 0:线上 1:线下
     */
    @TableField("cash_out_line_type")
    private Integer cashOutLineType;

    /**
     * 提现金额
     */
    private BigDecimal ammount;

    /**
     * 实际到账金额
     */
    private BigDecimal arriveAmmount;

    /**
     * 手续费
     */
    private BigDecimal transferCharge;
    /**
     * 当时的手续费率
     */
    private BigDecimal transferChargeRate;
    /**
     * 0:待提现 1:提现中 2:已提现
     */
    private Integer status;

    /**
     * 银行表ID
     */
    @TableField("band_id")
    private Long bandId;

    /**
     * 银行卡号
     */
    @TableField("band_num")
    private String bandNum;

    /**
     * 凭证
     */
    @TableField("voucher_url")
    private String voucherUrl;

    /**
     * 提现时间
     */
    @TableField("cash_out_time")
    private Date cashOutTime;

    @TableField("create_time")
    private Date createTime;

    @TableField("update_time")
    private Date updateTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
