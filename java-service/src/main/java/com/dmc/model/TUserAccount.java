package com.dmc.model;

import com.baomidou.mybatisplus.enums.IdType;
import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;



import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用户账户表
 * </p>
 *
 * @author dmc
 * @since 2020-09-05
 */
@Data
@TableName("t_user_account")
public class TUserAccount extends Model<TUserAccount> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 用户ID
     */
    @TableField("user_id")
    private Long userId;

    /**
     * 支付密码
     */
    @TableField("pay_password")
    private String payPassword;

    /**
     * 积分
     */
    private BigDecimal integral;

    /**
     * 需要返现的积分
     */
    @TableField("integral_return")
    private BigDecimal integralReturn;

    /**
     * 分红收益
     */
    private BigDecimal benefit;

    /**
     * '业绩统计金额'
     */
    private BigDecimal achievement;

    /**
     * '业绩奖统计金额'
     */
    private BigDecimal achievementBonus;

    @TableField("create_time")
    private Date createTime;

    @TableField("update_time")
    private Date updateTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
