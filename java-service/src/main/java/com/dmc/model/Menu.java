package com.dmc.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel("按钮")
public class Menu implements java.io.Serializable {

    @ApiModelProperty(value="按钮ID",name = "id")
    private Long id;
    @ApiModelProperty(value="按钮名称",name = "text")
    private String text;
    @ApiModelProperty(value="按钮地址",name = "url")
    private String url;
    @ApiModelProperty(value="按钮子集",name = "children")
    private List<Menu> children;
}
