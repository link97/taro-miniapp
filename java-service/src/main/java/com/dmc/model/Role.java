package com.dmc.model;

import com.baomidou.mybatisplus.annotations.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel("角色")
public class Role implements java.io.Serializable {
    @ApiModelProperty(value="角色ID",name = "id")
    private Long id;
    @ApiModelProperty(value="父级角色ID",name = "pid")
    private Long pid;
    @ApiModelProperty(value="父级角色名称",name = "pname")
    private String pname;
    @ApiModelProperty(value="角色名称",name = "name")
    private String name;
    @ApiModelProperty(value="角色说明",name = "remark")
    private String remark;
    @ApiModelProperty(value="排序 从小到大",name = "seq")
    private Integer seq;

    @TableField(exist = false)
    @ApiModelProperty(value="角色资源ID",name = "resourceIds")
    private List<Long> resourceIds;
    @TableField(exist = false)
    @ApiModelProperty(value="角色资源名称",name = "resourceNames")
    private List<String> resourceNames;


}
