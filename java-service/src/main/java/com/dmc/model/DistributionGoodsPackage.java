package com.dmc.model;

import com.baomidou.mybatisplus.enums.IdType;
import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;



import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 分销商品套餐表
 * </p>
 *
 * @author 
 * @since 2021-03-17
 */
@Data
@Accessors(chain = true)
@TableName("t_distribution_goods_package")
public class DistributionGoodsPackage extends Model<DistributionGoodsPackage> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.INPUT)
    private Long id;
    /**
     * 购买限制
     */
    @TableField("buy_limit")
    private BigDecimal buyLimit;

    /**
     * 套餐名称
     */
    @TableField("package_name")
    private String packageName;

    /**
     * 折扣
     */
    private BigDecimal discount;

    /**
     * 价格
     */
    private BigDecimal price;

    /**
     * 状态:1上架,0下架
     */
    private Integer status;

    /**
     * 删除标示(默认0未删除)
     */
    private Integer deleted;

    /**
     * 套餐包描述
     */
    private String des;

    @TableField("create_time")
    private Date createTime;

    @TableField("update_time")
    private Date updateTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
