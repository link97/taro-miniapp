package com.dmc.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @
 * @date 2020/9/418:00
 */
@Data
@ApiModel("返回值基类")
public class RestListRespResult<T> implements java.io.Serializable {
    public static final Integer OK = 200;
    public static final Integer ERROR = 500;
    public static final Integer NO_PERMISSION = 10001;
    public static final Integer NO_SESSION = 10002;
    public static final Integer NOT_FOUND = 404;

    // 默认成功
    @ApiModelProperty(value="返回体 code",name = "code")
    private Integer code = OK;
    @ApiModelProperty(value="返回体描述",name = "message")
    private String message;
    @ApiModelProperty(value="返回数据",name = "message")
    protected List<T> data = new ArrayList<T>();

    public RestListRespResult() {
    }
}
