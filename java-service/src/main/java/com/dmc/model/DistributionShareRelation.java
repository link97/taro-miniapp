package com.dmc.model;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;



import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 分销关系链
 * </p>
 *
 * @
 * @since 2020-09-08
 */
@Data
@Accessors(chain = true)
@TableName("t_distribution_share_relation")
public class DistributionShareRelation extends Model<DistributionShareRelation> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 被分享人ID
     */
    @TableField("user_id")
    private Long userId;

    /**
     * 分享人ID
     */
    @TableField("share_user_id")
    private Long shareUserId;



    /**
     * 分享购买的订单ID
     */
    @TableField("order_id")
    private Long orderId;

    @TableField("create_time")
    private Date createTime;

    @TableField("update_time")
    private Date updateTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
