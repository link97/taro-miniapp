package com.dmc.model;

import com.baomidou.mybatisplus.enums.IdType;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;



import com.baomidou.mybatisplus.annotations.Version;

import com.dmc.util.DecimalUtil;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 分销队列表 以 分销商品ID为分类
 * </p>
 *
 * @
 * @since 2020-09-08
 */
@Data
@Accessors(chain = true)
@TableName("t_distribution_order_queue")
public class DistributionOrderQueue extends Model<DistributionOrderQueue> {

    /**
     * 获取真实排名
     * @param vip
     * @param bonusPresentation
     * @param sort
     * @return
     */
    public static Long getQueueSort(int vip,BigDecimal bonusPresentation ,long sort){
        vip+=1;
        long queueSort = sort;
        BigDecimal vipDecimal = new BigDecimal(vip);
        BigDecimal sortDecimal = new BigDecimal(sort);
        if(bonusPresentation !=null && bonusPresentation.compareTo(BigDecimal.ZERO)>0){
            vipDecimal = DecimalUtil.mulArray(vipDecimal,bonusPresentation).divide(new BigDecimal("30"),0, RoundingMode.HALF_UP);
        }
        queueSort = sortDecimal.divide(vipDecimal,0, RoundingMode.HALF_UP).longValue();
        return queueSort;
    }

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 用户ID
     */
    @TableField("user_id")
    private Long userId;

    /**
     * 分销ID
     */
    @TableField("distribution_goods_id")
    private Long distributionGoodsId;

    @TableField("distribution_order_id")
    private Long distributionOrderId;
    /**
     * 分销级别标识
     */
    @TableField("level_flag")
    private String levelFlag;

    @TableField("sort")
    private Long sort;

    @TableField("real_sort")
    private Long realSort;


    @TableField("bonus_presentation")
    private BigDecimal bonusPresentation;

    @TableField("create_time")
    private Date createTime;

    @TableField("update_time")
    private Date updateTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
