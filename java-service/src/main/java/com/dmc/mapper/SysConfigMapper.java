package com.dmc.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.dmc.model.SysConfig;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 系统配置字典表 Mapper 接口
 * </p>
 *
 * @author
 * @since 2020-09-14
 */
@Mapper
public interface SysConfigMapper extends BaseMapper<SysConfig> {

}
