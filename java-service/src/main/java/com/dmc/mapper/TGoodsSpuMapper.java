package com.dmc.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.dmc.model.TGoodsSpu;
import com.dmc.vo.TGoodsSpuVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 * 商品spu表 Mapper 接口
 * </p>
 *
 * @author dmc
 * @since 2020-09-04
 */
@Mapper
public interface TGoodsSpuMapper extends BaseMapper<TGoodsSpu> {


    /**
     * 后台获取商品列表
     * @param vo
     * @return
     */
    List<TGoodsSpuVo> getGoodsList(TGoodsSpuVo vo);
}
