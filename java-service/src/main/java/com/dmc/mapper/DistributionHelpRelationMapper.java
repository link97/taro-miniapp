package com.dmc.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.dmc.model.DistributionHelpRelation;
import com.dmc.vo.DistributionHelpRelationVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * <p>
 * 助益关系链 Mapper 接口
 * </p>
 *
 * @
 * @since 2020-09-09
 */
@Mapper
public interface DistributionHelpRelationMapper extends BaseMapper<DistributionHelpRelation> {

    List<DistributionHelpRelationVo> getHelpRelation(@Param("currUid") Long currUid, @Param("type")Integer type,
                                                     @Param("orderId")Long orderId,@Param("levelFlag")String levelFlag);

    @Update("UPDATE t_distribution_help_relation set user_id=#{newUserId} WHERE user_id=#{oldUserId}")
    void updateUserId(@Param("oldUserId") Long oldUserId,@Param("newUserId") Long newUserId);

    @Update("UPDATE t_distribution_help_relation set help_user_id=#{newUserId} WHERE help_user_id=#{oldUserId}")
    void updateHelpUserId(@Param("oldUserId") Long oldUserId,@Param("newUserId") Long newUserId);
}
