package com.dmc.mapper;

import com.dmc.entity.Message;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 消息表 Mapper 接口
 * </p>
 *
 * @author 
 * @since 2020-09-25
 */
@Mapper
public interface MessageMapper extends BaseMapper<Message> {

}
