package com.dmc.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.dmc.model.TGoodsSku;
import com.dmc.vo.TGoodsDetailVo;
import com.dmc.vo.TGoodsListSkuVo;
import com.dmc.vo.TGoodsSpuVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * <p>
 * 商品SKU表 Mapper 接口
 * </p>
 *
 * @author dmc
 * @since 2020-09-04
 */
@Mapper
public interface TGoodsSkuMapper extends BaseMapper<TGoodsSku> {
    /**
     * 获取小程序商城列表以及搜索
     * @param vo
     * @return
     */
    List<TGoodsListSkuVo> getMallGoodsList(TGoodsListSkuVo vo);
    /**
     * 获取商品详情
     * @param skuId
     * @return
     */
    TGoodsDetailVo getGoodsDetail(@Param("skuId") Long skuId,@Param("spuId") Long spuId,@Param("skuTitle")String skuTitle);
    /**
     * 获取后台商品详情
     * @param spuId
     * @return
     */
    TGoodsDetailVo getGoodsDetailForPc(@Param("spuId") Long spuId);

    @Update("update t_goods_sku set status = 0 ,deleted = 1 where goods_spu_Id = #{spuId}")
    int deleteAllGoodsSku(@Param("spuId") Long spuId);
}
