package com.dmc.mapper;

import com.dmc.model.DistributionOrderGoods;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 分销商品表 Mapper 接口
 * </p>
 *
 * @
 * @since 2020-09-12
 */
@Mapper
public interface DistributionOrderGoodsMapper extends BaseMapper<DistributionOrderGoods> {

}
