package com.dmc.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.dmc.model.AchievementBonusConfig;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 业绩奖配置 Mapper 接口
 * </p>
 *
 * @author 
 * @since 2021-03-18
 */
@Mapper
public interface AchievementBonusConfigMapper extends BaseMapper<AchievementBonusConfig> {

    /**
     * 获取当前入单的配置
     * @param firstLevelFlag
     * @return
     */
    AchievementBonusConfig selectByFirstLevelFlag(@Param("firstLevelFlag") String firstLevelFlag);
}
