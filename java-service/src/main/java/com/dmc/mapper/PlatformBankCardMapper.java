package com.dmc.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.dmc.model.PlatformBankCard;
import com.dmc.vo.PlatformBankCardVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 * 平台银行账户信息 Mapper 接口
 * </p>
 *
 * @author 
 * @since 2020-10-28
 */
@Mapper
public interface PlatformBankCardMapper extends BaseMapper<PlatformBankCard> {

    /**
     * 获取平台银行卡信息
     * @param vo
     * @return
     */
    List<PlatformBankCardVo> getBankList(PlatformBankCardVo vo);
}
