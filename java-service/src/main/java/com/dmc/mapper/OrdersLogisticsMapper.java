package com.dmc.mapper;

import com.dmc.entity.OrdersLogistics;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 订单物流表 Mapper 接口
 * </p>
 *
 * @
 * @since 2020-09-12
 */
@Mapper
public interface OrdersLogisticsMapper extends BaseMapper<OrdersLogistics> {

}
