package com.dmc.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.dmc.model.BenefitTransaction;
import com.dmc.vo.BenefitTransactionVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 * 收益流水 Mapper 接口
 * </p>
 *
 * @
 * @since 2020-09-08
 */
@Mapper
public interface BenefitTransactionMapper extends BaseMapper<BenefitTransaction> {

    /**
     * 获取收益记录
     * @param vo
     * @return
     */
    List<BenefitTransactionVo> getBenefitTransaction(BenefitTransactionVo vo);
    /**
     * 根据类型获取此类型收益当前总额
     * @param typeList
     * @return
     */
    BigDecimal getSumByTypeList(@Param("userId")long userId,@Param("typeList") List<Integer> typeList);



}
