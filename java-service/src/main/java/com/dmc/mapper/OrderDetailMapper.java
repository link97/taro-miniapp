package com.dmc.mapper;

import com.dmc.entity.OrderDetail;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 订单明细表 Mapper 接口
 * </p>
 *
 * @
 * @since 2020-09-07
 */
@Mapper
public interface OrderDetailMapper extends BaseMapper<OrderDetail> {
    List<OrderDetail> getOrderDetail(@Param("ordersIds")List<Integer> ordersIds);
    List<OrderDetail> getOrderDetailByOrderId(@Param("ordersId")Integer ordersId);

    /**
     * 查看当前登录人已支付订单有多少个当前商品
     * @param spuId
     * @param userId
     * @return
     */
    int getPayNumBySpuId(@Param("spuId")Long spuId, @Param("userId")Long userId);
}
