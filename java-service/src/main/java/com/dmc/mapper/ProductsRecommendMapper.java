package com.dmc.mapper;

import com.dmc.entity.ProductsRecommend;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 推荐商品 Mapper 接口
 * </p>
 *
 * @
 * @since 2020-09-05
 */
@Mapper
public interface ProductsRecommendMapper extends BaseMapper<ProductsRecommend> {

}
