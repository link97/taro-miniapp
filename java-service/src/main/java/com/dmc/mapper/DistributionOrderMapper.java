package com.dmc.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.dmc.model.DistributionOrder;
import com.dmc.vo.DistributionOrderVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 分销订单表 Mapper 接口
 * </p>
 *
 * @
 * @since 2020-09-08
 */
@Mapper
public interface DistributionOrderMapper extends BaseMapper<DistributionOrder> {

    List<DistributionOrderVo> getDistributionOrderList(DistributionOrderVo vo);

    Integer getOrderNumByUserId(@Param("userId") Long userId, @Param("orderStatus")Integer orderStatus);

    Integer getOrderPackNumByUserId(@Param("userId") Long userId, @Param("orderStatus")Integer orderStatus);

    DistributionOrder getOrderByOrderNo(@Param("orderNo") String orderNo);

    /**
     * 获取第一个可以插队的分销订单
     * @param userId
     * @return
     */
    DistributionOrder getNeedUpdateOrderOne(@Param("userId")Long userId);

    /**
     * 获取最新购买的时间
     * @param userId
     * @return
     */
    DistributionOrderVo getNewOrder(Long userId);
}
