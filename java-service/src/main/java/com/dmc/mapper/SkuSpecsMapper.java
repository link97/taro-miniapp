package com.dmc.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.dmc.model.SkuSpecs;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 商品SKU规格表 Mapper 接口
 * </p>
 *
 * @author 
 * @since 2021-03-22
 */
@Mapper
public interface SkuSpecsMapper extends BaseMapper<SkuSpecs> {
    /**
     * 删除当前商品下的所有规格数据
     * @param skuId
     */
    void deleteBySkuId(@Param("skuId") Long skuId);
}
