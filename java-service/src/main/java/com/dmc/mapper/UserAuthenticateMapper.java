package com.dmc.mapper;

import com.dmc.entity.UserAuthenticate;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.dmc.vo.UserAuthenticateVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 * 身份认证 Mapper 接口
 * </p>
 *
 * @
 * @since 2020-09-05
 */
@Mapper
public interface UserAuthenticateMapper extends BaseMapper<UserAuthenticate> {
    UserAuthenticate getUserAuthenticateByUserId(Long userId);
    List<UserAuthenticateVo> getUserAuthenticateAll(UserAuthenticateVo userAuthenticateVo);
}
