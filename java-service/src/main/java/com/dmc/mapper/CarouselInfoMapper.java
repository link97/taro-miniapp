package com.dmc.mapper;

import com.dmc.entity.CarouselInfo;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 首页-轮播图 Mapper 接口
 * </p>
 *
 * @
 * @since 2020-09-05
 */
@Mapper
public interface CarouselInfoMapper extends BaseMapper<CarouselInfo> {

}
