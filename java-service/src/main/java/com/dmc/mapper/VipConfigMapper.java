package com.dmc.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.dmc.model.VipConfig;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 会员荣誉身份配置 Mapper 接口
 * </p>
 *
 * @author 
 * @since 2021-03-17
 */
@Mapper
public interface VipConfigMapper extends BaseMapper<VipConfig> {

    /**
     * 查询下一级别个荣誉配置
     * @param vipLevel
     * @return
     */
    VipConfig selectFirst(@Param("vipLevel") Integer vipLevel);

    /**
     * 获取当前级别荣誉配置
     * @param vipLevel
     * @return
     */
    VipConfig selectCurr(@Param("vipLevel") Integer vipLevel);
}
