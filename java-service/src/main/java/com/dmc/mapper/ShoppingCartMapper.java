package com.dmc.mapper;

import com.dmc.entity.ShoppingCart;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.dmc.vo.TGoodsDetailVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 购物车数据表 Mapper 接口
 * </p>
 *
 * @
 * @since 2020-09-07
 */
@Mapper
public interface ShoppingCartMapper extends BaseMapper<ShoppingCart> {
    ShoppingCart getShoppingCartByUserIdAndSkuId(@Param(value="skuId")Long skuId,@Param(value="userId")Long userId);

    /**
     * 获取我的购物车
     * @param userId
     * @return
     */
    List<TGoodsDetailVo> getShoppingCartByUserId(@Param(value="userId")Long userId);
}
