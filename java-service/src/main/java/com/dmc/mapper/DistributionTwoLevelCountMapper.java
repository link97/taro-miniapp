package com.dmc.mapper;

import com.baomidou.mybatisplus.annotations.SqlParser;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.dmc.model.DistributionTwoLevelCount;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * <p>
 * 两级分销订单汇总表 Mapper 接口
 * </p>
 *
 * @author 
 * @since 2020-10-27
 */
@Mapper
public interface DistributionTwoLevelCountMapper extends BaseMapper<DistributionTwoLevelCount> {

    /**
     * 获取所有升级标准的用户
     * @param limit 分销升级人数限制
     * @return
     */
    List<DistributionTwoLevelCount> selectNeedUpdate(@Param("limit") int limit);

}
