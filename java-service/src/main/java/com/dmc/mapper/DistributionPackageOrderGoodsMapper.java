package com.dmc.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.dmc.model.DistributionPackageOrderGoods;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 分销商品套餐表关联订单商品详情表 Mapper 接口
 * </p>
 *
 * @author 
 * @since 2021-03-17
 */
@Mapper
public interface DistributionPackageOrderGoodsMapper extends BaseMapper<DistributionPackageOrderGoods> {

}
