package com.dmc.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.dmc.model.DistributionOrderQueue;
import com.dmc.vo.DistributionOrderQueueVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;

/**
 * <p>
 * 分销队列表 以 分销商品ID为分类 Mapper 接口
 * </p>
 *
 * @
 * @since 2020-09-08
 */
@Mapper
public interface DistributionOrderQueueMapper extends BaseMapper<DistributionOrderQueue> {

    DistributionOrderQueueVo getFirstOrder(@Param("levelFlag") String levelFlag, @Param("limit")BigDecimal limit);

    int getRank(@Param("sort")Long sort,@Param("levelFlag") String levelFlag,@Param("vipOrders") Integer vipOrders);
}
