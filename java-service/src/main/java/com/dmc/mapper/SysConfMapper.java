package com.dmc.mapper;

import com.dmc.entity.SysConf;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 系统设置表 Mapper 接口
 * </p>
 *
 * @
 * @since 2020-09-12
 */
@Mapper
public interface SysConfMapper extends BaseMapper<SysConf> {

}
