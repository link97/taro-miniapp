package com.dmc.mapper;

import com.dmc.entity.UserShippingAddress;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 我的收货地址 Mapper 接口
 * </p>
 *
 * @
 * @since 2020-09-05
 */
@Mapper
public interface UserShippingAddressMapper extends BaseMapper<UserShippingAddress> {

}
