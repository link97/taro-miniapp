package com.dmc.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.dmc.model.TGoodsCategory;
import com.dmc.vo.TGoodsCategoryVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 分类表 Mapper 接口
 * </p>
 *
 * @author dmc
 * @since 2020-09-04
 */
@Mapper
public interface TGoodsCategoryMapper extends BaseMapper<TGoodsCategory> {

    List<TGoodsCategoryVo> getCategoryList(TGoodsCategoryVo tGoodsCategoryVo);
}
