package com.dmc.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.dmc.model.SpuSpecs;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 商品规格表 Mapper 接口
 * </p>
 *
 * @author 
 * @since 2021-03-22
 */
@Mapper
public interface SpuSpecsMapper extends BaseMapper<SpuSpecs> {
    /**
     * 删除当前商品下的所有规格数据
     * @param spuId
     */
    void deleteBySpuId(@Param("spuId") Long spuId);
}
