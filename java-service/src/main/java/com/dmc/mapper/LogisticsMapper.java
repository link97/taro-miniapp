package com.dmc.mapper;

import com.dmc.entity.Logistics;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 物流表 Mapper 接口
 * </p>
 *
 * @
 * @since 2020-09-12
 */
@Mapper
public interface LogisticsMapper extends BaseMapper<Logistics> {

}
