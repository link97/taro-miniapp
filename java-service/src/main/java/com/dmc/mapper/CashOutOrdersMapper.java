package com.dmc.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.dmc.model.CashOutOrders;
import com.dmc.vo.CashOutOrdersVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 * 提现表 Mapper 接口
 * </p>
 *
 * @author
 * @since 2020-09-13
 */
@Mapper
public interface CashOutOrdersMapper extends BaseMapper<CashOutOrders> {

    /**
     * 获取订单列表
     * @param vo
     * @return
     */
    List<CashOutOrdersVo> getCashOutOrderList(CashOutOrdersVo vo);

    /**
     * 获取 当前人提现总金额
     * @param userId
     * @return
     */
    BigDecimal getCashOutPriceByUserId(@Param("userId") Long userId);
}
