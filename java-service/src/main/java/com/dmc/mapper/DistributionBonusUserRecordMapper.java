package com.dmc.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.dmc.model.DistributionBonusUserRecord;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 互助分红使用记录 Mapper 接口
 * </p>
 *
 * @author 
 * @since 2020-10-31
 */
@Mapper
public interface DistributionBonusUserRecordMapper extends BaseMapper<DistributionBonusUserRecord> {

}
