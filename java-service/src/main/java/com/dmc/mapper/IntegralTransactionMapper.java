package com.dmc.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.dmc.model.IntegralTransaction;
import com.dmc.vo.IntegralDetailVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 积分流水 Mapper 接口
 * </p>
 *
 * @
 * @since 2020-09-08
 */
@Mapper
public interface IntegralTransactionMapper extends BaseMapper<IntegralTransaction> {

    /**
     * 查询用户积分使用记录
     * @param userId
     * @return
     */
    List<IntegralTransaction> selectUserIntegralList(@Param(value="userId")Long userId);

    /**
     *
     * @param integralDetailVo
     * @return
     */
    List<IntegralDetailVo> selectIntegralDetailList(IntegralDetailVo integralDetailVo);
}
