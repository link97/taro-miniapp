package com.dmc.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.dmc.model.UserSpuPayNum;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 用户商品购买数量统计 Mapper 接口
 * </p>
 *
 * @author 
 * @since 2021-04-01
 */
@Mapper
public interface UserSpuPayNumMapper extends BaseMapper<UserSpuPayNum> {

}
