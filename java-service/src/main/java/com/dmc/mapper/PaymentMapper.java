package com.dmc.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.dmc.model.Payment;
import com.dmc.vo.PaymentVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 * 支付相关记录表 Mapper 接口
 * </p>
 *
 * @author dmc
 * @since 2020-07-25
 */
@Mapper
public interface PaymentMapper extends BaseMapper<Payment> {

    /**
     * 根据订单ID获取支付单据
     * @param orderId 订单ID
     * @return
     */
    Payment getByOrderIdAndType(@Param("orderId") Long orderId, @Param("payType") Integer payType);


    /**
     * 根据订支付单据ID获取支付单据
     * @param checkoutId 支付单据ID
     * @return
     */
    Payment getByCheckoutId(@Param("checkoutId") Long checkoutId);

    int orderNum(int orderType);

    BigDecimal orderPrice(int orderType);

    List<PaymentVo> selectTransactionDetailList(PaymentVo paymentVo);
}
