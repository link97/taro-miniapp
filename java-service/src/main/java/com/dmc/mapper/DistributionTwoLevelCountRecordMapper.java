package com.dmc.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.dmc.model.DistributionTwoLevelCountRecord;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

/**
 * <p>
 * 两级分销订单汇总记录表 Mapper 接口
 * </p>
 *
 * @author 
 * @since 2020-10-27
 */
@Mapper
public interface DistributionTwoLevelCountRecordMapper extends BaseMapper<DistributionTwoLevelCountRecord> {

    /**
     * 更新订单ID
     * @param countId
     * @param orderId
     * @param limit
     */
    @Update("UPDATE t_distribution_two_level_count_record set order_id =#{orderId} WHERE two_level_count_id=#{countId} and order_id IS NULL limit #{limit}")
    void updateOrderIdByLimit(@Param("countId") Long countId, @Param("orderId")Long orderId, @Param("limit")int limit);
}
