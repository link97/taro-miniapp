package com.dmc.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.dmc.model.DistributionTwoLevelHelpRecord;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 助力记录表 Mapper 接口
 * </p>
 *
 * @author 
 * @since 2020-10-27
 */
@Mapper
public interface DistributionTwoLevelHelpRecordMapper extends BaseMapper<DistributionTwoLevelHelpRecord> {

}
