package com.dmc.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.dmc.model.DistributionGoods;
import com.dmc.vo.DistributionGoodsVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 分销商品表 Mapper 接口
 * </p>
 *
 * @
 * @since 2020-09-08
 */
@Mapper
public interface DistributionGoodsMapper extends BaseMapper<DistributionGoods> {

    /**
     * 获取分销商列表
     * @param vo
     * @return
     */
    List<DistributionGoodsVo> getDistributionGoodsList(DistributionGoodsVo vo);

    /**
     * 根据等级标识获取有效分销商商品
     * @param flagList
     * @return
     */
    List<DistributionGoods> getDistributionGoodsByFLag(@Param("flagList") List<String> flagList);
}
