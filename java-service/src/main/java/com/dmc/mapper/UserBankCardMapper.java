package com.dmc.mapper;

import com.dmc.entity.UserBankCard;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.dmc.vo.BankCardVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 * 我的银行卡 Mapper 接口
 * </p>
 *
 * @
 * @since 2020-09-05
 */
@Mapper
public interface UserBankCardMapper extends BaseMapper<UserBankCard> {

    List<BankCardVo> getBankCardList(BankCardVo bankCardVo);
}
