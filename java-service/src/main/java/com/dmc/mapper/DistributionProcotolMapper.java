package com.dmc.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.dmc.model.DistributionProcotol;
import com.dmc.vo.DistributionProcotolVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 * 分销协议 Mapper 接口
 * </p>
 *
 * @
 * @since 2020-09-10
 */
@Mapper
public interface DistributionProcotolMapper extends BaseMapper<DistributionProcotol> {

    List<DistributionProcotolVo> getProtolList(DistributionProcotolVo vo);
}
