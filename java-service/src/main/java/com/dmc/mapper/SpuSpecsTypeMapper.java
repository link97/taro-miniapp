package com.dmc.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.dmc.model.SpuSpecsType;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 商品规格类型表 Mapper 接口
 * </p>
 *
 * @author 
 * @since 2021-03-22
 */
@Mapper
public interface SpuSpecsTypeMapper extends BaseMapper<SpuSpecsType> {

    /**
     * 删除当前商品下的所有规格类型数据
     * @param spuId
     */
    void deleteBySpuId(@Param("spuId") Long spuId);
}
