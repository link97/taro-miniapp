package com.dmc.mapper;

import com.dmc.entity.BankInfo;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 银行信息 Mapper 接口
 * </p>
 *
 * @
 * @since 2020-09-05
 */
@Mapper
public interface BankInfoMapper extends BaseMapper<BankInfo> {

}
