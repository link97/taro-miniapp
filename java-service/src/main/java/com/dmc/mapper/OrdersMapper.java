package com.dmc.mapper;

import com.dmc.entity.Orders;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.dmc.vo.OrdersVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 * 订单表 Mapper 接口
 * </p>
 *
 * @
 * @since 2020-09-07
 */
@Mapper
public interface OrdersMapper extends BaseMapper<Orders> {
    List<Orders> getOrdersByUser(@Param(value="userId")Long userId,@Param(value="ordersState")Long ordersState,@Param(value="keyWord")String keyWord);

    List<Orders> getOrders(OrdersVo ordersVo);
    List<OrdersVo> getOrdersList(OrdersVo ordersVo);

    @Update("UPDATE t_orders set orders_state=20004 WHERE orders_state=20000 and create_time IS NOT NULL AND create_time>DATE_ADD(NOW(), INTERVAL #{orderCancelTime} MINUTE)")
    void cancelOrder(@Param("orderCancelTime") Integer orderCancelTime);

    @Update("UPDATE t_orders set orders_state=20003 where orders_state=20002 and trade_state=1 and DATE_SUB(CURDATE(), INTERVAL #{orderAutoArrival} DAY) >=create_time")
    void confirmOrderArrival(@Param("orderAutoArrival") Integer orderAutoArrival);

    /**
     * 获取需要自动确认的订单
     * @param orderAutoArrival
     * @return
     */
    @Select("select * from t_orders where orders_state=20002 and trade_state=1 and DATE_SUB(CURDATE(), INTERVAL #{orderAutoArrival} DAY) >=pay_Time")
    List<Orders> getAutoConfirmOrder(@Param("orderAutoArrival") Integer orderAutoArrival);

    /**
     * 获取会员商品的总金额
     * @param userId
     * @return
     */
    BigDecimal getTotalUserOrderFinalAmount(@Param(value="userId")Long userId);

    Integer getTotalUserOrder(@Param(value="userId")Long userId,@Param(value="orderStatusList")List<Long> orderStatusList);

    Orders getSumOrderAmount(@Param(value="ordersState")Long ordersState);


    Integer getTotalOrderNum(@Param(value="userId")Long userId);
    Integer getPendingOrdersNum();

    Orders getGoodOrderSumAmount(@Param(value="userId")Long userId);

    List<Orders> cancelOrderList(@Param("orderCancelTime") Integer orderCancelTime);

}
