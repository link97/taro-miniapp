package com.dmc.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.dmc.model.MsgCode;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 验证码 Mapper 接口
 * </p>
 *
 * @author dmc
 * @since 2020-09-04
 */
@Mapper
public interface TMsgCodeMapper extends BaseMapper<MsgCode> {

}
