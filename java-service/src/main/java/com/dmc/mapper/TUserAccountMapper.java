package com.dmc.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.dmc.model.TUserAccount;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

import java.math.BigDecimal;

/**
 * <p>
 * 用户账户表 Mapper 接口
 * </p>
 *
 * @author dmc
 * @since 2020-09-05
 */
@Mapper
public interface TUserAccountMapper extends BaseMapper<TUserAccount> {

    TUserAccount selectUserAccountByUserId(@Param("userId")Long userId);


    @Update("UPDATE t_user_account set integral=#{integral} where user_id=#{userId}")
    void updateUserAccount(@Param("userId")Long userId, @Param("integral")BigDecimal integral);
}
