package com.dmc.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.dmc.model.DistributionShareRelation;
import com.dmc.vo.DistributionShareRelationPcVo;
import com.dmc.vo.DistributionShareRelationVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * <p>
 * 分销关系链 Mapper 接口
 * </p>
 *
 * @
 * @since 2020-09-08
 */
@Mapper
public interface DistributionShareRelationMapper extends BaseMapper<DistributionShareRelation> {

    List<DistributionShareRelationVo> getShareRelation(@Param("shareUserId") Long shareUserId);

    @Update("UPDATE t_distribution_share_relation set user_id=#{newUserId} WHERE user_id=#{oldUserId}")
    void updateUserId(@Param("oldUserId") Long oldUserId,@Param("newUserId") Long newUserId);

    @Update("UPDATE t_distribution_share_relation set share_user_id=#{newUserId} WHERE share_user_id=#{oldUserId}")
    void updateShareUserId(@Param("oldUserId") Long oldUserId,@Param("newUserId") Long newUserId);
    /**
     * PC端获取分享列表
     * @param vo
     * @return
     */
    List<DistributionShareRelationPcVo> getShareRelationForPc(DistributionShareRelationPcVo vo);

    /**
     * 获取分享人分销订单总数
     * @param userId
     * @return
     */
    Integer getShareRelationOrderNum(@Param("userId") Long userId,@Param("shareLevel")Integer shareLevel);
}
