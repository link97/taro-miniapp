package com.dmc.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.dmc.model.OrderReceipt;
import com.dmc.vo.OrderReceiptVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 * 发票表 Mapper 接口
 * </p>
 *
 * @author
 * @since 2020-09-24
 */
@Mapper
public interface OrderReceiptMapper extends BaseMapper<OrderReceipt> {

    /**
     * 获取发票列表
     * @param vo
     * @return
     */
    List<OrderReceiptVo> getOrderReceiptList(OrderReceiptVo vo);
}
