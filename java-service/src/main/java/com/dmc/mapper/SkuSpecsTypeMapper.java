package com.dmc.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.dmc.model.SkuSpecsType;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 商品SKU规格类型表 Mapper 接口
 * </p>
 *
 * @author 
 * @since 2021-03-22
 */
@Mapper
public interface SkuSpecsTypeMapper extends BaseMapper<SkuSpecsType> {
    /**
     * 删除当前商品下的所有规格类型数据
     * @param skuId
     */
    void deleteBySkuId(@Param("skuId") Long skuId);
}
