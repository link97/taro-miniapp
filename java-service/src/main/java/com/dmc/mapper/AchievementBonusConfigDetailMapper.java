package com.dmc.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.dmc.model.AchievementBonusConfigDetail;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 业绩奖配置详情 Mapper 接口
 * </p>
 *
 * @author 
 * @since 2021-03-18
 */
@Mapper
public interface AchievementBonusConfigDetailMapper extends BaseMapper<AchievementBonusConfigDetail> {

    /**
     * 获取配置详情 并以代数正序
     * @param configId
     * @return
     */
    List<AchievementBonusConfigDetail> selectDetailByConfig(@Param("configId") Long configId);
}
