package com.dmc.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.dmc.model.DistributionPackageOrder;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 分销套餐包订单表 Mapper 接口
 * </p>
 *
 * @author 
 * @since 2021-03-17
 */
@Mapper
public interface DistributionPackageOrderMapper extends BaseMapper<DistributionPackageOrder> {

}
