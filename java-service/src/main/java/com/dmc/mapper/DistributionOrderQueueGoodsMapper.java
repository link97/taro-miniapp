package com.dmc.mapper;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.dmc.model.DistributionOrderQueueGoods;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 分销位置记录表 Mapper 接口
 * </p>
 *
 * @author 
 * @since 2020-10-06
 */
@Mapper
public interface DistributionOrderQueueGoodsMapper extends BaseMapper<DistributionOrderQueueGoods> {

}
