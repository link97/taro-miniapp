package com.dmc.vo;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 收益流水
 * </p>
 *
 * @
 * @since 2020-09-08
 */
@Data
@ApiModel("收益记录VO")
public class BenefitTransactionVo extends TablePage implements Serializable  {

    private static final long serialVersionUID = 135432243532432525L;

    @ApiModelProperty(value="主键ID",name = "id")
    private Long id;

    /**
     * 用户ID
     */
    @ApiModelProperty(value="用户ID",name = "userId")
    private Long userId;

    /**
     * 转账另一人ID
     */
    @ApiModelProperty(value="转账另一人ID",name = "otherUserId")
    private Long otherUserId;

    @ApiModelProperty(value="用户名称",name = "userName")
    private String userName;

    @ApiModelProperty(value="转账另一人用户名称",name = "otherUserName")
    private String otherUserName;

    @ApiModelProperty(value="用户头像",name = "userPhoto")
    private String userPhoto;

    @ApiModelProperty(value="用户手机号",name = "userPhone")
    private String userPhone;

    @ApiModelProperty(value="购买人",name = "payUserName")
    private String payUserName;
    /**
     * 订单ID
     */
    @ApiModelProperty(value="订单ID",name = "orderId")
    private Long orderId;
    /**
     * 变更后收益
     */
    @ApiModelProperty(value="变更后收益",name = "currentTotalBenefit")
    private BigDecimal currentTotalBenefit;

    /**
     * 当前类型总收益
     */
    @ApiModelProperty(value="当前类型总收益",name = "currentTypeBenefit")
    private BigDecimal currentTypeBenefit;

    /**
     * 流水类型
     */
    @ApiModelProperty(value="流水类型",name = "type")
    private Integer type;

    @ApiModelProperty(value="流水类型名称",name = "typeName")
    private String typeName;

    @ApiModelProperty(value="订单编号",name = "orderNo")
    private String orderNo;

    /**
     * 收益
     */
    @ApiModelProperty(value="收益",name = "benefit")
    private BigDecimal benefit;
    /**
     * 分销名称
     */
    @ApiModelProperty(value="分销名称",name = "distributionName")
    private String distributionName;
    /**
     * 描述
     */
    @ApiModelProperty(value="描述",name = "des")
    private String des;

    @ApiModelProperty(value="创建时间/收益时间",name = "createTime")
    private Date createTime;

    @ApiModelProperty(value="收益时间开始 用于查询",name = "createTimeStart")
    private Date createTimeStart;

    @ApiModelProperty(value="收益时间结束 用于查询",name = "createTimeEnd")
    private Date createTimeEnd;

    @ApiModelProperty(value="转账类型集合 用于查询",name = "typeList")
    private List<Integer> typeList;

}
