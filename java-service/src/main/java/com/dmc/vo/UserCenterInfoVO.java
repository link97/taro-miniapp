package com.dmc.vo;

import com.baomidou.mybatisplus.annotations.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by sail on 2016/12/25.
 */
@Data
public class UserCenterInfoVO implements Serializable {

    private static final long serialVersionUID = 136768324324L;
    @ApiModelProperty(value="用户ID",name = "id")
    private Long id;
    @ApiModelProperty(value="用户名",name = "username")
    private String username;
    @ApiModelProperty(value="真实名称",name = "name")
    private String name;
    @ApiModelProperty(value="创建时间",name = "createTime")
    private Date createTime;
    @ApiModelProperty(value="修改时间",name = "modifyTime")
    private Date modifyTime;
    @ApiModelProperty(value="用户类型 0:商家 1:客户",name = "type")
    private Integer type;
    @ApiModelProperty(value="用户头像",name = "userPhoto")
    private String userPhoto;
    @ApiModelProperty(value="用户手机号",name = "userPhone")
    private String userPhone;
    @ApiModelProperty(value="微信绑定手机号",name = "wxPhone")
    private String wxPhone;
    @ApiModelProperty(value="用户年龄",name = "age")
    private Integer age;
    @ApiModelProperty(value="用户性别 0:男 1:女",name = "sex")
    private Integer sex;
    @ApiModelProperty(value="用户生日",name = "birthday")
    private String birthday;
    @ApiModelProperty(value="用微信openId",name = "openId")
    private String openId;
    @ApiModelProperty(value="用户国家",name = "country")
    private String country;
    @ApiModelProperty(value="用户省份",name = "province")
    private String province;
    @ApiModelProperty(value="用户城市",name = "city")
    private String city;
    @ApiModelProperty(value="是否停用 0:启用 1:停用",name = "stop")
    private Integer stop;
    @ApiModelProperty(value="收益",name = "benefit")
    private BigDecimal benefit;
    @ApiModelProperty(value="积分",name = "integral")
    private BigDecimal integral;
    @ApiModelProperty(value="分销订单数量",name = "distributionNum")
    private Integer distributionNum;
    @ApiModelProperty(value="分销套餐包订单数量",name = "distributionPackNum")
    private Integer distributionPackNum;
    @ApiModelProperty(value="全部订单",name = "allOrderNum")
    private Integer allOrderNum;
    @ApiModelProperty(value="待支付",name = "readyPayOrderNum")
    private Integer readyPayOrderNum;
    @ApiModelProperty(value="待收货",name = "readyReciveOrderNum")
    private Integer readyReciveOrderNum;
    @ApiModelProperty(value="已完成",name = "closeOrderNum")
    private Integer closeOrderNum;
    @ApiModelProperty(value="用户等级",name = "level")
    private Integer level;
    @ApiModelProperty(value="等级名称",name = "levelName")
    private String levelName;
    @ApiModelProperty(value="0:不享受提成 1:享受提成",name = "haveBouns")
    private Integer haveBouns = 0;
    @ApiModelProperty(value="未读消息数量",name = "unMessageNum")
    private Integer unMessageNum;
}
