package com.dmc.vo;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 分类
 * </p>
 *
 * @author dmc
 * @since 2020-09-04
 */
@Data
@ApiModel("商品分类VO")
public class TGoodsCategoryVo  extends TablePage implements Serializable   {

    private static final long serialVersionUID = 123432143243232L;

    @TableId(value = "id", type = IdType.AUTO)
    @ApiModelProperty(value="品类主键",name = "id")
    private Long id;

    /**
     * 分类名称
     */
    @TableField("category_name")
    @ApiModelProperty(value="品类名称",name = "categoryName")
    private String categoryName;

    /**
     * 父级类型
     */
    @ApiModelProperty(value="父品类ID默认为0",name = "pid")
    private Long pid;

    /**
     * 排序
     */
    @ApiModelProperty(value="排序字段",name = "seq")
    private Integer seq;

    /**
     * 是否失效 0:有效 1:无效
     */
    @ApiModelProperty(value="是否失效 0:有效 1:无效",name = "stop")
    private Integer stop;

    /**
     * 删除标识:0:未删除,1删除
     */
    @ApiModelProperty(value="删除标识:0:未删除,1删除",name = "deleted")
    private Integer deleted;

    @TableField("create_time")
    @ApiModelProperty(value="创建时间",name = "createTime")
    private Date createTime;

    @TableField("update_time")
    @ApiModelProperty(value="更新时间",name = "updateTime")
    private Date updateTime;

}
