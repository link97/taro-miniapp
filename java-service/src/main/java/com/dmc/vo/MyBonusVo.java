package com.dmc.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @
 * @date 2020/9/515:43
 */
@Data
public class MyBonusVo implements Serializable {

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 用户级别
     */
    private Integer level;

    /**
     * 收费集合
     */
    private List<DistributionOrderVo> sfList;
    /**
     * 复投超高级
     */
    private List<DistributionOrderVo> ftChaoGaojiList;
    /**
     * 复投高级
     */
    private List<DistributionOrderVo> ftGaojiList;

    /**
     * 复投高级
     */
    private List<DistributionOrderVo> ftZhongjiList;
    /**
     * 复投初级 已取消
     */
    private List<DistributionOrderVo> ftChujiList;
}
