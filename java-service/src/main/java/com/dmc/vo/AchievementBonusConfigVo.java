package com.dmc.vo;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 业绩奖配置
 * </p>
 *
 * @author 
 * @since 2021-03-18
 */
@Data
@ApiModel("业绩奖配置 VO")
public class AchievementBonusConfigVo extends Model<AchievementBonusConfigVo> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value="主键",name = "id")
    private Long id;

    /**
     * 业绩奖配置名称
     */
    @ApiModelProperty(value="业绩奖配置名称",name = "configName")
    private String configName;

    /**
     * 分销级别标识
     */
    @ApiModelProperty(value="分销级别标识",name = "levelFlag")
    private String levelFlag;

    /**
     * 累积代数
     */
    @ApiModelProperty(value="累积代数",name = "generations")
    private Integer generations;

    /**
     * 享受此配置的业绩总额限制
     */
    @ApiModelProperty(value="享受此配置的业绩总额限制",name = "achievementLimit")
    private BigDecimal achievementLimit;

    /**
     * 1:停用 0:正常使用
     */
    @ApiModelProperty(value="1:停用 0:正常使用",name = "status")
    private Integer status;

    /**
     * 会员荣誉身份描述
     */
    @ApiModelProperty(value="会员荣誉身份描述",name = "des")
    private String des;

    /**
     * 删除标示(默认0未删除)
     */
    @ApiModelProperty(value="删除标示(默认0未删除)",name = "deleted")
    private Integer deleted;

    @TableField("create_time")
    private Date createTime;

    @TableField("update_time")
    private Date updateTime;

    @ApiModelProperty(value="配置详情",name = "configDetailList")
    private List<AchievementBonusConfigDetailVo> configDetailList;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
