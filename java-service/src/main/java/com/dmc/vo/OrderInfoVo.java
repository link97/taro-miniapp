package com.dmc.vo;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import com.dmc.entity.OrderDetail;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @
 * @date 2020/9/617:38
 */
@Data
@ApiModel("商品订单信息")
public class OrderInfoVo extends BaseRowModel implements Serializable {

    /**
     * 订单号
     */
    @ApiModelProperty(value="订单号",name = "ordersNo")
    @ExcelProperty( value = "订单编号",index = 0)
    private String ordersNo;

    /**
     * 订单生成时间
     */
    @ApiModelProperty(value="订单生成时间",name = "createTime")
    @ExcelProperty( value = "订单生成时间",index =1)
    private Date createTime;

    /**
     * 收货人姓名
     */
    @ApiModelProperty(value="收货人姓名",name = "receiverName")
    @ExcelProperty( value = "收货人姓名",index = 2)
    private String receiverName;

    /**
     * 区/县
     */
    @ApiModelProperty(value="区/县",name = "receiverDistrict")
    @ExcelProperty( value = "区/县",index = 3)
    private String receiverDistrict;

    /**
     * 详细地址
     */
    @ApiModelProperty(value="详细地址",name = "receiverAddress")
    @ExcelProperty( value = "详细地址",index = 4)
    private String receiverAddress;

    /**
     * 收货移动电话
     */
    @ApiModelProperty(value="收货移动电话",name = "receiverMobile")
    @ExcelProperty( value = "收货移动电话",index = 5)
    private String receiverMobile;

    /**
     * (1、微信支付 2、积分支付 3 扫码支付 4混合支付)
     */
    @ApiModelProperty(value="(1、微信支付 2、积分支付 3 扫码支付 6线下现金支付 7线下转账支付 4微信混合支付 5扫码混合支付 8线下现金支付 9线下转账支付)",name = "payWay")
    @ExcelProperty( value = "支付方式",index = 6)
    private Integer payWay;

    /**
     * 商品总金额
     */
    @ApiModelProperty(value="商品总金额",name = "totalAmount")
    @ExcelProperty( value = "商品总金额",index = 7)
    private BigDecimal totalAmount;

    /**
     * 应付金额
     */
    @ApiModelProperty(value="应付金额",name = "finalAmount")
    @ExcelProperty( value = "应付金额",index = 8)
    private BigDecimal finalAmount;

    /**
     * 使用积分值
     */
    @ApiModelProperty(value="使用积分值",name = "useIntegral")
    @ExcelProperty( value = "使用积分值",index = 9)
    private Integer useIntegral;


    /**
     * 订单状态20000、未付款(含义：等待付款)；20001、付款成功(含义：已经付款)；20002、已经发货；20003、收货确认(含义：已经完成)；20004、交易关闭(含义：取消订单)；20007、异常订单（退换货等） 20005申请退款 20006退款成功
     */
    @ApiModelProperty(value="订单状态",name = "ordersState")
    private Long ordersState;

    @ApiModelProperty(value="订单状态名称",name = "ordersName")
    @ExcelProperty( value = "订单状态名称",index = 10)
    private String ordersName;



}
