package com.dmc.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
@ApiModel("概况 VO")
public class StatInfoVo {

    @ApiModelProperty(value="商品付款订单个数",name = "goodsPayOrder")
    private Integer goodsPayOrderNum;

    @ApiModelProperty(value="商品付款订单金额",name = "goodsPayOrderPrice")
    private BigDecimal goodsPayOrderPrice;


    @ApiModelProperty(value="分销商品付款订单个数",name = "DistributionPayOrderNum")
    private Integer distributionPayOrderNum;

    @ApiModelProperty(value="分销商品付款订单金额",name = "DistributionPayOrderPrice")
    private BigDecimal distributionPayOrderPrice;

    @ApiModelProperty(value="店铺总营收",name = "revenuePrice")
    private BigDecimal revenuePrice;

    @ApiModelProperty(value="分销订单营收",name = "distributionPrice")
    private BigDecimal distributionPrice;

    @ApiModelProperty(value="商品订单营收",name = "goodsPrice")
    private BigDecimal goodsPrice;
    @ApiModelProperty(value="待处理订单",name = "pendingOrdersNum")
    private Integer pendingOrdersNum;
}
