package com.dmc.vo;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.dmc.model.SpuSpecsType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 商品spu表
 * </p>
 *
 * @author dmc
 * @since 2020-09-05
 */
@Data
@ApiModel("商城获取商品 VO")
public class TGoodsSpuVo extends TablePage implements Serializable  {

    private static final long serialVersionUID = 1324324324324L;

    @TableId(value = "id", type = IdType.AUTO)
    @ApiModelProperty(value="商品SPU 主键ID",name = "id")
    private Long id;

    /**
     * 商品SPU编号，唯一
     */
    @TableField("spu_no")
    @ApiModelProperty(value="商品SKU 编码",name = "id")
    private String spuNo;

    /**
     * 商品名称
     */
    @TableField("goods_name")
    @ApiModelProperty(value="商品名称",name = "goodsName")
    private String goodsName;

    /**
     * 积分商品的积分价格
     */
    @ApiModelProperty(value="分商品的积分价格",name = "integral")
    private BigDecimal integral;

    /**
     * 商品单人购买数量限制
     */
    @ApiModelProperty(value="商品单人购买数量限制",name = "payNumLimitStatus")
    private Integer payNumLimitStatus;
    /**
     * 商品单人购买数量限制
     */
    @ApiModelProperty(value="商品单人购买数量限制",name = "payNumLimit")
    private Integer payNumLimit;

    /**
     * 商品sku规格类型 如重量  颜色
     */
    @ApiModelProperty(value="商品sku规格类型 如重量  颜色",name = "skuTitleType")
    private String skuTitleType;


    @ApiModelProperty(value="分类名称",name = "categoryName")
    private String categoryName;

    @ApiModelProperty(value="默认第一个sku价格",name = "skuPrice")
    private BigDecimal skuPrice;
    /**
     *0:普通商品 1:积分商品
     */
    @ApiModelProperty(value="0:普通商品 1:积分商品",name = "skuPrice")
    private Integer integralStatus;

    /**
     * 商品库存
     */
    @TableField("sku_num")
    @ApiModelProperty(value="sku库存",name = "skuNum")
    private Integer skuNum;

    /**
     * 商品销售数量
     */
    @TableField("sku_sale_num")
    @ApiModelProperty(value="销量",name = "skuSaleNum")
    private Integer skuSaleNum;


    /**
     * 分类id
     */
    @TableField("category_id")
    @ApiModelProperty(value="分类id",name = "categoryId")
    private Long categoryId;

    /**
     * 0:普通商品 1:特殊商品
     */
    @ApiModelProperty(value=" 0:普通商品 1:特殊商品",name = "special")
    private Integer special;

    /**
     * 0:普通商品 1:推荐商品
     */
    @ApiModelProperty(value=" 0:普通商品 1:推荐商品",name = "special")
    private Integer recommend;

    /**
     * 0:普通商品 1:会员商品
     */
    @ApiModelProperty(value=" 0:普通商品 1:会员商品",name = "vip")
    private Integer vip;

    /**
     * 状态:1启用,0禁用
     */
    @ApiModelProperty(value=" SKU上下架状态 状态:1启用,0禁用",name = "status")
    private Integer status;

    /**
     * 删除标识:0:未删除,1删除
     */
    @ApiModelProperty(value="删除标识:0:未删除,1删除",name = "deleted")
    private Integer deleted;

    /**
     * 接口类型:1:推荐调用 2:会员商品调用  其他可不传或者传输0
     */
    @ApiModelProperty(value="接口类型:1:推荐调用 2:会员商品调用  其他可不传或者传输0",name = "interfaceType")
    private Integer interfaceType;

    /**
     * 商品详情
     */
    @TableField("sku_detail")
    @ApiModelProperty(value="商品详情 多个用;分割",name = "skuDetail")
    private String skuDetail;

    /**
     * 商品图片地址
     */
    @TableField("sku_img_url")
    @ApiModelProperty(value="商品图片地址 多个用;分割",name = "skuImgUrl")
    private String skuImgUrl;
    /**
     * 商品详情图片地址
     */
    @TableField("sku_detail_img_url")
    @ApiModelProperty(value="商品SPU 详情图片地址 多个用;分割",name = "id")
    private String skuDetailImgUrl;


    @TableField("create_time")
    private Date createTime;

    @ApiModelProperty(value="创建开始时间",name = "createTimeStart")
    private Date createTimeStart;
    @ApiModelProperty(value="创建结束时间",name = "createTimeEnd")
    private Date createTimeEnd;


    @ApiModelProperty(value="SKU集合",name = "skuList")
    private List<TGoodsSkuVo> skuList;


    @ApiModelProperty(value="规格类型集合",name = "specsTypeVoList")
    private List<SpuSpecsTypeVo> specsTypeList;


}
