package com.dmc.vo;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 我的收货地址
 * </p>
 *
 * @
 * @since 2020-09-05
 */
@Data
@ApiModel("我的收货地址")
public class UserShippingAddressVo extends Model<UserShippingAddressVo> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value="主键",name = "id")
    private Integer id;

    /**
     * 用户id
     */
    @ApiModelProperty(value="用户id",name = "userId")
    private Long userId;

    /**
     * 用户头像
     */
    @ApiModelProperty(value="用户头像",name = "userPhoto")
    private String userPhoto;

    /**
     * 收货姓名
     */
    @ApiModelProperty(value="收货姓名",name = "receiverName")
    private String receiverName;

    /**
     * 收货移动电话
     */
    @ApiModelProperty(value="收货移动电话",name = "receiverMobile")
    private String receiverMobile;

    /**
     * 区/县
     */
    @ApiModelProperty(value="区/县",name = "receiverDistrict")
    private String receiverDistrict;

    /**
     * 详细地址
     */
    @ApiModelProperty(value="详细地址",name = "receiverAddress")
    private String receiverAddress;

    /**
     * 邮编
     */
    @ApiModelProperty(value="邮编",name = "receiverZip")
    private String receiverZip;

    /**
     * 默认收货地址0 默认；1 默认地址
     */
    @ApiModelProperty(value="默认收货地址0 默认；1 默认地址",name = "defaultFlag")
    private Integer defaultFlag;

    @ApiModelProperty(value="创建时间",name = "createTime")
    private Date createTime;

    @ApiModelProperty(value="更新时间",name = "updateTime")
    private Date updateTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
