package com.dmc.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel("用户折扣VO")
@Data
public class UserDisCountVo {

    @ApiModelProperty(value="用户等级 1以及以上为会员,可以享受折扣",name = "level")
    private Integer level;
    @ApiModelProperty(value="折扣 如95折  则为9.5 如果为10 则不打折",name = "discountRate")
    private String discountRate;
}
