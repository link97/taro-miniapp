package com.dmc.vo;

import com.baomidou.mybatisplus.annotations.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by sail on 2016/12/25.
 */
@Data
@ApiModel("用户 VO")
public class UserInfoVO extends TablePage {
    @ApiModelProperty(value="用户ID",name = "id")
    private Long id;
    @ApiModelProperty(value="用户名",name = "username")
    private String username;
    @ApiModelProperty(value="年龄",name = "username")
    private Integer age;
    @ApiModelProperty(value="性别  0:男 1:女",name = "username")
    private Integer sex;
    @ApiModelProperty(value="真实名称",name = "name")
    private String name;
    @ApiModelProperty(value="创建时间",name = "createTime")
    private Date createTime;
    @ApiModelProperty(value="时间查询开始",name = "createTimeStart")
    private Date createTimeStart;
    @ApiModelProperty(value="时间查询结束",name = "createTimeEnd")
    private Date createTimeEnd;
    @ApiModelProperty(value="用户类型 0:商家 1:客户",name = "type")
    private Integer type;
    @ApiModelProperty(value="会员等级 0:初级会员 1:中级会员 2:高级会员",name = "level")
    private Integer level;
    @ApiModelProperty(value="用户头像",name = "userPhoto")
    private String userPhoto;
    @ApiModelProperty(value="用户手机号",name = "userPhone")
    private String userPhone;
    @ApiModelProperty(value="推荐人手机号",name = "shareUserPhone")
    private String shareUserPhone;
    @ApiModelProperty(value="推荐人",name = "shareUserName")
    private String shareUserName;
    @ApiModelProperty(value="下级分销人数",name = "shareNum")
    private Integer shareNum;
    @ApiModelProperty(value="微信绑定手机号",name = "wxPhone")
    private String wxPhone;
    @ApiModelProperty(value="积分",name = "integral")
    private BigDecimal integral;
    @ApiModelProperty(value="分红收益",name = "benefit")
    private BigDecimal benefit;
    @ApiModelProperty(value="总收益",name = "benefitAll")
    private BigDecimal benefitAll;
    @ApiModelProperty(value="提现金额",name = "cashOut")
    private BigDecimal cashOut;
    @ApiModelProperty(value="是否停用 0:启用 1:停用",name = "stop")
    private Integer stop;
    @ApiModelProperty(value="是否认证 0:未认证 1:已认证",name = "auth")
    private Integer auth;
    @ApiModelProperty(value="一级分销订单数量",name = "levelOneOrderNum")
    private Integer levelOneOrderNum;
    @ApiModelProperty(value="二级分销订单数量",name = "levelTwoOrderNum")
    private Integer levelTwoOrderNum;
    @ApiModelProperty(value="封号原因",name = "stopDes")
    private String stopDes;
    @ApiModelProperty(value="封号时间",name = "stopTime")
    private Date stopTime;
    @ApiModelProperty(value="开启原因",name = "openDes")
    private String openDes;
    @ApiModelProperty(value="解封原因",name = "openTime")
    private Date openTime;
}
