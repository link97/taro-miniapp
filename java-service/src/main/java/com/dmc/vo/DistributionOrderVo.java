package com.dmc.vo;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.dmc.model.DistributionOrderGoods;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 分销订单表
 * </p>
 *
 * @
 * @since 2020-09-08
 */
@Data
@ApiModel("分销订单表 VO")
public class DistributionOrderVo extends TablePage implements Serializable {

    private static final long serialVersionUID = 144324145253L;

    @ApiModelProperty(value="分销订单ID",name = "id")
    private Long id;

    @ApiModelProperty(value="分销订单编码",name = "id")
    private String orderNo;

    @ApiModelProperty(value="分销套餐包订单编码",name = "packageOrderNo")
    private String packageOrderNo;

    /**
     * 0:分销商订单 1:套餐包订单 2:套餐包子订单
     */
    @ApiModelProperty(value="0:分销商订单 1:套餐包订单 2:套餐包子订单",name = "orderType")
    private Integer orderType;

    /**
     * 套餐包ID
     */
    @ApiModelProperty(value="套餐包ID",name = "packageId")
    private Long packageId;
    /**
     * 用户ID
     */
    @ApiModelProperty(value="用户ID",name = "userId")
    private Long userId;

    /**
     * 上级分销订单ID
     */
    @ApiModelProperty(value="上级分销订单ID",name = "pId")
    private Long pId;

    @ApiModelProperty(value="下单人姓名",name = "userName")
    private String userName;

    @ApiModelProperty(value="下单人手机号",name = "userPhone")
    private String userPhone;
    /**
     * 购买金额
     */
    @TableField("price")
    private BigDecimal price;

    /**
     * 初始级别
     */
    @ApiModelProperty(value="初始级别",name = "levelFlagInit")
    private String levelFlagInit;

    /**
     * 现在级别
     */
    @ApiModelProperty(value="现在级别",name = "levelFlagQueue")
    private String levelFlagQueue;

    @ApiModelProperty(value="级别查询",name = "levelQuery")
    private Integer levelQuery;

    /**
     * 邀请人ID
     */
    @ApiModelProperty(value="邀请人ID",name = "shareUserId")
    private Long shareUserId;


    /**
     * 是否特权订单 0:否 1:是
     */
    @ApiModelProperty(value="是否特权订单 0:否 1:是",name = "vipOrders")
    private Integer vipOrders;
    /**
     * 成为vip的时间
     */
    @ApiModelProperty(value="成为vip的时间",name = "vipTime")
    private Date vipTime;

    /**
     * 购买单据ID
     */
    @ApiModelProperty(value="购买单据ID",name = "shareUserId")
    private Long checkoutId;

    /**
     * 邀请人ID
     */
    @ApiModelProperty(value="推荐人姓名",name = "shareUserName")
    private String shareUserName;

    /**
     * 分销ID
     */
    @ApiModelProperty(value="分销商品ID",name = "distributionGoodsId")
    private Long distributionGoodsId;

    /**
     * 队列中的分销商品ID 因涉及到升级
     */
    @ApiModelProperty(value="队列中的分销商品ID",name = "distributionQueueGoodsId")
    private Long distributionQueueGoodsId;

    @ApiModelProperty(value="队列中 sort值",name = "sort")
    private Long sort;

    @ApiModelProperty(value="队列中 真实sort值",name = "realSort")
    private Long realSort;
    /**
     * 支付方式 0:金钱购买 1:分红支付 2:分红支付-终极会员支付
     */
    @ApiModelProperty(value="支付方式 0:金钱购买 1:分红支付 2:分红支付-终极会员支付",name = "payType")
    private Integer payType;

    //支付方式结合 用于查询
    private List<Integer> payTypeList;

    //分销订单类型集合 用于查询
    private List<Integer> orderTypeList;


    @ApiModelProperty(value="订单来源",name = "payTypeName")
    private String payTypeName;

    @ApiModelProperty(value="支付金额/消耗互助金",name = "payTypeName")
    private BigDecimal payMoney;

    /**
     * 分销名称
     */
    @ApiModelProperty(value="分销名称",name = "distributionName")
    private String distributionName;

    /**
     * 订单状态 0:待支付 1:已支付
     */
    @ApiModelProperty(value="订单状态 0:待支付 1:已支付",name = "orderStatus")
    private Integer orderStatus;

    /**
     * 订单状态集合用于查询
     */
    private List<Integer> orderStatusList;

    @ApiModelProperty(value="排名",name = "rank")
    private Integer rank;

    /**
     * 赠送积分
     */
    @ApiModelProperty(value="赠送积分",name = "bonusPoints")
    private BigDecimal bonusPoints;

    /**
     * 获得分红
     */
    @ApiModelProperty(value="获得分红",name = "bonusPresentation")
    private BigDecimal bonusPresentation;


    /**
     * 当前级别赠送积分
     */
    @ApiModelProperty(value="当前级别赠送积分",name = "currentLevelBonusPoints")
    private BigDecimal currentLevelBonusPoints;

    /**
     * 当前级别获得分红
     */
    @ApiModelProperty(value="当前级别获得分红",name = "currentLevelBonusPresentation")
    private BigDecimal currentLevelBonusPresentation;

    /**
     * 当前互助分红
     */
    @ApiModelProperty(value="当前互助分红",name = "currentBonus")
    private BigDecimal currentBonus;



    @ApiModelProperty(value="结算时间",name = "settleTime")
    private Date settleTime;

    @ApiModelProperty(value="结算时间开始",name = "settleTimeStart")
    private Date settleTimeStart;

    @ApiModelProperty(value="结算时间结束",name = "settleTimeEnd")
    private Date settleTimeEnd;

    @ApiModelProperty(value="创建时间",name = "createTime")
    private Date createTime;

    @ApiModelProperty(value="分销订单对应商品信息",name = "distributionGoodsVo")
    private DistributionOrderGoodsVo distributionOrderGoodsVo;

    @ApiModelProperty(value="进度",name = "progress")
    private List<ProcessVo> progress;

    @ApiModelProperty(value="互助我的",name = "helpMeList")
    private List<DistributionHelpRelationVo> helpMeList;

    @ApiModelProperty(value="套餐包内子订单",name = "packSubOrderList")
    private List<DistributionOrderVo> packSubOrderList;

    @ApiModelProperty(value="我互助的",name = "meHelpList")
    private List<DistributionHelpRelationVo> meHelpList;

    @ApiModelProperty(value="互助分红使用记录",name = "bonusUserRecordList")
    private List<DistributionbonusUserRecordVo> bonusUserRecordList;

    /**
     * 银行表主键ID
     */
    @ApiModelProperty(value="银行表主键ID",name = "bankId")
    private Integer bankId;
    /**
     * 银行卡号
     */
    @ApiModelProperty(value="银行卡号",name = "bankCardNum")
    private String bankCardNum;

    /**
     * 转账凭证地址
     */
    @ApiModelProperty(value="转账凭证地址",name = "receiptUrl")
    private String receiptUrl;

    /**
     * 平台银行表主键ID
     */
    @ApiModelProperty(value="转账凭证地址",name = "receiptUrl")
    private Integer platformId;

    /**
     * 银行卡所属银行ID
     */
    @ApiModelProperty(value="平台银行卡所属银行ID",name = "platformBankId")
    private Integer platformBankId;

    /**
     * 平台银行名称
     */
    @ApiModelProperty(value="平台银行名称",name = "bankName")
    private String bankName;

    /**
     * 平台银行卡号
     */
    @ApiModelProperty(value="平台银行卡号",name = "bankNum")
    private String bankNum;

    /**
     *平台 银行卡号持卡人姓名
     */
    @ApiModelProperty(value="平台银行卡号持卡人姓名",name = "bankUserName")
    private String bankUserName;

    /**
     *推荐奖
     */
    @ApiModelProperty(value="推荐奖",name = "recommendAward")
    private BigDecimal recommendAward;

}
