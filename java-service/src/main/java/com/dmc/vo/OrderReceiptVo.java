package com.dmc.vo;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 发票表
 * </p>
 *
 * @author
 * @since 2020-09-24
 */
@Data
@ApiModel("发票VO")
public class OrderReceiptVo extends TablePage implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 用户ID
     */
    @ApiModelProperty(value="用户ID",name = "userId")
    private Long userId;
    /**
     * 发票用户名称
     */
    @ApiModelProperty(value="发票用户名称",name = "receiptUserName")
    private String receiptUserName;

    /**
     * 订单ID
     */
    @ApiModelProperty(value="订单ID",name = "orderId")
    private Long orderId;

    /**
     * 订单类型 0:商品订单 1:分销商订单
     */
    @ApiModelProperty(value="订单类型 0:商品订单 1:分销商订单 ",name = "orderType;")
    private Integer orderType;

    /**
     * 发票类型 1:增值税专用发票 2:增值税普通发票
     */
    @ApiModelProperty(value="票类型 1:增值税专用发票 2:增值税普通发票",name = "receiptType")
    private Integer receiptType;

    /**
     * 发票状态 0:提交发票申请 1:已开区发票
     */
    @ApiModelProperty(value="发票状态 0:提交发票申请 1:已开区发票",name = "receiptStatus")
    private Integer receiptStatus;

    /**
     * 电子发票地址
     */
    @ApiModelProperty(value="电子发票地址",name = "receiptUrl")
    private String receiptUrl;

    /**
     * 发票金额
     */
    @ApiModelProperty(value="发票金额",name = "receiptPrice")
    private BigDecimal receiptPrice;

    /**
     * 开票公司名称
     */
    @ApiModelProperty(value="开票公司名称",name = "receiptCompany")
    private String receiptCompany;

    /**
     * 纳税人识别号
     */
    @ApiModelProperty(value="纳税人识别号",name = "receiptTaxpayerNum")
    private String receiptTaxpayerNum;

    /**
     * 邮箱地址
     */
    @ApiModelProperty(value="邮箱地址",name = "email")
    private String email;
    /**
     * 开户行名称
     */
    @ApiModelProperty(value="开户行名称",name = "receiptBrandName")
    private String receiptBrandName;

    /**
     * 银行账号
     */
    @ApiModelProperty(value="银行账号",name = "receiptBrandNum")
    private String receiptBrandNum;

    /**
     * 地址
     */
    @ApiModelProperty(value="地址",name = "address")
    private String address;

    /**
     * 电话
     */
    @ApiModelProperty(value="电话",name = "phone")
    private String phone;

    @ApiModelProperty(value="创建时间开始",name = "createTimeStart")
    private Date createTimeStart;

    @ApiModelProperty(value="创建时间结束",name = "createTimeEnd")
    private Date createTimeEnd;

    @ApiModelProperty(value="创建时间",name = "createTime")
    private Date createTime;

    @ApiModelProperty(value="开票时间",name = "receiptTime")
    private Date receiptTime;



}
