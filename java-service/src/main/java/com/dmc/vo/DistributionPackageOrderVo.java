package com.dmc.vo;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 分销套餐包订单表
 * </p>
 *
 * @author 
 * @since 2021-03-17
 */
@Data
@ApiModel("分销套餐包订单表 VO")
public class DistributionPackageOrderVo extends Model<DistributionPackageOrderVo> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value="套餐包订单ID",name = "id")
    private Long id;

    /**
     * 订单编码
     */
    @ApiModelProperty(value="订单编码",name = "orderNo")
    private String orderNo;

    /**
     * 用户ID
     */
    @ApiModelProperty(value="用户ID",name = "userId")
    private Long userId;

    /**
     * 金额购买单据ID
     */
    @ApiModelProperty(value="金额购买单据ID",name = "checkoutId;")
    private Long checkoutId;

    /**
     * 邀请人ID
     */
    @ApiModelProperty(value="邀请人ID",name = "shareUserId")
    private Long shareUserId;

    /**
     * 支付方式 0:金钱购买 1:分红支付 2:分红支付-终极会员支付
     */
    @ApiModelProperty(value="支付方式 0:金钱购买 1:分红支付 2:分红支付-终极会员支付",name = "payType")
    private Integer payType;

    /**
     * 订单状态 0:待支付 1:已支付
     */
    @ApiModelProperty(value="订单状态 0:待支付 1:已支付",name = "orderStatus")
    private Integer orderStatus;

    /**
     * 套餐包ID
     */
    @ApiModelProperty(value="套餐包ID",name = "packageId")
    private Long packageId;

    /**
     * 银行表主键ID
     */
    @ApiModelProperty(value="银行表主键ID",name = "bankId")
    private Integer bankId;

    /**
     * 银行卡号
     */
    @ApiModelProperty(value="银行卡号",name = "bankCardNum")
    private String bankCardNum;

    /**
     * 转账凭证地址
     */
    @ApiModelProperty(value="转账凭证地址",name = "receiptUrl")
    private String receiptUrl;

    /**
     * 平台银行表主键ID
     */
    @ApiModelProperty(value="平台银行表主键ID",name = "platformId")
    private Integer platformId;

    /**
     * 银行卡所属银行ID
     */
    @ApiModelProperty(value="银行卡所属银行ID",name = "platformBankId")
    private Integer platformBankId;

    /**
     * 银行名称
     */
    @ApiModelProperty(value="银行名称",name = "bankName")
    private String bankName;

    /**
     * 银行卡号
     */
    @ApiModelProperty(value="银行卡号",name = "bankNum")
    private String bankNum;

    /**
     * 银行卡号持卡人姓名
     */
    @ApiModelProperty(value="银行卡号持卡人姓名",name = "bankUserName")
    private String bankUserName;

    /**
     * 当时折扣
     */
    @ApiModelProperty(value="当时折扣",name = "discount")
    private BigDecimal discount;

    /**
     * 当时价格
     */
    @ApiModelProperty(value="当时价格",name = "price")
    private BigDecimal price;

    /**
     * 获得分红
     */
    @ApiModelProperty(value="获得分红",name = "bonusPresentation")
    private BigDecimal bonusPresentation;

    /**
     * 结算时间
     */

    @ApiModelProperty(value="结算时间",name = "settleTime")
    private Date settleTime;

    @ApiModelProperty(value="创建时间",name = "createTime")
    private Date createTime;

    @ApiModelProperty(value="更新时间",name = "updateTime")
    private Date updateTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
