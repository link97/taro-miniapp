package com.dmc.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @
 * @date 2020/9/518:16
 */
@ApiModel
public class QueryBaseVo {
    @ApiModelProperty(value="手机号",name = "phone")
    String phone;
    @ApiModelProperty(value="验证码类型,0默认用户验证,1绑定银行卡",name = "type")
    Integer type;
    @ApiModelProperty(value="验证码",name = "code")
    String code;
    @ApiModelProperty(value="内容",name = "msg")
    String msg;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
