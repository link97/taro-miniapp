package com.dmc.vo;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class ImportUserVo implements Serializable {
    @ExcelProperty(value = "申购人", index = 0)
    private String field1;
    @ExcelProperty(value = "推荐人", index = 1)
    private String field2;
    @ExcelProperty(value = "级别", index = 2)
    private String field3;
    @ExcelProperty(value = "位置", index = 3)
    private String field4;
    @ExcelProperty(value = "身份证", index = 4)
    private String field5;
    @ExcelProperty(value = "手机号", index = 5)
    private String field6;
    @ExcelProperty(value = "微信号", index = 6)
    private String field7;
    @ExcelProperty(value = "购买分销", index = 7)
    private String field8;
    @ExcelProperty(value = "推荐奖", index = 8)
    private String field9;
    @ExcelProperty(value = "产品积分", index = 9)
    private String field10;
    @ExcelProperty(value = "互助金", index = 10)
    private String field11;
    @ExcelProperty(value = "晋级", index = 11)
    private String field12;
    @ExcelProperty(value = "收益", index = 12)
    private String field13;

}
