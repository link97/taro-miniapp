package com.dmc.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * PC端分销详情VO
 * </p>
 *
 * @
 * @since 2020-09-08
 */
@Data
@ApiModel("PC端分销详情VO ")
public class DistributionOrderDetailPcVo extends TablePage implements Serializable {

    private static final long serialVersionUID = 1443232423253L;

    @ApiModelProperty(value="分销订单ID",name = "id")
    private Long id;

    @ApiModelProperty(value="分销订单编码",name = "id")
    private String orderNo;

    @ApiModelProperty(value="分销名称",name = "distributionName")
    private String distributionName;

    /**
     * 支付方式 0:金钱购买 1:分红支付 2:分红支付-终极会员支付
     */
    @ApiModelProperty(value="支付方式 0:金钱购买 1:分红支付 2:分红支付-终极会员支付",name = "payType")
    private Integer payType;

    @ApiModelProperty(value="订单来源",name = "payTypeName")
    private String payTypeName;

    @ApiModelProperty(value="支付金额/消耗互助金",name = "payTypeName")
    private BigDecimal payMoney;

    @ApiModelProperty(value="升级金额",name = "upgradeMoney")
    private BigDecimal upgradeMoney;

    @ApiModelProperty(value="复投中级金额",name = "zhongJiMoney")
    private BigDecimal zhongJiMoney;

    @ApiModelProperty(value="复投中级订单号",name = "zhongJiOrderNo")
    private String zhongJiOrderNo;

    @ApiModelProperty(value="高级中级金额",name = "gaoJiMoney")
    private BigDecimal gaoJiMoney;

    @ApiModelProperty(value="复投高级订单号",name = "gaoJiOrderNo")
    private String gaoJiOrderNo;

    @ApiModelProperty(value="分红转收益",name = "benefitMoney")
    private BigDecimal benefitMoney;

    /**
     * 赠送积分
     */
    @ApiModelProperty(value="赠送积分",name = "bonusPoints")
    private BigDecimal bonusPoints;
    /**
     * 获得分红
     */
    @ApiModelProperty(value="获得分红",name = "bonusPresentation")
    private BigDecimal bonusPresentation;

    @ApiModelProperty(value="互助分红金",name = "helpBonusPresentation")
    private BigDecimal helpBonusPresentation;


    @ApiModelProperty(value="创建时间",name = "createTime")
    private Date createTime;


    @ApiModelProperty(value="互助我的",name = "helpMeList")
    private List<DistributionHelpRelationVo> helpMeList;

    @ApiModelProperty(value="我互助的",name = "meHelpList")
    private List<DistributionHelpRelationVo> meHelpList;

}
