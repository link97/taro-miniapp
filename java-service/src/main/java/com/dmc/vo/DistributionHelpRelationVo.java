package com.dmc.vo;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 助益关系链
 * </p>
 *
 * @
 * @since 2020-09-09
 */
@Data
@ApiModel("助益关系链 VO")
public class DistributionHelpRelationVo extends Model<DistributionHelpRelationVo> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    @ApiModelProperty(value="主键ID",name = "id")
    private Long id;

    /**
     * 分销订单ID
     */
    @ApiModelProperty(value="分销订单ID",name = "distributionOrderId")
    private Long distributionOrderId;

    /**
     * 助益分红时,订单的级别
     */
    @ApiModelProperty(value="助益分红时,订单的级别",name = "levelFlag")
    private String levelFlag;

    /**
     * 分销订单ID
     */
    @ApiModelProperty(value="分销订单编码",name = "distributionOrderNo")
    private String distributionOrderNo;
    /**
     * 订单购买人
     */
    @ApiModelProperty(value="订单购买人",name = "userId")
    private Long userId;

    /**
     * 订单购买人
     */
    @ApiModelProperty(value="订单购买人",name = "userName")
    private String userName;

    /**
     * 订单购买人
     */
    @ApiModelProperty(value="订单购买人头像",name = "userPhoto")
    private String userPhoto;

    /**
     * 被助人
     */
    @ApiModelProperty(value="被助人",name = "helpUserId")
    private Long helpUserId;

    /**
     * 被助人头像
     */
    @ApiModelProperty(value="被助人头像",name = "userPhoto")
    private String helpUserPhoto;

    /**
     * 0:我被助益 1:我助益别人
     */
    @ApiModelProperty(value="0:我被助益 1:我助益别人",name = "type")
    private Integer type;

    /**
     * 被助人
     */
    @ApiModelProperty(value="订单购买人",name = "helpUserName")
    private String helpUserName;


    /**
     * 被助益订单
     */
    @ApiModelProperty(value="被助益订单",name = "helpOrderId")
    private Long helpOrderId;

    /**
     * 被助益订单编码
     */
    @ApiModelProperty(value="被助益订单编码",name = "helpOrderNo")
    private String helpOrderNo;

    /**
     * 助益金额
     */
    @ApiModelProperty(value="助益金额",name = "helpMoney")
    private BigDecimal helpMoney;

    /**
     * 被分享人会员等级
     */
    @ApiModelProperty(value="会员等级",name = "level")
    private Integer level;

    /**
     * 会员等级名称
     */
    @ApiModelProperty(value="会员等级名称",name = "levelName")
    private String levelName;

    @TableField("create_time")
    private Date createTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
