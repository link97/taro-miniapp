package com.dmc.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * Created by sail on 2016/12/25.
 */
@Data
@ApiModel("分页组件")
public class TablePage {
    @ApiModelProperty(value="起始页",name = "start")
    private Integer start;
    @ApiModelProperty(value="size",name = "length")
    private Integer length;
    @ApiModelProperty(value="未知",name = "length")
    private Integer draw;
}
