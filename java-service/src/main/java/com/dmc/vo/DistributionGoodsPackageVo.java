package com.dmc.vo;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 分销商品套餐表
 * </p>
 *
 * @author 
 * @since 2021-03-17
 */
@Data
@ApiModel("分销商品套餐表 VO")
public class DistributionGoodsPackageVo extends Model<DistributionGoodsPackageVo> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value="主键",name = "id")
    private Long id;

    /**
     * 套餐名称
     */
    @ApiModelProperty(value="套餐名称",name = "packageName")
    private String packageName;

    @ApiModelProperty(value="购买限制",name = "buyLimit")
    private BigDecimal buyLimit;

    /**
     * 折扣
     */
    @ApiModelProperty(value="折扣",name = "discount")
    private BigDecimal discount = BigDecimal.ONE;

    /**
     * 价格
     */
    @ApiModelProperty(value="价格",name = "price;")
    private BigDecimal price;

    /**
     * 状态:1上架,0下架
     */
    @ApiModelProperty(value=" 状态:1上架,0下架",name = "status")
    private Integer status;

    /**
     * 删除标示(默认0未删除)
     */
    @ApiModelProperty(value="删除标示(默认0未删除)",name = "deleted")
    private Integer deleted;

    /**
     * 套餐包描述
     */
    @ApiModelProperty(value="套餐包描述",name = "des")
    private String des;

    /**
     * 套餐包对应分销商品集合
     */
    @ApiModelProperty(value="套餐包对应分销商品集合",name = "goodList")
    private List<DistributionPackageGoodsVo> distributionGoodList;

    @TableField("create_time")
    private Date createTime;

    @TableField("update_time")
    private Date updateTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
