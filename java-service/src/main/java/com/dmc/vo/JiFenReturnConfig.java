package com.dmc.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
@ApiModel("积分返现配置 VO")
public class JiFenReturnConfig {
    @ApiModelProperty(value="消费满多少积分返现",name = "payIntegralLimit")
    private BigDecimal payIntegralLimit;
    @ApiModelProperty(value="返多少现金",name = "returnMoney")
    private BigDecimal returnMoney;
    @ApiModelProperty(value="返多少积分",name = "returnIntegral")
    private BigDecimal returnIntegral;
}
