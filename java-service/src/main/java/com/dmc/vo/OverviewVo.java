package com.dmc.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
@Data
@ApiModel
public class OverviewVo implements Serializable {
    /**
     * 商品付款订单
     */
    @ApiModelProperty(value="商品付款订单",name = "paymentOrderNum")
    private Integer paymentOrderNum;
    /**
     * 付款金额
     */
    @ApiModelProperty(value="商品付款金额",name = "paymentOrderAmount")
    private BigDecimal paymentOrderAmount;
    /**
     * 分销商付款订单
     */
    @ApiModelProperty(value="分销商付款订单",name = "paymentDistributorNum")
    private Integer paymentDistributorNum;
    /**
     * 分销商付款金额
     */
    @ApiModelProperty(value="分销商付款金额",name = "paymentDistributorAmount")
    private BigDecimal paymentDistributorAmount;
    /**
     * 未上架商品
     */
    @ApiModelProperty(value="未上架商品",name = "notOnTheShelvesNum")
    private Integer notOnTheShelvesNum;
    /**
     * 待处理订单
     */
    @ApiModelProperty(value="待处理订单",name = "pendingOrdersNum")
    private Integer pendingOrdersNum;
}
