package com.dmc.vo;

import com.baomidou.mybatisplus.annotations.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Date;

/**
 * @
 * @date 2020/9/515:43
 */
@ApiModel("我的银行卡信息")
public class BankCardVo implements Serializable {

    @ApiModelProperty(value="主键",name = "id")
    private Integer id;

    /**
     * 用户id
     */
    @ApiModelProperty(value="用户id",name = "userId")
    private Long userId;

    /**
     * 银行卡所属银行ID
     */
    @ApiModelProperty(value="银行卡所属银行ID",name = "bankId")
    private Integer bankId;

    /**
     * 银行卡号
     */
    @ApiModelProperty(value="银行卡号",name = "cardId")
    private Long cardId;

    /**
     * 银行绑定手机号码
     */
    @ApiModelProperty(value="银行绑定手机号码",name = "cardMobile")
    private String cardMobile;

    /**
     * 逻辑删除标记（0：显示；1：隐藏）
     */
    @ApiModelProperty(value="逻辑删除标记（0：显示；1：隐藏）",name = "delFlag")
    private Integer delFlag;

    @ApiModelProperty(value="创建时间",name = "createTime")
    private Date createTime;

    @ApiModelProperty(value="更新时间",name = "updateTime")
    private Date updateTime;

    //手机验证码
    @ApiModelProperty(value="手机验证码",name = "verifyCode")
    private Integer verifyCode;

    @ApiModelProperty(value="银行卡银行",name = "cardName")
    private String cardName;

    //默认标志
    @ApiModelProperty(value="默认标志",name = "defaultFlag")
    private Integer defaultFlag;
    //银行名称
    @ApiModelProperty(value="银行名称",name = "bankName")
    private String bankName;
    //银行标志
    @ApiModelProperty(value="银行标志",name = "bankImg")
    private String bankImg;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Integer getBankId() {
        return bankId;
    }

    public void setBankId(Integer bankId) {
        this.bankId = bankId;
    }

    public Long getCardId() {
        return cardId;
    }

    public void setCardId(Long cardId) {
        this.cardId = cardId;
    }

    public String getCardMobile() {
        return cardMobile;
    }

    public void setCardMobile(String cardMobile) {
        this.cardMobile = cardMobile;
    }

    public Integer getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(Integer delFlag) {
        this.delFlag = delFlag;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getVerifyCode() {
        return verifyCode;
    }

    public void setVerifyCode(Integer verifyCode) {
        this.verifyCode = verifyCode;
    }

    public String getCardName() {
        return cardName;
    }

    public void setCardName(String cardName) {
        this.cardName = cardName;
    }

    public Integer getDefaultFlag() {
        return defaultFlag;
    }

    public void setDefaultFlag(Integer defaultFlag) {
        this.defaultFlag = defaultFlag;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankImg() {
        return bankImg;
    }

    public void setBankImg(String bankImg) {
        this.bankImg = bankImg;
    }
}
