package com.dmc.vo;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 提现表
 * </p>
 *
 * @author
 * @since 2020-09-13
 */
@Data
@ApiModel("提现VO")
public class CashOutOrdersVo extends TablePage implements Serializable {

    private static final long serialVersionUID = 1234342324213L;

    @ApiModelProperty(value="主键ID",name = "id")
    private Long id;

    /**
     * 用户ID
     */
    @ApiModelProperty(value="用户ID",name = "userId")
    private Long userId;

    /**
     * 用户名称
     */
    @ApiModelProperty(value="用户名称",name = "userName")
    private String userName;


    /**
     * 用户手机号
     */
    @ApiModelProperty(value="用户手机号",name = "userPhone")
    private String userPhone;

    /**
     * 持卡人姓名
     */
    @ApiModelProperty(value="持卡人姓名",name = "bandUserName")
    private String bandUserName;

    /**
     * 备注
     */
    @ApiModelProperty(value="注",name = "remark")
    private String remark;

    /**
     * 提现订单编码
     */
    @ApiModelProperty(value="提现订单编码",name = "orderCode")
    private String orderCode;

    /**
     * 提现类型 0:银行卡 1:零钱
     */
    @ApiModelProperty(value="提现类型 0:银行卡 1:零钱",name = "cashOutType")
    private Integer cashOutType;

    /**
     * 0:线上 1:线下
     */
    @ApiModelProperty(value=" 0:线上 1:线下",name = "cashOutLineType")
    private Integer cashOutLineType;

    /**
     * 提现金额
     */
    @ApiModelProperty(value="提现金额",name = "ammount")
    private BigDecimal ammount;

    /**
     * 实际到账金额
     */
    @ApiModelProperty(value="实际到账金额",name = "arriveAmmount")
    private BigDecimal arriveAmmount;

    /**
     * 手续费
     */
    @ApiModelProperty(value="手续费",name = "transferCharge")
    private BigDecimal transferCharge;

    /**
     * 当时的手续费率
     */
    @ApiModelProperty(value="当时的手续费率",name = "transferChargeRate")
    private BigDecimal transferChargeRate;

    /**
     * 0:待提现 1:提现中 2:已提现
     */
    @ApiModelProperty(value="0:待提现 1:提现中 2:已提现",name = "status")
    private Integer status;

    /**
     * 银行表ID
     */
    @ApiModelProperty(value="银行表ID",name = "bandId")
    private Long bandId;

    /**
     * 银行表名称
     */
    @ApiModelProperty(value="银行表名称",name = "bandName")
    private String bandName;

    /**
     * 银行卡号
     */
    @ApiModelProperty(value="银行卡号",name = "bandNum")
    private String bandNum;

    /**
     * 凭证
     */
    @ApiModelProperty(value="凭证",name = "voucherUrl")
    private String voucherUrl;

    /**
     * 提现时间
     */
    @ApiModelProperty(value="提现时间",name = "cashOutTime")
    private Date cashOutTime;

    /**
     * 提现时间
     */
    @ApiModelProperty(value="提现时间-开始",name = "cashOutTimeStart")
    private Date cashOutTimeStart;

    /**
     * 提现时间
     */
    @ApiModelProperty(value="提现时间-结束",name = "cashOutTimeEnd")
    private Date cashOutTimeEnd;

    @ApiModelProperty(value="创建时间",name = "id")
    private Date createTime;

    /**
     * 提示语
     */
    @ApiModelProperty(value="提示语",name = "message")
    private String message;

}
