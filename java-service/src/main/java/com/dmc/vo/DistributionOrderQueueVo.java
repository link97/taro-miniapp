package com.dmc.vo;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 分销队列表 以 分销商品ID为分类
 * </p>
 *
 * @
 * @since 2020-09-08
 */
@Data
public class DistributionOrderQueueVo extends Model<DistributionOrderQueueVo> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 用户ID
     */
    @TableField("user_id")
    private Long userId;

    /**
     * 分销ID
     */
    @TableField("distribution_goods_id")
    private Long distributionGoodsId;

    @TableField("distribution_order_id")
    private Long distributionOrderId;

    @TableField("bonus_presentation")
    private BigDecimal bonusPresentation;

    @TableField("sort")
    private Long sort;

    @TableField("real_sort")
    private Long realSort;
    /**
     * 分销级别标识
     */
    @TableField("level_flag")
    private String levelFlag;


    @TableField("create_time")
    private Date createTime;

    @TableField("update_time")
    private Date updateTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
