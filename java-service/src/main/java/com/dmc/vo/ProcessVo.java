package com.dmc.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 分销订单表
 * </p>
 *
 * @
 * @since 2020-09-08
 */
@Data
@ApiModel("进度 VO")
public class ProcessVo implements Serializable {

    private static final long serialVersionUID = 144324124324345253L;

    @ApiModelProperty(value="进度名称",name = "processName")
    private String processName;

    @ApiModelProperty(value="级别标识",name = "levelFlag")
    private String levelFlag;

    /**
     * 2:中级 3:高级 4:超高级
     */
    @ApiModelProperty(value="2:中级 3:高级 4:超高级",name = "level")
    private Integer level;

    /**
     * 0:未开启 1:进行中 2:已结束
     */
    @ApiModelProperty(value="0:未开启 1:进行中 2:已结束",name = "processStatus")
    private Integer processStatus;


}
