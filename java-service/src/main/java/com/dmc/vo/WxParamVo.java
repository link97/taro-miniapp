package com.dmc.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class WxParamVo implements Serializable {

    @ApiModelProperty(value="微信授权code",name = "code")
    private String code;
    @ApiModelProperty(value="微信授权秘钥",name = "session_key")
    private String session_key;
    @ApiModelProperty(value="微信用户信息加密字符串",name = "encryptedData")
    private String encryptedData;
    @ApiModelProperty(value="iv",name = "iv")
    private String iv;
}
