package com.dmc.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@ApiModel("身份认证")
public class UserAuthenticateVo  extends TablePage implements Serializable {
    private Integer id;

    /**
     * 用户id
     */
    @ApiModelProperty(value="用户id",name = "userId")
    private Long userId;

    /**
     * 姓名
     */
    @ApiModelProperty(value="身份证姓名",name = "fullName")
    private String fullName;

    /**
     * 身份证号码
     */
    @ApiModelProperty(value="身份证号码",name = "identificationNo")
    private String identificationNo;

    /**
     * 认证渠道
     */
    @ApiModelProperty(value="认证渠道",name = "channels")
    private Integer channels;

    /**
     * 认证状态
     */
    @ApiModelProperty(value="认证状态",name = "status")
    private Integer status;

    /**
     * 身份证图片
     */
    @ApiModelProperty(value="身份证图片",name = "imgUrl")
    private String imgUrl;

    /**
     * 身份证反面图片
     */
    @ApiModelProperty(value="身份证反面图片",name = "negativeImgUrl")
    private String negativeImgUrl;

    /**
     * 认证通过日期
     */
    @ApiModelProperty(value="认证审核日期",name = "checkTime")
    private Date checkTime;
    @ApiModelProperty(value="提交申请认证记录日期",name = "createTime")
    private Date createTime;
    @ApiModelProperty(value="更新认证记录日期",name = "updateTime")
    private Date updateTime;

    @ApiModelProperty(value="用户手机号",name = "userPhone")
    private String userPhone;
    @ApiModelProperty(value="用户名",name = "username")
    private String username;
}
