package com.dmc.vo;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 分销关系链
 * </p>
 *
 * @
 * @since 2020-09-08
 */
@Data
@ApiModel("分销关系链 VO")
public class DistributionShareRelationVo extends Model<DistributionShareRelationVo> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value="主键ID",name = "id")
    private Long id;

    /**
     * 被分享人ID
     */
    @ApiModelProperty(value="被分享人ID",name = "userId")
    private Long userId;

    /**
     * 被分享人名称
     */
    @ApiModelProperty(value="被分享人名称",name = "userName")
    private String userName;

    /**
     * 被分享人头像
     */
    @ApiModelProperty(value="被分享人头像",name = "userPhoto")
    private String userPhoto;

    /**
     * 被分享人会员等级
     */
    @ApiModelProperty(value="被分享人会员等级",name = "level")
    private Integer level;

    /**
     * 分销订单数量
     */
    @ApiModelProperty(value="分销订单数量",name = "distributionOrderNum")
    private Integer distributionOrderNum;

    /**
     * 会员等级名称
     */
    @ApiModelProperty(value="会员等级名称",name = "levelName")
    private String levelName;


    /**
     * 分享人ID
     */
    @ApiModelProperty(value="分享人ID",name = "shareUserId")
    private Long shareUserId;


    @TableField("create_time")
    private Date createTime;

    @TableField("update_time")
    private Date updateTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
