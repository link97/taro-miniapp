package com.dmc.vo;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.dmc.dict.SmallPayModeDict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 系统设置表
 * </p>
 *
 * @
 * @since 2020-09-12
 */
@Data
@ApiModel("系统设置VO")
public class SysConfVo extends Model<SysConfVo> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "conf_id", type = IdType.AUTO)
    @ApiModelProperty(value="主键",name = "confId")
    private Integer confId;

    /**
     * 配置类型,1订单交易设置
     */
    @TableField("conf_type")
    @ApiModelProperty(value="配置类型,1订单交易设置",name = "confType")
    private Integer confType;

    /**
     * 订单取消时间,分钟数
     */
    @TableField("order_cancel_time")
    @ApiModelProperty(value="订单取消时间,分钟数",name = "orderCancelTime")
    private Integer orderCancelTime;

    /**
     * 订单自动确认时间，天数
     */
    @TableField("order_auto_arrival")
    @ApiModelProperty(value="订单自动确认时间，天数",name = "orderAutoArrival")
    private Integer orderAutoArrival;

    /**
     * 退换货地址
     */
    @ApiModelProperty(value="退换货地址",name = "returnAddress")
    @TableField("return_address")
    private String returnAddress;

    /**
     * 普通会员购买限制
     */
    @ApiModelProperty(value="普通会员购买限制",name = "buyLimit")
    private String buyLimit;

    /**
     * 提现规则
     */
    @ApiModelProperty(value="提现规则",name = "withdrawalRules")
    private String withdrawalRules;

    /**
     * 大额提现限制
     */
    @ApiModelProperty(value="大额提现限制",name = "cashOutLimit")
    private String cashOutLimit;

    /**
     * 提现手续费率
     */
    @ApiModelProperty(value="提现手续费率",name = "cashOutRate")
    private String cashOutRate;
    /**
     * 库存提醒限制
     */
    @ApiModelProperty(value="库存提醒限制",name = "kuCunLimit")
    private String kuCunLimit;

    /**
     * 推荐人商品收益比率
     */
    @ApiModelProperty(value="推荐人商品收益比率 首次",name = "goodsRewordRate")
    private String goodsRewordRate;
    /**
     * 推荐人商品收益比率 二次以后
     */
    @ApiModelProperty(value="推荐人商品收益比率 二次以后",name = "goodsRewordRateOther")
    private String goodsRewordRateOther;
    /**
     * 商品折扣比率
     */
    @ApiModelProperty(value="商品折扣比率",name = "goodsDiscountRate")
    private String goodsDiscountRate;
    /**
     * 分销升级插队人数限制
     */
    @ApiModelProperty(value="分销升级插队人数限制",name = "distributionUpgradeLimt")
    private String distributionUpgradeLimt;
    /**
     * 支付方式接受参数 以,分割 如0,1,2,3
     */
    @ApiModelProperty(value="支付方式接受参数 以,分割 如0,1,2,3",name = "payModes")
    private String payModes;
    /**
     * 支付方式集合
     */
    @ApiModelProperty(value="支付方式集合",name = "payModeList")
    private List<SmallPayModeVo> payModeList;

    @ApiModelProperty(value="创建时间",name = "createTime")
    @TableField("create_time")
    private Date createTime;

    @ApiModelProperty(value="更新时间",name = "updateTime")
    @TableField("update_time")
    private Date updateTime;
    @ApiModelProperty(value="积分返现配置",name = "jiFenReturnConfig")
    private JiFenReturnConfig jiFenReturnConfig;


    @Override
    protected Serializable pkVal() {
        return this.confId;
    }

}
