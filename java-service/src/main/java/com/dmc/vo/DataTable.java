package com.dmc.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * Created by sail on 2016/12/25.
 */
@Data
@ApiModel("分页返回基类")
public class DataTable<T> {
    @ApiModelProperty(value="位置",name = "draw")
    private Integer draw;
    @ApiModelProperty(value="总数",name = "recordsTotal")
    private Long recordsTotal;
    @ApiModelProperty(value="总数",name = "recordsFiltered")
    private Long recordsFiltered;
    @ApiModelProperty(value="列表内容",name = "data")
    private List<T> data;
    @ApiModelProperty(value="附属数据",name = "otherData")
    private T otherData;
}
