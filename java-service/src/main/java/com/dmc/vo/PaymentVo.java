package com.dmc.vo;

import com.baomidou.mybatisplus.annotations.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * 支付VO
 */
@ApiModel("支付VO")
public class PaymentVo extends TablePage {
    @NotNull(message="订单ID不可为空",
            groups = {ValidatedGroups.RefundPay.class, ValidatedGroups.Pay.class})
    @ApiModelProperty(value="订单ID",name = "orderId")
    private String orderId;
    @NotNull(message="订单类型不可为空",
            groups = {ValidatedGroups.RefundPay.class, ValidatedGroups.Pay.class})
    @ApiModelProperty(value="订单类型",name = "orderType")
    private Integer orderType;
    @ApiModelProperty(value="订单编码",name = "orderCode")
    private String orderCode;
    @NotBlank(message="支付类型不可为空")
    @ApiModelProperty(value="支付类型",name = "payType")
    private String payType;
    @NotBlank(message="支付方式不可为空")
    @ApiModelProperty(value="支付方式 ",name = "payModel")
    private String payModel;
    @ApiModelProperty(value="openId",name = "openId")
    private String openId;
    @ApiModelProperty(value="ip",name = "ip")
    private String ip;
    @NotBlank(message="金额不可为空",groups = {ValidatedGroups.Pay.class, ValidatedGroups.RefundPay.class, ValidatedGroups.Transfer.class})
    @ApiModelProperty(value="金额 分",name = "amount")
    private String amount;
    @ApiModelProperty(value="退款原因",name = "reason")
    private String reason;
    @ApiModelProperty(value="用户ID",name = "userId")
    private Long userId;
    @ApiModelProperty(value="昵称",name = "userName")
    private String userName;
    @ApiModelProperty(value="用户手机号",name = "userPhone")
    private String userPhone;
    @NotNull(message="支付单据ID不可为空",
            groups = {ValidatedGroups.QueryPay.class, ValidatedGroups.RefundPay.class})
    @ApiModelProperty(value="支付单据ID",name = "checkoutId")
    private Long checkoutId;
    @NotBlank(message="提现账户不能为空",groups = {ValidatedGroups.Transfer.class})
    @ApiModelProperty(value="提现账户",name = "payeeAccount")
    private String payeeAccount;
    @NotBlank(message="提现人名称不可为空",groups = {ValidatedGroups.Transfer.class})
    @ApiModelProperty(value="提现人名称",name = "payerShowName")
    private String payerShowName;
    @NotBlank(message="提现人真实姓名不可为空",groups = {ValidatedGroups.Transfer.class})
    @ApiModelProperty(value="提现人真实姓名",name = "payeeRealName")
    private String payeeRealName;
    @NotBlank(message="提现类型 0:零钱 1:银行卡",groups = {ValidatedGroups.Transfer.class})
    @ApiModelProperty(value="提现类型 0:零钱 1:银行卡",name = "transferType")
    private int transferType;
    @ApiModelProperty(value="银行卡号",name = "bankCardNum")
    private String bankCardNum;
    @ApiModelProperty(value="银行编码",name = "bankCode")
    private String bankCode;
    @ApiModelProperty(value="交易编码",name = "payNo")
    private String payNo;
    @ApiModelProperty(value="交易开始日期",name = "sDate")
    private String sDate;
    @ApiModelProperty(value="交易结束日期",name = "eDate")
    private String eDate;
    @ApiModelProperty(value="交易开始日期 查询",name = "sDateQuery")
    private String sDateQuery;
    @ApiModelProperty(value="交易结束日期 查询",name = "eDateQuery")
    private String eDateQuery;
    @ApiModelProperty(value="交易类型",name = "payTypeInt")
    private Integer payTypeInt;
    @ApiModelProperty(value="交易日期",name = "createTime")
    private Date createTime;



    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Integer getOrderType() {
        return orderType;
    }

    public void setOrderType(Integer orderType) {
        this.orderType = orderType;
    }

    public String getOrderCode() {
        return orderCode;
    }

    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode;
    }

    public String getPayType() {
        return payType;
    }

    public void setPayType(String payType) {
        this.payType = payType;
    }

    public String getPayModel() {
        return payModel;
    }

    public void setPayModel(String payModel) {
        this.payModel = payModel;
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getCheckoutId() {
        return checkoutId;
    }

    public void setCheckoutId(Long checkoutId) {
        this.checkoutId = checkoutId;
    }

    public String getPayeeAccount() {
        return payeeAccount;
    }

    public void setPayeeAccount(String payeeAccount) {
        this.payeeAccount = payeeAccount;
    }

    public String getPayerShowName() {
        return payerShowName;
    }

    public void setPayerShowName(String payerShowName) {
        this.payerShowName = payerShowName;
    }

    public String getPayeeRealName() {
        return payeeRealName;
    }

    public void setPayeeRealName(String payeeRealName) {
        this.payeeRealName = payeeRealName;
    }

    public int getTransferType() {
        return transferType;
    }

    public void setTransferType(int transferType) {
        this.transferType = transferType;
    }

    public String getBankCardNum() {
        return bankCardNum;
    }

    public void setBankCardNum(String bankCardNum) {
        this.bankCardNum = bankCardNum;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getsDate() {
        return sDate;
    }

    public void setsDate(String sDate) {
        this.sDate = sDate;
    }

    public String geteDate() {
        return eDate;
    }

    public void seteDate(String eDate) {
        this.eDate = eDate;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getPayNo() {
        return payNo;
    }

    public void setPayNo(String payNo) {
        this.payNo = payNo;
    }

    public String getsDateQuery() {
        return sDateQuery;
    }

    public void setsDateQuery(String sDateQuery) {
        this.sDateQuery = sDateQuery;
    }

    public String geteDateQuery() {
        return eDateQuery;
    }

    public void seteDateQuery(String eDateQuery) {
        this.eDateQuery = eDateQuery;
    }
}
