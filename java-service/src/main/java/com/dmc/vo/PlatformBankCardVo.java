package com.dmc.vo;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 平台银行账户信息
 * </p>
 *
 * @author 
 * @since 2020-10-28
 */
@Data
@ApiModel("平台银行卡信息")
public class PlatformBankCardVo extends Model<PlatformBankCardVo> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value="平台银行主键ID",name = "id")
    private Integer id;

    /**
     * 银行卡所属银行ID
     */
    @ApiModelProperty(value="银行卡所属银行ID",name = "bankId")
    private Integer bankId;

    /**
     * 银行名称
     */
    @ApiModelProperty(value="银行名称",name = "bankName")
    private String bankName;

    /**
     * 银行卡号
     */
    @ApiModelProperty(value="银行卡号",name = "bankNum")
    private String bankNum;

    /**
     * 银行卡号持卡人姓名
     */
    @ApiModelProperty(value="银行卡号持卡人姓名",name = "bankUserName")
    private String bankUserName;

    /**
     * 0:启用 1:禁用
     */
    @ApiModelProperty(value="0:启用 1:禁用",name = "stop")
    private Integer stop;

    @TableField("create_time")
    private Date createTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
