package com.dmc.vo;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 商品SKU规格类型表
 * </p>
 *
 * @author 
 * @since 2021-03-22
 */
@Data
@ApiModel("商品SKU规格类型表 VO")
public class SkuSpecsTypeVo extends Model<SkuSpecsTypeVo> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value="主键",name = "id")
    private Long id;

    /**
     * 规格类型 如重量大小
     */
    @ApiModelProperty(value="规格类型 如重量大小",name = "skuType")
    private String skuType;

    /**
     * 商品ID
     */
    @ApiModelProperty(value="商品SKUID",name = "skuId")
    private Long skuId;

    /**
     * 状态:1上架,0下架
     */
    @ApiModelProperty(value=" 状态:1上架,0下架",name = "status")
    private Integer status;

    @ApiModelProperty(value="SKU规格集合",name = "skuSpecsList")
    private List<SkuSpecsVo> skuSpecsList;

    /**
     * 删除标识:0:未删除,1删除
     */
    @ApiModelProperty(value="0:未删除,1删除",name = "deleted")
    private Integer deleted;

    @TableField("create_time")
    private Date createTime;

    @TableField("update_time")
    private Date updateTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
