package com.dmc.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 小程序支付方式vo
 */
@Data
@ApiModel("小程序支付方式vo")
public class SmallPayModeVo implements Serializable {
    private static final long serialVersionUID = 1234321446443243232L;
    @ApiModelProperty(value="支付方式key",name = "payModeInt")
    private int payModeInt;
    @ApiModelProperty(value="支付方式名称",name = "payModeName")
    private String payModeName;
    @ApiModelProperty(value="支付图标地址",name = "payPngUrl")
    private String payPngUrl;
    @ApiModelProperty(value="是否选中",name = "payModeSelected")
    private int payModeSelected;
    @ApiModelProperty(value="线下支付平台账号信息",name = "platformBankCardVo")
    private PlatformBankCardVo platformBankCardVo;
}
