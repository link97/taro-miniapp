package com.dmc.vo;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;
import com.dmc.model.TGoodsSku;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 商品SKU表
 * </p>
 *
 * @author dmc
 * @since 2020-09-05
 */
@Data
@ApiModel("商品详情 VO")
public class TGoodsDetailVo implements Serializable  {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    @ApiModelProperty(value="商品SKU 主键ID",name = "id")
    private Long id;

    /**
     * spu Id
     */
    @TableField("goods_spu_id")
    @ApiModelProperty(value="商品SPU ID",name = "goodsSpuId")
    private Long goodsSpuId;

    /**
     * 商品sku编号
     */
    @TableField("sku_no")
    @ApiModelProperty(value="SKU编码",name = "skuNo")
    private String skuNo;

    /**
     * 商品sku规格类型 如重量  颜色
     */
    @TableField("sku_title_type")
    @ApiModelProperty(value="商品规格类型",name = "skuTitleType")
    private String skuTitleType;

    @ApiModelProperty(value=" 0:普通商品 1:会员商品",name = "vip")
    private Integer vip;
    /**
     * 积分商品的积分价格
     */
    @ApiModelProperty(value="分商品的积分价格",name = "integral")
    private BigDecimal integral;

    /**
     *0:普通商品 1:积分商品
     */
    @ApiModelProperty(value="0:普通商品 1:积分商品",name = "skuPrice")
    private Integer integralStatus;
    /**
     * 商品单人购买数量限制
     */
    @ApiModelProperty(value="商品单人购买数量限制",name = "payNumLimitStatus")
    private Integer payNumLimitStatus;
    /**
     * 商品单人购买数量限制
     */
    @ApiModelProperty(value="商品单人购买数量限制",name = "payNumLimit")
    private Integer payNumLimit;
    /**
     * 商品sku规格
     */
    @TableField("sku_title")
    @ApiModelProperty(value="商品规格",name = "skuTitle")
    private String skuTitle;

    /**
     * 商品封面图片地址
     */
    @TableField("sku_covers_url")
    @ApiModelProperty(value="商品SKU封面图片 多个用;分割",name = "skuCoversUrl")
    private String skuCoversUrl;

    /**
     * 商品图片地址
     */
    @TableField("sku_img_url")
    @ApiModelProperty(value="商品SKU图片 多个用;分割",name = "skuImgUrl")
    private String skuImgUrl;

    /**
     * 商品库存
     */
    @TableField("sku_num")
    @ApiModelProperty(value="商品SKU数量",name = "skuNum")
    private BigDecimal skuNum;

    /**
     * 商品销售数量
     */
    @TableField("sku_sale_num")
    @ApiModelProperty(value="商品SKU销售数量",name = "skuSaleNum")
    private BigDecimal skuSaleNum;

    /**
     * sku 原价
     */
    @TableField("sku_cost_price")
    @ApiModelProperty(value="商品SKU原价",name = "skuCostPrice")
    private BigDecimal skuCostPrice;

    /**
     * sku 现价
     */
    @TableField("sku_curr_price")
    @ApiModelProperty(value="商品SKU现在售价",name = "skuCurrPrice")
    private BigDecimal skuCurrPrice;

    /**
     * 商品描述
     */
    @ApiModelProperty(value="商品SKU描述",name = "des")
    private String des;
    /**
     * 状态:1启用,0禁用
     */
    @ApiModelProperty(value="状态:1启用,0禁用",name = "status")
    private Integer status;

    @TableField("create_time")
    @ApiModelProperty(value="创建时间",name = "createTime")
    private Date createTime;

    @TableField("update_time")
    private Date updateTime;

    /**
     * 商品SPU编号，唯一
     */
    @TableField("spu_no")
    @ApiModelProperty(value="商品SKU 编码",name = "spuNo")
    private String spuNo;

    /**
     * 商品名称
     */
    @TableField("goods_name")
    @ApiModelProperty(value="商品名称",name = "goods_name")
    private String goodsName;

    /**
     * 分类id
     */
    @TableField("category_id")
    @ApiModelProperty(value="分类id",name = "categoryId")
    private Long categoryId;

    /**
     * 分类名称
     */
    @TableField("category_name")
    @ApiModelProperty(value="分类名称",name = "categoryName")
    private String categoryName;

    /**
     * 0:普通商品 1:特殊商品
     */
    @ApiModelProperty(value=" 0:普通商品 1:特殊商品",name = "special")
    private Integer special;
    /**
     * 详情上面图片
     */
    @ApiModelProperty(value="商品图片地址",name = "spuImgUrl")
    private String spuImgUrl;

    /**
     * 商品详情
     */
    @TableField("sku_detail")
    @ApiModelProperty(value="商品详情",name = "skuDetail")
    private String skuDetail;

    /**
     * 商品详情图片地址
     */
    @TableField("sku_detail_img_url")
    private String skuDetailImgUrl;

    @ApiModelProperty(value="商品SKU集合 选择规格时直接从sku获取信息即可",name = "skuList")
    private List<TGoodsSkuVo> skuList;

    @ApiModelProperty(value="规格类型集合",name = "specsTypeVoList")
    private List<SpuSpecsTypeVo> specsTypeList;



    /*****************************start 购物车信息*********************/
    /**
     * 购物车ID
     */
    @ApiModelProperty(value="购物车ID",name = "cartId")
    private Integer cartId;
    /**
     * 用户id
     */
    @ApiModelProperty(value="用户ID",name = "userId")
    private Long userId;
    @ApiModelProperty(value="商品ID",name = "skuId")
    private Long skuId;
    /**
     * 购买商品数量
     */
    @ApiModelProperty(value="购买数量",name = "quantity")
    private Integer quantity;
    @ApiModelProperty(value="购物车创建记录时间",name = "cartCreateTime")
    private Date cartCreateTime;
    @ApiModelProperty(value="购物车更新时间",name = "cartUpdateTime")
    private Date cartUpdateTime;
    /*****************************end 购物车信息*********************/
}
