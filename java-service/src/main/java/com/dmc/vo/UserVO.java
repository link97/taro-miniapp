package com.dmc.vo;

import com.baomidou.mybatisplus.annotations.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by sail on 2016/12/25.
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel("用户 VO")
public class UserVO extends TablePage {
    @ApiModelProperty(value="用户ID",name = "id")
    private Long id;
    @ApiModelProperty(value="用户名",name = "username")
    private String username;
    @ApiModelProperty(value="是否给过分享奖励 0:无 1:有",name = "shareUserId")
    private Integer shareRewardsHave;
    @ApiModelProperty(value="是否给过商品奖励 0:无 1:有",name = "shareGoodsRewardsHave")
    private Integer shareGoodsRewardsHave;
    @ApiModelProperty(value="真实名称",name = "name")
    private String name;
    @ApiModelProperty(value="创建时间",name = "createTime")
    private Date createTime;
    @ApiModelProperty(value="修改时间",name = "modifyTime")
    private Date modifyTime;
    @TableField(exist = false)
    @ApiModelProperty(value="用户角色ID",name = "roleIds")
    private List<Long> roleIds;
    @ApiModelProperty(value="用户角色名称",name = "roleNames")
    @TableField(exist = false)
    private List<String> roleNames;
    @ApiModelProperty(value="用户类型 0:商家 1:客户",name = "type")
    private Integer type;
    @ApiModelProperty(value="会员等级 0:初级会员 1:中级会员 2:高级会员",name = "level")
    private Integer level;
    @ApiModelProperty(value="会员荣誉身份级别",name = "vipLevel")
    private Integer vipLevel;
    @ApiModelProperty(value="用户头像",name = "userPhoto")
    private String userPhoto;
    @ApiModelProperty(value="用户手机号",name = "userPhone")
    private String userPhone;
    @ApiModelProperty(value="微信绑定手机号",name = "wxPhone")
    private String wxPhone;
    @ApiModelProperty(value="用户年龄",name = "age")
    private Integer age;
    @ApiModelProperty(value="用户性别 0:男 1:女",name = "sex")
    private Integer sex;
    @ApiModelProperty(value="用户生日",name = "birthday")
    private String birthday;
    @ApiModelProperty(value="用户唯一ID",name = "unionid")
    private Integer unionid;
    @ApiModelProperty(value="用户国家",name = "country")
    private String country;
    @ApiModelProperty(value="用户省份",name = "province")
    private String province;
    @ApiModelProperty(value="用户城市",name = "city")
    private String city;
    @ApiModelProperty(value="是否停用 0:启用 1:停用",name = "stop")
    private Integer stop;
    @ApiModelProperty(value="封号原因",name = "stopDes")
    private String stopDes;
    @ApiModelProperty(value="封号时间",name = "stopTime")
    private Date stopTime;
    @ApiModelProperty(value="开启原因",name = "openDes")
    private String openDes;
    @ApiModelProperty(value="解封原因",name = "openTime")
    private Date openTime;

    @ApiModelProperty(value="小程序登录方式 0:小程序 1:微信绑定手机号 2:其他手机号",name = "loginType")
    private Integer loginType;
    @ApiModelProperty(value="微信code",name = "code")
    private String code;
    @ApiModelProperty(value="微信encryptedData",name = "encryptedData")
    private String encryptedData;
    @ApiModelProperty(value="微信iv",name = "iv")
    private String iv;
    @ApiModelProperty(value="微信用户信息",name = "userInfo")
    private String userInfo;
    @ApiModelProperty(value="第一单分销商品级别",name = "firstLevelFlag")
    private String firstLevelFlag;
    @ApiModelProperty(value="分销商金额购买总额",name = "distributionTotalPrice")
    private BigDecimal distributionTotalPrice;
    @ApiModelProperty(value="业绩奖开关 0:关闭 1:开启",name = "achievementCountBonusStatus")
    private Integer achievementCountBonusStatus;

}
