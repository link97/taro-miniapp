package com.dmc.vo;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 商品SKU表
 * </p>
 *
 * @author dmc
 * @since 2020-09-05
 */
@Data
@ApiModel("商品SKU VO")
public class TGoodsSkuVo extends Model<TGoodsSkuVo> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    @ApiModelProperty(value="商品SKU 主键ID",name = "id")
    private Long id;

    /**
     * spu Id
     */
    @TableField("goods_spu_id")
    @ApiModelProperty(value="商品SPU ID",name = "goodsSpuId")
    private Long goodsSpuId;

    /**
     * 商品sku编号
     */
    @TableField("sku_no")
    @ApiModelProperty(value="SKU编码",name = "skuNo")
    private String skuNo;

    /**
     * 商品sku规格类型 如重量  颜色
     */
    @TableField("sku_title_type")
    @ApiModelProperty(value="商品规格类型",name = "skuTitleType")
    private String skuTitleType;

    /**
     * 商品sku规格
     */
    @TableField("sku_title")
    @ApiModelProperty(value="商品规格",name = "skuTitle")
    private String skuTitle;

    /**
     * 商品封面图片地址
     */
    @TableField("sku_covers_url")
    @ApiModelProperty(value="商品SKU封面图片 多个用;分割",name = "skuCoversUrl")
    private String skuCoversUrl;

    /**
     * 商品图片地址
     */
    @TableField("sku_img_url")
    @ApiModelProperty(value="商品SKU图片 多个用;分割",name = "skuImgUrl")
    private String skuImgUrl;

    /**
     * 商品库存
     */
    @TableField("sku_num")
    @ApiModelProperty(value="商品SKU数量",name = "skuNum")
    private BigDecimal skuNum;

    /**
     * 需要使用积分
     */
    @ApiModelProperty(value="需要使用积分",name = "integral")
    private BigDecimal integral;

    /**
     * 商品销售数量
     */
    @TableField("sku_sale_num")
    @ApiModelProperty(value="商品SKU销售数量",name = "skuSaleNum")
    private BigDecimal skuSaleNum;

    /**
     * sku 原价
     */
    @TableField("sku_cost_price")
    @ApiModelProperty(value="商品SKU原价",name = "skuCostPrice")
    private BigDecimal skuCostPrice;

    /**
     * sku 现价
     */
    @TableField("sku_curr_price")
    @ApiModelProperty(value="商品SKU现在售价",name = "skuCurrPrice")
    private BigDecimal skuCurrPrice;

    /**
     * 删除标识:0:未删除,1删除
     */
    @ApiModelProperty(value="删除标识:0:未删除,1删除",name = "deleted")
    private Integer deleted;

    /**
     * 商品描述
     */
    @ApiModelProperty(value="商品SKU描述",name = "des")
    private String des;

    /**
     * 状态:1启用,0禁用
     */
    @ApiModelProperty(value="状态:1启用,0禁用",name = "status")
    private Integer status;

    @TableField("create_time")
    @ApiModelProperty(value="创建时间",name = "createTime")
    private Date createTime;

    @TableField("update_time")
    private Date updateTime;

    /**
     * 购买商品数量
     */
    private Integer quantity;




    @ApiModelProperty(value="SKU规格类型集合",name = "skuSpecsTypeList")
    private List<SkuSpecsTypeVo> skuSpecsTypeList;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
