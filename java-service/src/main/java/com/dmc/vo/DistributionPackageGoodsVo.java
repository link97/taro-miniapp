package com.dmc.vo;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 分销商品套餐表关联商品表
 * </p>
 *
 * @author 
 * @since 2021-03-17
 */
@Data
@ApiModel("分销商品套餐表关联商品表 VO")
public class DistributionPackageGoodsVo extends Model<DistributionPackageGoodsVo> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value="主键",name = "id")
    private Long id;

    /**
     * 套餐包ID
     */
    @ApiModelProperty(value="套餐包ID",name = "packageId")
    private Long packageId;

    /**
     * 商品ID
     */
    @ApiModelProperty(value="商品ID",name = "goodId")
    private Long goodId;

    /**
     * 商品数量
     */
    @ApiModelProperty(value="商品数量",name = "goodNum")
    private Integer goodNum;

    /**
     * 套餐包描述
     */
    @ApiModelProperty(value="套餐包描述",name = "des")
    private String des;

    /**
     * 删除标示(默认0未删除)
     */
    @ApiModelProperty(value="删除标示(默认0未删除)",name = "deleted")
    private Integer deleted;

    @TableField("create_time")
    private Date createTime;

    @TableField("update_time")
    private Date updateTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
