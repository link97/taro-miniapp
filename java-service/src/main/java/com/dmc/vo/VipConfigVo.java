package com.dmc.vo;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 会员荣誉身份配置
 * </p>
 *
 * @author 
 * @since 2021-03-17
 */
@Data
@ApiModel("会员荣誉身份 VO")
public class VipConfigVo extends Model<VipConfigVo> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value="主键",name = "id")
    private Long id;

    /**
     * 会员荣誉身份名称
     */
    @ApiModelProperty(value="会员荣誉身份名称",name = "vipName")
    private String vipName;

    /**
     * 会员荣誉身份级别
     */
    @ApiModelProperty(value="会员荣誉身份级别",name = "level")
    private Integer level;

    /**
     * 业绩累积代数
     */
    @ApiModelProperty(value="业绩累积代数",name = "generations")
    private Integer generations;



    /**
     * 分销商数量
     */
    @ApiModelProperty(value="分销商数量",name = "distributionUserNum")
    private Integer distributionUserNum;

    /**
     * 分销商会员级别
     */
    @ApiModelProperty(value="分销商会员级别",name = "distributionUserLevel")
    private Integer distributionUserLevel;

    /**
     * 业基金额
     */
    @ApiModelProperty(value="业基金额",name = "achievement")
    private BigDecimal achievement;

    /**
     * 1:停用 0:正常使用
     */
    @ApiModelProperty(value="1:停用 0:正常使用",name = "status")
    private Integer status;

    /**
     * 会员荣誉身份描述
     */
    @ApiModelProperty(value="会员荣誉身份描述",name = "des")
    private String des;

    /**
     * 删除标示(默认0未删除)
     */
    @ApiModelProperty(value="删除标示(默认0未删除)",name = "deleted")
    private Integer deleted;

    @TableField("create_time")
    private Date createTime;

    @TableField("update_time")
    private Date updateTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
