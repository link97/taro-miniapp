package com.dmc.vo;

import com.baomidou.mybatisplus.annotations.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@ApiModel("转让收益 VO")
@Data
public class TransferProfitVo {


    @ApiModelProperty(value="被转让人手机号",name = "userPhone")
    private String userPhone;

    @ApiModelProperty(value="转账金额",name = "transferPrice")
    private BigDecimal transferPrice;
}
