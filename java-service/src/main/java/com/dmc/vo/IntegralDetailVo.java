package com.dmc.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author
 * @date 2020/9/1616:50
 */
@Data
@ApiModel
public class IntegralDetailVo extends TablePage implements Serializable {
    @ApiModelProperty(value="积分记录ID",name = "id")
    private Long id;
    /**
     * 用户ID
     */
    @ApiModelProperty(value="用户ID",name = "userId")
    private Long userId;
    /**
     * 流水类型,1兑换普通商品 2分享商品 3分享好友  4下级分销
     */
    @ApiModelProperty(value="流水类型,1购买普通商品 2分享商品 3分享好友  4下级分销",name = "type")
    private Integer type;

    @ApiModelProperty(value="查询字段,1:积分增加 2:积分减少",name = "subOrAdd")
    private Integer subOrAdd;

    @ApiModelProperty(value="流水类型名称",name = "typeName")
    private String typeName;
    /**
     * 积分
     */
    @ApiModelProperty(value="SKU编码",name = "skuNo")
    private BigDecimal integral;

    /**
     * 产生积分的订单
     */
    @ApiModelProperty(value="订单号",name = "orderId")
    private Long orderId;

    @ApiModelProperty(value="订单编号",name = "orderNo")
    private String orderNo;

    /**
     * 描述
     */
    @ApiModelProperty(value="描述",name = "des")
    private String des;

    @ApiModelProperty(value="创建时间",name = "createTime")
    private Date createTime;

    @ApiModelProperty(value="更新数据时间",name = "updateTime")
    private Date updateTime;

    @ApiModelProperty(value="用户账户总积分",name = "userTotalIntegral")
    private BigDecimal userTotalIntegral;

    @ApiModelProperty(value="用户手机号",name = "userPhone")
    private String userPhone;
    @ApiModelProperty(value="用户昵称",name = "userName")
    private String userName;
    @ApiModelProperty(value="创建开始日期",name = "sDate")
    private String sDate;
    @ApiModelProperty(value="创建结束日期",name = "eDate")
    private String eDate;
    @ApiModelProperty(value="创建开始日期Date",name = "sDateQuery")
    private Date sDateQuery;
    @ApiModelProperty(value="创建结束日期Date",name = "eDateQuery")
    private Date eDateQuery;

    @ApiModelProperty(value="用户账户变更总积分",name = "accountTotalIntegral")
    private BigDecimal accountTotalIntegral;
}
