package com.dmc.vo;

import com.baomidou.mybatisplus.annotations.TableField;
import com.dmc.entity.OrderDetail;
import com.dmc.entity.OrdersLogistics;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @
 * @date 2020/9/617:38
 */
@Data
@ApiModel("商品订单信息")
public class OrdersVo extends TablePage implements Serializable {
    /**
     * 订单ID
     */
    @ApiModelProperty(value="订单ID",name = "ordersId")
    private Integer ordersId;

    /**
     * 用户id
     */
    @ApiModelProperty(value="用户id",name = "userId")
    private Long userId;

    /**
     * 订单号
     */
    @ApiModelProperty(value="订单号",name = "ordersNo")
    private String ordersNo;

    /**
     * 订单生成时间
     */
    @ApiModelProperty(value="订单生成时间",name = "createTime")
    private Date createTime;

    @ApiModelProperty(value="会员商品订单（0:否 1：是）",name = "vipOrder")
    private Integer vipOrder;

    /**
     * 积分商品订单（0:否 1：是）
     */
    @ApiModelProperty(value="积分商品订单（0:否 1：是）",name = "integralOrder")
    private Integer integralOrder;

    /**
     * 收货人姓名
     */
    @ApiModelProperty(value="收货人姓名",name = "receiverName")
    private String receiverName;

    /**
     * 区/县
     */
    @ApiModelProperty(value="区/县",name = "receiverDistrict")
    private String receiverDistrict;

    /**
     * 详细地址
     */
    @ApiModelProperty(value="详细地址",name = "receiverAddress")
    private String receiverAddress;

    /**
     * 收货移动电话
     */
    @ApiModelProperty(value="收货移动电话",name = "receiverMobile")
    private String receiverMobile;

    /**
     * (1、微信支付 2、积分支付 3 扫码支付 4微信混合支付 5扫码混合支付)
     */
    @ApiModelProperty(value="(1、微信支付 2、积分支付 3 扫码支付 6线下现金支付 7线下转账支付 4微信混合支付 5扫码混合支付 8线下现金支付 9线下转账支付)",name = "payWay")
    private Integer payWay;

    /**
     * 订单享受的折扣
     */
    @ApiModelProperty(value="订单享受的折扣",name = "discount_rate")
    private BigDecimal discountRate;

    /**
     * 订单奖励百分比
     */
    @TableField("reword_rate")
    @ApiModelProperty(value="订单奖励百分比",name = "rewordRate")
    private BigDecimal rewordRate;

    /**
     * 商品总金额
     */
    @ApiModelProperty(value="商品总金额",name = "totalAmount")
    private BigDecimal totalAmount;

    /**
     * 商品原总金额
     */
    @ApiModelProperty(value="商品原总金额",name = "originalAmount")
    private BigDecimal originalAmount;

    /**
     * 商品优惠金額
     */
    @ApiModelProperty(value="优惠金額",name = "preferentialAmount")
    private BigDecimal preferentialAmount;

    /**
     * 应付金额
     */
    @ApiModelProperty(value="应付金额",name = "finalAmount")
    private BigDecimal finalAmount;

    /**
     * 使用积分值
     */
    @ApiModelProperty(value="使用积分值",name = "useIntegral")
    private BigDecimal useIntegral;

    /**
     * 使用积分金额
     */
    @ApiModelProperty(value="使用积分金额",name = "intergralAmount")
    private BigDecimal intergralAmount;

    /**
     * 订单状态20000、未付款(含义：等待付款)；20001、付款成功(含义：已经付款)；20002、已经发货；20003、收货确认(含义：已经完成)；20004、交易关闭(含义：取消订单)；20007、异常订单（退换货等） 20005申请退款 20006退款成功
     */
    @ApiModelProperty(value="订单状态",name = "ordersState")
    private Long ordersState;

    @ApiModelProperty(value="订单状态名称",name = "ordersName")
    private String ordersName;

    /**
     * 0是无退货,1是部分退货,2是全部退货
     */
    @ApiModelProperty(value="0是无退货,1是部分退货,2是全部退货",name = "returnState")
    private Integer returnState;

    /**
     * 退款金额
     */
    @ApiModelProperty(value="退款金额",name = "refundAmount")
    private BigDecimal refundAmount;

    /**
     * 退货数量
     */
    @ApiModelProperty(value="退货数量",name = "returnNum")
    private Integer returnNum;

    /**
     * 0是无退款,1是部分退款,2是全部退款
     */
    @ApiModelProperty(value="0是无退款,1是部分退款,2是全部退款",name = "refundState")
    private Integer refundState;

    /**
     * 订单赠送积分
     */
    @ApiModelProperty(value="订单赠送积分",name = "orderPointScount")
    private Integer orderPointScount;

    /**
     * 交易状态（0:未支付 1：已支付）
     */
    @ApiModelProperty(value="交易状态（0:未支付 1：已支付）",name = "tradeState")
    private Integer tradeState;

    /**
     * 物流状态 0 是新建  1是签收   2是拒收
     */
    @ApiModelProperty(value="物流状态",name = "logisticsState")
    private Integer logisticsState;

    /**
     * 删除标示(默认0未删除)
     */
    @ApiModelProperty(value="删除标示(默认0未删除)",name = "deleteFlag")
    private Integer deleteFlag;

    /**
     * 是否有发票（0没有，1有）
     */
    @ApiModelProperty(value="是否有发票（0没有，1有）",name = "isInvoice")
    private Integer isInvoice;
    /**
     * 父订单号
     */
    @TableField("father_orders_no")
    private String fatherOrdersNo;
    /**
     * 是否为父订单（0表示否、1表示是）
     */
    @TableField("father_orders")
    private Integer fatherOrders;
    /**
     * 订单支付单据ID
     */
    @TableField("payment_id")
    private Long paymentId;

    /**
     * 订单退款单据ID
     */
    @TableField("refund_payment_id")
    private Long refundPaymentId;
    /**
     * 0:普通商品 1:特殊商品
     */
    @ApiModelProperty(value=" 0:普通商品 1:特殊商品",name = "special")
    private Integer special;
    @ApiModelProperty(value="商品ID",name = "skuId")
    private Long skuId;
    @ApiModelProperty(value="商品名称",name = "goodsName")
    private String goodsName;

    @ApiModelProperty(value="SKU集合",name = "skuList")
    private List<TGoodsDetailVo> skuList;

    @ApiModelProperty(value="订单明细数据",name = "orderDetailList")
    private List<OrderDetail> orderDetailList;
    @ApiModelProperty(value="下单开始时间",name = "sDate")
    private String sDate;
    @ApiModelProperty(value="下单结束时间",name = "eDate")
    private String eDate;
    @ApiModelProperty(value="订单物流信息",name = "ordersLogisticsList")
    private List<OrdersLogistics> ordersLogisticsList;

    /**
     * 退款积分
     */
    @ApiModelProperty(value="退款积分",name = "refundIntegral")
    private BigDecimal refundIntegral;

    /**
     * 退款申请时间
     */
    @ApiModelProperty(value="退款申请时间",name = "refundTime")
    private Date refundTime;
    /**
     * 退款原因
     */
    @ApiModelProperty(value="退款原因",name = "refundDes")
    private String refundDes;
    /**
     * 退款图片
     */
    @ApiModelProperty(value="退款图片",name = "refundImg")
    private String refundImg;
    /**
     * 订单支付时间
     */
    @ApiModelProperty(value="订单支付时间",name = "payTime")
    private Date payTime;
    /**
     * 退货时间
     */
    @ApiModelProperty(value="退货时间",name = "returnTime")
    private Date returnTime;
    /**
     * 退货原因
     */
    @ApiModelProperty(value="退货原因",name = "returnDes")
    private String  returnDes;
    /**
     * 退货图片
     */
    @ApiModelProperty(value="退货图片",name = "returnImg")
    private String  returnImg;

    /**
     * 订单修改时间
     */
    @TableField("update_time")
    @ApiModelProperty(value="订单修改时间",name = "updateTime")
    private Date updateTime;

    /**
     * 0:直接下单 1:购物车下单
     */
    @ApiModelProperty(value="0:直接下单 1:购物车下单",name = "createOrderType")
    private Integer createOrderType;



}
