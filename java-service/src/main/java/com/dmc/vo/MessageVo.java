package com.dmc.vo;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 消息表
 * </p>
 *
 * @author 
 * @since 2020-09-25
 */
@Data
@ApiModel("站内信 VO")
public class MessageVo{

    private static final long serialVersionUID = 1L;
    /**
     * 消息体
     */
    @ApiModelProperty(value="消息体",name = "message")
    private String message;
    /**
     * 0:未读 1:已读 
     */
    @ApiModelProperty(value="0:未读 1:已读 ",name = "status")
    private Integer status;

    @TableField("create_time")
    @ApiModelProperty(value="创建时间",name = "createTime")
    private Date createTime;

    @TableField("update_time")
    @ApiModelProperty(value="更新时间",name = "updateTime")
    private Date updateTime;

}
