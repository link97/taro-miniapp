package com.dmc.vo;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 分销订单导出表
 * </p>
 *
 * @
 * @since 2020-09-08
 */
@Data
@ApiModel("分销订单表 VO")
public class DistributionOrderExportVo extends BaseRowModel {


    @ExcelProperty( value = "订单编号",index = 0)
    private String orderNo;


    /**
     * 邀请人ID
     */
    @ExcelProperty( value = "下单人姓名",index = 1)
    private String userName;


    /**
     * 邀请人ID
     */
    @ExcelProperty( value = "邀请人姓名",index = 2)
    private String shareUserName;

}
