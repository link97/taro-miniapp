package com.dmc.vo;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 首页-轮播图
 * </p>
 *
 * @
 * @since 2020-09-05
 */
@Data
@ApiModel("首页-轮播图Vo")
public class CarouselInfoVo extends Model<CarouselInfoVo> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value="主键",name = "id")
    private Long id;

    /**
     * 图片地址
     */
    @ApiModelProperty(value="图片地址",name = "imgUrl")
    private String imgUrl;

    /**
     * 链接地址
     */
    @ApiModelProperty(value="链接地址",name = "urlLink")
    private String urlLink;

    /**
     * 标题
     */
    @ApiModelProperty(value="标题",name = "title")
    private String title;

    /**
     * 描述
     */
    @ApiModelProperty(value="描述",name = "remarks")
    private String remarks;

    /**
     * 排序
     */
    @ApiModelProperty(value="排序",name = "sort")
    private Integer sort;

    /**
     * 商品类型 0:无跳转 1:商品 2:分销
     */
    @ApiModelProperty(value="商品类型 0:无跳转 1:商品 2:分销",name = "goodsType")
    private Integer goodsType;

    /**
     * 商品ID
     */
    @ApiModelProperty(value="商品ID",name = "goodsId")
    private Long goodsId;

    @ApiModelProperty(value="VIP商品ID",name = "goodsVipId")
    private Long goodsVipId;


    /**
     * 逻辑删除标记（0：显示；1：隐藏）
     */
    @ApiModelProperty(value="辑删除标记（0：显示；1：隐藏）",name = "delFlag")
    private Integer delFlag;

    @ApiModelProperty(value="创建时间",name = "createTime")
    private Date createTime;

    @ApiModelProperty(value="更新时间",name = "updateTime")
    private Date updateTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
