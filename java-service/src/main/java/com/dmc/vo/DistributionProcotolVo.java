package com.dmc.vo;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 分销协议
 * </p>
 *
 * @
 * @since 2020-09-10
 */
@Data
@ApiModel("分销协议 VO")
public class DistributionProcotolVo extends TablePage implements Serializable  {

    private static final long serialVersionUID = 132435234124213L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 分销协议
     */
    @ApiModelProperty(value="协议内容",name = "procotolContent")
    private String procotolContent;

    /**
     * 状态:1上架,0下架
     */
    @ApiModelProperty(value="状态:1上架,0下架",name = "status")
    private Integer status;

    @ApiModelProperty(value="创建时间",name = "createTime")
    private Date createTime;


}
