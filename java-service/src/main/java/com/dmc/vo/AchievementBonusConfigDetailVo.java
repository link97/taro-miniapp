package com.dmc.vo;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 业绩奖配置详情
 * </p>
 *
 * @author 
 * @since 2021-03-18
 */
@Data
@ApiModel("业绩奖配置详情 VO")
public class AchievementBonusConfigDetailVo extends Model<AchievementBonusConfigDetailVo> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value="主键",name = "id")
    private Long id;

    /**
     * 会员荣誉身份名称
     */
    @ApiModelProperty(value="配置主键",name = "configId")
    private Long configId;

    /**
     * 代数
     */
    @ApiModelProperty(value="代数",name = "generations")
    private Integer generations;

    /**
     * 中级奖金
     */
    @ApiModelProperty(value="中级奖金",name = "zhongJiBonus")
    private BigDecimal zhongJiBonus;

    /**
     * 高级奖金
     */
    @ApiModelProperty(value="高级奖金",name = "gaoJiBonus")
    private BigDecimal gaoJiBonus;

    /**
     * 会员荣誉身份描述
     */
    @ApiModelProperty(value="配置详情描述",name = "des")
    private String des;

    /**
     * 删除标示(默认0未删除)
     */
    @ApiModelProperty(value="删除标示(默认0未删除)",name = "deleted")
    private Integer deleted;

    @TableField("create_time")
    private Date createTime;

    @TableField("update_time")
    private Date updateTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
