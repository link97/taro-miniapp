package com.dmc.vo;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 商品SKU规格表
 * </p>
 *
 * @author 
 * @since 2021-03-22
 */
@Data
@ApiModel("商品SKU规格表 VO")
public class SkuSpecsVo extends Model<SkuSpecsVo> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value="主键",name = "id")
    private Long id;

    /**
     * 规格类型
     */
    @ApiModelProperty(value="规格类型",name = "skuType")
    private String skuType;

    /**
     * 规格名称
     */
    @ApiModelProperty(value="规格名称",name = "skuTitle")
    private String skuTitle;

    /**
     * 商品ID
     */
    @ApiModelProperty(value="商品ID",name = "skuId")
    private Long skuId;

    /**
     * 状态:1上架,0下架
     */
    @ApiModelProperty(value="状态:1上架,0下架",name = "status")
    private Integer status;

    /**
     * 删除标识:0:未删除,1删除
     */
    @ApiModelProperty(value="删除标识:0:未删除,1删除",name = "deleted")
    private Integer deleted;

    @TableField("create_time")
    private Date createTime;

    @TableField("update_time")
    private Date updateTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
