package com.dmc.vo;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import com.baomidou.mybatisplus.annotations.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 互助分红使用记录
 * </p>
 *
 * @
 * @since 2020-09-08
 */
@Data
@ApiModel("互助分红使用记录 VO")
public class DistributionbonusUserRecordVo extends BaseRowModel {

    @ApiModelProperty(value="订单ID",name = "orderId")
    private Long orderId;

    @ApiModelProperty(value="订单编号",name = "orderNo")
    private String orderNo;


    @ApiModelProperty(value="0:分销复投 1:转收益",name = "type")
    private Integer type;


    @ApiModelProperty(value="类型名称",name = "typeName")
    private String typeName;

    @ApiModelProperty(value="分红金额",name = "bonusMoney")
    private BigDecimal bonusMoney;

    @ApiModelProperty(value="复投/转入时间",name = "time")
    private Date time;
    /**
     * 使用分红时,订单的级别
     */
    @ApiModelProperty(value="使用分红时,订单的级别",name = "levelFlag")
    private String levelFlag;

}
