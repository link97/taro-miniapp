package com.dmc.vo;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.dmc.model.DistributionOrder;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 分销关系链
 * </p>
 *
 * @
 * @since 2020-09-08
 */
@Data
@ApiModel("分销关系链PC VO")
public class DistributionShareRelationPcVo extends TablePage implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value="用户ID",name = "userId")
    private Long userId;

    @ApiModelProperty(value="推荐人",name = "shareUserName")
    private String shareUserName;

    @ApiModelProperty(value="昵称",name = "userName")
    private String userName;


    @ApiModelProperty(value="手机号",name = "userPhone")
    private String userPhone;


    @ApiModelProperty(value="被分享人头像",name = "userPhoto")
    private String userPhoto;


    @ApiModelProperty(value="被分享人会员等级",name = "level")
    private Integer level;

    @ApiModelProperty(value="分销等级",name = "shareLevel")
    private Integer shareLevel;


    @ApiModelProperty(value="分销订单数量",name = "distributionOrderNum")
    private Integer distributionOrderNum;

    @ApiModelProperty(value="会员等级名称",name = "levelName")
    private String levelName;


    @ApiModelProperty(value="最新购买时间",name = "lastPayTime")
    private Date lastPayTime;

    @ApiModelProperty(value="创建时间",name = "createTime")
    private Date createTime;

    @ApiModelProperty(value="用户注册时间开始",name = "userCreateTimeStart")
    private Date userCreateTimeStart;

    @ApiModelProperty(value="用户注册时间结束",name = "userCreateTimeEnd")
    private Date userCreateTimeEnd;

    @ApiModelProperty(value="当前用户所有订单",name = "orderList")
    private List<DistributionOrderVo> orderList;

}
