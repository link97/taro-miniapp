package com.dmc.vo;

/**
 * 参数校验 分组class
 */
public class ValidatedGroups {

    public interface Add{
    }

    public interface Delete{
    }

    public interface Update{
    }

    public interface Query{
    }

    /**
     * pay  相关
     */
    public interface QueryPay{

    }

    public interface RefundPay{

    }

    public interface Pay{

    }

    public interface Transfer{

    }
}
