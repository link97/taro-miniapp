package com.dmc.vo;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.dmc.model.DistributionPackageGoods;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 分销商品表
 * </p>
 *
 * @
 * @since 2020-09-08
 */
@Data
@ApiModel("分销商商品 VO")
public class DistributionGoodsVo extends TablePage implements Serializable  {

    private static final long serialVersionUID = 1453243245234L;

    @ApiModelProperty(value="主键ID",name = "id")
    private Long id;

    /**
     * 分销名称
     */
    @ApiModelProperty(value="分销名称",name = "distributionName")
    private String distributionName;

    /**
     * 分销级别标识
     */
    @ApiModelProperty(value="分销级别标识",name = "levelFlag")
    private String levelFlag;

    @ApiModelProperty(value="商品类型 0:分销商品 1:套餐包",name = "goodsType")
    private Integer goodsType;


    @ApiModelProperty(value=" 状态:1上架,0下架",name = "distributionName")
    private Integer status;

    /**
     * 购买消费限制 如必须购买288才能购买初级分销商
     */
    @ApiModelProperty(value="购买消费限制 如必须购买288才能购买初级分销商",name = "buyLimit")
    private BigDecimal buyLimit;

    /**
     * 分销价格
     */
    @ApiModelProperty(value="分销价格",name = "price")
    private BigDecimal price;

    /**
     * 赠送积分
     */
    @ApiModelProperty(value="赠送积分",name = "bonusPoints")
    private BigDecimal bonusPoints;

    /**
     * 赠送分红
     */
    @ApiModelProperty(value="赠送分红",name = "bonusPresentation")
    private BigDecimal bonusPresentation;

    /**
     * 推荐人奖励
     */
    @TableField("share_rewards")
    private BigDecimal shareRewards;


    /**
     * VIP会员推荐人奖励
     */
    @TableField("vip_share_rewards")
    private BigDecimal vipShareRewards;

    /**
     * 晋升所需分红限制
     */
    @ApiModelProperty(value="晋升所需分红限制",name = "promotedPresentationLimit")
    private BigDecimal promotedPresentationLimit;

    /**
     * 晋升可得分红
     */
    @ApiModelProperty(value="分红购买价格",name = "promotedPresentationAdd")
    private BigDecimal promotedPresentationAdd;

    /**
     * 晋升可得收益
     */
    @ApiModelProperty(value="分红购买金额",name = "promotedIncomeAdd")
    private BigDecimal promotedIncomeAdd;

    /**
     * 分销商品描述
     */
    @ApiModelProperty(value="分销商品描述",name = "des")
    private String des;

    @ApiModelProperty(value="创建时间",name = "createTime")
    private Date createTime;

    @ApiModelProperty(value="套餐包包含的分销商商品",name = "packageGoodList")
    private List<DistributionPackageGoods> packageGoodList;
}
