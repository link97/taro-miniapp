package com.dmc.vo;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 商品SKU表
 * </p>
 *
 * @author dmc
 * @since 2020-09-05
 */
@Data
@ApiModel("商品SKU VO")
public class TGoodsListSkuVo extends TablePage implements Serializable   {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    @ApiModelProperty(value="商品SKU 主键ID",name = "skuId")
    private Long skuId;

    /**
     * 商品名称
     */
    @TableField("goods_name")
    @ApiModelProperty(value="商品名称",name = "goods_name")
    private String goodsName;

    /**
     * 分类id
     */
    @TableField("category_id")
    @ApiModelProperty(value="分类id",name = "categoryId")
    private Long categoryId;

    /**
     * 分类名称
     */
    @TableField("category_name")
    @ApiModelProperty(value="分类名称",name = "categoryName")
    private String categoryName;

    /**
     * 商品sku编号
     */
    @TableField("sku_no")
    @ApiModelProperty(value="SKU编码",name = "skuNo")
    private String skuNo;

    /**
     * 商品sku规格类型 如重量  颜色
     */
    @TableField("sku_title_type")
    @ApiModelProperty(value="商品规格类型",name = "skuTitleType")
    private String skuTitleType;

    /**
     * 商品sku规格
     */
    @TableField("sku_title")
    @ApiModelProperty(value="商品规格",name = "skuTitle")
    private String skuTitle;

    /**
     * 商品封面图片地址
     */
    @TableField("sku_covers_url")
    @ApiModelProperty(value="商品SKU封面图片 多个用;分割",name = "skuCoversUrl")
    private String skuCoversUrl;

    /**
     * 积分商品的积分价格
     */
    @ApiModelProperty(value="分商品的积分价格",name = "integral")
    private BigDecimal integral;

    /**
     *0:普通商品 1:积分商品
     */
    @ApiModelProperty(value="0:普通商品 1:积分商品",name = "skuPrice")
    private Integer integralStatus;


    /**
     * 商品库存
     */
    @TableField("sku_num")
    @ApiModelProperty(value="商品SKU数量",name = "skuNum")
    private BigDecimal skuNum;

    /**
     * 商品销售数量
     */
    @TableField("sku_sale_num")
    @ApiModelProperty(value="商品SKU销售数量",name = "skuSaleNum")
    private BigDecimal skuSaleNum;

    /**
     * sku 原价
     */
    @TableField("sku_cost_price")
    @ApiModelProperty(value="商品SKU原价",name = "skuCostPrice")
    private BigDecimal skuCostPrice;

    /**
     * sku 现价
     */
    @TableField("sku_curr_price")
    @ApiModelProperty(value="商品SKU现在售价",name = "skuCurrPrice")
    private BigDecimal skuCurrPrice;

    /**
     * 商品描述
     */
    @ApiModelProperty(value="商品SKU描述",name = "des")
    private String des;

    /**
     * 状态:1启用,0禁用
     */
    @ApiModelProperty(value="状态:1启用,0禁用",name = "status")
    private Integer status;

    /**
     * 状态:1启用,0禁用
     */
    @ApiModelProperty(value="0普通1:推荐商品",name = "recommend")
    private Integer recommend;

    /**
     * 0:普通商品 1:会员商品
     */
    @ApiModelProperty(value="0:普通商品 1:会员商品",name = "vip")
    private Integer vip;

    @TableField("create_time")
    @ApiModelProperty(value="创建时间",name = "createTime")
    private Date createTime;

    @TableField("update_time")
    private Date updateTime;
}
