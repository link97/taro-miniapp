/*
 * Copyright (c) 2016 xiaomaihd and/or its affiliates.All Rights Reserved.
 *            http://www.xiaomaihd.com
 */
package com.dmc.conf;

import com.dmc.jwt.JsonWebTokenSecurityConfig;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

/**
 * Created by YangFan on 2016/11/28 上午10:30.
 * <p/>
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends JsonWebTokenSecurityConfig {


    public static String stopExUrls = "{/user/login},{/user/wx_login},{/goods/getCategoryList},{/goods/getGoodsDetail},{/indexProducts/getCarousel},{/distribution/getDistributionGoodsList}";
    @Override
    protected void setupAuthorization(HttpSecurity http) throws Exception {
        http.authorizeRequests()

                // allow anonymous access to /user/login endpoint
                .antMatchers("/user/login").permitAll()
                .antMatchers("/user/wx_login").permitAll()
                .antMatchers("/swagger/**").permitAll()
                .antMatchers("/swagger-ui.html/**").permitAll()
                .antMatchers("/doc.html").permitAll()
                .antMatchers("/webjars/**").permitAll()
                .antMatchers("/swagger-resources/**").permitAll()
                .antMatchers("/v2/**").permitAll()
                .antMatchers("/goods/getMallGoodsList").permitAll()
                .antMatchers("/goods/getCategoryList").permitAll()
                .antMatchers("/goods/getGoodsDetail").permitAll()
                .antMatchers("/indexProducts/getCarousel").permitAll()
                .antMatchers("/distribution/getDistributionGoodsList").permitAll()
                .antMatchers("/pay/wechat/callback").permitAll()
                .antMatchers("/pay/alipay/callback").permitAll()
                .antMatchers("/pay/wechat/refundCallback").permitAll()
                .antMatchers("/webjars/springfox-swagger-ui/**").permitAll()
                .antMatchers("/web/**").permitAll()
                .antMatchers("/").permitAll()
                // authenticate all other requests
                .anyRequest().authenticated();
    }
}
