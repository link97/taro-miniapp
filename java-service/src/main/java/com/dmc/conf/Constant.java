package com.dmc.conf;

public class Constant {

    public static final Long CLIENT_ROLE_ID = 0L;

    public static final String CHU_JI_DISTRIBUTION_FLAG = "CHU_JI";

    //中级分销商
    public static final String ZHONG_JI_DISTRIBUTION_FLAG = "ZHONG_JI";
    //高级分销商
    public static final String GAO_JI_DISTRIBUTION_FLAG = "GAO_JI";
    //超高级分销商
    public static final String CHAO_GAO_JI_DISTRIBUTION_FLAG = "CHAO_GAO_JI";

    /**
     * 配置表 成为普通会员限制key
     */
    public static final String CONFIG_KEY_FENXIAO_LIMIT = "FENXIAO_LIMIT";
    /**
     * 配置表 开启线上提现开关key
     */
    public static final String CONFIG_KEY_TI_XIAN_SWITH = "TI_XIAN_SWITH";
    /**
     * 配置表 大额提现限制key
     */
    public static final String CONFIG_KEY_TI_XIAN_LIMIT = "TI_XIAN_LIMIT";
    /**
     * 配置表 库存提醒限制
     */
    public static final String CONFIG_KEY_KU_CUN_TI_XING = "KU_CUN_TI_XING";
    /**
     * 配置表 提现比例key
     */
    public static final String CONFIG_KEY_CASH_OUT_RATE = "CASH_OUT_RATE";
    /**
     * 配置表 分销协议配置key
     */
    public static final String CONFIG_KEY_CASH_OUT_PROCOTOL = "CASH_OUT_PROCOTOL";
    /**
     * 配置表 推荐人商品收益比例第一次key
     */
    public static final String CONFIG_KEY_GOOS_REWORD_RATE = "GOOS_REWORD_RATE_ONE";

    /**
     * 配置表 推荐人商品收益比例key
     */
    public static final String CONFIG_KEY_GOOS_REWORD_RATE_OTHER = "GOOS_REWORD_RATE_OTHER";

    /**
     * 配置表 商品折扣比例key discount
     */
    public static final String CONFIG_KEY_GOOS_DISCOUNT_RATE = "GOOS_DISCOUNT_RATE";

    /**
     * 配置表 支付方式key VALUE 以,分割
     */
    public static final String CONFIG_KEY_PAY_MODES = "PAY_MODES";

    /**
     * 配置表 分销升级插队人数限制
     */
    public static final String CONFIG_KEY_DISTRIBUTION_UPGRADE_LIMIT = "DISTRIBUTION_UPGRADE_LIMIT";

    /**
     * 配置表 积分返现配置
     */
    public static final String CONFIG_KEY_JI_FEN_LIMIT = "JI_FEN_LIMIT";

    /**
     * 规格分割符号
     */
    public static final String SPECS_SEPARATOR= ";";

}
