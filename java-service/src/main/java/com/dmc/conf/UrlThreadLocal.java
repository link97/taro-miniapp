package com.dmc.conf;

import java.util.Map;

/**
 * url全局调用
 */
public class UrlThreadLocal {
    private static InheritableThreadLocal<String> urlLocal;

    static {
        urlLocal = new InheritableThreadLocal<String>();
    }

    public static void setUrl(String url) {
        urlLocal.set(url);
    }

    public static void removeUrl() {
        urlLocal.remove();
    }

    public static String getuRL() {
        return urlLocal.get();
    }
}
