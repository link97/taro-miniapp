package com.dmc.conf;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
@Data
public class PayCallBackConfig {

    @Value("${payCallBack.wechat}")
    private String wechat;

    @Value("${payCallBack.aliPay}")
    private String aliPay;

    @Value("${payCallBack.unionPay}")
    private String unionPay;

    @Value("${payCallBack.wechatRefund}")
    private String wechatRefund;

    @Value("${payCallBack.aliPayRefund}")
    private String aliPayRefund;

    @Value("${payCallBack.unionPayRefund}")
    private String unionPayRefund;


}
