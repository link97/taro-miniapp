/*
 * Copyright (c) 2016 xiaomaihd and/or its affiliates.All Rights Reserved.
 *            http://www.xiaomaihd.com
 */
package com.dmc.security;

import cn.hutool.core.collection.CollectionUtil;
import com.dmc.conf.UrlThreadLocal;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDecisionManager;
import org.springframework.security.access.SecurityMetadataSource;
import org.springframework.security.access.intercept.AbstractSecurityInterceptor;
import org.springframework.security.access.intercept.InterceptorStatusToken;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.web.FilterInvocation;
import org.springframework.security.web.access.intercept.FilterInvocationSecurityMetadataSource;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

/**
 * Created by YangFan on 2016/11/28 上午11:32.
 * <p/>
 */
@Component
@Slf4j
public class DMCSecurityInterceptor extends AbstractSecurityInterceptor
        implements Filter {


    @Autowired
    private FilterInvocationSecurityMetadataSource securityMetadataSource;

    @Autowired
    @Override
    public void setAccessDecisionManager(AccessDecisionManager accessDecisionManager) {
        super.setAccessDecisionManager(accessDecisionManager);
    }

    @Autowired
    @Override
    public void setAuthenticationManager(AuthenticationManager authenticationManager) {
        super.setAuthenticationManager(authenticationManager);
    }


    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest requestHttp = (HttpServletRequest)request;

        Map<String, String[]> parameterMap = requestHttp.getParameterMap();
        StringBuffer para = new StringBuffer();
        if(CollectionUtil.isNotEmpty(parameterMap)){
            for (String s : parameterMap.keySet()) {
                para.append(s).append(":");
                String[] strings = parameterMap.get(s);
                if(strings!=null && strings.length>0){
                    for (String string : strings) {
                        para.append(string).append("-");
                    }
                }
            }
        }
        log.info("接口地址 :{}",requestHttp.getRequestURI()+"&"+para.toString());
        UrlThreadLocal.setUrl(requestHttp.getRequestURI());
        FilterInvocation fi = new FilterInvocation(request, response, chain);
        invoke(fi);
        UrlThreadLocal.removeUrl();
    }


    public Class<? extends Object> getSecureObjectClass() {
        return FilterInvocation.class;
    }


    public void invoke(FilterInvocation fi) throws IOException, ServletException {

        InterceptorStatusToken token = super.beforeInvocation(fi);

        try {
            fi.getChain().doFilter(fi.getRequest(), fi.getResponse());
        } finally {
            super.afterInvocation(token, null);
        }

    }


    @Override
    public SecurityMetadataSource obtainSecurityMetadataSource() {
        return this.securityMetadataSource;
    }




    public void destroy() {

    }

    public void init(FilterConfig filterconfig) throws ServletException {

    }




}
