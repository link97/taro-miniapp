/*
 * Copyright (c) 2016 xiaomaihd and/or its affiliates.All Rights Reserved.
 *            http://www.xiaomaihd.com
 */
package com.dmc.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * Created by YangFan on 2016/11/28 上午10:53.
 * <p/>
 */
@Data
@ApiModel("登录用户信息返回体")
public class AuthTokenDTO {
    @ApiModelProperty(value="token",name = "token")
    private String token;
    @ApiModelProperty(value="用户ID",name = "userId")
    private Long userId;
    @ApiModelProperty(value="用户权限资源ID集合",name = "resourceList")
    private List<String> resourceList;
    @ApiModelProperty(value="用户名",name = "token")
    private String name;
    @ApiModelProperty(value="openId",name = "openId")
    private String openId;
    @ApiModelProperty(value="sessionKey",name = "sessionKey")
    private String sessionKey;
    @ApiModelProperty(value="用户手机号",name = "phone")
    private String phone;
}
