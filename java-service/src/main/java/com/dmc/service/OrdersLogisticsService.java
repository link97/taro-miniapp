package com.dmc.service;

import com.dmc.entity.OrdersLogistics;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 订单物流表 服务类
 * </p>
 *
 * @
 * @since 2020-09-12
 */
public interface OrdersLogisticsService extends IService<OrdersLogistics> {

}
