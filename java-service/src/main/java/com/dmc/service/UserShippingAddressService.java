package com.dmc.service;

import com.baomidou.mybatisplus.service.IService;
import com.dmc.entity.UserShippingAddress;

/**
 * <p>
 * 我的收货地址 服务类
 * </p>
 *
 * @author dmc
 * @since 2020-09-04
 */
public interface UserShippingAddressService extends IService<UserShippingAddress> {

}
