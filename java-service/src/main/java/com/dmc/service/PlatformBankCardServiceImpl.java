package com.dmc.service;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import cn.hutool.core.lang.Assert;
import com.dmc.entity.BankInfo;
import com.dmc.mapper.BankInfoMapper;
import com.dmc.mapper.PlatformBankCardMapper;
import com.dmc.model.PlatformBankCard;
import com.dmc.vo.PlatformBankCardVo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 银行信息 服务实现类
 * </p>
 *
 * @
 * @since 2020-09-05
 */
@Service
@Transactional
public class PlatformBankCardServiceImpl implements PlatformBankCardService {

    @Resource
    private PlatformBankCardMapper platformBankCardMapper;

    @Resource
    private BankInfoMapper bankInfoMapper;



    @Override
    public List<PlatformBankCardVo> getBankList(PlatformBankCardVo vo) {
        String bankName = vo.getBankName();
        if(StringUtils.isNotBlank(bankName)){
            vo.setBankName("%"+bankName.trim()+"%");
        }
        String bankNum = vo.getBankNum();
        if(StringUtils.isNotBlank(bankNum)){
            vo.setBankNum("%"+bankNum.trim()+"%");
        }
        String bankUserName = vo.getBankUserName();
        if(StringUtils.isNotBlank(bankUserName)){
            vo.setBankUserName("%"+bankUserName.trim()+"%");
        }
        return platformBankCardMapper.getBankList(vo);
    }

    @Override
    public void saveOrUpdate(PlatformBankCardVo vo) {
        Integer bankId = vo.getBankId();
        Assert.notNull(bankId,"参数错误,请选择银行");
        BankInfo bankInfo = bankInfoMapper.selectById(bankId);
        Assert.notNull(bankId,"参数错误,请选择银行");
        vo.setBankName(bankInfo.getBankName());
        Integer stop = vo.getStop();
        stop = stop == null?0:stop;
        vo.setStop(stop);
        Integer id = vo.getId();
        PlatformBankCard card = new PlatformBankCard();
        card.setStop(0);
        PlatformBankCard platformBankCard = platformBankCardMapper.selectOne(card);
        if(id == null){
            if(platformBankCard!=null){
                Assert.isTrue(vo.getStop() == 1,"添加失败,已经有收款账户");
            }
            platformBankCard = new PlatformBankCard();
            vo.setCreateTime(new Date());
            BeanUtil.copyProperties(vo,platformBankCard);
            platformBankCardMapper.insert(platformBankCard);
        }else{
            if(platformBankCard!=null && stop==0){
                Assert.isTrue(platformBankCard.getId().equals(id) ,"修改失败,已经有收款账户");
            }
            platformBankCard = platformBankCardMapper.selectById(id);
            BeanUtil.copyProperties(vo,platformBankCard, CopyOptions.create().setIgnoreNullValue(true));
            platformBankCardMapper.updateById(platformBankCard);
        }
    }

    @Override
    public void editStop(PlatformBankCardVo vo) {
        Integer id = vo.getId();
        Assert.notNull(id,"参数错误,请传输ID");
        Integer stop = vo.getStop();
        Assert.notNull(stop,"参数错误,请传输状态值");
        if(stop == 0){
            PlatformBankCard card = new PlatformBankCard();
            card.setStop(0);
            PlatformBankCard platformBankCard = platformBankCardMapper.selectOne(card);
            if(platformBankCard!=null){
                Assert.isTrue(vo.getStop() == 0 && platformBankCard.getId().equals(id) ,"修改失败,已经有收款账户");
            }
        }
        PlatformBankCard platformBankCard = platformBankCardMapper.selectById(id);
        platformBankCard.setStop(stop);
        platformBankCardMapper.updateById(platformBankCard);
    }

    @Override
    public void delete(PlatformBankCardVo vo) {
        Integer id = vo.getId();
        Assert.notNull(id,"参数错误,请传输ID");
        platformBankCardMapper.deleteById(id);
    }
}
