package com.dmc.service;

import java.io.InputStream;

/**
 * 阿里云服务
 */
public interface AliyunOssService {

    /**
     * 上传文件
     * @param fileName 文件名称
     * @param filePath 文件路径
     * @param inputStream 文件输入流
     * @return
     */
    public String uploadFile(String fileName, String filePath, InputStream inputStream);

    /**
     * 上传文件
     * @param fileName 文件名称
     * @param inputStream 文件输入流
     * @return
     */
    public String uploadFile(String fileName, InputStream inputStream);

}
