package com.dmc.service;

import com.dmc.vo.PlatformBankCardVo;

import java.util.List;

/**
 * <p>
 * 平台银行信息 服务类
 * </p>
 * @
 * @since 2020-09-05
 */
public interface PlatformBankCardService{

    /**
     * 获取平台银行卡信息
     * @param vo
     * @return
     */
    List<PlatformBankCardVo> getBankList(PlatformBankCardVo vo);

    /**
     * 添加或者编辑平台银行卡信息
     * @param vo
     */
    void saveOrUpdate(PlatformBankCardVo vo);

    /**
     * 设置收款或者非收款
     * @param vo
     */
    void editStop(PlatformBankCardVo vo);

    /**
     * 删除平台银行卡信息
     * @param vo
     */
    void delete(PlatformBankCardVo vo);
}
