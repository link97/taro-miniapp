package com.dmc.service;

import com.dmc.entity.UserBankCard;
import com.baomidou.mybatisplus.service.IService;
import com.dmc.vo.BankCardVo;

import java.util.List;

/**
 * <p>
 * 我的银行卡 服务类
 * </p>
 *
 * @author dmc
 * @since 2020-09-04
 */
public interface UserBankCardService extends IService<UserBankCard> {

    /**
     * 添加我的银行卡
     * @param bankCardVo
     * @return
     */
    boolean addBankCard(BankCardVo bankCardVo);

    /**
     * 获取我的银行卡列表
     * @param bankCardVo
     * @return 银行卡列表
     */
    List<BankCardVo> getBankCardList(BankCardVo bankCardVo);
}
