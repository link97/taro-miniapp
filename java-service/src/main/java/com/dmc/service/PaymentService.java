package com.dmc.service;

import com.dmc.vo.*;

/**
 * <p>
 * 提现
 * </p>
 *
 * @
 * @since 2020-09-05
 */
public interface PaymentService {


    /**
     * 获取概况信息
     * @return
     */
    StatInfoVo getStatInfo();

    /**
     * 获取交易明细
     * @param vo
     * @return
     */
    DataTable<PaymentVo> getTransactionDetailList(PaymentVo vo);
    /**
     * 获取收益记录
     * @param vo
     * @return
     */
    DataTable<BenefitTransactionVo> getBenefitTransaction(BenefitTransactionVo vo);
}
