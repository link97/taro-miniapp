package com.dmc.service;

import com.baomidou.mybatisplus.service.IService;
import com.dmc.entity.BankInfo;
import com.dmc.vo.CashOutOrdersVo;
import com.dmc.vo.DataTable;

/**
 * <p>
 * 提现
 * </p>
 *
 * @
 * @since 2020-09-05
 */
public interface CashOutOrderService{
    /**
     * 添加提现订单
     * @param vo
     */
    CashOutOrdersVo addCashOutOrder(CashOutOrdersVo vo);
    /**
     * 修改提现订单
     * @param vo
     */
    void updateCashOutOrder(CashOutOrdersVo vo);
    /**
     * 获取提现订单列表
     * @param vo
     * @return
     */
    DataTable<CashOutOrdersVo> getCashOutOrderList(CashOutOrdersVo vo);
}
