package com.dmc.service;

import com.dmc.vo.CashOutOrdersVo;
import com.dmc.vo.DataTable;
import com.dmc.vo.OrderReceiptVo;

/**
 * <p>
 * 提现
 * </p>
 *
 * @
 * @since 2020-09-05
 */
public interface OrderReceiptService {

    /**
     * 添加发票
     * @param vo
     */
    OrderReceiptVo addOrderReceipt(OrderReceiptVo vo);
    /**
     * 修改发票
     * @param vo
     */
    void updateOrderReceipt(OrderReceiptVo vo);
    /**
     * 获取发票列表
     * @param vo
     * @return
     */
    DataTable<OrderReceiptVo> getOrderReceiptList(OrderReceiptVo vo);

    /**
     * 获取订单发票
     * @param vo
     * @return
     */
    OrderReceiptVo getOrderReceiptByOrderId(OrderReceiptVo vo);
}
