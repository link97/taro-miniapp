package com.dmc.service;

import com.dmc.entity.Message;
import com.dmc.model.BenefitTransaction;
import com.dmc.model.IntegralTransaction;
import com.dmc.model.TUserAccount;
import com.dmc.vo.DataTable;
import com.dmc.vo.IntegralDetailVo;
import java.math.BigDecimal;
import java.util.List;

/**
 * 用户账户Service
 *
 * @author yangfan
 */
public interface UserAccountService {

    /**
     * 保存用户账户信息
     * @param userAccount
     * @return
     */
    void save(TUserAccount userAccount);

    /**
     * 初始化账户信息
     * @param userId
     * @return
     */
    TUserAccount initUserAccountByUserId(Long userId);

    /**
     * 更新账户信息,如积分数值等
     * @param userId
     * @param integral
     */
    public void updateUserAccount(Long userId, BigDecimal integral);

    /**
     * 获取用户使用积分记录
     * @param userId
     * @return
     */
    public List<IntegralTransaction> getUserIntegralList(Long userId);

    /**
     * 获取用户使用积分记录明细
     * @param vo
     * @return
     */
    public DataTable<IntegralDetailVo> getUserIntegralDetailList(IntegralDetailVo vo);

    /**
     * 我的消息
     * @param userId
     * @return
     */
    public List<Message> getUserMessage(Long userId);

    /**
     * 添加用户消息,0:订单发货 1:分销商升级添加消息
     * @param message
     * @return
     */
    public Integer addUserMessage(Message message);

    /**
     * 添加收益流水
     * @param benefitTransaction
     * @return
     */
    public void addBenefitTransaction(BenefitTransaction benefitTransaction);
    /**
     * 验证支付密码
     * @param userId
     * @param password
     */
    void checkPayPassword(Long userId, String password);
    /**
     * 新增支付密码
     * @param userId
     * @param password
     */
    void addPayPassword(Long userId, String password);
    /**
     * 修改支付密码
     * @param userId
     * @param password
     */
    void updatePayPassword(Long userId, String password, String passwordOld);
    /**
     * 校验是否有支付密码
     * @param userId
     */
    Boolean checkHavePayPassword(Long userId);

    /**
     * 添加积分流水
     * @param integralTransaction
     * @return
     */
    public void addIntegralTransaction(IntegralTransaction integralTransaction);

    /**
     * 积分返现的处理
     * @param userIntegral 使用的积分
     * @param userId
     * @param configValue 规则参数
     * @return
     */
    public void integralReturn(BigDecimal userIntegral,Long userId,String configValue);
    /**
     * 后台修改支付密码
     * @param userId
     * @param payPassWord
     * @return
     */
    void updatePayPasswordForPc(Long userId, String payPassWord);
}
