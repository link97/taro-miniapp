package com.dmc.service;

import com.dmc.vo.*;

import java.math.BigDecimal;

/**
 * 商品服务
 */
public interface GoodsService {

    /**
     * 添加商品品类
     * @param vo
     */
    void addCategory(TGoodsCategoryVo vo);

    /**
     * 删除品类
     * @param vo
     */
    void updateCategory(TGoodsCategoryVo vo);

    /**
     * 获取品类列表
     * @param vo
     * @return
     */
    DataTable<TGoodsCategoryVo> getCategoryList(TGoodsCategoryVo vo);
    /**
     * 添加商品
     * @param vo
     * @return
     */
    void saveOrUpdateGoods(TGoodsSpuVo vo);
    /**
     * 获取小程序商城列表以及搜索
     * @param vo
     * @return
     */
    DataTable<TGoodsListSkuVo> getMallGoodsList(TGoodsListSkuVo vo);

    /*
     *获取商品详情
     */
    TGoodsDetailVo getGoodsDetail(Long skuId,Long spuId,String skuTitle);
    /*
     *获取后台商品列表
     */
    DataTable<TGoodsSpuVo> getGoodsList(TGoodsSpuVo vo);

    /**
     * 检查购买数量
     * @param skuId
     * @param skuNum
     * @return
     */
    public String checkAddSkuNum(Long skuId, BigDecimal skuNum);
    /*
     *获取后台商品详情
     */
    TGoodsDetailVo getGoodsDetailForPc(Long spuId);

    /**
     * 删除品类
     * @param id
     */
    void deleteCategory(Long id);
    /**
     * 删除商品
     * @param id
     */
    void deleteGoods(Long id);

    /**
     * 校验商品是否超初购买限制
     * @param spuId
     */
    Boolean checkPayNumLimit(Long spuId,Integer num);

    /**
     * 多规格后之前分销数据的处理
     */
    void oldGoodsHandle();
}
