package com.dmc.service;

import com.dmc.entity.BankInfo;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 银行信息 服务类
 * </p>
 *
 * @
 * @since 2020-09-05
 */
public interface BankInfoService extends IService<BankInfo> {

}
