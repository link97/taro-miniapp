package com.dmc.service.impl;

import com.dmc.entity.Logistics;
import com.dmc.mapper.LogisticsMapper;
import com.dmc.service.LogisticsService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 * 物流表 服务实现类
 * </p>
 *
 * @
 * @since 2020-09-12
 */
@Service
@Transactional
public class LogisticsServiceImpl extends ServiceImpl<LogisticsMapper, Logistics> implements LogisticsService {

}
