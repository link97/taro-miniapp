package com.dmc.service.impl;

import com.dmc.entity.CarouselInfo;
import com.dmc.mapper.CarouselInfoMapper;
import com.dmc.service.CarouselInfoService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 * 首页-轮播图 服务实现类
 * </p>
 *
 * @
 * @since 2020-09-05
 */
@Service
@Transactional
public class CarouselInfoServiceImpl extends ServiceImpl<CarouselInfoMapper, CarouselInfo> implements CarouselInfoService {

}
