package com.dmc.service.impl;

import com.dmc.entity.OrdersLogistics;
import com.dmc.mapper.OrdersLogisticsMapper;
import com.dmc.service.OrdersLogisticsService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 * 订单物流表 服务实现类
 * </p>
 *
 * @
 * @since 2020-09-12
 */
@Service
@Transactional
public class OrdersLogisticsServiceImpl extends ServiceImpl<OrdersLogisticsMapper, OrdersLogistics> implements OrdersLogisticsService {

}
