package com.dmc.service.impl;

import com.dmc.entity.UserBankCard;
import com.dmc.mapper.UserBankCardMapper;
import com.dmc.mapper.UserMapper;
import com.dmc.model.RestResp;
import com.dmc.service.UserBankCardService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.dmc.service.UserService;
import com.dmc.vo.BankCardVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 我的银行卡 服务实现类
 * </p>
 *
 * @
 * @since 2020-09-05
 */
@Service
@Transactional
public class UserBankCardServiceImpl extends ServiceImpl<UserBankCardMapper, UserBankCard> implements UserBankCardService {

    @Resource
    private UserService userService;

    @Autowired
    private UserBankCardMapper userBankCardMapper;
    /**
     * 添加我的银行卡
     * @param bankCardVo
     * @return
     */
    @Override
    public boolean addBankCard(BankCardVo bankCardVo) {
        if(bankCardVo!=null&&bankCardVo.getVerifyCode()!=null){
            //比较验证码,验证成功添加数据
            String codeRespone = userService.checkMsgCode(bankCardVo.getCardMobile(),
                    String.valueOf(bankCardVo.getVerifyCode()),1);
            if(codeRespone!=null) {
                UserBankCard userBankCard=new UserBankCard();
                BeanUtils.copyProperties(bankCardVo, userBankCard);
                boolean result=this.insert(userBankCard);
                return result;
            }
        }
        return false;
    }
    /**
     * 获取我的银行卡列表
     * @param bankCardVo
     * @return 银行卡列表
     */
    @Override
    public List<BankCardVo> getBankCardList(BankCardVo bankCardVo) {
        return userBankCardMapper.getBankCardList(bankCardVo);
    }
}
