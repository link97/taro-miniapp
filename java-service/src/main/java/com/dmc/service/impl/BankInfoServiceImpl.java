package com.dmc.service.impl;

import com.dmc.entity.BankInfo;
import com.dmc.mapper.BankInfoMapper;
import com.dmc.service.BankInfoService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 * 银行信息 服务实现类
 * </p>
 *
 * @
 * @since 2020-09-05
 */
@Service
@Transactional
public class BankInfoServiceImpl extends ServiceImpl<BankInfoMapper, BankInfo> implements BankInfoService {

}
