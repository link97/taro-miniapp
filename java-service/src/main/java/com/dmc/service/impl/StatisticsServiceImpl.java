package com.dmc.service.impl;

import com.dmc.dict.OrderStatusDict;
import com.dmc.entity.Orders;
import com.dmc.mapper.OrdersMapper;
import com.dmc.service.StatisticsService;
import com.dmc.vo.OverviewVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @date 2020/9/1312:36
 */

@Service
@Transactional
public class StatisticsServiceImpl implements StatisticsService {
    @Autowired
    private OrdersMapper ordersMapper;
    /**
     * 统计概况
     * @param sDate
     * @param eDate
     * @return
     */
    @Override
    public OverviewVo getOverview(String sDate, String eDate) {
        OverviewVo overviewVo=new OverviewVo();
        List<Long> orderStatusList = new ArrayList<>();
        orderStatusList.add(OrderStatusDict.ORDER_STATUS_PAIED.getCn());
        Integer paymentOrderNum=ordersMapper.getTotalUserOrder(null, orderStatusList);
        overviewVo.setPaymentOrderNum(paymentOrderNum);
        Orders orders= ordersMapper.getSumOrderAmount(OrderStatusDict.ORDER_STATUS_PAIED.getCn());
        if(orders!=null){
            overviewVo.setPaymentOrderAmount(orders.getFinalAmount());
        }
        //分销商付款订单
        overviewVo.setPaymentDistributorNum(0);
        //分销商付款金额
        overviewVo.setPaymentDistributorAmount(BigDecimal.ZERO);
        //待处理订单
        overviewVo.setPendingOrdersNum(0);
        //未上架商品
        overviewVo.setNotOnTheShelvesNum(0);
        return overviewVo;
    }
}
