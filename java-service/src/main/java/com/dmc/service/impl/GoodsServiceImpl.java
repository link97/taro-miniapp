package com.dmc.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.lang.Assert;
import com.dmc.conf.Constant;
import com.dmc.mapper.*;
import com.dmc.model.*;
import com.dmc.service.GoodsService;
import com.dmc.util.SessionUtil;
import com.dmc.util.id.IdUtil;
import com.dmc.util.util.StringUtil;
import com.dmc.vo.*;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import javafx.scene.input.InputMethodTextRun;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;

@Service
@Transactional
public class GoodsServiceImpl implements GoodsService {

    @Autowired
    private TGoodsCategoryMapper tGoodsCategoryMapper;

    @Autowired
    private TGoodsSpuMapper tGoodsSpuMapper;

    @Autowired
    private TGoodsSkuMapper tGoodsSkuMapper;


    @Autowired
    private SkuSpecsMapper skuSpecsMapper;

    @Autowired
    private SkuSpecsTypeMapper skuSpecsTypeMapper;


    @Autowired
    private SpuSpecsMapper spuSpecsMapper;

    @Autowired
    private SpuSpecsTypeMapper spuSpecsTypeMapper;

    @Autowired
    private OrderDetailMapper orderDetailMapper;

    @Autowired
    private UserSpuPayNumMapper userSpuPayNumMapper;

    @Override
    public void addCategory(TGoodsCategoryVo vo) {
        //判断当前名称是否有
        String categoryName = vo.getCategoryName();
        Map<String,Object> param = new HashMap<>();
        param.put("category_name",categoryName);
        List<TGoodsCategory> tGoodsCategories = tGoodsCategoryMapper.selectByMap(param);
        Assert.isTrue(CollectionUtil.isEmpty(tGoodsCategories),"无法添加,已经含有此品类");
        TGoodsCategory ca = new TGoodsCategory();
        BeanUtil.copyProperties(vo,ca);
        ca.setCreateTime(new Date());
        ca.setPid(0L);
        ca.setStop(0);
        tGoodsCategoryMapper.insert(ca);
    }

    @Override
    public void updateCategory(TGoodsCategoryVo vo) {
        Long id = vo.getId();
        TGoodsCategory tGoodsCategory = tGoodsCategoryMapper.selectById(id);
        Assert.notNull(tGoodsCategory,"参数错误,无此品类");
        BeanUtil.copyProperties(vo,tGoodsCategory, CopyOptions.create().setIgnoreNullValue(true));
        tGoodsCategoryMapper.updateById(tGoodsCategory);
    }

    @Override
    public DataTable<TGoodsCategoryVo> getCategoryList(TGoodsCategoryVo vo) {
        Integer start = vo.getStart();
        if(start == null){
            vo.setStart(0);
        }
        Integer length = vo.getLength();
        if(length == null){
            vo.setLength(10);
        }
        String categoryName = vo.getCategoryName();
        if(StringUtils.isNotBlank(categoryName)){
            vo.setCategoryName("%"+categoryName.trim()+"%");
        }
        PageHelper.startPage(vo.getStart(), vo.getLength());
        vo.setDeleted(0);
        List<TGoodsCategoryVo> list = tGoodsCategoryMapper.getCategoryList(vo);
        DataTable<TGoodsCategoryVo> tables = new DataTable<>();
        tables.setRecordsTotal(((Page) list).getTotal());
        tables.setRecordsFiltered(tables.getRecordsTotal());
        tables.setDraw(vo.getDraw());
        tables.setData(list);
        return tables;
    }

    @Override
    public void saveOrUpdateGoods(TGoodsSpuVo vo) {
        Long spuId = vo.getId();
        Integer interfaceType = vo.getInterfaceType();
        if(interfaceType!=null && 0!=interfaceType){
            TGoodsSpu tGoodsSpu = tGoodsSpuMapper.selectById(spuId);
            Assert.notNull(tGoodsSpu,"参数错误,无当前商品");
            if( 1 == interfaceType){
                //推荐调用
                tGoodsSpu.setRecommend(vo.getRecommend());
            }else if(2 == interfaceType){
                //会员商品调用
                tGoodsSpu.setVip(vo.getVip());
            }
            tGoodsSpuMapper.updateById(tGoodsSpu);
            return;
        }
        Integer integralStatus = vo.getIntegralStatus();
        if(integralStatus!=null && integralStatus == 1){
            BigDecimal integral = vo.getIntegral();
            Assert.isTrue(integral!=null && integral.compareTo(BigDecimal.ZERO)>=0,"积分商品,必须添加积分价格");
        }
        Integer payNumLimitStatus = vo.getPayNumLimitStatus();
        if(payNumLimitStatus!=null && payNumLimitStatus == 1){
            Integer payNumLimit = vo.getPayNumLimit();
            Assert.isTrue(payNumLimit!=null && payNumLimit>=0,"打开购买数量限制时,购买限制数量,必须大于0");
        }
        List<SpuSpecsTypeVo> specsTypeList = vo.getSpecsTypeList();
        //规格同名校验
        if(!CollectionUtil.isEmpty(specsTypeList)){
            for (SpuSpecsTypeVo spuSpecsTypeVo : specsTypeList){
                String skuType = spuSpecsTypeVo.getSkuType();
                Assert.isTrue(StringUtil.isNotEmpty(skuType),"请填写规格类型名称");
                int same = 0;
                for (SpuSpecsTypeVo spuSpecsTypeVoEmp : specsTypeList){
                    String skuType1 = spuSpecsTypeVoEmp.getSkuType();
                    Assert.isTrue(StringUtil.isNotEmpty(skuType1),"请填写规格类型名称");
                    if(skuType.equals(skuType1)){
                        same++;
                    }
                }
                Assert.isTrue(same<=1,"规格类型名称不可重复");
                List<SpuSpecsVo> specsList = spuSpecsTypeVo.getSpecsList();
                Assert.isTrue(CollectionUtil.isNotEmpty(specsList),"规格明细不可为空");
                for (SpuSpecsVo spuSpecsVo : specsList) {
                    String skuTitle= spuSpecsVo.getSkuTitle();
                    Assert.isTrue(StringUtil.isNotEmpty(skuTitle),"请填写规格名称");
                    int sameTitle= 0;
                    for (SpuSpecsVo specsVo : specsList) {
                        String skuTitleEmp= specsVo.getSkuTitle();
                        Assert.isTrue(StringUtil.isNotEmpty(skuTitleEmp),"请填写规格名称");
                        if(skuTitle.equals(skuTitleEmp)){
                            sameTitle++;
                        }
                    }
                    Assert.isTrue(sameTitle<=1,"规格名称不可重复");
                }
            }
        }
        if(spuId == null){
            TGoodsSpu spu = new TGoodsSpu();
            vo.setSpuNo(IdUtil.generateId()+"");
            BeanUtil.copyProperties(vo,spu, CopyOptions.create().setIgnoreNullValue(true));
            tGoodsSpuMapper.insert(spu);
            spuId = spu.getId();
        }else{
            TGoodsSpu tGoodsSpu = tGoodsSpuMapper.selectById(spuId);
            BeanUtil.copyProperties(vo,tGoodsSpu, CopyOptions.create().setIgnoreNullValue(true));
            tGoodsSpuMapper.updateById(tGoodsSpu);
            //删除当前spu的规格数据
            spuSpecsTypeMapper.deleteBySpuId(spuId);
            spuSpecsMapper.deleteBySpuId(spuId);
        }
        
        //规格类型
        if(!CollectionUtil.isEmpty(specsTypeList)){
            for (SpuSpecsTypeVo spuSpecsTypeVo : specsTypeList) {
                SpuSpecsType spuSpecsType = new SpuSpecsType();
                spuSpecsTypeVo.setSpuId(spuId);
                BeanUtil.copyProperties(spuSpecsTypeVo,spuSpecsType, CopyOptions.create().setIgnoreNullValue(true));
                spuSpecsTypeMapper.insert(spuSpecsType);
                List<SpuSpecsVo> specsVoList = spuSpecsTypeVo.getSpecsList();
                if(!CollectionUtil.isEmpty(specsVoList)){
                    for (SpuSpecsVo spuSpecsVo : specsVoList) {
                        SpuSpecs spuSpecs = new SpuSpecs();
                        spuSpecs.setSkuType(spuSpecsTypeVo.getSkuType());
                        spuSpecs.setSpuId(spuId);
                        BeanUtil.copyProperties(spuSpecsVo,spuSpecs, CopyOptions.create().setIgnoreNullValue(true));
                        spuSpecsMapper.insert(spuSpecs);
                    }
                }
            }
        }
        //删除所有sku
        Map<String,Object> map = new HashMap<>();
        map.put("goods_spu_id",spuId);
        List<TGoodsSku> tGoodsSpus = tGoodsSkuMapper.selectByMap(map);
        Map<Long,TGoodsSku> oldSkuMap = new HashMap<>();
        if(!CollectionUtil.isEmpty(tGoodsSpus)){
            for (TGoodsSku tGoodsSku : tGoodsSpus) {
                oldSkuMap.put(tGoodsSku.getId(),tGoodsSku);
            }
        }
        List<TGoodsSkuVo> skuList = vo.getSkuList();
        if(!CollectionUtil.isEmpty(skuList)){
            for (TGoodsSkuVo tGoodsSkuVo : skuList) {
                if(integralStatus!=null && integralStatus == 1){
                    BigDecimal integral = vo.getIntegral();
                    BigDecimal skuCurrPrice = tGoodsSkuVo.getSkuCurrPrice();
                    Assert.isTrue(integral!=null && integral.compareTo(skuCurrPrice)<= 0,"积分商品,积分价格必须小于商品售价");
                }
                Long skuId = tGoodsSkuVo.getId();
                tGoodsSkuVo.setIntegral(vo.getIntegral());
                TGoodsSku sku = null;
                if(skuId == null || skuId == 0){
                    sku = new TGoodsSku();
                    sku.setSkuNo(IdUtil.generateId()+"");
                    sku.setGoodsSpuId(spuId);
                    BeanUtil.copyProperties(tGoodsSkuVo,sku, CopyOptions.create().setIgnoreNullValue(true));
                    tGoodsSkuMapper.insert(sku);
                    skuId = sku.getId();
                }else{
                    sku = oldSkuMap.get(skuId);
                    if(sku != null){
                        oldSkuMap.remove(skuId);
                        BeanUtil.copyProperties(tGoodsSkuVo,sku, CopyOptions.create().setIgnoreNullValue(true));
                        tGoodsSkuMapper.updateById(sku);
                    }
                }
                //删除当前sku的规格数据
                skuSpecsTypeMapper.deleteBySkuId(spuId);
                skuSpecsMapper.deleteBySkuId(spuId);
                //规格类型
                List<SkuSpecsTypeVo> skuSpecsTypeList = tGoodsSkuVo.getSkuSpecsTypeList();
                String skuTitle = null;
                if(!CollectionUtil.isEmpty(skuSpecsTypeList)){
                    StringBuffer skuTitleBuffer = new StringBuffer();
                    for (SkuSpecsTypeVo skuSpecsTypeVo : skuSpecsTypeList) {
                        SkuSpecsType skuSpecsType = new SkuSpecsType();
                        skuSpecsTypeVo.setSkuId(skuId);
                        BeanUtil.copyProperties(skuSpecsTypeVo,skuSpecsType, CopyOptions.create().setIgnoreNullValue(true));
                        skuSpecsTypeMapper.insert(skuSpecsType);
                        List<SkuSpecsVo> skuSpecsVoList = skuSpecsTypeVo.getSkuSpecsList();
                        if(!CollectionUtil.isEmpty(skuSpecsVoList)){
                            for (SkuSpecsVo skuSpecsVo : skuSpecsVoList) {
                                SkuSpecs skuSpecs = new SkuSpecs();
                                skuSpecs.setSkuType(skuSpecsTypeVo.getSkuType());
                                skuSpecs.setSkuId(skuId);
                                BeanUtil.copyProperties(skuSpecsVo,skuSpecs, CopyOptions.create().setIgnoreNullValue(true));
                                skuSpecsMapper.insert(skuSpecs);
                                skuTitleBuffer.append(Constant.SPECS_SEPARATOR).append(skuSpecs.getSkuTitle());
                            }
                        }
                    }
                    skuTitle = skuTitleBuffer.toString().substring(1,skuTitleBuffer.length());
                    sku.setSkuTitle(skuTitle);
                    tGoodsSkuMapper.updateById(sku);
                }
            }
        }
        //删除处理
        if(!CollectionUtil.isEmpty(oldSkuMap)){
            for (Long aLong : oldSkuMap.keySet()) {
                tGoodsSkuMapper.deleteById(aLong);
            }
        }
    }



    @Override
    public DataTable<TGoodsListSkuVo> getMallGoodsList(TGoodsListSkuVo vo) {
        Integer start = vo.getStart();
        if(start == null){
            vo.setStart(0);
        }
        Integer length = vo.getLength();
        if(length == null){
            vo.setLength(10000);
        }
        PageHelper.startPage(vo.getStart(), vo.getLength());
        String goodsName = vo.getGoodsName();
        if(StringUtils.isNotBlank(goodsName)){
            goodsName = "%"+goodsName.trim()+"%";
            vo.setGoodsName(goodsName);
        }
        String categoryName = vo.getCategoryName();
        if(StringUtils.isNotBlank(categoryName)){
            categoryName = "%"+categoryName.trim()+"%";
            vo.setCategoryName(categoryName);
        }
        List<TGoodsListSkuVo> list =  tGoodsSkuMapper.getMallGoodsList(vo);
        DataTable<TGoodsListSkuVo> tables = new DataTable<>();
        tables.setRecordsTotal(((Page) list).getTotal());
        tables.setRecordsFiltered(tables.getRecordsTotal());
        tables.setDraw(vo.getDraw());
        tables.setData(list);
        return tables;
    }

    @Override
    public TGoodsDetailVo getGoodsDetail(Long skuId,Long spuId,String skuTitle) {
        TGoodsDetailVo googdetail = tGoodsSkuMapper.getGoodsDetail(skuId,spuId,skuTitle);
        getGoodsDetailSku(googdetail,1);
        return googdetail;
    }

    @Override
    public DataTable<TGoodsSpuVo> getGoodsList(TGoodsSpuVo vo) {
        Integer vip = vo.getVip();
        if(vip!=null && vip ==2){
            vo.setVip(null);
        }
        PageHelper.startPage(vo.getStart(), vo.getLength());
        String goodsName = vo.getGoodsName();
        if(StringUtils.isNotBlank(goodsName)){
            vo.setGoodsName("%"+goodsName.trim()+"%");
        }
        String spuNo = vo.getSpuNo();
        if(StringUtils.isNotBlank(spuNo)){
            vo.setSpuNo("%"+spuNo.trim()+"%");
        }
        vo.setDeleted(0);
        List<TGoodsSpuVo> volist = tGoodsSpuMapper.getGoodsList(vo);
        if(!CollectionUtil.isEmpty(volist)){
            Map<String,Object> paramSkus = new HashMap<>();
            if(vo.getStatus() == null){
                paramSkus.put("status",vo.getStatus());
            }

            for (TGoodsSpuVo tGoodsSpu : volist) {

                Integer skuSaleNumAll = 0;
                Integer skuNumAll = 0;
                Long spuId = tGoodsSpu.getId();
                paramSkus.put("goods_spu_id",spuId);
                List<TGoodsSku> tGoodsSkus = tGoodsSkuMapper.selectByMap(paramSkus);
                List<TGoodsSkuVo> skuVoList = new ArrayList<>();
                Integer statusSpu = CollectionUtil.isEmpty(tGoodsSkus)?1:0;
                if(!CollectionUtil.isEmpty(tGoodsSkus)){
                    for (TGoodsSku goodsSkus : tGoodsSkus) {
                        Integer status = goodsSkus.getStatus();
                        if(status == 1){
                            statusSpu = status;
                        }
                        if(tGoodsSpu.getSkuPrice() == null){
                            tGoodsSpu.setSkuPrice(goodsSkus.getSkuCurrPrice());
                        }
                        Integer skuSaleNum = goodsSkus.getSkuSaleNum();
                        skuSaleNum = skuSaleNum == null?0:skuSaleNum;
                        skuSaleNumAll+=skuSaleNum;
                        Integer skuNum = goodsSkus.getSkuNum();
                        skuNum = skuNum == null?0:skuNum;
                        skuNumAll+=skuNum;
                        TGoodsSkuVo skuVo = new TGoodsSkuVo();
                        BeanUtil.copyProperties(goodsSkus,skuVo);
                        skuVoList.add(skuVo);
                    }
                }
                tGoodsSpu.setSkuNum(skuNumAll);
                tGoodsSpu.setSkuSaleNum(skuSaleNumAll);
                tGoodsSpu.setStatus(statusSpu);
            }
        }
        DataTable<TGoodsSpuVo> tables = new DataTable<>();
        tables.setRecordsTotal(((Page) volist).getTotal());
        tables.setRecordsFiltered(tables.getRecordsTotal());
        tables.setDraw(vo.getDraw());
        tables.setData(volist);
        return tables;
    }
    @Override
    public String checkAddSkuNum(Long skuId, BigDecimal skuNum){
        TGoodsDetailVo goodsDetail=getGoodsDetail(skuId,null,null);
        Assert.notNull(goodsDetail, "商品不存在");
        String result=null;
        if(skuNum.compareTo(goodsDetail.getSkuNum())==1){
            result="库存不足 请重新选择商品";
        }
        return result;
    }

    /**
     * 获取商品sku  规格等  set到detail
     * @param googdetail
     * @param skuStatus
     */
    private void  getGoodsDetailSku( TGoodsDetailVo googdetail,Integer skuStatus){
        long spuId = googdetail.getGoodsSpuId();
        Map<String,Object> param = new HashMap<>();
        param.put("spu_id",spuId);
        param.put("status",1);
        param.put("deleted",0);
        List<SpuSpecsTypeVo> spuSpecsTypeVoList = new ArrayList<>();
        List<SpuSpecsType> spuSpecsTypes = spuSpecsTypeMapper.selectByMap(param);
        if(CollectionUtil.isNotEmpty(spuSpecsTypes)){
            for (SpuSpecsType spuSpecsType : spuSpecsTypes) {
                SpuSpecsTypeVo spuSpecsTypeVo = new SpuSpecsTypeVo();
                BeanUtil.copyProperties(spuSpecsType,spuSpecsTypeVo);
                spuSpecsTypeVoList.add(spuSpecsTypeVo);
                param.clear();
                param.put("sku_type",spuSpecsType.getSkuType());
                param.put("status",1);
                param.put("spu_id",spuId);
                param.put("deleted",0);
                List<SpuSpecs> spuSpecs = spuSpecsMapper.selectByMap(param);
                List<SpuSpecsVo> spuSpecsVoList = new ArrayList<>();
                if(CollectionUtil.isNotEmpty(spuSpecsTypes)){
                    for (SpuSpecs spuSpec : spuSpecs) {
                        SpuSpecsVo spuSpecsVo = new SpuSpecsVo();
                        BeanUtil.copyProperties(spuSpec,spuSpecsVo);
                        spuSpecsVoList.add(spuSpecsVo);
                    }
                }
                spuSpecsTypeVo.setSpecsList(spuSpecsVoList);
            }
        }
        googdetail.setSpecsTypeList(spuSpecsTypeVoList);
        //获取所有规格
        param.clear();
        param.put("goods_spu_id",spuId);
        if(skuStatus!=null){
            param.put("status",skuStatus);
        }
        List<TGoodsSku> tGoodsSkus = tGoodsSkuMapper.selectByMap(param);
        List<TGoodsSkuVo> tGoodsSkuVoList = new ArrayList<>();
        if(CollectionUtil.isNotEmpty(tGoodsSkus)){
            for (TGoodsSku goodsSkus : tGoodsSkus) {
                TGoodsSkuVo skuVo = new TGoodsSkuVo();
                BeanUtil.copyProperties(goodsSkus,skuVo);
                tGoodsSkuVoList.add(skuVo);
                param.clear();
                param.put("sku_id",goodsSkus.getId());
                param.put("status",1);
                param.put("deleted",0);
                List<SkuSpecsTypeVo> skuSpecsTypeVoList = new ArrayList<>();
                List<SkuSpecsType> skuSpecsTypes = skuSpecsTypeMapper.selectByMap(param);
                if(CollectionUtil.isNotEmpty(skuSpecsTypes)){
                    StringBuffer skutitle = new StringBuffer();
                    for (SkuSpecsType skuSpecsType : skuSpecsTypes) {
                        SkuSpecsTypeVo skuSpecsTypeVo = new SkuSpecsTypeVo();
                        BeanUtil.copyProperties(skuSpecsType,skuSpecsTypeVo);
                        skuSpecsTypeVoList.add(skuSpecsTypeVo);
                        param.clear();
                        param.put("sku_type",skuSpecsType.getSkuType());
                        param.put("sku_id",goodsSkus.getId());
                        param.put("status",1);
                        param.put("deleted",0);
                        List<SkuSpecs> spuSpecs = skuSpecsMapper.selectByMap(param);
                        List<SkuSpecsVo> skuSpecsVoList = new ArrayList<>();
                        if(CollectionUtil.isNotEmpty(spuSpecs)){
                            for (SkuSpecs skuSpec : spuSpecs) {
                                SkuSpecsVo skuSpecsVo = new SkuSpecsVo();
                                skutitle.append("-").append(skuSpec.getSkuTitle());
                                BeanUtil.copyProperties(skuSpec,skuSpecsVo);
                                skuSpecsVoList.add(skuSpecsVo);
                            }
                        }
                        skuSpecsTypeVo.setSkuSpecsList(skuSpecsVoList);
                    }
                    skuVo.setSkuTitle(skutitle.toString().substring(1,skutitle.length()));
                }

                skuVo.setSkuSpecsTypeList(skuSpecsTypeVoList);
            }
        }
        googdetail.setSkuList(tGoodsSkuVoList);
    }

    @Override
    public TGoodsDetailVo getGoodsDetailForPc(Long spuId) {
        Assert.notNull(spuId,"参数错误,商品ID不可为null");
        TGoodsDetailVo googdetail = tGoodsSkuMapper.getGoodsDetailForPc(spuId);
        Assert.notNull(googdetail,"参数错误,无此商品");
        getGoodsDetailSku(googdetail,null);
        return googdetail;
    }


    @Override
    public void deleteCategory(Long id) {
        Assert.notNull(id,"参数错误,请传输ID");
        TGoodsCategory tGoodsCategory = tGoodsCategoryMapper.selectById(id);
        Assert.notNull(tGoodsCategory,"参数错误,无此品类");
        Assert.isTrue(tGoodsCategory.getDeleted() == 0,"删除失败,当前品类已经是删除状态");
        TGoodsSpuVo vo = new TGoodsSpuVo();
        vo.setCategoryId(id);
        vo.setDeleted(0);
        List<TGoodsSpuVo> goodsList = tGoodsSpuMapper.getGoodsList(vo);
        Assert.isTrue(CollectionUtil.isEmpty(goodsList),"删除失败,当前品类下有未删除的商品");
        tGoodsCategory.setDeleted(1);
        tGoodsCategoryMapper.updateById(tGoodsCategory);
    }

    @Override
    public void deleteGoods(Long id) {
        Assert.notNull(id,"参数错误,请传输ID");
        TGoodsSpu tGoodsSpu = tGoodsSpuMapper.selectById(id);
        Assert.notNull(tGoodsSpu,"参数错误,无此商品");
        Assert.isTrue(tGoodsSpu.getDeleted() == 0,"删除失败,当前商品已经是删除状态");
        //查看当前商品是否是下架状态
        Map<String,Object>paramSkus = new HashMap<>();
        paramSkus.put("goods_spu_id",id);
        paramSkus.put("status",1);
        paramSkus.put("deleted",0);
        List<TGoodsSku> tGoodsSkus = tGoodsSkuMapper.selectByMap(paramSkus);
        //Assert.isTrue(CollectionUtil.isEmpty(tGoodsSkus),"删除失败,当前商品非下架状态,请下架所有商品SKU");
        tGoodsSpu.setDeleted(1);
        tGoodsSpuMapper.updateById(tGoodsSpu);
        //删除所有sku
        int aa = tGoodsSkuMapper.deleteAllGoodsSku(id);
    }

    @Override
    public Boolean checkPayNumLimit(Long spuId,Integer num) {
        Long currUid = SessionUtil.getCurrUid();
        TGoodsSpu tGoodsSpu = tGoodsSpuMapper.selectById(spuId);
        Integer payNumLimitStatus = tGoodsSpu.getPayNumLimitStatus();
        Integer payNumLimit = tGoodsSpu.getPayNumLimit();
        if(payNumLimitStatus!=0 &&payNumLimitStatus==1){
            //查看当前登录人已支付订单有多少个当前商品
            UserSpuPayNum userSpuPayNum = new UserSpuPayNum();
            userSpuPayNum.setUserId(currUid);
            userSpuPayNum.setSpuId(spuId);
            userSpuPayNum = userSpuPayNumMapper.selectOne(userSpuPayNum);
            int payNum = userSpuPayNum == null || userSpuPayNum.getNum() == null ?0:userSpuPayNum.getNum();
            return payNumLimit >= (payNum+num);
        }
        return true;

    }

    @Override
    public void oldGoodsHandle() {
        Map<String,Object> param = new HashMap<>();
        List<TGoodsSpu> tGoodsSpus = tGoodsSpuMapper.selectByMap(param);
        for (TGoodsSpu goodsSpus : tGoodsSpus) {
            TGoodsSpuVo vo = new TGoodsSpuVo();
            BeanUtil.copyProperties(goodsSpus,vo);
            Long id = goodsSpus.getId();
            param.clear();
            param.put("spu_id",id);
            List<SpuSpecsType> spuSpecsTypes = spuSpecsTypeMapper.selectByMap(param);
            if(CollectionUtil.isNotEmpty(spuSpecsTypes)){
                continue;
            }
            param.clear();
            param.put("goods_spu_id",id);
            List<TGoodsSku> tGoodsSkus = tGoodsSkuMapper.selectByMap(param);
            if(CollectionUtil.isEmpty(tGoodsSkus)){
                continue;
            }
            Map<String,List<String>> map = new HashMap<>();
            for (TGoodsSku goodsSkus : tGoodsSkus) {
                String skuTitle = goodsSkus.getSkuTitle();
                String skuTitleType = goodsSkus.getSkuTitleType();
                List<String> strings = map.get(skuTitleType);
                if(strings == null){
                    strings = new ArrayList<>();
                }
                strings.add(skuTitle);
                map.put(skuTitleType,strings);
                SkuSpecsType skuSpecsType = new SkuSpecsType();
                skuSpecsType.setSkuId(goodsSkus.getId());
                skuSpecsType.setSkuType(skuTitleType);
                skuSpecsTypeMapper.insert(skuSpecsType);
                SkuSpecs skuSpecs = new SkuSpecs();
                skuSpecs.setSkuId(goodsSkus.getId());
                skuSpecs.setSkuType(skuTitleType);
                skuSpecs.setSkuTitle(skuTitle);
                skuSpecsMapper.insert(skuSpecs);
            }
            if(CollectionUtil.isNotEmpty(map)){
                Set<Map.Entry<String, List<String>>> entries = map.entrySet();
                for (Map.Entry<String, List<String>> entry : entries) {
                    String key = entry.getKey();
                    SpuSpecsType spuSpecsType = new SpuSpecsType();
                    spuSpecsType.setSpuId(id);
                    spuSpecsType.setSkuType(key);
                    spuSpecsTypeMapper.insert(spuSpecsType);
                    List<String> value = entry.getValue();
                    for (String s : value) {
                        SpuSpecs spuSpecs = new SpuSpecs();
                        spuSpecs.setSpuId(id);
                        spuSpecs.setSkuType(key);
                        spuSpecs.setSkuTitle(s);
                        spuSpecsMapper.insert(spuSpecs);
                    }
                }
            }
        }
    }
}
