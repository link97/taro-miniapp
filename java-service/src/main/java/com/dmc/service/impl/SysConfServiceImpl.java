package com.dmc.service.impl;

import com.dmc.entity.SysConf;
import com.dmc.mapper.SysConfMapper;
import com.dmc.service.SysConfService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 * 系统设置表 服务实现类
 * </p>
 *
 * @
 * @since 2020-09-12
 */
@Service
@Transactional
public class SysConfServiceImpl extends ServiceImpl<SysConfMapper, SysConf> implements SysConfService {

}
