package com.dmc.service.impl;

import com.dmc.entity.OrderDetail;
import com.dmc.mapper.OrderDetailMapper;
import com.dmc.service.OrderDetailService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 * 订单明细表 服务实现类
 * </p>
 *
 * @
 * @since 2020-09-06
 */
@Service
@Transactional
public class OrderDetailServiceImpl extends ServiceImpl<OrderDetailMapper, OrderDetail> implements OrderDetailService {

}
