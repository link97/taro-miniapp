package com.dmc.service.impl;

import com.dmc.dict.BenefitTranTypeDict;
import com.dmc.dict.OrderTypeDict;
import com.dmc.entity.Orders;
import com.dmc.mapper.*;
import com.dmc.model.CashOutOrders;
import com.dmc.model.DistributionOrder;
import com.dmc.model.DistributionOrderGoods;
import com.dmc.model.User;
import com.dmc.service.PaymentService;
import com.dmc.util.util.StringUtil;
import com.dmc.vo.BenefitTransactionVo;
import com.dmc.vo.DataTable;
import com.dmc.vo.PaymentVo;
import com.dmc.vo.StatInfoVo;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>
 * 银行信息 服务实现类
 * </p>
 *
 * @
 * @since 2020-09-05
 */
@Service
@Transactional
public class PaymentServiceImpl implements PaymentService {

    @Autowired
    private PaymentMapper paymentMapper;

    @Autowired
    private OrdersMapper ordersMapper;

    @Autowired
    private BenefitTransactionMapper benefitTransactionMapper;

    @Autowired
    private DistributionOrderMapper distributionOrderMapper;

    @Autowired
    private CashOutOrdersMapper cashOutOrdersMapper;

    @Autowired
    private UserMapper userMapper;


    @Autowired
    private DistributionOrderGoodsMapper distributionOrderGoodsMapper;



    /**
     * 获取概况信息
     * @return
     */
    @Override
    public StatInfoVo getStatInfo() {
        StatInfoVo vo = new StatInfoVo();
        Integer paymentOrderNum=ordersMapper.getTotalOrderNum(null);
        Orders orders= ordersMapper.getGoodOrderSumAmount(null);
        BigDecimal goodsPrice=BigDecimal.ZERO;
        //获取已支付商品订单数
        if(orders!=null){
            vo.setGoodsPayOrderPrice(orders.getFinalAmount().setScale(2, BigDecimal.ROUND_HALF_UP));
            goodsPrice=orders.getFinalAmount().subtract(orders.getRefundAmount());
        }else{
            vo.setGoodsPayOrderPrice(BigDecimal.ZERO);
        }
        //获取已支付商品订单数
        vo.setGoodsPayOrderNum(paymentOrderNum!=null?paymentOrderNum:0);
        int distributionsum = paymentMapper.orderNum(OrderTypeDict.ORDER_TYPE_DISTRBUTION.getCn());
        //分销商付款订单
        vo.setDistributionPayOrderNum(distributionsum);
        BigDecimal distributionsumBig = paymentMapper.orderPrice(OrderTypeDict.ORDER_TYPE_DISTRBUTION.getCn());
        BigDecimal  distributionPayOrderPrice=BigDecimal.ZERO;
        if(distributionsumBig!=null){
            distributionPayOrderPrice=distributionsumBig.divide(new BigDecimal("100"));
        }
        vo.setDistributionPayOrderPrice(distributionPayOrderPrice);//分销商品付款订单金额
        Integer pendingOrdersNum=ordersMapper.getPendingOrdersNum();
        vo.setPendingOrdersNum(pendingOrdersNum!=null?pendingOrdersNum:0);//待处理订单
        vo.setGoodsPrice(goodsPrice);//商品订单营收
        return vo;
    }

    /**
     * 获取交易明细
     * @param vo
     * @return
     */
    @Override
    public DataTable<PaymentVo> getTransactionDetailList(PaymentVo vo){
        Integer start = vo.getStart();
        if(start == null){
            vo.setStart(0);
        }
        Integer length = vo.getLength();
        if(length == null){
            vo.setLength(10);
        }
        PageHelper.startPage(vo.getStart(), vo.getLength());
        String userName = vo.getUserName();
        if(StringUtils.isNotBlank(userName)){
            vo.setUserName("%"+userName.trim()+"%");
        }
        String userPhone = vo.getUserPhone();
        if(StringUtils.isNotBlank(userPhone)){
            vo.setUserPhone("%"+userPhone.trim()+"%");
        }
        String payNo = vo.getPayNo();
        if(StringUtils.isNotBlank(payNo)){
            vo.setPayNo("%"+payNo.trim()+"%");
        }
        String orderId = vo.getOrderId();
        if(StringUtils.isNotBlank(orderId)){
            vo.setOrderId("%"+orderId.trim()+"%");
        }
        //搜索条件
        List<PaymentVo> integralDetailVoList=paymentMapper.selectTransactionDetailList(vo);
        DataTable<PaymentVo> tables = new DataTable<>();
        tables.setRecordsTotal(((Page) integralDetailVoList).getTotal());
        tables.setRecordsFiltered(tables.getRecordsTotal());
        tables.setDraw(vo.getDraw());
        tables.setData(integralDetailVoList);
        return tables;
    }

    @Override
    public DataTable<BenefitTransactionVo> getBenefitTransaction(BenefitTransactionVo vo) {
        Integer start = vo.getStart();
        if(start == null){
            vo.setStart(0);
        }
        Integer length = vo.getLength();
        if(length == null){
            vo.setLength(10);
        }
        String userName = vo.getUserName();
        if(StringUtils.isNotBlank(userName)){
            userName = "%"+userName.trim()+"%";
            vo.setUserName(userName);
        }
        String userPhone = vo.getUserPhone();
        if(StringUtils.isNotBlank(userPhone)){
            userPhone = "%"+userPhone.trim()+"%";
            vo.setUserPhone(userPhone);
        }
        PageHelper.startPage(vo.getStart(), vo.getLength());
        List<BenefitTransactionVo> benefitVoList=benefitTransactionMapper.getBenefitTransaction(vo);
        if(!CollectionUtils.isEmpty(benefitVoList)){
            for (BenefitTransactionVo benefitTransactionVo : benefitVoList) {
                Long orderId = benefitTransactionVo.getOrderId();
                Integer type = benefitTransactionVo.getType();
                String descByCn = BenefitTranTypeDict.getDescByCn(type);
                benefitTransactionVo.setTypeName(descByCn);
                int orderType = BenefitTranTypeDict.checkOrderType(type);

                Long payUserId = null;
                if(orderType == 1){
                    DistributionOrder distributionOrder = distributionOrderMapper.selectById(orderId);
                    benefitTransactionVo.setOrderNo(distributionOrder.getOrderNo());
                    payUserId = distributionOrder.getUserId();
                    DistributionOrderGoods distributionOrderGoods=new DistributionOrderGoods();
                    distributionOrderGoods.setOrderId(orderId);
                    distributionOrderGoods=distributionOrderGoodsMapper.selectOne(distributionOrderGoods);
                    if(distributionOrderGoods!=null){
                        benefitTransactionVo.setDistributionName(distributionOrderGoods.getDistributionName());
                    }
                }else if(orderType == 2){
                    Orders orders = ordersMapper.selectById(orderId);
                    benefitTransactionVo.setOrderNo(orders.getOrdersNo());
                    payUserId = orders.getUserId();
                }else if(orderType == 3){
                    CashOutOrders cashOutOrders = cashOutOrdersMapper.selectById(orderId);
                    benefitTransactionVo.setOrderNo(cashOutOrders.getOrderCode());
                    benefitTransactionVo.setBenefit(benefitTransactionVo.getBenefit().negate());
                }
                if(payUserId!=null){
                    User payUser = userMapper.getById(payUserId);
                    if(payUser!=null){
                        benefitTransactionVo.setPayUserName(payUser.getUsername());
                    }
                }
                String payUserName = benefitTransactionVo.getPayUserName();
                if(StringUtil.isNotEmpty(payUserName)){
                    benefitTransactionVo.setDes(payUserName+":"+benefitTransactionVo.getDes());
                }else{
                    benefitTransactionVo.setDes(benefitTransactionVo.getDes());
                }

            }
        }
        DataTable<BenefitTransactionVo> tables = new DataTable<>();
        tables.setRecordsTotal(((Page) benefitVoList).getTotal());
        tables.setRecordsFiltered(tables.getRecordsTotal());
        tables.setDraw(vo.getDraw());
        tables.setData(benefitVoList);
        return tables;
    }
}
