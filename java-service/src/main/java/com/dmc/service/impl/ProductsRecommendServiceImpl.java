package com.dmc.service.impl;

import com.dmc.entity.ProductsRecommend;
import com.dmc.mapper.ProductsRecommendMapper;
import com.dmc.service.ProductsRecommendService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 * 推荐商品 服务实现类
 * </p>
 *
 * @
 * @since 2020-09-05
 */
@Service
@Transactional
public class ProductsRecommendServiceImpl extends ServiceImpl<ProductsRecommendMapper, ProductsRecommend> implements ProductsRecommendService {

}
