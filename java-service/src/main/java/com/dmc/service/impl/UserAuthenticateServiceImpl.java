package com.dmc.service.impl;

import com.dmc.entity.UserAuthenticate;
import com.dmc.mapper.UserAuthenticateMapper;
import com.dmc.service.UserAuthenticateService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.dmc.vo.DataTable;
import com.dmc.vo.UserAuthenticateVo;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 * 身份认证 服务实现类
 * </p>
 *
 * @
 * @since 2020-09-05
 */
@Service
@Transactional
public class UserAuthenticateServiceImpl extends ServiceImpl<UserAuthenticateMapper, UserAuthenticate> implements UserAuthenticateService {

    @Autowired
    private UserAuthenticateMapper userAuthenticateMapper;
    /**
     * 获取用户个人身份认证记录
     * @param userId
     * @return
     */
    @Override
    public UserAuthenticate getUserAuthenticateByUserId(Long userId) {
        return userAuthenticateMapper.getUserAuthenticateByUserId(userId);
    }
    /**
     * 获取身份认证记录
     * @param userAuthenticate
     * @return
     */
    @Override
    public DataTable<UserAuthenticateVo> getUserAuthenticateAll(UserAuthenticateVo userAuthenticate){
        Integer start = userAuthenticate.getStart();
        if(start == null){
            userAuthenticate.setStart(0);
        }
        Integer length = userAuthenticate.getLength();
        if(length == null){
            userAuthenticate.setLength(10);
        }
        PageHelper.startPage(userAuthenticate.getStart(), userAuthenticate.getLength());
        List<UserAuthenticateVo> list =  userAuthenticateMapper.getUserAuthenticateAll(userAuthenticate);
        DataTable<UserAuthenticateVo> tables = new DataTable<>();
        tables.setRecordsTotal(((Page) list).getTotal());
        tables.setRecordsFiltered(tables.getRecordsTotal());
        tables.setDraw(userAuthenticate.getDraw());
        tables.setData(list);
        return tables;
    }
}
