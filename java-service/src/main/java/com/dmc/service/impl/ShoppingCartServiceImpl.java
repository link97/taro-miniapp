package com.dmc.service.impl;

import com.dmc.entity.ShoppingCart;
import com.dmc.mapper.ShoppingCartMapper;
import com.dmc.service.GoodsService;
import com.dmc.service.ShoppingCartService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.dmc.vo.TGoodsDetailVo;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 购物车数据表 服务实现类
 * </p>
 *
 * @
 * @since 2020-09-06
 */
@Service
@Transactional
public class ShoppingCartServiceImpl extends ServiceImpl<ShoppingCartMapper, ShoppingCart> implements ShoppingCartService {

    @Resource
    private GoodsService goodsService;
    @Autowired
    private ShoppingCartMapper shoppingCartMapper;
    /**
     * 添加购物车
     * @param shoppingCart
     * @return
     */
    @Override
    public Boolean addCart(ShoppingCart shoppingCart) {
        Assert.notNull(shoppingCart, "参数不能为空");
        Assert.isTrue(shoppingCart.getUserId()!=null, "用户ID不能为空");
        Assert.isTrue(shoppingCart.getSkuId()!=null, "商品不能为空");
        Assert.isTrue(shoppingCart.getQuantity()!=null&&shoppingCart.getQuantity()!=0, "商品数量不能为空或不能为0");
        //去购物车找该用户是否有该商品记录,没有记录创建否则累加
        ShoppingCart existShoppingCart=shoppingCartMapper.getShoppingCartByUserIdAndSkuId(shoppingCart.getSkuId(),shoppingCart.getUserId());
        if(existShoppingCart!=null){
            BigDecimal currQuantity=BigDecimal.ZERO;
            currQuantity=BigDecimal.valueOf(existShoppingCart.getQuantity()).add(BigDecimal.valueOf(shoppingCart.getQuantity()));
            shoppingCart.setQuantity(Integer.parseInt(currQuantity.toString()));
        }
        //数量不能超出库存
        String resultCheckNum=goodsService.checkAddSkuNum(shoppingCart.getSkuId(), BigDecimal.valueOf(shoppingCart.getQuantity()));
        if(StringUtils.isNotBlank(resultCheckNum)) {
            Assert.isTrue(false, resultCheckNum);
        }
        Boolean result=false;
        Date now=new Date();
        if(existShoppingCart!=null){
            shoppingCart.setCartId(existShoppingCart.getCartId());
            shoppingCart.setCreateTime(existShoppingCart.getCreateTime());
            shoppingCart.setUpdateTime(now);
            result=updateAllColumnById(shoppingCart);
        }else{
            shoppingCart.setCreateTime(now);
            shoppingCart.setUpdateTime(now);
            result=insert(shoppingCart);
        }
        return result;
    }
    /**
     * 获取我的购物车
     * @param userId
     * @return
     */
    @Override
    public List<TGoodsDetailVo> getShoppingCartByUserId(Long userId) {
        return shoppingCartMapper.getShoppingCartByUserId(userId);
    }
    /**
     * 添加购物车商品数量
     * @param shoppingCart
     * @return
     */
    @Override
    public Boolean addCartSkuNum(ShoppingCart shoppingCart){
        Boolean result=false;
        ShoppingCart existShoppingCart=shoppingCartMapper.getShoppingCartByUserIdAndSkuId(shoppingCart.getSkuId(),shoppingCart.getUserId());
        if(existShoppingCart!=null) {
//            BigDecimal currQuantity = BigDecimal.ZERO;
//            currQuantity = BigDecimal.valueOf(existShoppingCart.getQuantity()).add(BigDecimal.valueOf(shoppingCart.getQuantity()));
            existShoppingCart.setQuantity(shoppingCart.getQuantity());
            existShoppingCart.setUpdateTime(new Date());
            result=updateAllColumnById(existShoppingCart);
        }
        return result;
    }

}
