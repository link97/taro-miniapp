package com.dmc.service.impl;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import cn.hutool.core.collection.CollectionUtil;
import com.dmc.entity.Message;
import com.dmc.mapper.MessageMapper;
import com.dmc.mapper.TGoodsCategoryMapper;
import com.dmc.service.MessageService;
import com.dmc.vo.DataTable;
import com.dmc.vo.MessageVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
@Transactional
public class MessageServiceImpl implements MessageService {

    @Autowired
    private MessageMapper messageMapper;

    @Override
    public List<MessageVo> getPlatformMessage(Integer status) {
        Map<String,Object> param = new HashMap<>();
        param.put("user_id",-1);
        if(status!=null){
            param.put("status",status);
        }
        List<MessageVo> result = new ArrayList<>();
        List<Message> messages = messageMapper.selectByMap(param);
        if(CollectionUtil.isNotEmpty(messages)){
            for (Message message : messages) {
                MessageVo vo = new MessageVo();
                BeanUtil.copyProperties(message,vo, CopyOptions.create().setIgnoreNullValue(true));
                result.add(vo);
                Integer messageStatus = message.getStatus();
                if(messageStatus==null || messageStatus==0){
                    message.setStatus(1);
                    messageMapper.updateById(message);
                }
            }
        }
        return result;
    }

    @Override
    public int getPlatformMessageNum(Integer status) {
        Map<String,Object> param = new HashMap<>();
        param.put("user_id",-1);
        if(status!=null){
            param.put("status",status);
        }
        List<MessageVo> result = new ArrayList<>();
        List<Message> messages = messageMapper.selectByMap(param);
        return messages.size();
    }
}
