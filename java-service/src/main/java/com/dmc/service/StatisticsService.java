package com.dmc.service;

import com.dmc.vo.OverviewVo;

/**
 *
 * @date 2020/9/1312:35
 */
public interface StatisticsService {

    /**
     * 统计概况
     * @param sDate
     * @param eDate
     * @return
     */
    OverviewVo getOverview(String sDate, String eDate);
}
