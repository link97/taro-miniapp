package com.dmc.service;

import com.dmc.entity.*;
import com.baomidou.mybatisplus.service.IService;
import com.dmc.vo.DataTable;
import com.dmc.vo.OrdersVo;
import com.dmc.vo.SmallPayModeVo;
import com.dmc.vo.SysConfVo;

import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 * 订单表 服务类
 * </p>
 *
 * @
 * @since 2020-09-06
 */
public interface OrdersService extends IService<Orders> {
    /**
     * 创建订单
     * @param ordersVo
     * @return
     */
    public List<String> createOrder(OrdersVo ordersVo);

    /**
     * 审核线下支付订单
     * @param ordersVo
     * @return
     */
    public void reviewOfflineOrder(OrdersVo ordersVo);

    /**
     * 获取用户订单
     * @return
     */
    List<OrdersVo> getOrderByUserId(Long currUid,Long ordersState,String keyWord);

    /**
     * 获取订单列表
     * @return
     */
    DataTable<OrdersVo> getOrder(OrdersVo ordersVo);

    /**
     * 获取订单交易配置
     * @return
     */
    public SysConfVo getSysOrderConf();

    /**
     * 添加系统配置
     * @param sysConf
     * @return
     */
    public Integer createOrUpdateSysOrderConf(SysConfVo sysConf);
    /**
     * 获取物流快递公司
     * @return
     */
    public List<Logistics> getLogisticsInfo();
    /**
     * 获取订单物流信息
     * @param ordersLogistics
     * @return
     */
    public List<OrdersLogistics> getOrdersLogisticsList(OrdersLogistics ordersLogistics);
    /**
     * 添加订单快递信息(发货换货)
     * @param ordersLogistics
     * @return
     */
    public Integer createOrUpdateOrdersLogistics(OrdersLogistics ordersLogistics);

    /**
     * 系统按设定的时间取消
     */
    public void cancelOrder();

    /**
     * 系统自动确认收货
     */
    public void confirmOrderArrival();

    /**
     * 订单退款
     * @param ordersVo
     * @return
     */
    public Integer orderRefund(OrdersVo ordersVo);

    /**
     * 订单退款
     * @param ordersVo
     * @return
     */
    public Integer reviewOrderRefunds(OrdersVo ordersVo);
    /**
     * 统计用户已付款实付金额
     * @return
     */
    public BigDecimal getTotalUserOrderFinalAmount();

    /**
     * 统计用户已付款实付金额
     * @param orderNo
     * @return
     */
    public BigDecimal getFinalAmountByOrderNo(String orderNo);

    /**
     * 取消订单
     * @param ordersNo
     * @return
     */
    public Integer cancelOrder(String ordersNo);


    /**
     * 根据订单号查订单明细
     * @param ordersNo
     * @return
     */
    public List<OrderDetail> getOrderDetailByOrderNo(String ordersNo);


    /**
     * 使用积分支付
     * @param ordersNo
     * @param userId
     * @param useIntegral
     */
    public void useIntegralPay(String ordersNo,Long userId,BigDecimal useIntegral);

    /**
     * 支付成功商品订单相关回调处理
     * @param payType
     * @param orderId
     * @param orderType
     */
    public void payOrderProcess(Integer payType, String orderId, Integer orderType);

    /**
     * 支付时商品订单相关前置处理
     * @param orderId
     * @param checkoutId
     */
    public void payOrderProcess(String orderId,Long checkoutId);

    /**
     * 用户确认收货
     * @param userId
     * @param orderNo
     * @return
     */
    public Integer userConfirmOrderArrival(Long userId,String orderNo);

    /**
     * 统计用户已退款金额
     * @param orderNo
     * @return
     */
    public BigDecimal getRefundAmountByOrderNo(String orderNo);


    /**
     * 申请订单退货
     * @param ordersLogistics
     * @return
     */
    public Integer reviewOrderReturn(OrdersLogistics ordersLogistics);

    /**
     * 申请订单退货
     * @param ordersLogistics
     * @return
     */
    public Integer returnReview(OrdersLogistics ordersLogistics);


    /**
     * 撤销订单申请
     * @param ordersNo
     * @param type
     * @return
     */
    public Integer revokeApply(String ordersNo,Integer type);

    /**
     * 退款回调处理,更新订单状态
     * @param ordersNo
     */
    public void refundBackOrderExePre(String ordersNo);

    /**
     * 检查订单库存是否充足
     * @param ordersNo
     * @return
     */
    public String payCheckSkuNum(String ordersNo);

    /**
     * 获取小程序支付方式
     * @return
     */
    List<SmallPayModeVo> getSmallPayMode();
    /**
     * 使用收益支付
     * @return
     */
    void useBalancePay(String ordersNo);

    /**
     * 使用收益支付
     * @return
     */
    void addPaySpuNum(Long userId,Long spuId,Integer num);
}
