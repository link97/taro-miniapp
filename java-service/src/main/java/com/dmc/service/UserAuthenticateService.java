package com.dmc.service;

import com.baomidou.mybatisplus.service.IService;
import com.dmc.entity.UserAuthenticate;
import com.dmc.vo.DataTable;
import com.dmc.vo.UserAuthenticateVo;

import java.util.List;

/**
 * <p>
 * 身份认证 服务类
 * </p>
 *
 * @author dmc
 * @since 2020-09-04
 */
public interface UserAuthenticateService extends IService<UserAuthenticate> {
    /**
     * 获取用户个人身份认证记录
     * @param userId
     * @return
     */
    UserAuthenticate getUserAuthenticateByUserId(Long userId);

    /**
     * 获取身份认证记录
     * @param userAuthenticate
     * @return
     */
    DataTable<UserAuthenticateVo> getUserAuthenticateAll(UserAuthenticateVo userAuthenticate);
}
