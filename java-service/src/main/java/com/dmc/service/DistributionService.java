package com.dmc.service;

import com.dmc.model.DistributionShareRelation;
import com.dmc.model.RestListRespResult;
import com.dmc.vo.*;
import org.springframework.web.multipart.MultipartFile;

import java.math.BigDecimal;
import java.util.List;

/**
 * 分销服务
 */
public interface DistributionService {

    /**
     * 添加分享商商品
     * @param vo
     */
    void saveGoods(DistributionGoodsVo vo);
    /**
     * 编辑分享商商品
     * @param vo
     */
    void updateGoods(DistributionGoodsVo vo);

    /**
     * 获取分销商商品列表
     * @param vo
     * @return
     */
    DataTable<DistributionGoodsVo> getDistributionGoodsList(DistributionGoodsVo vo);

    /**
     * 获取分销关系链
     * @return
     */
    DataTable<DistributionShareRelationVo> getShareRelation(Integer shareLevel);
    /**
     * 获取助益关系链
     * @return
     */
    DataTable<DistributionHelpRelationVo> getHelpRelation(Integer type);
    /**
     * 获取有效分销协议
     * @return
     */
    DistributionProcotolVo getProtol();
    /**
     * 获取分销协议列表
     * @return
     */
    DataTable<DistributionProcotolVo> getProtolList(DistributionProcotolVo vo);

    /**
     * 修改或者更新协议
     * @param vo
     */
    void saveOrUpdateProcotol(DistributionProcotolVo vo);

    /**
     * 添加分销订单
     * @param vo
     * @param isAuth 是否需要认证身份
     * @return
     */
    DistributionOrderVo addDistributionOrder(DistributionOrderVo vo,Boolean isAuth);
    /**
     * 获取分销商订单列表信息
     * @param vo
     * @return
     */
    DataTable<DistributionOrderVo> getDistributionOrderList(DistributionOrderVo vo);
    /**
     * 分销商订单支付完成之后的逻辑处理
     * @param orderId
     * @return
     */
    void packAgePostProcess(Long orderId,Integer payType,Boolean isZero,boolean update);

    /**
     * 分销商订单支付完成之后的逻辑处理
     * @param orderId
     * @return
     */
    void postProcess(Long orderId,Integer payType,Boolean isZero,boolean update);

    /**
     * 获取我的分红
     * @return
     */
    MyBonusVo getMyBonusList(Integer packStatus, Long packageId);

    /**
     * 导入老数据
     * @param file
     */
    void inportOldData(MultipartFile file);

    /**
     * 线下支付审核
     * @param vo
     */
    void reviewOfflineOrder(DistributionOrderVo vo);

    /**
     * 提交线下支付证明
     * @param vo
     * @return
     */
    DistributionOrderVo submitPayEvidence(DistributionOrderVo vo);

    /**
     * 获取代付款订单
     * @return
     */
    DataTable<DistributionOrderVo> getReadyPayOrderList();

    /**
     * 小程序获取分销订单详情
     * @param orderId
     * @return
     */
    DistributionOrderVo getDistributionOrderDetail(long orderId,Integer level);

    /**
     * PC端获取分销订单详情
     * @param orderId
     * @return
     */
    RestListRespResult<DistributionOrderDetailPcVo> getOrderDetailForPc(long orderId);
    /**
     * PC端获取分享列表
     * @param vo
     * @return
     */
    DataTable<DistributionShareRelationPcVo> getShareRelationForPc(DistributionShareRelationPcVo vo);

    /**
     * 添加推荐人关联关系
     * @param relation
     * @return
     */
    int insertRelation(DistributionShareRelation relation);


    /**
     *添加套餐包
     * @param vo
     * @return
     */
    void saveOrUpdatePackage(DistributionGoodsPackageVo vo);

    /**
     * 添加分销商套餐订单
     * @param vo
     * @param isAuth
     * @return
     */
    DistributionOrderVo addDistributionPackageOrder(DistributionOrderVo vo, boolean isAuth);
    /**
     * 添加或者修改业绩奖配置
     * @param vo
     * @return
     */
    void saveOrUpdateAchievementConfig(AchievementBonusConfigVo vo);
    /**
     * 添加或者修改业绩奖配置
     * @param vo
     * @return
     */
    List<AchievementBonusConfigVo> getAchievementConfigList(AchievementBonusConfigVo vo);

    /**
     * 获取几代generations内所有业绩
     * @param userId
     * @param achievementAll
     * @param generations
     * @return
     */
    public BigDecimal getNextAchievement(long userId, BigDecimal achievementAll, Integer generations);
}
