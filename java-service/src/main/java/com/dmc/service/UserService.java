package com.dmc.service;

import com.dmc.jwt.AuthTokenDetails;
import com.dmc.model.User;
import com.dmc.vo.*;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 用户Service
 *
 * @author yangfan
 */
public interface UserService {

    /**
     * 用户登录
     *
     * @param user 里面包含登录名和密码
     * @return 用户对象
     */
    User login(User user);


    /**
     * 添加用户
     *
     * @param user
     */
    void add(User user);

    /**
     * 添加用户
     *
     * @param user
     */
    void addUser(User user);

    /**
     * 获得用户对象
     *
     * @param id
     * @return
     */
    User get(Long id);

    /**
     * 编辑用户
     *
     * @param user
     */
    void edit(User user);

    /**
     * 删除用户
     *
     * @param id
     */
    void delete(Long id);

    /**
     * 用户授权
     *
     * @param user 需要user.roleIds的属性值
     */
    void grant(User user);

    /**
     * 获得用户能访问的资源地址
     *
     * @param id 用户ID
     * @return
     */
    List<String> resourceList(Long id);

    /**
     * 编辑用户密码
     *
     * @param user
     */
    void editPwd(User user);

    /**
     * 修改用户自己的密码
     *
     * @param currUid
     * @param oldPwd
     * @param pwd
     * @return
     */
    boolean editCurrentUserPwd(Long currUid, String oldPwd, String pwd);


    List<String> getUserRoleNames(Long id);

    User getUserByOpenId(String openId);

    User getUserByPhone(String phone);

    User getUserByWxPhone(String wxPhone);

    DataTable<User> tables(UserVO userVO);

    /**
     * 发送手机验证码
     * @param phone
     * @return
     */
    String sendMsgCode(String phone,Integer type);

    /**
     * 发送手机验证码
     * @param phone
     * @return
     */
    String checkMsgCode(String phone,String code,Integer type);

    /**
     * 用户绑定手机号
     * @param phone
     * @return
     */
    void bindPhone(String phone);

    /**
     * 获取用户个人中心数据
     * @param currUid
     * @return
     */
    UserCenterInfoVO getUserCenterInfo(Long currUid);
    /**
     * 小程序更新用户信息
     * @param u
     * @return
     */
    void updateUserWx(User u);

    /**
     * 获取会员列表
     * @param vo
     * @return
     */
    DataTable<UserInfoVO> getUserList(UserInfoVO vo);

    /**
     * 发送短信
     * @param queryBaseVo
     */
    void sendMsg(QueryBaseVo queryBaseVo);

    /**
     * 禁用或者取用会员
     * @param vo
     */
    void closeUser(UserVO vo);

    /**
     * 获取用户折扣信息
     * @param currUid
     * @return
     */
    UserDisCountVo getDisCount(Long currUid);

    /**
     * 获取会员荣誉身份猎豹
     * @param status
     */
    List<VipConfigVo> getVipConfigList(Integer status);

    /**
     * 添加会员荣誉身份
     * @param vo
     */
    void addVipConfig(VipConfigVo vo);

    /**
     * 更新会员荣誉身份
     * @param vo
     */
    void updateVipConfig(VipConfigVo vo);

    /**
     * 转让收益
     * @param vo
     */
    void transferProfit(TransferProfitVo vo);

    /**
     * 修改推荐人
     * @param id
     * @param shareUserPhone
     */
    void uddateSharePhone(Long id, String shareUserPhone);
}
