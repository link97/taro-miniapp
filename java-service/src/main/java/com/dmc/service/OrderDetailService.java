package com.dmc.service;

import com.dmc.entity.OrderDetail;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 订单明细表 服务类
 * </p>
 *
 * @
 * @since 2020-09-06
 */
public interface OrderDetailService extends IService<OrderDetail> {

}
