package com.dmc.service;
import com.dmc.entity.ShoppingCart;
import com.baomidou.mybatisplus.service.IService;
import com.dmc.vo.TGoodsDetailVo;

import java.util.List;


/**
 * <p>
 * 购物车数据表 服务类
 * </p>
 *
 * @
 * @since 2020-09-06
 */
public interface ShoppingCartService extends IService<ShoppingCart> {
    /**
     * 添加购物车
     * @param shoppingCart
     * @return
     */
    public Boolean addCart(ShoppingCart shoppingCart);
    /**
     * 获取我的购物车
     * @param userId
     * @return
     */
    List<TGoodsDetailVo> getShoppingCartByUserId(Long userId);

    /**
     * 添加购物车商品数量
     * @param shoppingCart
     * @return
     */
    public Boolean addCartSkuNum(ShoppingCart shoppingCart);
}
