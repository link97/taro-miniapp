package com.dmc.service;

import com.dmc.entity.CarouselInfo;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 首页-轮播图 服务类
 * </p>
 *
 * @author dmc
 * @since 2020-09-04
 */
public interface CarouselInfoService extends IService<CarouselInfo> {

}
