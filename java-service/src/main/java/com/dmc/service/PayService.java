package com.dmc.service;

import com.alibaba.fastjson.JSONObject;
import com.dmc.model.RestResp;
import com.dmc.model.RestRespResult;
import com.dmc.vo.PaymentVo;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * 第三方支付接口
 */
public interface PayService {

    /**
     * 支付接口
     * @return
     */
    RestRespResult<Map<String, String>> pay(PaymentVo payment);


    /**
     * 查询支付状态接口
     * @return
     */
    RestRespResult<Map<String, String>> queryPay(PaymentVo payment);


    /**
     * 支付回调接口
     * @return
     */
    RestRespResult<String> payCallBack(HttpServletRequest request);


    /**
     * 提现(企业付款/转账到个人接口)
     * @return
     */
    RestRespResult<String> transfer(PaymentVo payment);

    /**
     * 退款
     * @return
     */
    RestRespResult<String> refund(PaymentVo payment);


    /**
     * 查询退款状态接口
     * @return
     */
    RestRespResult<String> queryRefund(PaymentVo payment);

    /**
     * 退款回调接口
     * @return
     */
    RestResp refundCallBack(PaymentVo payment);

}
