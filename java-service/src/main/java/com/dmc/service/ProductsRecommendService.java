package com.dmc.service;

import com.dmc.entity.ProductsRecommend;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 推荐商品 服务类
 * </p>
 *
 * @author dmc
 * @since 2020-09-04
 */
public interface ProductsRecommendService extends IService<ProductsRecommend> {

}
