package com.dmc.service;

import com.dmc.entity.SysConf;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 系统设置表 服务类
 * </p>
 *
 * @
 * @since 2020-09-12
 */
public interface SysConfService extends IService<SysConf> {

}
