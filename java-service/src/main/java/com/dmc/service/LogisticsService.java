package com.dmc.service;

import com.dmc.entity.Logistics;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 物流表 服务类
 * </p>
 *
 * @
 * @since 2020-09-12
 */
public interface LogisticsService extends IService<Logistics> {

}
