package com.dmc.service;

import com.dmc.vo.*;

import java.math.BigDecimal;
import java.util.List;

/**
 * 消息服务
 */
public interface MessageService {

    /**
     * 获取站内信
     * @param status
     * @return
     */
    List<MessageVo> getPlatformMessage(Integer status);

    /**
     * 获取站内信数量
     * @param status
     * @return
     */
    int getPlatformMessageNum(Integer status);
}
