package com.dmc.util;

import org.apache.commons.lang.StringUtils;

import java.math.BigDecimal;

public class DecimalUtil {


	public static BigDecimal  round(int scale,BigDecimal d) {
		
		return d.setScale(scale, BigDecimal.ROUND_DOWN);
	}
	
	public static BigDecimal roundHalfUp(int scale,BigDecimal d) {
		return d.setScale(scale, BigDecimal.ROUND_HALF_UP);
	}

	public static BigDecimal add(String d1, String d2) {
		BigDecimal b1 = new BigDecimal(d1);
		BigDecimal b2 = new BigDecimal(d2);
		return b1.add(b2).setScale(DEF_DIV_SCALE, BigDecimal.ROUND_HALF_UP);
	}

	public static BigDecimal sub(String d1, String d2) {
		BigDecimal b1 = new BigDecimal(d1).setScale(DEF_DIV_SCALE,   BigDecimal.ROUND_HALF_UP);
		BigDecimal b2 = new BigDecimal(d2).setScale(DEF_DIV_SCALE,   BigDecimal.ROUND_HALF_UP);
		return b1.subtract(b2).setScale(DEF_DIV_SCALE).setScale(DEF_DIV_SCALE, BigDecimal.ROUND_HALF_UP);
	}
	
	public static BigDecimal sub(BigDecimal b1, BigDecimal b2) {
		return b1.subtract(b2).setScale(DEF_DIV_SCALE, BigDecimal.ROUND_HALF_UP);
	}

	public static BigDecimal mul(String d1, String d2) {
		BigDecimal b1 = new BigDecimal(d1);
		BigDecimal b2 = new BigDecimal(d2);
		return b1.multiply(b2).setScale(DEF_DIV_SCALE, BigDecimal.ROUND_HALF_UP);
	}

	 public static double sum(double d1,double d2){ 
	        BigDecimal bd1 = new BigDecimal(Double.toString(d1)); 
	        BigDecimal bd2 = new BigDecimal(Double.toString(d2)); 
	        return bd1.add(bd2).doubleValue(); 
	 }
	 

	/** scale小数点位数 */
	private static final int DEF_DIV_SCALE = 2;


	/**
	 * 获取两个数据的百分比
	 * @param one
	 * @param two
	 * @return 如果结果是百分之10  则返回10
	 */
	public static BigDecimal getRate(BigDecimal one,BigDecimal two){
		if(one == null){
			return null;
		}
		if(two == null){
			return null;
		}
		if(BigDecimal.ZERO.compareTo(two) == 0){
			return BigDecimal.ZERO;
		}
		return one.divide(two, 4, BigDecimal.ROUND_HALF_UP);
	}

	public static BigDecimal addArray(BigDecimal... d) {
		BigDecimal tmp = new BigDecimal(0);
		for (BigDecimal bigDecimal : d) {
			bigDecimal = bigDecimal == null ? new BigDecimal(0) : bigDecimal;
			tmp = tmp.add(bigDecimal);
		}
		return tmp.setScale(2, BigDecimal.ROUND_HALF_UP);
	}

	public static BigDecimal subArray(BigDecimal sub, BigDecimal... d) {
		sub = sub == null ? new BigDecimal(0) : sub;
		for (BigDecimal bigDecimal : d) {
			bigDecimal = bigDecimal == null ? new BigDecimal(0) : bigDecimal;
			sub = sub.subtract(bigDecimal);
		}
		if (sub.compareTo(new BigDecimal(0)) < 0) {
			sub = new BigDecimal(0);
		}
		return sub.setScale(2, BigDecimal.ROUND_HALF_UP);
	}
	
	public static BigDecimal subArrayOriginalValue(BigDecimal sub, BigDecimal... d) {
		sub = sub == null ? new BigDecimal(0) : sub;
		for (BigDecimal bigDecimal : d) {
			bigDecimal = bigDecimal == null ? new BigDecimal(0) : bigDecimal;
			sub = sub.subtract(bigDecimal);
		}
		return sub.setScale(2, BigDecimal.ROUND_HALF_UP);
	}

	public static BigDecimal mulArray(BigDecimal sub, BigDecimal... d) {
		sub = sub == null ? new BigDecimal(0) : sub;
		for (BigDecimal bigDecimal : d) {
			bigDecimal = bigDecimal == null ? new BigDecimal(0) : bigDecimal;
			sub = sub.multiply(bigDecimal);
		}
		if (sub.compareTo(new BigDecimal(0)) < 0) {
			sub = new BigDecimal(0);
		}
		return sub.setScale(2, BigDecimal.ROUND_HALF_UP);
	}

	/**
	 * 元转换成万,保留两位小数
	 * @param yuan
	 * @return
	 */
	public static BigDecimal yuanConvertWan(BigDecimal yuan){
		if(yuan == null){
			return BigDecimal.ZERO;
		}
		return yuan.divide(new BigDecimal("10000"),2,BigDecimal.ROUND_HALF_UP);
	}

}
