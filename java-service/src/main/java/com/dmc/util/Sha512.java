package com.dmc.util;

import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Sha512 {

    public static  final String payPasswordSalt="dmc_abc";
    private Sha512() {}
    
    private static final String[] HEX_DIGITS = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f"};
    private static MessageDigest digest = null;
    
    static {
        try {
            digest = MessageDigest.getInstance("SHA-512");
        } catch (NoSuchAlgorithmException e) {
        }
    }

    public static String getSha512(String origin) {
        if (origin == null || digest == null) {
            return null;
        } else {
            digest.update(origin.getBytes());
            byte[] b = digest.digest();
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < b.length; i++) {
                sb.append(HEX_DIGITS[(0xF0 & b[i]) >>> 4]);
                sb.append(HEX_DIGITS[(0xF & b[i])]);
            }
            return sb.toString();
        }
    }

    public static String getSha512(String origin, String charsetName) {
        if (origin == null || digest == null) {
            return null;
        } else {
            digest.update(origin.getBytes(Charset.forName(charsetName)));
            byte[] b = digest.digest();
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < b.length; i++) {
                sb.append(HEX_DIGITS[(0xF0 & b[i]) >>> 4]);
                sb.append(HEX_DIGITS[(0xF & b[i])]);
            }
            return sb.toString();
        }
    }
    
    public static void main(String[] args) {
		System.out.println(getSha512(getSha512("17house")+"abcdef"));
	}
}
