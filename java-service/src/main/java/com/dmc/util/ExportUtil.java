package com.dmc.util;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.write.metadata.WriteSheet;
import org.apache.poi.ss.formula.functions.T;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ExportUtil {
    /**
     * @param outputStream 输出流
     * @param sheetName 定义sheet名称
     * @param headList sheet表头
     * @param lineList sheet行数据
     */
    public static void writeExcel(OutputStream outputStream, String sheetName, List<String> headList, List<List<Object>> lineList){
        List<List<String>> list = new ArrayList<>();
        if(headList != null){
            headList.forEach(h -> list.add(Collections.singletonList(h)));
        }
        EasyExcel.write(outputStream).head(list).sheet(sheetName).doWrite(lineList);
    }
    /**
     * 获取输出流
     * @param response
     * @param fileName
     * @return
     * @throws IOException
     */
    public static ServletOutputStream getServletOutputStream(HttpServletResponse response, String fileName) throws IOException {
        fileName = URLEncoder.encode(fileName, "UTF-8");
        response.setContentType("application/vnd.ms-excel");
        response.setCharacterEncoding("utf8");
        response.setHeader("Content-Disposition", "attachment; filename=" + fileName + ".xlsx");
        response.setHeader("Pragma", "public");
        response.setHeader("Cache-Control", "no-store");
        response.addHeader("Cache-Control", "max-age=0");
        return response.getOutputStream();
    }
    /**
     * 开始导出
     * @param response
     * @param dataList
     * @param head
     * @param fileName
     * @throws IOException
     */
    public static void exportStart(HttpServletResponse response, List<List<Object>> dataList, List<String> head, String fileName) throws IOException {
        // 获取输出流
        ServletOutputStream outputStream = ExportUtil.getServletOutputStream(response, fileName);
        // 导出文件
        ExportUtil. writeExcel(outputStream, fileName ,head,dataList);
    }

    /**
     * 使用 模型 来写入Excel
     *
     * @param outputStream Excel的输出流
     * @param dataList         要写入的以 模型 为单位的数据
     * @param clazz        模型的类
     */
    public static void writeExcelWithModel(OutputStream outputStream, String sheetName, List<? extends Object> dataList, Class<? extends Object> clazz) {
        //这里指定需要表头，因为model通常包含表头信息
        ExcelWriter writer = EasyExcel.write(outputStream, clazz).build();
        //创建writeSheet，设置基本信息
        WriteSheet writeSheet = new WriteSheet();
        writeSheet.setSheetName(sheetName);
        writer.write(dataList, writeSheet);
        writer.finish();
    }

    /**
     * 开始导出
     * @param response
     * @param objectClASS
     * @throws IOException
     */
    public static void exportModelStart(HttpServletResponse response, String sheetName, List<Object> objectList, Class objectClASS) throws IOException {
        // 获取输出流
        ServletOutputStream outputStream = ExportUtil.getServletOutputStream(response, sheetName);
        // 导出文件
        writeExcelWithModel(outputStream, sheetName, objectList, objectClASS);
    }


}
