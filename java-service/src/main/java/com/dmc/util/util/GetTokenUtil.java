package com.dmc.util.util;

/**
 * 生成一个32位的token
 * @author
 *
 */
public class GetTokenUtil {

	public static String createtoken(int length) {
		/****默认去掉了容易混淆的字符oOLl,9gq,Vv,Uu,I1****/
		String chars = "ABCDEFGHJKMNPQRSTWXYZabcdefhijkmnprstwxyz2345678";    
		int maxPos = chars.length();
		String pwd = "";
		for (int i = 0; i < length; i++) {
			pwd += chars.charAt((int) Math.floor(Math.random() * maxPos));
		}
		return pwd;
	}
}
