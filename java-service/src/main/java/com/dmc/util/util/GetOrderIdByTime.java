package com.dmc.util.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

/**
 * 生成唯一订单号
 * @author Administrator
 *
 */
public class GetOrderIdByTime {
	
	//高并发性下，确保订单的唯一性，可以把用户id加进去
	public static String getOrderIdByTime(String user_id) {
		SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMddHHmmss");
		String newDate=sdf.format(new Date());
		String result="";
		Random random=new Random();
		for(int i=0;i<3;i++){
			result+=random.nextInt(10);
			}
		//+user_id
		return newDate+result;
	}

	public static void main(String[] args) {
		System.out.println(GetOrderIdByTime.getOrderIdByTime("7783964195259392"));
	}
}
