package com.dmc.util.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.security.*;
import java.security.spec.AlgorithmParameterSpec;
import java.util.Arrays;

@Slf4j
public class WXBizDataCrypt {

    private static final String APPID = "wx8bb75d0aec3c7393";
    private static final String SECRET = "48b8b19309a23e52d72d0171a4d0f463";

    public static final String URL="https://api.weixin.qq.com/sns/jscode2session?appid="+APPID+
            "&secret="+SECRET+"&js_code={code}&grant_type=authorization_code";
    /**
     * AES解密
     *
     * @param data   //密文，被加密的数据
     * @param key    //秘钥
     * @param iv     //偏移量
     * @return
     * @throws Exception
     */
    public static String decrypt1(String data, String key,String iv){
        //被加密的数据
        byte[] dataByte = Base64.decodeBase64(data);
        //加密秘钥
        byte[] keyByte = Base64.decodeBase64(key);
        //偏移量
        byte[] ivByte = Base64.decodeBase64(iv);
        try {
            AlgorithmParameterSpec ivSpec = new IvParameterSpec(ivByte);
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            SecretKeySpec keySpec = new SecretKeySpec(keyByte, "AES");
            cipher.init(Cipher.DECRYPT_MODE, keySpec, ivSpec);
            return new String(cipher.doFinal(dataByte),"UTF-8");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    /**
     * 获取微信openId
     * @param code
     * @param iv
     * @return
     */
    public static JSONObject getOpenId(String code, String iv){
        JSONObject jsonObject = null;
        String url = URL;
        url = url.replace("{code}",code);
        CloseableHttpClient httpClient= HttpClients.createDefault();
        HttpGet httpGet=new HttpGet(url);
        CloseableHttpResponse httpResponse=null;
        try{
            httpResponse=httpClient.execute(httpGet);
            HttpEntity entity=httpResponse.getEntity();
            if (entity != null) {
                String resultStr = EntityUtils.toString(entity, "UTF-8");
                jsonObject = JSON.parseObject(resultStr);
            }
        }catch (Exception e){
            log.error("getOpenId code:{},iv:{}",code,iv,e);
        }
        return jsonObject;
    }

}
