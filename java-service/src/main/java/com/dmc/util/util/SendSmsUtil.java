package com.dmc.util.util;

import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.exceptions.ServerException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;

/**
 * 阿里发送短信验证码
 */
public class SendSmsUtil {

    /**
     * 短信有效时间 5分钟
     */
    public static final long STOP_TIME = 1000 * 60 * 10;

    //阿里发送短信验证码
    public static String sendsms(String PhoneNumbers, String PhoneCode) {
        DefaultProfile profile = DefaultProfile.getProfile("cn-hangzhou", "LTAI4GGHuYte8kuGJsceK7Fx", "yEI5q53Wn4ujL9rt6Z20gqTpRKUZro");
        IAcsClient client = new DefaultAcsClient(profile);
        CommonRequest request = new CommonRequest();
        request.setSysMethod(MethodType.POST);
        request.setSysDomain("dysmsapi.aliyuncs.com");
        request.setSysVersion("2017-05-25");
        request.setSysAction("SendSms");
        request.putQueryParameter("RegionId", "cn-hangzhou");
        request.putQueryParameter("PhoneNumbers", PhoneNumbers);
        request.putQueryParameter("SignName", "晶品城");
        request.putQueryParameter("TemplateCode", "SMS_196652934");
        String TemplateParam = "{\"code\":\""+PhoneCode+"\"}";

        request.putQueryParameter("TemplateParam", TemplateParam);
        String data = null;
        try {
            CommonResponse response = client.getCommonResponse(request);
            data = response.getData();
            System.out.println("data:"+response.getData());
        } catch (ServerException e) {
            e.printStackTrace();
        } catch (ClientException e) {
            e.printStackTrace();
        }
        return data;
    }
}
