package com.dmc.util.util;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * 创建6位验证码
 */
public class CreateCodeUtil {
    public static void main(String[] args) {
        String result = GetBindNum();
        System.out.println("result:"+result);
    }

    /**
     * 生成6位数字验证码
     * @return
     */
    public static String GetBindNum(){
        String[] beforeShuffle = new String[] {
                "0","1", "2", "3", "4", "5", "6", "7", "8", "9"};
        List list = Arrays.asList(beforeShuffle);
        Collections.shuffle(list);
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < list.size(); i++) {
            sb.append(list.get(i));
        }
        String afterShuffle = sb.toString();
        String result = afterShuffle.substring(3, 9);
        return result;
    }
}
