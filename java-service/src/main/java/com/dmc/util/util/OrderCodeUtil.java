package com.dmc.util.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

public class OrderCodeUtil {
    /**
     * 订单类别头
     */

    private static final String ORDER_CODE = "SP";
    /**
     * 退货类别头
     */
    private static final String RETURN_ORDER = "2000";
    /**
     * 退款类别头
     */
    private static final String REFUND_ORDER = "3000";
    /**
     * 提现类别头
     */
    private static final String CASH_OUT_ORDER = "TX";

    /**
     * 分销类别头
     */
    private static final String DISTRIBUTION_ORDER = "FX";
    /**
     * 分销类别头
     */
    private static final String DISTRIBUTION_PACK_ORDER = "FXP";
    /**
     * 交易编码类别头
     */
    private static final String PAYMENT = "JY";

    /**
     * 随即编码
     */

    private static final int[] r = new int[]{7, 9, 6, 2, 8, 1, 3, 0, 5, 4};
    /**
     * 用户id和随机数总长度
     */

    private static final int maxLength = 14;

    /**
     * 根据id进行加密+加随机数组成固定长度编码
     */
    private static String toCode(Long userId) {
        String idStr = userId.toString();
        StringBuilder idsbs = new StringBuilder();
        for (int i = idStr.length() - 1; i >= 0; i--) {
            idsbs.append(r[idStr.charAt(i) - '0']);
        }
        return idsbs.append(getRandom(maxLength - idStr.length())).toString();
    }

    /**
     * 生成时间戳
     */
    private static String getDateTime() {
        DateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
        return sdf.format(new Date());
    }

    /**
     * 生成固定长度随机码
     * @param n 长度
     */

    private static long getRandom(long n) {
        long min = 1, max = 9;
        for (int i = 1; i < n; i++) {
            min *= 10;
            max *= 10;
        }
        long rangeLong = (((long) (new Random().nextDouble() * (max - min)))) + min;
        return rangeLong;
    }


    /**
     * 生成不带类别标头的编码
     *
     * @param userId
     */
    private static synchronized String getCode(Long userId) {
        return getDateTime();
    }


    /**
     * 生成订单单号编码(调用方法)
     * @param userId  网站中该用户唯一ID 防止重复
     */

    public static String getOrderCode(Long userId) {
        return ORDER_CODE + getCode(userId);
    }

    /**
     * 生成分销订单编码
     * @param userId  网站中该用户唯一ID 防止重复
     */
    public static String getDistributionCode(Long userId) {
        return DISTRIBUTION_ORDER + getCode(userId);
    }

    /**
     * 生成分销套餐包订单编码
     * @param userId  网站中该用户唯一ID 防止重复
     */
    public static String getDistributionPackCode(Long userId) {
        return DISTRIBUTION_PACK_ORDER + getCode(userId);
    }

    /**
     * 生成交易编码
     * @param userId  网站中该用户唯一ID 防止重复
     */
    public static String getPayCode(Long userId) {
        return PAYMENT + getCode(userId);
    }

    /**
     * 生成提现订单单号编码(调用方法)
     * @param userId  网站中该用户唯一ID 防止重复
     */

    public static String getCashOutOrderCode(Long userId) {
        return CASH_OUT_ORDER + getCode(userId);
    }


    /**
     * 生成退货单号编码（调用方法）
     * @param userId 网站中该用户唯一ID 防止重复
     */
    public static String getReturnCode(Long userId) {
        return RETURN_ORDER + getCode(userId);
    }


    /**
     * 生成退款单号编码(调用方法)
     * @param userId  网站中该用户唯一ID 防止重复
     */
    public static String getRefundCode(Long userId) {
        return REFUND_ORDER + getCode(userId);
    }

}
