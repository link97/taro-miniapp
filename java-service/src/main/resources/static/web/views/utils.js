window.baseUrl = window.location.protocol+"//"+window.location.host;;

// 请求封装
function ajax({ path, type, data, success }, $) {
    var loading = null;
    if(path.indexOf(window.baseUrl) == -1){
        path = window.baseUrl + path;
    }
    loading = layer.load(1, {
        shade: [0.6, '#000'] //0.1透明度的白色背景
    });

    $.ajax({
        url: path,
        type: type || "POST",
        data: JSON.stringify(data),
        headers: {
            "Content-Type": "application/json",
            Authorization: localStorage.token
        },
        success: function (res) {
            layer.close(loading);
            if (res.code == 200) {
                success(res.data);
            } else {
                layer.msg(res.msg)
            }
        }
    })
}

// 图片上传函数
function uploads(upload, dom) {
    var loading = null;
    upload.render({
        elem: dom
        ,url: window.baseUrl + '/file/uploadFile' //改成您自己的上传接口
        , headers: {
            Authorization: localStorage.token
        }
        , before: function() {
            loading = layer.load(1, {
                shade: [0.6,'#000'] //0.1透明度的白色背景
            });
        }
        ,done: function(res){
            layer.close(loading);

            if(res.data) {
                layer.msg('上传成功');
                layui.$('#uploadDemoView').removeClass('layui-hide').find('img').attr('src', res.data);
                // 设置图片地址
                layui.$("#imgUrl").val(res.data);
            } else {
                layer.msg(res.message);
            }
        }
    });
}

// 图片上传函数
function uploadsFile(upload, dom, fn) {
    var loading = null;
    upload.render({
        elem: dom
        ,url: window.baseUrl + '/file/uploadFile' //改成您自己的上传接口
        ,accept: 'file'
        , headers: {
            Authorization: localStorage.token
        }
        , before: function() {
            loading = layer.load(1, {
                shade: [0.6,'#000'] //0.1透明度的白色背景
            });
        }
        ,done: function(res){
            layer.close(loading);
            fn(res.data);
        }
    });
}


// 获取url参数
function getQueryString(name) { 
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i"); 
    var r = window.location.search.substr(1).match(reg); 
    if (r != null) return unescape(r[2]); 
    return null; 
} 